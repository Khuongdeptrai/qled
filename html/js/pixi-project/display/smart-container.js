(function(){
/**
 * @class
 * @extends PIXI.Container
 * @memberof PIXI
 * @PIXI v4 ONLY
 * version: 1.0
 */

function SmartContainer()
{
    PIXI.Container.call(this);

    this.children = [];
}

SmartContainer.prototype = Object.create(PIXI.Container.prototype);
SmartContainer.prototype.constructor = SmartContainer;

Object.defineProperties(SmartContainer.prototype, {
    zIndex: {
        get: function ()
        {
            var zIndex = this._zIndex;
            return zIndex;
        },
        set: function (value)
        {
            //console.log("set zIndex of SmartContainer")
            var item1 = this;
            var item2 = this.parent.children[value];
            for(var i=0; i<this.parent.children.length; i++){
                var mc = this.parent.children[i];
                if(value > item1._zIndex){
                    if(mc._zIndex > item1._zIndex && mc._zIndex <= value) mc._zIndex--;
                } else {
                    if(mc._zIndex > value && mc._zIndex < item1._zIndex) mc._zIndex++;
                }
            }
            item1._zIndex = value;
            //sort children..
            this.parent.children.sort(function(a,b) {
                a._zIndex = a._zIndex || 0;
                b._zIndex = b._zIndex || 0;
                return a._zIndex - b._zIndex;
            });
        }
    }
})

SmartContainer.prototype.addChild = function (child)
{
    var argumentsLength = arguments.length;
    // if there is only one argument we can bypass looping through the them
    if(argumentsLength > 1)
    {
        // loop through the arguments property and add all children
        // use it the right way (.length and [i]) so that this function can still be optimised by JS runtimes
        for (var i = 0; i < argumentsLength; i++)
        {
            this.addChild( arguments[i] );
        }
    }
    else
    {
        // if the child has a parent then lets remove it as Pixi objects can only exist in one place
        if (child.parent)
        {
            child.parent.removeChild(child);
            if(child.name) child.parent[child.name] = null;
        }
        child.parent = this;
        //
        if(child.name) child.parent[child.name] = child;
        // ensure a transform will be recalculated..
        this.transform._parentID = -1;
        this.children.push(child);
        child._zIndex = this.children.length - 1;
        // TODO - lets either do all callbacks or all events.. not both!
        this.onChildrenChange(this.children.length-1);
        child.emit('added', this);
    }
    return child;
};

SmartContainer.prototype.removeChild = function (child)
{
    var argumentsLength = arguments.length;
    // if there is only one argument we can bypass looping through the them
    if(argumentsLength > 1)
    {
        // loop through the arguments property and add all children
        // use it the right way (.length and [i]) so that this function can still be optimised by JS runtimes
        for (var i = 0; i < argumentsLength; i++)
        {
            this.removeChild( arguments[i] );
        }
    }
    else
    {
        var index = this.children.indexOf(child);
        if (index === -1)
        {
            return;
        }
        if(child.name) child.parent[child.name] = null;
        child.parent = null;
        utils.removeItems(this.children, index, 1);
        // TODO - lets either do all callbacks or all events.. not both!
        this.onChildrenChange(index);
        child.emit('removed', this);
    }
    return child;
};

SmartContainer.prototype.alignCenterParent = function(){
    if(this.parent == this.stage){
        this.x = this.stage.stageWidth / 2;
        this.y = this.stage.stageHeight / 2;
    } else {
        this.x = this.parent.width / 2;
        this.y = this.parent.height / 2;
    }
}

//

PIXI.SmartContainer = SmartContainer;

}).call(this);