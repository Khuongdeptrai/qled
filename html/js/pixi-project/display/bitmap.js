(function(){
/**
 * @class
 * @extends PIXI.Container
 * @memberof PIXI
 * @PIXI v4 ONLY
 * version: 1.0
 */

function Bitmap(texture)
{
    PIXI.Sprite.call(this);
}

Bitmap.prototype = Object.create(PIXI.Sprite.prototype);
Bitmap.prototype.constructor = Bitmap;

Bitmap.prototype.addChild = function (child)
{
    var argumentsLength = arguments.length;
    // if there is only one argument we can bypass looping through the them
    if(argumentsLength > 1)
    {
        // loop through the arguments property and add all children
        // use it the right way (.length and [i]) so that this function can still be optimised by JS runtimes
        for (var i = 0; i < argumentsLength; i++)
        {
            this.addChild( arguments[i] );
        }
    }
    else
    {
        // if the child has a parent then lets remove it as Pixi objects can only exist in one place
        if (child.parent)
        {
            child.parent.removeChild(child);
            if(child.name) child.parent[child.name] = null;
        }
        child.parent = this;
        //
        if(child.name) child.parent[child.name] = child;
        // ensure a transform will be recalculated..
        this.transform._parentID = -1;
        this.children.push(child);
        // TODO - lets either do all callbacks or all events.. not both!
        this.onChildrenChange(this.children.length-1);
        child.emit('added', this);
    }
    return child;
};

Bitmap.prototype.removeChild = function (child)
{
    var argumentsLength = arguments.length;
    // if there is only one argument we can bypass looping through the them
    if(argumentsLength > 1)
    {
        // loop through the arguments property and add all children
        // use it the right way (.length and [i]) so that this function can still be optimised by JS runtimes
        for (var i = 0; i < argumentsLength; i++)
        {
            this.removeChild( arguments[i] );
        }
    }
    else
    {
        var index = this.children.indexOf(child);
        if (index === -1)
        {
            return;
        }
        if(child.name) child.parent[child.name] = null;
        child.parent = null;
        utils.removeItems(this.children, index, 1);
        // TODO - lets either do all callbacks or all events.. not both!
        this.onChildrenChange(index);
        child.emit('removed', this);
    }
    return child;
};

//

PIXI.Bitmap = Bitmap;

}).call(this);