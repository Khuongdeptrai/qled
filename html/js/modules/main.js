/* GAME POSTER
Author: KHUONGDINH
================================================== */



$(function(){
  if(typeof path_resource == "undefined" || !path_resource) path_resource = "";
  GScript.loadList([
    // external libraries
    path_resource + "js/libraries/plugin.js",
    path_resource + "js/libraries/tweenmax/TweenMax.min.js",
    path_resource + "js/libraries/tweenmax/plugins/ScrollToPlugin.min.js",
    path_resource + "js/libraries/scrollmagic/uncompressed/ScrollMagic.js", 
    path_resource + "js/libraries/scrollmagic/uncompressed/plugins/animation.gsap.js", 
    path_resource + "js/libraries/scrollmagic/uncompressed/plugins/debug.addIndicators.js",

    path_resource + "js/libraries/three.min.js",
    path_resource + "js/libraries/D.js",
    path_resource + "js/libraries/uevent.min.js",
    path_resource + "js/libraries/doT.min.js",
    path_resource + "js/libraries/photo-sphere-viewer.min.js",

    // goon
    path_resource + "js/plugins/before-after.js",
    path_resource + "js/libraries/goon.js"
    
  ], function(result){
    QLED.init();
  })


})


var wScreen = $(window).outerWidth();

var QLED = {

  init : function(){
    
    if( $('#dgtHome').length > 0 ) {
      QLED.intro();
    } else if( $('#dgtLibrary').length > 0 ) {
      QLED.library();
    } else if ( $('#dgtBooking').length > 0  ) {
      QLED.movie();
    } else if( $('#dgtGalerry').length > 0 ) {
      QLED.galery();
    } else if ( $('#dgtQcinema').length > 0 ) {
      $('.js_qcinema__menu').css({ 'display': 'block' });
    } else if ( $('#dgtQhomeJoin').length > 0 ) {
      QLED.qhome();
    } else if ( $('#dgtQhomeRegister').length > 0 ) {
      $('.js_qhome__menu').css({ 'display': 'block' });
    } else if ( $('#dgtQhome').length > 0 ) {
      QLED.qhomemain();
    } else if ( $('#dgtQsuite').length > 0 ) {
      QLED.qsuite();
    } else if ( $('#dgtQsuiteRegister').length > 0 ) {
      QLED.qsuiteRegister();
    } else if ( $('#dgtQsuiteDetail').length > 0 ) {
      QLED.qsuitePanorama();
    } else if ( $('#dgtReview').length > 0 ) {
      QLED.review();
    }

    
    if(wScreen > 1024) {
      QLED.stick();
    }
    QLED.popup();
    QLED.mobile();
  },

  stick : function() {

    var $fwindow = $(window);
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    var h = $('.dgt_header').outerHeight() ; 

    var navPosition=$('.dgt_header').offset().top;
    var scrollTop=$(window).scrollTop();

    $fwindow.on('scroll resize', function() {
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      wScreen = $(window).outerWidth();
      if(navPosition < scrollTop){
        $('.dgt_header').css({ 'position': 'fixed' });
      } else{
        $('.dgt_header').css({ 'position': 'relative' });
      }
    });

  },

  intro : function() {  
    
    $(window).on('load resize', function(event) {
      $(".dgt_cinema").width( $(window).width()/2 );
      $(".dgt_else").width( $(window).width()/2 );
    });
    
    setTimeout(function() {
      $("#dgtHome").css('visibility', 'visible');
      TweenMax.from('#dgtHome',1, { opacity: 0 })
    }, 200);

    
  },

  library: function() {

    $('.js_qcinema__menu').css({ 'display': 'block' });

    $('.js_dgt__library').slick({
      arrows: false,
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            arrows: false,
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: true,
            centerPadding: '100px',
            variableWidth: true
          }
        }
      ]
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__library').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__library').slick('slickNext');
    })

    
  },

  movie : function() {

    $('.js_qcinema__menu').css({ 'display': 'block' });

    $('.js_dgt__library').slick({
      arrows: false,
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            arrows: false,
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: true,
            centerPadding: '100px',
            variableWidth: true
          }
        }
      ]
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__library').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__library').slick('slickNext');
    })

  },
  
  popup: function() {
    $('#dgt_alert .ovl').on('click', function() {
        $('#dgt_alert').hide();
        $('#dgt_alert2').hide();
    });

    $('.dgt_popup .btn-close-2').on('click', function(e){
      e.preventDefault();
      $('#dgt_alert').hide();
    })

  },

  galery: function() {

    $('.js_dgt__photo').slick({
      arrows: false,
      dots: false,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      variableWidth: true
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__photo').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__photo').slick('slickNext');
    })

    

    $('.js_dgt__video').slick({
      arrows: false,
      dots: false,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      variableWidth: true
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__video').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__video').slick('slickNext');
    })

    $('.dgt_js_tabs li').on('click', function() {

      var addres = $(this).attr('rel');
      var ytb = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/VpBUfx9uVh0?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>'; 

      $('.dgt_js_tabs li').removeClass('active');
      $(this).addClass('active');

      $('.dgt_glr__item').hide();
      $('#'+addres).show();

      if( addres=='dgt_photo') {
        $('.dgt_ytb').html('');
      } else {
        $('.dgt_ytb').html('').append(ytb);
      }

    });


    $('.js_dgt__video .slick-slide').on('click', function() {

      var id = $(this).attr('data');
      console.log(id);
      var ytb = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+id+'?autoplay=0&rel=0" frameborder="0" allowfullscreen></iframe>'; 
      $('.dgt_ytb').html('').append(ytb);

    });

    $('.js_dgt__photo .slick-slide').on('click', function() {

      var src = $(this).find('.thumb').attr('src');
      $('#dgt_photo .dgt_glr__copy > img').attr('src',src)
    });

  },

  mobile: function() {
    
    $('.btn-menu').on('click', function(event) {
      event.preventDefault();
      if ($(this).hasClass('menu-open')) {
        $(this).removeClass('menu-open').addClass('menu-closed');
        $('#dgt_nav').hide();
      } else {
        $(this).removeClass('menu-closed').addClass('menu-open');
        $('#dgt_nav').show();
        
      };
    });

  },
  review : function() {
    TweenMax.set(".dgt_banner h2", {opacity: 0, y: -80});
    TweenMax.to(".dgt_banner h2", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.2});
  },

  qhome: function() {

    $('.js_qhome__menu').css({ 'display': 'block' });

    var controller = new ScrollMagic.Controller();

    $('[data-qhome]').each(function (index, elem) {

      var scene = new ScrollMagic.Scene({triggerElement: elem, triggerHook: '0.8'})
      .addTo(controller)
      .on("enter", function (e) {
        $(elem).addClass('display');
      });

    });
    
    TweenMax.set(".dgt_video", {opacity: 0, y: -80});
    TweenMax.set(".dgt_qhome__video h2 ", {opacity: 0, y: 80});

    TweenMax.to(".dgt_video", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.5});
    TweenMax.to(".dgt_qhome__video h2", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.6});

  },

  qhomemain: function() { 
    var hScreen = getDocumentSize(3);
    var wScreen = getDocumentSize(0);

    $(window).on('load resize', function() {
      hScreen = getDocumentSize(3);
      wScreen = getDocumentSize(0);
    });
   
    if (wScreen < 901) {
      $('.js_dgt__popup').slick({
        arrows: false,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        centerMode: true
      });
    } 
    
    TweenMax.set(".dgt_copy h3 , .dgt_copy p", {opacity: 0, y: 80});
    TweenMax.set(".dgt_copy .dgt_btn ", {opacity: 0, y: 80});

    TweenMax.to(".dgt_copy h3", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.5});
    TweenMax.to(".dgt_copy p", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.6});
    TweenMax.to(".dgt_copy .dgt_btn", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.7});

  },

  qsuite: function() {

    // TweenMax.set(".twentytwenty-container .twentytwenty-before", {opacity: 0, transformOrigin: "25% 50%"});
    // TweenMax.set(".twentytwenty-container .twentytwenty-after", {opacity: 0, transformOrigin: "75% 50%"});

    // TweenMax.to(".twentytwenty-container .twentytwenty-before", 0.8, {opacity: 1, scale:1, ease: Quart.easeOut})
    // TweenMax.to(".twentytwenty-container .twentytwenty-after", 0.8, {opacity: 1, scale:1, ease: Quart.easeOut})

    // $(window).on('load resize', function(event) {
    //   $(".dgt_cinema").width( $(window).width()/2 );
    //   $(".dgt_else").width( $(window).width()/2 );
    // });
      
    // setTimeout(function() {
    //   $("#dgtQsuite").css('visibility', 'visible');
    //   TweenMax.from('#dgtQsuite',1, { opacity: 0 })
    // }, 200);


    TweenMax.set(".dgt_copy h2 , .dgt_copy p", {opacity: 0, y: 80});
    TweenMax.set(".dgt_buttons ", {opacity: 0, y: 80});

    TweenMax.to(".dgt_copy h2", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.5});
    TweenMax.to(".dgt_copy p", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.6});
    TweenMax.to(".dgt_buttons", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.7});

    

  },

  qsuiteRegister: function() {

    $('.js_dgt__gallery').slick({
      arrows: false,
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            arrows: false,
            dots: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: '100px',
            variableWidth: true
          }
        }
      ]
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__gallery').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__gallery').slick('slickNext');
    })
  },

  qsuitePanorama: function() {

    var hScreen = getDocumentSize(3);
    var wScreen = getDocumentSize(0);

    $(window).on('load resize', function() {
      hScreen = getDocumentSize(3);
      wScreen = getDocumentSize(0);
    });
   
    if (wScreen < 901) {
      $('.js_dgt__popup').slick({
        arrows: false,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        centerMode: true
      });
    } 
    

   
    var PSV = new PhotoSphereViewer({
        panorama: path_resource + '/html/images/Bryce-Canyon-National-Park-Mark-Doliner.jpg',
        container: 'dgt_room360',
        loading_img: path_resource + '/html/images/photosphere-logo.gif',
        time_anim: false,
        default_fov: 70,
        default_lat: 0.3,
        navbar: false,
        mousewheel: false,
        size: {
          height: hScreen
        },
        markers: [
          {
            id: 'image',
            x: 2280,
            y: 940,
            image: path_resource + '/html/images/pin2.png',
            width: 32,
            height: 32,
            tooltip: {
              content: '<img src="images/tooltip.jpg"/><p>Cảm nhận độ trung thực và sống động của HD Audio âm thanh chất lượng 92 ~ 192kHZ/24 bit</p>',
              position: 'top center bottom'
            }
          },
          {
            id: 'image2',
            x: 3100,
            y: 1000,
            image: path_resource + '/html/images/pin2.png',
            width: 32,
            height: 32,
            tooltip: {
              content: '<img src="images/tooltip.jpg"/><p>Cảm nhận độ trung thực và sống động của HD Audio âm thanh chất lượng 92 ~ 192kHZ/24 bit</p>',
              position: 'top center bottom'
            }
          },
          {
            id: 'button',
            x: 3100,
            y: 1500,
            html: '<a href="qsuite_register.html" class="dgt_btn">Tham gia ngay</a>'
          },
        ]
      });

    if (wScreen < 1024) {
      PSV.on('ready', function(enabled) {
         PSV.clearMarkers();
      });
    }
    
  }
  
};
 

var randomNum = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};



function showAlert() {
  $('#dgt_alert').show()
}










