/*!
 * pixi.project.js - v0.0.1
 * Compiled Tue Sep 20 2016 12:24:31 GMT+0700 (BST)
 *
 * pixi.project.js is licensed under the MIT License.
 * http://www.opensource.org/licenses/mit-license
 */

(function(){
/**
 * @class
 * @PIXI.Project
 * @PIXI v4 ONLY
 */
var isDebugging = true;

function PIXIProject(jCanvasContainer, params) {
	/*
	params:
		- scaleMode {PIXIAppScaleMode}
		- transparent {bool}
		- backgroundColor {UInt}
		- 
	*/
	if(isDebugging) console.log("[PIXI.App] Initialized!");

	if(!jCanvasContainer){
		throw "[PIXI.App] jCanvasContainer is undefined.";
	}

	var scope = this;

	this.jCanvasContainer 	= jCanvasContainer;
    this.transparent 		= (params && (typeof params.transparent != "undefined")) ? params.transparent : false;
	this.scaleMode 			= (params && (typeof params.scaleMode != "undefined")) ? params.scaleMode : PIXIProjectScaleMode.NO_SCALE;
	this.backgroundColor 	= (params && (typeof params.backgroundColor != "undefined")) ? params.backgroundColor : 0xffffff;
	this.width = this.jCanvasContainer.outerWidth();
	this.height = this.jCanvasContainer.outerHeight();
	if(!this.transparent){
		this.renderer = PIXI.autoDetectRenderer(this.width, this.height, {transparent: this.transparent, backgroundColor: this.backgroundColor, antialias: true});
	} else {
		this.renderer = PIXI.autoDetectRenderer(this.width, this.height, {transparent: this.transparent, antialias: true});
	}
	this.center = null;
	this.stage = new PIXI.SmartContainer();
	this.stage.name = "stage";
	this.stage.interactive = true;
	this.loader = new PIXI.loaders.Loader();
	this.library = [];
	this.renderMode = (this.renderer instanceof PIXI.CanvasRenderer) ? PIXIProjectRenderMode.CANVAS : PIXIProjectRenderMode.WEBGL;

	this.canvas = this.renderer.view;
	this.canvasSize = {width: this.width, height: this.height};
	this.globalScale = 1;

	this.stage.stageWidth = this.canvasSize.width;
    this.stage.stageHeight = this.canvasSize.height;

	this.jCanvasContainer[0].appendChild(this.renderer.view);

	requestAnimationFrame(animate);

	function animate() {
		scope.onRuntime();
	    scope.renderer.render(scope.stage);
	    requestAnimationFrame(animate);
	}

	// events
	this.onInit = null;
	this.onResize = null;

	// bools
	this.isInit = false;
	this.isActive = true;
	this.isDebugging = true; // turn on to see debug messages

	//this.init(this.jCanvasContainer, params);
}

// inherited from other Class:
//PIXIProject.prototype  = Object.create(PIXI.Container.prototype);
PIXIProject.prototype.constructor = PIXIProject;

PIXIProject.prototype.destroy = function() {

}

PIXIProject.prototype.init = function(renderer, stage) {
	//

	this.isInit = true;
	if(this.onInit) this.onInit(renderer, stage);
	this.onCanvasSizeChange();
}

PIXIProject.prototype.addFileToLibrary = function(nameAndUrl) {
	var existed = false;
	for(var i=0; i<this.library.length; i++){
		if(nameAndUrl.name == this.library[i].name){
			existed = true;
		}
	}
	//console.log(existed)
	if(existed){
		throw "[PIXI.Project] This library item's name with \""+nameAndUrl.name+"\" already existed."
		return;
	}
	this.library.push(nameAndUrl);
}

PIXIProject.prototype.addListToLibrary = function(array) {
	for(var i=0; i<array.length; i++){
		this.addFileToLibrary(array[i]);
	}
}

PIXIProject.prototype.start = function() {
	var scope = this;

	if(scope.library.length == 0){
		scope.init();
	} else {
		for(var i=0; i<scope.library.length; i++){
			scope.loader.add(scope.library[i].name, scope.library[i].url);
		}
		//PIXI.loader.on('progress', scope.onLoadProgress);
	}

	//-- end load assets --

	scope.loader.once('complete', function(){
		scope.init(scope.renderer, scope.stage);
	});
	scope.loader.load();
}

PIXIProject.prototype.onRuntime = function(){
	var scope = this;

	if(scope.scaleMode == PIXIProjectScaleMode.SHOW_ALL)
	{
		if(Math.round(scope.jCanvasContainer.width()) != Math.round($(scope.canvas).width() * scope.globalScale) ){
			scope.globalScale = Math.round(scope.jCanvasContainer.width()) / Math.round($(scope.canvas).width());
			
            var newHeight = Math.round($(scope.canvas).height() * scope.globalScale);
            if(newHeight > Math.round(scope.jCanvasContainer.height()) ){
            	scope.globalScale = Math.round(scope.jCanvasContainer.height()) / Math.round($(scope.canvas).height());
            }

            TweenMax.set($(scope.canvas), {
            	scaleX: scope.globalScale, 
            	scaleY: scope.globalScale, 
            	transformOrigin: "0% 0%"
            });
            //$(scope.canvas).width( Math.round(scope.jCanvasContainer.width()) );
			console.log( Math.round(scope.jCanvasContainer.width()), Math.round($(scope.canvas).width() * scope.globalScale) );

            scope.onCanvasSizeChange();
        }
		
	}
	else if(scope.scaleMode == PIXIProjectScaleMode.NO_SCALE)
	{
		scope.canvasOldSize = {width: scope.width, height: scope.height};

        if(Math.round(scope.jCanvasContainer.width()) != scope.canvasSize.width){
            scope.canvasSize.width = Math.round(scope.jCanvasContainer.width());
            scope.canvas.width = scope.jCanvasContainer.width();
            scope.width = scope.canvas.width;
            
            scope.onCanvasSizeChange();
        }

        if(Math.round(scope.jCanvasContainer.height()) != scope.canvasSize.height){
            scope.canvasSize.height = Math.round(scope.jCanvasContainer.height());
            scope.canvas.height = scope.jCanvasContainer.height();
            scope.height = scope.canvas.height;

            scope.onCanvasSizeChange();
        }
	} 
	else if(scope.scaleMode == PIXIProjectScaleMode.STRETCH)
	{ // STRETCH
		if($(scope.canvas).width() != scope.jCanvasContainer.width()) {
			$(scope.canvas).width( scope.jCanvasContainer.width() );
			scope.onCanvasSizeChange();
		}
		if($(scope.canvas).height() != scope.jCanvasContainer.height()) {
			$(scope.canvas).height( scope.jCanvasContainer.height() );
			scope.onCanvasSizeChange();
		}
		//console.log(scope.jCanvasContainer.css("height"))
	}
}

PIXIProject.prototype.onCanvasSizeChange = function(){
    //console.log("Canvas size change");
    var scope = this;

    var sw = scope.canvas.width;
    var sh = scope.canvas.height;

    scope.center = {x: scope.canvas.width/2, y: scope.canvas.height/2};

    if(scope.isInit){
        // resize code here...
        scope.stage.stageWidth = sw;
        scope.stage.stageHeight = sh;
        if(scope.onResize) scope.onResize();
        // -- finish resize --
    }

    if(scope.renderer){
        scope.renderer.resize(scope.canvas.width, scope.canvas.height);
    }
}

PIXIProject.prototype.on = function(event, callback){
	var scope = this;
	switch(event){
		case "init":
			scope.onInit = callback;
		break;

		case "resize":
			scope.onResize = callback;
		break;

		default:
			if(isDebugging) console.log("Event not found.");
		break;
	}
}

// == SMART LAYOUT ==

PIXIProject.prototype.alignCenter = function(item, withTarget){
	if(withTarget == item.parent){
		item.x = -item.width/2;
		item.y = -item.height/2;
	} else {
		if(withTarget.parent != item.parent){
			console.log("Can't align Objects in different levels");
			return;
		}
		item.x = withTarget.x + (withTarget.width - item.width)/2;
		item.y = withTarget.y + (withTarget.height - item.height)/2;
	}
}

// == CREATE SPRITE & CONTAINER ==

PIXIProject.prototype.newSprite = function(linkage, name){
	var app = this;
	var texture = this.loader.resources[linkage].texture;
	var sprite = new PIXI.SmartSprite(texture);
	sprite.stage = this.stage;
	sprite.alignCenterOf = function(target){
		app.alignCenter(this, target);
	}
	if(name) sprite.name = name;
	return sprite;
}

PIXIProject.prototype.newContainer = function(name){
	var app = this;
	var container = new PIXI.SmartContainer();
	container.stage = this.stage;
	container.alignCenterOf = function(target){
		app.alignCenter(this, target);
	}
	if(name) container.name = name;
	return container;
}

PIXIProject.prototype.newButton = function(normalTextureName, name, params){
	var app = this;
	var btn = new PIXI.SmartContainer();
	btn.stage = this.stage;
	btn.alignCenterOf = function(target){
		app.alignCenter(this, target);
	}
	if(name) btn.name = name;

	btn.normal = this.newSprite(normalTextureName, "normal");
	btn.addChild(btn.normal);

	btn.interactive = true;
	btn.buttonMode = true;

	btn.state = "normal";

	if(params && params.hover){
		btn.hover = this.newSprite(params.hover);
		btn.addChild(btn.hover);
		btn.hover.visible = false;
	}
	if(params && params.press){
		btn.press = GPixi.newSprite(params.press);
		btn.addChild(btn.press);
		btn.press.visible = false;
	}

	if(params && params.auto){
		btn.on("mouseover", function(){
			btn.changeState("hover");
		})
		btn.on("mouseout", function(){
			btn.changeState("normal");
		})
	}

	btn.changeState = function(_state){
		this.state = _state;

		switch(_state){
			case "press":
				if(this.hover) this.hover.visible = false;
				if(this.normal) this.normal.visible = false;
				if(this.press) this.press.visible = true;
			break;

			case "hover":
				if(this.normal) this.normal.visible = false;
				if(this.press) this.press.visible = false;
				if(this.hover) this.hover.visible = true;
			break;

			default:
				if(this.hover) this.hover.visible = false;
				if(this.press) this.press.visible = false;
				this.normal.visible = true;
			break;
		}
	}

	btn.setStateTexture = function(_state, textureName){
		switch(_state){
			case "press":
				if(btn.press) btn.press.texture = app.loader.resources[textureName].texture;
			break;

			case "hover":
				if(btn.hover) btn.hover.texture = app.loader.resources[textureName].texture;
			break;

			default:
				btn.normal.texture = app.loader.resources[textureName].texture;
			break;
		}
	}

	btn.anchor = {}
	btn.anchor.set = function(x,y){
		btn.normal.anchor.set(x,y);
		if(btn.hover) btn.hover.anchor.set(x,y);
		if(btn.press) btn.press.anchor.set(x,y);
	}

	return btn;
}

PIXIProject.prototype.newShapeCircle = function(radius, lineWidth, lineColor, fillColor){
	var container = new PIXI.Graphics();
	container.stage = this.stage;
	container.lineStyle(lineWidth, lineColor);
	if(fillColor == null){
		container.beginFill(0xffffff, 0);
	} else {
		container.beginFill(fillColor);
	}
	
	container.drawCircle(0,0, radius);
	container.endFill();
	return container;
}

// == ADD TO PIXI PLUGINS ==
PIXI.Project = PIXIProject;

}).call(this);

// CONSTANTS :

var PIXIProjectScaleMode = {
	get NO_SCALE(){
		return "noscale";
	},
	get SHOW_ALL(){
		return "showall";
	},
	get STRETCH(){
		return "stretch";
	}
};

var PIXIProjectRenderMode = {
	get WEBGL(){
		return "webgl";
	},
	get CANVAS(){
		return "canvas";
	}
}

var PIXIProjectStageAlign = {
	get TOP_LEFT(){
		return "tl";
	},
	get TOP_RIGHT(){
		return "tr";
	},
	get BOTTOM_LEFT(){
		return "bl";
	},
	get BOTTOM_RIGHT(){
		return "br";
	},
	get CENTER(){
		return "c";
	}
}


