/* GAME POSTER
Author: KHUONGDINH
================================================== */



$(function(){
  if(typeof path_resource == "undefined" || !path_resource) path_resource = "";
  GScript.loadList([
    // external libraries
    path_resource + "js/libraries/plugin.js",
    path_resource + "js/libraries/tweenmax/TweenMax.min.js",
    path_resource + "js/libraries/tweenmax/plugins/ScrollToPlugin.min.js",
    
  ], function(result){
    QLED.init();
  })


})

// $(window).load(function() { 
//   if( $('#dgtLibrary').length > 0 ) {
//     $('.js_dgt__library').slick({
//       dots: true,
//       infinite: true,
//       slidesToShow: 3,
//       slidesToScroll: 3,
//       variableWidth: true
//     });
//   } 
// });

var wScreen = $(window).outerWidth();

var QLED = {

  init : function(){
    
    if( $('#dgtHome').length > 0 ) {
      QLED.intro();
    } else if( $('#dgtLibrary').length > 0 ) {
      QLED.library();
    } else if ( $('#dgtBooking').length > 0  ) {
      QLED.movie();
    } else if( $('#dgtGalerry').length > 0 ) {
      QLED.galery();
    }
    
    if(wScreen > 1024) {
      QLED.stick();
    }
    QLED.popup();
    QLED.mobile();
  },

  stick : function() {

    var $fwindow = $(window);
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      
    $fwindow.on('scroll resize', function() {
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      wScreen = $(window).outerWidth();

      if( scrollTop > 200 ) {
        $('.dgt_header').css({ 'position': 'fixed' });
      } else {
        $('.dgt_header').css({ 'position': 'relative' });
      }

    });
  },

  intro : function() {  
    $('.dgt_kv').twentytwenty();
    $(window).load(function() { 
      TweenMax.to('#dgtHome',1, { visibility: 'visible' })
    });
    
  },

  library: function() {

    $('.js_dgt__library').slick({
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: true,
            centerPadding: '100px',
            variableWidth: true
          }
        }
      ]
    });

    

  },

  movie : function() {

    $('.dgt_comparasion__slider').twentytwenty();

    $('.js_dgt__library').slick({
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: true,
            centerPadding: '100px',
            variableWidth: true
          }
        }
      ]
    });


    $('.js_scroll__first').on('click', function() {
      var top = $('.dgt_movie__detail').offset().top;
      TweenLite.to(window, 1, {scrollTo:top });
    });
    $('.js_scroll__second').on('click', function() {
      var top = $('#dgt_loa').offset().top;
      TweenLite.to(window, 1, {scrollTo:top });
    });
    $('.js_scroll__last').on('click', function() {
      var top = $('.dgt_booking__wrap').offset().top;
      TweenLite.to(window, 1, {scrollTo:top });
    });


  },
  
  popup: function() {
    $('#dgt_alert .ovl').on('click', function() {
      $('#dgt_alert').hide()
    });
  },

  galery: function() {

    $('.js_dgt__photo').slick({
      dots: false,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      variableWidth: true
    });

    

    $('.js_dgt__video').slick({
      dots: false,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      variableWidth: true
    });

    $('.dgt_js_tabs li').on('click', function() {

      var addres = $(this).attr('rel');
      var ytb = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/VpBUfx9uVh0?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>'; 

      $('.dgt_js_tabs li').removeClass('active');
      $(this).addClass('active');

      $('.dgt_glr__item').hide();
      $('#'+addres).show();

      if( addres=='dgt_photo') {
        $('.dgt_ytb').html('');
      } else {
        $('.dgt_ytb').html('').append(ytb);
      }

    });


    $('.js_dgt__video .slick-slide').on('click', function() {

      var id = $(this).attr('data');
      console.log(id);
      var ytb = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+id+'?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>'; 
      $('.dgt_ytb').html('').append(ytb);

    });

    $('.js_dgt__photo .slick-slide').on('click', function() {

      var src = $(this).find('.thumb').attr('src');
      $('#dgt_photo .dgt_glr__copy > img').attr('src',src)
    });

  
  },

  mobile: function() {
    
    $('.btn-menu').on('click', function(event) {
      event.preventDefault();
      if ($(this).hasClass('menu-open')) {
        $(this).removeClass('menu-open').addClass('menu-closed');
        $('#dgt_nav').hide();
      } else {
        $(this).removeClass('menu-closed').addClass('menu-open');
        $('#dgt_nav').show();
        
      };
    });




  }

  




};
 

var randomNum = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};



function showAlert() {
  $('#dgt_alert').show()
}











