function App(){
	
	this.historyArr = []; 
	this.historyId = 0;
	this.maxHistory = 3;

	/*var matrixStr = "matrix(1, 0, 0, 1, -32, 9.5)";
	matrixStr = matrixStr.substring(matrixStr.indexOf("(") + 1, matrixStr.indexOf(")"));
	var mSplit = matrixStr.split(", ");
	console.log(mSplit)*/

	// {"action": "add", "target": abc, "dom": ""}
	// {"action": "delete", "target": abc, "dom": ""}
	// {"action": "up", "target": abc, "dom": ""}
	// {"action": "down", "target": abc, "dom": ""}
	// {"action": "flip", "target": abc, "dom": ""}
	// {"action": "flop", "target": abc, "dom": ""}
	// {"action": "move", "target": abc, "dom": ""}

	this.dom = document.getElementById("dom");
	var toolElem = document.getElementById("svg-tool");
	this.tool = new DOMTransformTool(toolElem);
	
	this.displayList = [
		/*new Picture(document.getElementById("dunny"),0,0),
		new Picture(document.getElementById("fatcap"),150,100),
		new Picture(document.getElementById("piggy"),300,200)*/
	];

	this.curSelectItem = null;

	//toolElem.style.zIndex = this.displayList.length;
	
	this.setupTool();
	
	this.bindHandlers();
	
	// selects pictures on mouse down
	this.dom.addEventListener(Mouse.START, this.down);
	
	// draws initial screen
	this.render();
}

App.create = function(){
	App.instance = new App();
	return App.instance;
}

App.instance = null;

App.prototype.addItem = function(item, itemid, x, y){
	var newItem = new Picture(item, x, y);
	newItem.itemId = itemid;

	this.displayList.push(newItem);
	console.log("App.instance.displayList -> " + this.displayList.length)

	newItem.draw();
	
	this.selectItem(newItem);

	this.saveHistory(newItem, "add", newItem.image.src);
}

App.prototype.addItemWithMatrix = function(item,itemid,matrix){
	var newItem = new Picture(item, 0, 0);
	newItem.itemId = itemid;

	this.displayList.push(newItem);

	newItem.transform = new Transformable(newItem.image.width, newItem.image.height, matrix, newItem);

	newItem.draw();
	
	this.selectItem(newItem);

	this.saveHistory(newItem, "add", newItem.image.src);
}

App.prototype.saveHistory = function(target, action, src, itemId){

	//enableUndo();

	var historyData = {};
	historyData.target = target;
	historyData.action = action;
	historyData.src = src;
	historyData.itemId = itemId;
	
	var newMatrix = jQuery.extend({}, this.curSelectItem.transform.matrix);
	console.log(newMatrix)
	historyData.matrix = newMatrix;

	//new Transformable(transform.width, transform.height, transform.matrix, transform.owner);

	//console.log("-> saveHistory: -----------------------")
	//console.log(historyData.style)
	//console.log("-----------------------")
	//console.log(target)
	//console.log(style)
	//console.log(action)

	if(this.historyArr.length > this.maxHistory){
		this.historyArr.shift();
	}

	this.historyArr.push(historyData);

	this.historyId = this.historyArr.length;
}

App.prototype.parseHistory = function(id){

	var historyData = this.historyArr[id];

	//matrix(1, 0, 0, 1, -32, 9.5)
	/*var matrixStr = historyData.style["transform"];
	matrixStr = matrixStr.substring(7);
	matrixStr = matrixStr.substring(0, matrixStr.length-1)
	console.log(matrixStr)
	var mSplit = matrixStr.split(", ");*/
	
	console.log("-> parseHistory: " + historyData.action);

	var target = historyData.target;

	var m = historyData.matrix;

	if(historyData.action == "add"){
		this.deleteItem(historyData.target, true);

	} 
	else if(historyData.action == "delete")
	{
		console.log(historyData.target.image.src);
		console.log(historyData.src);

		//addItemToCanvas(historyData.src, historyData.itemId, 0, 0);
		addItemWithMatrix(historyData.src, historyData.itemId, m);

		/*var app = this;

		setTimeout(function(){
			console.log(app.displayList.length)
			target = app.displayList[app.displayList.length-1];
			console.log(app.displayList.length)
			console.log(target);

			target.transform = new Transformable(target.image.width, target.image.height, m, target);

			target.draw();

			app.selectItem(target);
		},20)*/
	} else {
		target.transform = new Transformable(historyData.target.transform.width, historyData.target.transform.height, m, historyData.target);

		target.draw();

		this.selectItem(target)	
	}

	

	// ----------- BEGIN OF USING DELETE & ADD --------------
	
	//this.deleteItem(target);
	//addItemToCanvas(historyData.data["image"].src, historyData.data["itemId"], historyData.data["x"] + historyData.data["image"].width/2, historyData.data["y"] + historyData.data["image"].height/2);
	
	// ----------- END OF USING DELETE & ADD --------------


	// ----------- BEGIN OF USING MATRIX --------------
	/*var matrix = new Matrix(Number(mSplit[0]), Number(mSplit[1]), Number(mSplit[2]), Number(mSplit[3]), Number(mSplit[4]), Number(mSplit[5]));
	
	target.transform = new Transformable(target.width, target.height, matrix, target);

	this.tool.setTarget(target.transform);

	target.draw();*/
	// ----------- END OF USING MATRIX --------------


	/*
	// ----------- BEGIN OF USE TOOL --------------

	var controlled = this.tool.start(target.transform.matrix.x, target.transform.matrix.y);

	if (!controlled && this.selectImage(target.transform.matrix.x, target.transform.matrix.y)){
		// selection occurred
		// force select the translate control
		// to be able to start moving right away
		controlled = this.tool.start(target.transform.matrix.x, target.transform.matrix.y, this.findControlByType(Control.TRANSLATE)); 
	}

	this.tool.update(historyData.transform.matrix)

	requestAnimationFrame(this.render);
	event.preventDefault();
	
	// ----------- END OF USE TOOL --------------
	*/
}

App.prototype.undo = function(){
	this.historyId--;
	if(this.historyId <= 0){
		this.historyId = 0;
	}
	this.parseHistory(this.historyId);
}

App.prototype.redo = function(){
	this.historyId++;
	if(this.historyId >= this.historyArr.length - 1){
		this.historyId = this.historyArr.length - 1;
	}
	this.parseHistory(this.historyId);
}

App.prototype.cloneItem = function(){
	var itemTarget = this.curSelectItem;

	if(itemTarget){
		var itemIndex;

		for(var i=0; i<this.displayList.length; i++){
			if(this.displayList[i] == itemTarget){
				cloned = this.displayList[i];
				itemIndex = i;
			}
		}

		console.log("clone item " + cloned.itemId);

		addItemToCanvas(cloned.image.src, cloned.itemId, 150 + 50 * Math.random(), 150 + 50 * Math.random());
	}
}

App.prototype.deleteItem = function(target, shouldIgnoreHistory){
	var itemTarget = (target) ? target : this.curSelectItem;

	if(itemTarget){
		
		/*if(!shouldIgnoreHistory){
			this.saveHistory(itemTarget, "delete", itemTarget.image.src, itemTarget.itemId);
		}*/

		this.saveHistory(itemTarget, "delete", itemTarget.image.src, itemTarget.itemId);
		
		var itemIndex;
		for(var i=0; i<this.displayList.length; i++){
			if(this.displayList[i] == itemTarget){
				itemIndex = i;
			}
		}

		this.displayList.splice(itemIndex,1);

		//

		itemTarget.image.parentNode.removeChild(itemTarget.image);

		this.tool.setTarget(null);

		this.curSelectItem = null;

		this.render();


	}
}

App.prototype.moveForward = function(){

	//alert("moveForward")

	var itemTarget = this.curSelectItem;
	
	if(itemTarget){

		var itemIndex;
		for(var i=0; i<this.displayList.length; i++){
			if(this.displayList[i] == itemTarget){
				itemIndex = i;
			}
		}
		
		if(itemIndex == this.displayList.length-1){
			return;
		}

		var itemAbove = this.displayList[itemIndex+1];

		// swap elements:

		this.displayList[itemIndex] = itemAbove;
		this.displayList[itemIndex+1] = itemTarget;

		this.render();
	}

}

App.prototype.moveBackward = function(){
	
	var itemTarget = this.curSelectItem;

	if(itemTarget){

		var itemIndex;
		for(var i=0; i<this.displayList.length; i++){
			if(this.displayList[i] == itemTarget){
				itemIndex = i;
			}
		}

		if(itemIndex <= 0){
			return;
		}

		var itemAbove = this.displayList[itemIndex-1];

		// swap elements:

		this.displayList[itemIndex] = itemAbove;
		this.displayList[itemIndex-1] = itemTarget;

		this.render();
	}
}

App.prototype.flipItem = function(){

	var control = this.findControlByType(Control.TRANSLATE)
	
	this.tool.flip(control);
	
	requestAnimationFrame(this.render);

	this.saveHistory(this.curSelectItem, "move");
}

App.prototype.flopItem = function(){

	var control = this.findControlByType(Control.TRANSLATE)
	
	this.tool.flop(control);
	
	requestAnimationFrame(this.render);

	this.saveHistory(this.curSelectItem, "move");
}

App.prototype.selectItem = function(item){
	this.tool.setTarget(item.transform);

	this.curSelectItem = item;

	requestAnimationFrame(this.render);
}

App.prototype.bindHandlers = function(){
	// instance-specific event handlers bound to this
	this.up = this.up.bind(this);
	this.down = this.down.bind(this);
	this.move = this.move.bind(this);
	this.render = this.render.bind(this);
};

App.prototype.setupTool = function(){
	ControlSet.controlClass = DOMControl;
	//var controls = this.getCustomControls();
	var controls = ControlSet.getScalerWithRotate();
	this.tool.setControls(controls);	
};

App.prototype.getCustomControls = function(){
	var translater = new DOMControl(Control.TRANSLATE);
	// translate control is "selected" by clicking
	// on the target's shape, not the control point
	translater.hitTestTarget = true;
	
	var targetContent = new DOMControl(Control.TARGET);
	return [
		new DOMControl(Control.ROTATE, 0,0, 0,0, 30),
		new DOMControl(Control.ROTATE, 0,1, 0,0, 30),
		new DOMControl(Control.ROTATE, 1,0, 0,0, 30),
		new DOMControl(Control.ROTATE, 1,1, 0,0, 30),
		targetContent, // renders target between controls
		translater,
		new DOMControl(Control.BORDER),
		new DOMControl(Control.REGISTRATION, .5,.5, 0,0, 10),
		new DOMControl(Control.SKEW_Y, 0,.5, 0,0, 10),
		new DOMControl(Control.SCALE_X, 1,.5, 0,0, 10),
		new DOMControl(Control.SKEW_X, .5,0, 0,0, 10),
		new DOMControl(Control.SCALE_Y, .5,1, 0,0, 10),
		new DOMControl(Control.SCALE, 0,0, 0,0, 10),
		new DOMControl(Control.SCALE, 0,1, 0,0, 10),
		new DOMControl(Control.SCALE, 1,0, 0,0, 10),
		new DOMControl(Control.SCALE, 1,1, 0,0, 10),
		new DOMControl(Control.ROTATE_SCALE, 1,0, 15,-15, 10),
		new DOMControl(Control.SCALE_UNIFORM, 1,1, 15,15, 10),
		new DOMControl(Control.ROTATE, .5,0, 0,-20)
	];
};

App.prototype.down = function(event){
	
	Mouse.get(event, this.dom);
	var controlled = this.tool.start(Mouse.x, Mouse.y);
	
	// if tool wasnt selected and being controlled
	// attempt to make a new selection at this location
	if (!controlled && this.selectImage(Mouse.x, Mouse.y)){
		// selection occurred
		// force select the translate control
		// to be able to start moving right away
		controlled = this.tool.start(Mouse.x, Mouse.y, this.findControlByType(Control.TRANSLATE)); 
	}
	
	if (controlled){
		// events for moving selection
		this.dom.addEventListener(Mouse.MOVE, this.move);
		document.addEventListener(Mouse.END, this.up);
	}

	if(this.curSelectItem){

		//console.log(this.curSelectItem.transform)

		this.saveHistory(this.curSelectItem, "move");

		/*this.saveHistory(this.curSelectItem, "move", {
			"transform-origin": this.curSelectItem.image.style["transform-origin"], 
			"transform": this.curSelectItem.image.style["transform"], 
			"z-index": this.curSelectItem.image.style["z-index"], 
		});*/
	}
	
	requestAnimationFrame(this.render);
	event.preventDefault();
};

App.prototype.move = function(event){
	
	Mouse.get(event, this.dom);
	this.applyDynamicControls(event);
	this.tool.move(Mouse.x, Mouse.y);
	
	requestAnimationFrame(this.render);
	event.preventDefault();
};

App.prototype.up = function(event){
	
	this.tool.end();
	
	this.dom.removeEventListener(Mouse.MOVE, this.move);
	document.removeEventListener(Mouse.END, this.up);
	
	requestAnimationFrame(this.render);
	event.preventDefault();
};

App.prototype.applyDynamicControls = function(event){
	// if dynamic, set controls based on 
	// keyboard keys
	var dyn = this.getDynamicControl();
	if (dyn){
		if (event.ctrlKey){
			if (event.shiftKey){
				dyn.type = Control.ROTATE_SCALE;
			}else{
				dyn.type = Control.ROTATE;
			}
		}else if (event.shiftKey){
			dyn.type = Control.SCALE;
		}else{
			dyn.type = Control.TRANSLATE;
		}
	}
};

App.prototype.getDynamicControl = function(){
	var i = 0;
	var n = this.tool.controls.length;
	for (i=0; i<n; i++){
		if (this.tool.controls[i].dynamicUV){
			return this.tool.controls[i];
		}
	}
	return null;
};

App.prototype.findControlByType = function(type){
	var i = 0;
	var n = this.tool.controls.length;
	for (i=0; i<n; i++){
		if (this.tool.controls[i].type == type){
			return this.tool.controls[i];
		}
	}
	return null;
}

App.prototype.selectImage = function(x, y){
	var pic = null;
	var t = null;
	
	// walk backwards selecting top-most first
	var i = this.displayList.length;
	while (i--){
		pic = this.displayList[i];
		t = pic.transform;
		if (t.matrix.containsPoint(x, y, t.width, t.height)){
			if (this.tool.target !== t){
				
				// select
				this.curSelectItem = pic;
				this.tool.setTarget(t);

				// reorder for layer rendering
				//this.displayList.splice(i,1);
				//this.displayList.push(pic);

				return true;
			}

			// already selected
			return false;
		}
	}
	
	// deselect
	this.curSelectItem = null;
	this.tool.setTarget(null);
	return false;
};

App.prototype.render = function(){
	this.drawDisplayList();
	this.tool.draw();
};


App.prototype.drawDisplayList = function(){
	var i = 0;
	var n = this.displayList.length;
	for (i=0; i<n; i++){
		this.displayList[i].image.style.zIndex = i;
		this.displayList[i].draw();
	}
};



/**
 * Display list item showing a picture
 */
function Picture(image, x, y, ignoreSize){
	this.itemId = 0;

	this.image = image;

	var maxW = 300;
	
	var w = image.width || image.clientWidth;
	var h = image.height || image.clientHeight;

	if (typeof ignoreSize === 'undefined')
	{
		if(w > maxW){
			var ratio = maxW/w;
			w = maxW;
			h = h * ratio;
			console.log(w + "x" + h);

			x = 200;
			y = 200;
		}
	}

	this.image.width = w;
	this.image.height = h;

	var m = new Matrix(1,0,0,1,x,y);

	this.transform = new Transformable(w, h, m, this);
	
	var origin = "0 0";
	var style = image.style;
	if (typeof style.transformOrigin !== "undefined")
	{
		style.transformOrigin = origin;
	}
	else if (typeof style.webkitTransformOrigin !== "undefined")
	{
		style.webkitTransformOrigin = origin;
	}
	else if (typeof style.msTransformOrigin !== "undefined")
	{
		style.msTransformOrigin = origin;
	}
	else if (typeof style.MozTransformOrigin !== "undefined")
	{
		style.MozTransformOrigin = origin;
	}
	else if (typeof style.OTransformOrigin !== "undefined")
	{
		style.OTransformOrigin = origin;
	}
};

Picture.prototype.drawWithMatrix = function(matrix){

}

Picture.prototype.draw = function(){
	
	var trans = this.transform.matrix.toString();
	var style = this.image.style;
	
	if (typeof style.transform !== "undefined"){
		style.transform = trans;
	}else if (typeof style.webkitTransform !== "undefined"){
		style.webkitTransform = trans;
	}else if (typeof style.msTransform !== "undefined"){
		style.msTransform = trans;
	}else if (typeof style.MozTransform !== "undefined"){
		style.MozTransform = trans;
	}else if (typeof style.OTransform !== "undefined"){
		style.OTransform = trans;
	}
};