var GFilters = {
	// const:
	NONE: "none",
	BLACK_AND_WHITE: "bnw",
	SEPIA: "sepia",
	POLAROID: "polaroid",
	VINTAGE: "vintage",
	CONTRAST: "contrast",
	SIN_CITY: "sincity",

	// renderType:
	_renderType: "canvas",
	get renderType(){
		return this._renderType;
	},
	set renderType(val) {
		this._renderType = val;
	},

	// methods:
	set: function(container, filterName, onComplete){
		//GFilters.canvasSet(container, filterName);

		container.filters = [];
		if(container.borderShadow) container.borderShadow.visible = false;
		if(this.renderType == "canvas") CanvasFilter.reset(container);

		switch(filterName){
			case GFilters.BLACK_AND_WHITE:
				if(GFilters.renderType == "canvas"){
					CanvasFilter.blackAndWhite(container, function(){
						CanvasFilter.brightness(2, container, function(){
							if(onComplete) onComplete();
						})
					})
				} else {
					//console.log(container);
				    var bnwMatrix = new PIXI.filters.ColorMatrixFilter();
					bnwMatrix.blackAndWhite(true);
					//bnwMatrix.browni(true)

					var brightMatrix = new PIXI.filters.ColorMatrixFilter();
					brightMatrix.brightness(1.2, false);

					var contrast = new PIXI.filters.ColorMatrixFilter();
					//contrast.polaroid();

					container.filters = [bnwMatrix, brightMatrix, contrast];
				}
				//colorMatrix.contrast(100);
				if(container.borderShadow) container.borderShadow.visible = false;
			break;

			case GFilters.BROWNI:
				if(GFilters.renderType == "canvas"){
					CanvasFilter.browni(container, function(){
						CanvasFilter.brightness(1.5, container, function(){
							if(onComplete) onComplete();
						})
					})
				} else {
					var filter = new PIXI.filters.ColorMatrixFilter();
					filter.browni(true);

					var brightMatrix = new PIXI.filters.ColorMatrixFilter();
					brightMatrix.brightness(1.5, true);

					container.filters = [filter, brightMatrix];
				}

				if(container.borderShadow) container.borderShadow.visible = false;
			break;

			case GFilters.SEPIA:
				if(GFilters.renderType == "canvas"){
					CanvasFilter.sepia(container, function(){
						if(onComplete) onComplete();
					})
					//CanvasFilter.brightness(2, container)
				} else {
					var filter = new PIXI.filters.ColorMatrixFilter();
					filter.sepia(true);

					var brightMatrix = new PIXI.filters.ColorMatrixFilter();
					//brightMatrix.brightness(0.8, false);

					container.filters = [filter, brightMatrix];
				}

				if(container.borderShadow) container.borderShadow.visible = false;
			break;

			case GFilters.POLAROID:
				if(GFilters.renderType == "canvas"){
					CanvasFilter.blackAndWhite(container, function(){
						CanvasFilter.polaroid(container, function(){
							if(onComplete) onComplete();
						})
					});
					
					//CanvasFilter.brightness(2, container)
				} else {
					var bnwMatrix = new PIXI.filters.ColorMatrixFilter();
					bnwMatrix.blackAndWhite(true);

					var filter = new PIXI.filters.ColorMatrixFilter();
					filter.polaroid();
					container.filters = [bnwMatrix, filter];
				}

				if(container.borderShadow) container.borderShadow.visible = false;
			break;

			case GFilters.VINTAGE:
				if(GFilters.renderType == "canvas"){
					CanvasFilter.sepia(container, function(){
						if(container.borderShadow){
							container.addChild(container.borderShadow);
						}
						if(onComplete) onComplete();
					})
				} else {
					var filter = new PIXI.filters.ColorMatrixFilter();
					filter.sepia();
					container.filters = [filter];
				}

				if(!container.borderShadow){
					container.borderShadow = new PIXI.Sprite.fromImage("assets/fx/border-shadow.png");
					container.borderShadow.anchor.set(container.anchor.x, container.anchor.y);
					container.borderShadow.width = container.width / container.scale.x;
					container.borderShadow.height = container.height / container.scale.y;
					container.borderShadow.alpha = 0.5;
					container.addChild(container.borderShadow);
				} else {
					container.borderShadow.visible = true;
				}
			break;

			case GFilters.CONTRAST:
				if(GFilters.renderType == "canvas"){
					CanvasFilter.contrast(2, container, function(){
						CanvasFilter.brightness(1.2, container, function(){
							if(onComplete) onComplete();
						})
					})
				} else {
					var filter = new PIXI.filters.ColorMatrixFilter();
					filter.contrast(2, true);

					var brightMatrix = new PIXI.filters.ColorMatrixFilter();
					brightMatrix.brightness(1.2, false);

					container.filters = [filter, brightMatrix];
				}

				if(container.borderShadow) container.borderShadow.visible = false;
			break;

			case GFilters.SIN_CITY:
				if(GFilters.renderType == "canvas"){
					CanvasFilter.contrast(3, container, function(){
						CanvasFilter.blackAndWhite(container, function(){
							if(onComplete) onComplete();
						})
					})
					//CanvasFilter.brightness(1.2, container)
				} else {
					var filter = new PIXI.filters.ColorMatrixFilter();
					filter.contrast(3, true);

					var bnwMatrix = new PIXI.filters.ColorMatrixFilter();
					bnwMatrix.blackAndWhite(true);

					var brightMatrix = new PIXI.filters.ColorMatrixFilter();
					brightMatrix.brightness(1.2, false);

					container.filters = [filter, bnwMatrix, brightMatrix];
				}

				if(container.borderShadow) container.borderShadow.visible = false;
			break;

			case GFilters.NONE:
				container.filters = [];
				if(container.borderShadow) container.borderShadow.visible = false;
				if(GFilters.renderType == "canvas") CanvasFilter.reset(container);
			break;

			default:
				container.filters = [];
				if(container.borderShadow) container.borderShadow.visible = false;
				//console.log("none");
				if(GFilters.renderType == "canvas") CanvasFilter.reset(container);
			break;
		}
	}
}

var CanvasFilter = {
	canvas: null,
	renderer: null,

	enable: function(renderer){
		this.renderer = renderer;
	},

	blackAndWhite: function(img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		/*var matrix = [
			0.33, 0.34, 0.33, 0, 0, 
			0.33, 0.34, 0.33, 0, 0, 
			0.33, 0.34, 0.33, 0, 0, 
			0, 0, 0, 1, 0]*/
		var matrix = [
	        0.3, 0.6, 0.1, 0, 0,
	        0.3, 0.6, 0.1, 0, 0,
	        0.3, 0.6, 0.1, 0, 0,
	        0, 0, 0, 1, 0
	    ];

		CanvasFilter.apply(matrix, img, onComplete);
	},

	grayscale: function(scale, img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		var matrix = [
	        scale, scale, scale, 0, 0,
	        scale, scale, scale, 0, 0,
	        scale, scale, scale, 0, 0,
	        0, 0, 0, 1, 0
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	brightness: function(b, img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		//window.open(exportImg, "_blank");
		//return;
		var matrix = [
	        b, 0, 0, 0, 0,
	        0, b, 0, 0, 0,
	        0, 0, b, 0, 0,
	        0, 0, 0, 1, 0
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	hue: function(rotation, img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		rotation = (rotation || 0) / 180 * Math.PI;
	    var cosR = Math.cos(rotation),
	        sinR = Math.sin(rotation),
	        sqrt = Math.sqrt;
	    /*a good approximation for hue rotation
	    This matrix is far better than the versions with magic luminance constants
	    formerly used here, but also used in the starling framework (flash) and known from this
	    old part of the internet: quasimondo.com/archives/000565.php
	    This new matrix is based on rgb cube rotation in space. Look here for a more descriptive
	    implementation as a shader not a general matrix:
	    https://github.com/evanw/glfx.js/blob/58841c23919bd59787effc0333a4897b43835412/src/filters/adjust/huesaturation.js
	    This is the source for the code:
	    see http://stackoverflow.com/questions/8507885/shift-hue-of-an-rgb-color/8510751#8510751
	    */
	    var w = 1/3, sqrW = sqrt(w);//weight is
	    var a00 = cosR + (1.0 - cosR) * w;
	    var a01 = w * (1.0 - cosR) - sqrW * sinR;
	    var a02 = w * (1.0 - cosR) + sqrW * sinR;
	    var a10 = w * (1.0 - cosR) + sqrW * sinR;
	    var a11 = cosR + w*(1.0 - cosR);
	    var a12 = w * (1.0 - cosR) - sqrW * sinR;
	    var a20 = w * (1.0 - cosR) - sqrW * sinR;
	    var a21 = w * (1.0 - cosR) + sqrW * sinR;
	    var a22 = cosR + w * (1.0 - cosR);
	    var matrix = [
	      a00, a01, a02, 0, 0,
	      a10, a11, a12, 0, 0,
	      a20, a21, a22, 0, 0,
	      0, 0, 0, 1, 0,
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	contrast: function(amount, img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		var v = (amount || 0) + 1;
	    var o = -128 * (v - 1);
	    var matrix = [
	        v, 0, 0, 0, o,
	        0, v, 0, 0, o,
	        0, 0, v, 0, o,
	        0, 0, 0, 1, 0
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	negative: function(img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		var matrix = [
	        0, 1, 1, 0, 0,
	        1, 0, 1, 0, 0,
	        1, 1, 0, 0, 0,
	        0, 0, 0, 1, 0
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	sepia: function(img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		var matrix = [
	        0.393, 0.7689999, 0.18899999, 0, 0,
	        0.349, 0.6859999, 0.16799999, 0, 0,
	        0.272, 0.5339999, 0.13099999, 0, 0,
	        0, 0, 0, 1, 0
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	technicolor: function(img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		var matrix = [
	        1.9125277891456083, -0.8545344976951645, -0.09155508482755585, 0, 11.793603434377337,
	        -0.3087833385928097, 1.7658908555458428, -0.10601743074722245, 0, -70.35205161461398,
	        -0.231103377548616, -0.7501899197440212, 1.847597816108189, 0, 30.950940869491138,
	        0, 0, 0, 1, 0
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	polaroid: function(img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		var matrix = [
	        1.438, -0.062, -0.062, 0, 0,
	        -0.122, 1.378, -0.122, 0, 0,
	        -0.016, -0.016, 1.483, 0, 0,
	        0, 0, 0, 1, 0
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	browni: function(img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		var matrix = [
	        0.5997023498159715, 0.34553243048391263, -0.2708298674538042, 0, 47.43192855600873,
	        -0.037703249837783157, 0.8609577587992641, 0.15059552388459913, 0, -36.96841498319127,
	        0.24113635128153335, -0.07441037908422492, 0.44972182064877153, 0, -7.562075277591283,
	        0, 0, 0, 1, 0
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	vintage: function(img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		var matrix = [
	        0.6279345635605994, 0.3202183420819367, -0.03965408211312453, 0, 9.651285835294123,
	        0.02578397704808868, 0.6441188644374771, 0.03259127616149294, 0, 7.462829176470591,
	        0.0466055556782719, -0.0851232987247891, 0.5241648018700465, 0, 5.159190588235296,
	        0, 0, 0, 1, 0
	    ];

	    CanvasFilter.apply(matrix, img, onComplete);
	},

	apply: function(matrix, img, onComplete){
		if(!this.renderer){
			console.log("[Error] CanvasFilter hasn't been enabled yet.");
			return;
		}
		var renderer = this.renderer;
		var texture = (img.canvasFilter) ? img.canvasFilter.texture : img.texture;
		var actualImg = new PIXI.Sprite(texture);
		var exportImg = GPixi.containerToBase64(renderer, actualImg);
		//var newImg = CanvasFilter.applyColorMatrixFilter(exportImg, matrix);
		//var filterTexture = new PIXI.Texture.fromImage(newImg);
		CanvasFilter.applyColorMatrixFilter(exportImg, matrix, function(canvas){
			var filterTexture = new PIXI.Texture.fromCanvas(canvas)
			if(img.canvasFilter){
				img.removeChild(img.canvasFilter);
				img.canvasFilter = null;
			}
			img.canvasFilter = new PIXI.Sprite(filterTexture);
			img.canvasFilter.anchor.set(img.anchor.x, img.anchor.y);
			img.addChild(img.canvasFilter);

			if(onComplete) onComplete();
		})
		
		//img.texture = PIXI.Texture.fromImage(newImg);
		//return newImg;
	},

	reset: function(img){
		if(img.canvasFilter){
			img.removeChild(img.canvasFilter);
			img.canvasFilter = null;
		}/* else {
			console.log("[Error] There is no filters applied.");
		}*/
	},

	//Apply the current color matrix to the currently loaded image
	filterCallback: null,
	applyColorMatrixFilter: function(image, matrix, callback) {
		this.filterCallback = callback;
        if (!image) return;
        if (String(image).indexOf("data:image") >= 0){
        	var src = image;
        	image = new Image();
        	image.src = src;
        	image.onload = imgLoaded;
        } else {
        	imgLoaded();
        }
        
        function imgLoaded(){
	        if (!CanvasFilter.canvas){
	        	CanvasFilter.canvas = document.createElement('canvas');
	        	document.body.appendChild(CanvasFilter.canvas);
	        	//CanvasFilter.canvas.style.opacity = 0;
	        	CanvasFilter.canvas.style.position = "absolute";
	        	CanvasFilter.canvas.style.zIndex = "-1";
	        	$(CanvasFilter.canvas).css({
	        		"position": "fixed",
	        		"opacity": "0",
	        		"z-index": "-1",
	        		"top": "0",
	        		"left": "0"
	        	})
	        }

	        var canvas = CanvasFilter.canvas;

	        if (image.width != canvas.width)
	          	canvas.width = image.width;
	        if (image.height != canvas.height)
	          	canvas.height = image.height;
	        
	        context = canvas.getContext("2d");
	        context.clearRect(0, 0, canvas.width, canvas.height);
	        context.drawImage(image, 0, 0, canvas.width, canvas.height);

	        if (canvas.width > 0 && canvas.height > 0) {
	        	var pixels = context.getImageData(0, 0, canvas.width, canvas.height);
	        	var m = matrix;
	        	var d = pixels.data;
		        for (var i = 0; i < d.length; i += 4) {
					var r = d[i];
					var g = d[i + 1];
					var b = d[i + 2]; 
					var a = d[i + 3];

					d[i]   = r * m[0] + g * m[1] + b * m[2] + a * m[3] + m[4];
					d[i+1] = r * m[5] + g * m[6] + b * m[7] + a * m[8] + m[9];
					d[i+2] = r * m[10]+ g * m[11]+ b * m[12]+ a * m[13]+ m[14];
					d[i+3] = r * m[15]+ g * m[16]+ b * m[17]+ a * m[18]+ m[19]; 
		        }

	            context.putImageData(pixels, 0, 0);
	        }

	        if(callback) callback(canvas);
        }

        //var base64 = canvas.toDataURL();
        //return base64;
        //return canvas;
	} 
}