﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class captcha : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Captcha _captcha = new Captcha();
        _captcha.Show();
        Session["sessionCaptcha"] = _captcha.Value;
    }
}