﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class library_edit : System.Web.UI.Page
{
    Database db = new Database();
    public int intId = 0;
    public DataRow dtRow = null;    
    public string dir_photo = Common.UploadDir()+"gallery/";
    public Messages message = new Messages();
    public string formhash = "";
    public string listCitys = "";        
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }
        intId = Request.QueryString["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (!Page.IsPostBack)
        {                      
            if (intId <= 0)
            {
                Response.Redirect(Common.Webroot + "library.aspx");
                Response.End();
            }
            LoadDetail();            
        }
        FormHash objFormHash = new FormHash();
        message.Message = "";
        if (Request.HttpMethod=="POST")
        {
            objFormHash.formKey = Request.Form["Formhash"] != null ? Common.CleanValue(Request.Form["Formhash"]) : "";
            if (!objFormHash.validate())
            {
                message.FormHash = objFormHash.getFormKey();                
                message.Message = "Thông tin không hợp lệ. Vui lòng thử lại";
            }
            else
            {
                Update();
            }
        }
        formhash = objFormHash.printFormHash();                
    }

    protected void LoadDetail()
    {
        DataTable dtDetail = db.Exec("SELECT TOP 1 * FROM Feedback WHERE Id="+intId);
        if (dtDetail==null || dtDetail.Rows.Count <= 0)
        {
            Response.Redirect(Common.Webroot + "library.aspx");
            Response.End();
        }
        dtRow = dtDetail.Rows[0];      
    }
    protected void Update()
    {
        DataTable dtDetail = db.Exec("SELECT TOP 1 * FROM Feedback WHERE Id=" + intId);
        if (dtDetail != null && dtDetail.Rows.Count > 0)
        {
            string Name = Request.Form["Name"] != null ? Common.CleanValue(Request.Form["Name"]) : "";
            string Content = Request.Form["Content"] != null ? Common.CleanValue(Request.Form["Content"]) : "";
            int Status = Request.Form["Status"] != null ? Common.ParseInt(Request.Form["Status"]) : 0;
            int RemoveThumb = Request.Form["thumbnailRemove"] != null ? Common.ParseInt(Request.Form["thumbnailRemove"]) : 0;
            string thumbnailOld = dtDetail.Rows[0]["Thumbnail"].ToString();
            string listGallery = dtDetail.Rows[0]["Gallery"].ToString();
            string removeItems = Request.Form["removeItems"] != null ? Common.CleanValue(Request.Form["removeItems"]) : "";
            string cityName = Request.Form["city"] != null ? Common.CleanValue(Request.Form["city"]) : "";
            int OrderId = Request.Form["OrderId"] != null ? Common.ParseInt(Request.Form["OrderId"]) : 0;
            int WeekId = Request.Form["WeekId"] != null ? Common.ParseInt(Request.Form["WeekId"]) : 0;
            if (Name == "")
            {
                message.Message = "Vui lòng nhập Họ và Tên";
            }
            else
            {
                if (RemoveThumb == 1 && thumbnailOld != "")
                {
                    DeleteFiles(thumbnailOld);
                    thumbnailOld = "";
                }
                if (removeItems != "")
                {
                    string strItemsOld = "";
                    string[] stringArray = removeItems.Split(',');
                    foreach (string img in listGallery.Split(','))
                    {
                        int pos = Array.IndexOf(stringArray, img);
                        if (pos >0)
                        {
                            DeleteFiles(img);
                        }
                        else
                        {
                            strItemsOld += img + ",";
                        }
                    }
                    listGallery = strItemsOld.TrimEnd(new char[] { ',' });
                }
                if (Request.Files.Count > 0)
                {
                    message = UploadMultiple();
                    if (message.Status == 1)
                    {
                        message.Message = "";
                        listGallery = message.Content != "" ? message.Content + "," + listGallery : listGallery;
                        HttpPostedFile fileThumb = Request.Files["Thumbnail"];
                        if (fileThumb != null && fileThumb.ContentLength > 0)
                        {
                            Random rand = new Random();
                            if (Common.CheckExtension(fileThumb, Common.allowedExtPic))
                            {
                                string ext = Path.GetExtension(fileThumb.FileName).ToLower();
                                if (thumbnailOld != "")
                                {
                                    DeleteFiles(thumbnailOld);
                                }
                                if (!File.Exists(dir_photo + "" + fileThumb.FileName))
                                {
                                    thumbnailOld = Common.Slug(Path.GetFileNameWithoutExtension(fileThumb.FileName)).ToLower() + ext;
                                }
                                else
                                {
                                    thumbnailOld = Common.Slug(Path.GetFileNameWithoutExtension(fileThumb.FileName)).ToLower() + "_" + rand.Next(0, 100) + "" + ext;
                                }
                                fileThumb.SaveAs(dir_photo + "" + thumbnailOld);
                            }
                            else
                            {
                                message.Message = "Vui lòng chỉ upload những định dạng hình ảnh " + String.Join(",", Common.allowedExtPic);
                            }
                        }
                    }
                }
                if (string.IsNullOrWhiteSpace(message.Message))
                {
                    Dictionary<string, object> dataInsert = new Dictionary<string, object>();
                    dataInsert.Add("Name", Name);
                    dataInsert.Add("Content", Content);
                    dataInsert.Add("Status", Status);
                    dataInsert.Add("Thumbnail", thumbnailOld);
                    dataInsert.Add("Slug", Common.Slug(Name));
                    dataInsert.Add("Gallery", listGallery);
                    dataInsert.Add("City", cityName);
                    dataInsert.Add("OrderId", OrderId);
                    dataInsert.Add("WeekId", WeekId);
                    int isUpdate = db.UpdateTable("Feedback", dataInsert, "Id=" + intId);
                    if (isUpdate > 0)
                    {
                        message.Status = 1;
                        message.Message = "Thông tin được lưu thành công ";
                        dtDetail = db.Exec("SELECT TOP 1 * FROM Feedback WHERE Id=" + intId);
                        if (dtDetail != null && dtDetail.Rows.Count > 0)
                        {
                            dtRow = dtDetail.Rows[0];
                        }
                    }
                    else
                    {
                        message.Message = db.strError;
                    }
                }
            }
        }
        else
        {
            message.Message = "Thông tin này không tồn tại";
        }

    }
    protected  Messages UploadMultiple()
    {
        Messages msg = new Messages();
        msg.Content = "";
        msg.Status = 1;
        string comas = "";
        int i = 0;
        bool isOk = true;
        foreach (string str in Request.Files.AllKeys)
        {
            if (str == "images")
            {
                HttpPostedFile file = Request.Files[i];
                if (file != null && file.ContentLength > 0)
                {
                    if (Common.CheckExtension(file, Common.allowedExtPic))
                    {   
                        string ex = Path.GetExtension(file.FileName).ToLower();
                        string fname = DateTime.Now.Ticks+ "_" + i;
                        file.SaveAs(dir_photo + "" + fname + ex);
                        msg.Content += comas+""+fname + "" + ex;
                        comas = ",";
                    }
                    else
                    {
                        msg.Status = 0;
                        msg.Message = "Vui lòng chỉ upload những định dạng hình ảnh "+ String.Join(",", Common.allowedExtPic); 
                        isOk = false;
                        break;
                    }
                }
            }
            i = i + 1;
        }
        if (!isOk)
        {
            foreach(string img in msg.Content.Split(','))
            {
                if (img != null)
                {
                    DeleteFiles(img);
                }
            }
        }
        else
        {            
            msg.Content = msg.Content.TrimEnd(new char[]{ ',' });
        }
        return msg;
    }

    protected void DeleteFiles(string filename)
    {
        if (System.IO.File.Exists(dir_photo + "" + filename))
            System.IO.File.Delete(dir_photo + "" + filename);
        if (System.IO.File.Exists(dir_photo + "thumb_" + filename))
            System.IO.File.Delete(dir_photo + "thumb_" + filename);
    }
}