﻿<%@ Page Language="C#" MasterPageFile="~/CMS.master" AutoEventWireup="true" CodeFile="qhomeregister.aspx.cs" Inherits="qhomeregister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<%
    string dateView = Request.QueryString["dateview"] != null ? Request.QueryString["dateview"].ToString() : "";
    string hotel = Request.QueryString["hotel"] != null ? Request.QueryString["hotel"].ToString() : "";
    string status = Request.QueryString["status"] != null ? Request.QueryString["status"].ToString() : "";
    string provinceQueryString = Request.QueryString["province"] != null ? Request.QueryString["province"].ToString() : "";
    Dictionary<int, string> provinces = ListProvince();
%>
    <section class="content-header">
        <h1>
        DANH SÁCH ĐĂNG KÝ TRẢI NGHIỆM QHOME  (<%=_totalRows %>)
        <small></small>
        </h1>
    </section>    
    <section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">             
              
          </h3>
        <div class="box-tools">
            <form method="get">
            <div class="input-group">       
                 <div class="input-group group-date" style="width:100%">
                     
                      <select name="status" class="form-control" style="width:200px; margin-right:5px;">
                         <option value="">Trạng thái</option>
                          <option <%= status == "0" ? "selected=selected" : "" %> value="1">Mới đăng ký</option>
                         <option <%= status == "1" ? "selected=selected" : "" %> value="1">Đồng ý trải nghiệm</option>
                         <option <%= status == "2" ? "selected=selected" : "" %> value="2">Không đồng ý trải nghiệm</option>
                     </select>  
                     <select class="form-control" style="width:200px" name="province">
                         <option value="">Tỉnh thành phố</option>
                         <% foreach(var province in provinces){ %>
                            <option <%= provinceQueryString == province.Key.ToString() ? "selected=selected" : "" %> value="<%= province.Key.ToString() %>"><%= province.Value %></option>
                         <%} %>
                     </select>
                    <input type="hidden" value="<%=confirmed %>" name="confirmed" />
                    <!-- <input type="text" class="form-control" style="width:110px; margin-right:5px;" readonly="true" name="dateview" id="startdate" style=" width: 50%; " placeholder="Ngày xem phim" value="<%= dateView %>" /> -->
                    <input type="text" name="search" class="form-control" style="width: 150px;margin-right:5px;" placeholder="Search" value="<%= search%>"/>
                    <button class="btn btn-primary" type="submit">Lọc</button>     
                     <a href="<%=Common.BaseUrl %>qhomeregister.aspx?action=export<%=queryString %>" class="btn btn-primary" >Xuất Excel</a>                     
                     <a href="<%= Common.Webroot %>qhomeregister.aspx" class="btn btn-primary" >Reset Form</a>
                </div>                       
            </div>            
            </form>
         </div>
        </div><!-- /.box-header -->
        <div class="box-body" style="overflow-x:scroll">

          <table class="table table-bordered table-hover js-order-data">
            <thead>
              <tr>                 
                <!--<th><input type="checkbox" id="selectall" class="checkbox" name="cbSelectAll" /></th>-->
				<th>STT</th>
                <th>ID</th>
                <th>Tên</th>          
                 <th>Facebook</th>                     
                <th>Điện thoại</th>
                <th>Email</th>
                <th>Diện tích phòng</th>
                  <th>Địa chỉ</th>
                  <th>Tỉnh thành</th>
                <td>Hình ảnh</td>
                <th>Nhãn hiệu TV</th>
                <th>Năm sinh</th>
                <th>Nghề nghiệp</th>
                <th>Địa chỉ</th>
                  <th>Lý do</th>
                <th>Ngày tạo</th>
				<th>Trạng thái</th>  
                <th>Xóa đăng kí</th>
                <td>Đổi trạng thái</td>
              </tr>
            </thead>
            <tbody>
              <% foreach(System.Data.DataRow item in dtMember.Rows ) { %>
              <tr> 			    
                <!--<td><input type="checkbox" class="selectedId" name="chkitem[]" value="<%= item["QhomeRegisterId"] %>" /></td>-->
				<td><%=stt%></td>
                <td><%= item["QhomeRegisterId"].ToString() %></td>  
                <td><%= item["Name"].ToString() %></td>  
                 <td><a target="_blank" href="https://facebook.com/<%= item["FacebookId"].ToString() %>"><%= item["FacebookName"].ToString() != "" ? item["FacebookName"].ToString() : item["Name"].ToString() %></a></td> 
                <td><%= item["Phone"] %></td>
                  <td><%= item["Email"] %></td>
                  <td><%= item["Acreage"] %><sup>m2</sup></td>
                  <td><%= item["Address"].ToString() %></td>
                  <td><%= provinces.Where(x=>x.Key == (int.Parse(item["City"].ToString()))).SingleOrDefault().Value %></td>
                  <td>
                      <% if (item["Image"]!=""){ %>
                      <a target="_blank" href="<%= System.Configuration.ConfigurationManager.AppSettings["FRONT_END_URL"] %>/Uploads/<%= item["Image"] %>">
                          <img src="<%= System.Configuration.ConfigurationManager.AppSettings["FRONT_END_URL"] %>/Uploads/<%= item["Image"] %>" style="max-width:200px"/>
                      </a>
                      <% } %>
                  </td>
                   <td><%= item["Brand"] %></td>
                  <td><%= item["Birthday"] %></td>
                   <td><%= item["Job"] %></td>
                  <td><%= item["Address"] %></td>
                  <td class="s">
                      <% if(item["Reason"] != ","){ %>
                        <%= item["Reason"].ToString().Trim(',') %>
                      <% } %>
                  </td>
                  <td><%= ((DateTime)item["Created"]).ToString("dd-MM-yyyy :HH:m:s") %></td>
                <td>                    
                    <% if (item["Status"].ToString() == "1")
                        { %>
                        <span class="js-item-<%=item["QhomeRegisterId"]%>"><a class="btn btn-primary btn-sm js-selectAction" href="#"  data-href="<%= Common.BaseUrl %>member-waiting.aspx?id=<%= item["QhomeRegisterId"].ToString() %>&action=assignType">Đồng ý trải nghiệm</a></span>
                    <%}
                    else if(item["status"].ToString() == "0")
                    { %>
                     <span class="btn btn-warning">Mới đăng ký</span>
                    <%}else{ %>
                        <span class="btn btn-danger">Không đồng ý trải nghiệm</span>
                    <%} %>
                </td>
                  
                <td><a href="javascript:;" class="luc-delete" data-qsuite="2" data-id="<%= item["QhomeRegisterId"] %>">Xóa đăng ký</a></td>
                 <td>
                      <select class="form-control change-register-status" data-url="qhomeregister.aspx" data-id="<%= item["QhomeRegisterId"].ToString() %>" >
                          <option <%= item["Status"].ToString() == "0" ? "selected=selected" : "" %> value="0">Mới đăng ký</option>
                          <option <%= item["Status"].ToString() == "1" ? "selected=selected" : "" %> value="1">Đồng ý trải nghiệm</option>
                          <option <%= item["Status"].ToString() == "2" ? "selected=selected" : "" %> value="2">Không đồng ý trải nghiệm</option>
                      </select>
                  </td>
              </tr>
              <% stt = stt+1;} %>
            </tbody>
          </table>

        </div><!-- /.box-body -->
        <div class="box-footer clearfix pagination">
            <%=pagingHtml %>
        </div>
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</asp:Content>