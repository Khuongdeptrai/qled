﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cms_member_waiting : System.Web.UI.Page
{
    public int _totalRows = 0;
    public string search = string.Empty;
    public string pagingHtml = string.Empty;
    public DataTable dtMember = new DataTable();
    public string _startDate = string.Empty;
    public string _endDate = string.Empty;
    private int TotalL = 20;
    private System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
    List<string> Citys = new List<string> { "Hà Nội", "TP HCM", "An Giang", "Bà Rịa - Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước", "Bình Thuận", "Cà Mau", "Cao Bằng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương", "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái", "Phú Yên", "Cần Thơ", "Đà Nẵng", "Hải Phòng" };
    System.Text.StringBuilder builder = new System.Text.StringBuilder();
    private Dictionary<DateTime, List<FilmSchedule>> filmsHanoi = new Dictionary<DateTime,List<FilmSchedule>>();
    private Dictionary<DateTime, List<FilmSchedule>> filmsHcm = new Dictionary<DateTime, List<FilmSchedule>>();
    
    public string listCitys = "";
    public string listModels = "";
    public int sent = 0;
	public string queryString  = "";
	public int stt = 0;
    public int confirmed = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
		if (Session["CMSUSER"] == null){
				Response.Redirect(Common.Webroot+"login.aspx");
				Response.End();
		}
       

        if (Request.HttpMethod=="POST")
        {
            string op = Request.Form["op"] != null ? Request.Form["op"] : "";
            string action = Request.Form["action"] != null ? Request.Form["action"] : "";
          
                if (action == "deleteRegister")
                {
                    Delete(Int32.Parse(Request.Form["id"].ToString()));
                }
          
        }

        Database db = new Database();
        string cinemaId = Request.QueryString["cinemaid"] != null ? Common.CleanValue(Server.HtmlDecode(Request.QueryString["cinemaid"].Trim())) : "";
        string filmId = Request.QueryString["filmid"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["filmid"]).Trim()) : "";
        string dateView = Request.QueryString["dateview"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["dateview"]).Trim()) : "";
        string timeView = Request.QueryString["timeview"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["timeview"]).Trim()) : "";
        confirmed = Request.QueryString["confirmed"] != null ? Common.ParseInt(Request.QueryString["confirmed"]) : 0;
        string strQuery = "ISNULL(Success,0)="+ confirmed;
        queryString += "&confirmed=" + confirmed;
        string orderBy = " order by RegisterId desc";
        int perPage = 50;
        int currentPage = Request.QueryString["page"] == null ? 1 : Common.ParseInt(Request.QueryString["page"].ToString());
        currentPage = currentPage <= 0 ? 1 : currentPage;
        db.dbCommand.Parameters.Clear();

        if (cinemaId != "")
        {
            queryString += "&CinameId=" + Server.HtmlEncode(cinemaId); ;
            strQuery += " and CinemaIdCode = @CinameId";
            int cinemaSqlId = 0;
            int.TryParse(cinemaId, out cinemaSqlId);
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@CinameId", DbType.Int32, cinemaSqlId));

        }
        if (filmId != "")
        {
            queryString += "&filmId=" + Server.HtmlEncode(filmId);
            strQuery += " and FimlIdCode =  @filmId";
            int filmSqlId = 0;
            int.TryParse(filmId, out filmSqlId);
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@filmId", DbType.Int32, filmSqlId));
        }
        if (dateView != "") 
        {
            queryString += "&dateView=" + Server.HtmlEncode(dateView);
            //dateView = dateView.Replace("-", ":");
            strQuery += " and DateView =  @DateView";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@DateView", DbType.String, dateView));
        }

        if(timeView != ""){
            string timeV = timeView.Replace('-', ':');
            queryString += "&timeview=" + Server.HtmlEncode(timeView);
            dateView = dateView.Replace("-", ":");
            strQuery += " and TimeView =  @timeView";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@TimeView", DbType.String, timeV));
        }

        search = Request.QueryString["search"] == null ? string.Empty : Common.CleanScripts(Server.HtmlDecode(Request.QueryString["search"].ToString()));
        search = Common.CleanValue(search);
        
        if (search != "")
        {
            if (search.All(char.IsDigit))
            {
                strQuery += search != string.Empty ? " and ( Phone like N'%'+@searchKey+'%' or Email like N'%'+@searchKey+'%' ) " : string.Empty;
            }
            else
            {
                strQuery += search != string.Empty ? " and ( Name COLLATE Latin1_General_CI_AI like N'%'+@searchKey+'%' COLLATE Latin1_General_CI_AI or Phone=@searchKey or Email like N'%'+@searchKey+'%' or TvBrandOld like N'%'+@searchKey+'%' ) " : string.Empty;
            }
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@searchKey", DbType.String, search));
        }

        queryString += search != string.Empty ? "&search=" + Server.HtmlEncode(search) : "";

        

        string sqlSelect = string.Empty;
        DataTable dtTotalRows = db.Exec("select count(RegisterId) as total from Registers where " + strQuery);
        _totalRows = dtTotalRows.Rows.Count > 0 ? Common.ParseInt(dtTotalRows.Rows[0]["total"]) : 0 ;
        int _maxPage = (_totalRows % perPage) == 0 ? _totalRows / perPage : (_totalRows / perPage) + 1;
        int _startPage = (currentPage - 1) * perPage + 1;
        int _endPage = _startPage + perPage - 1;
		stt = _startPage;

        string _action = Request.QueryString["action"] == null ? string.Empty : Request.QueryString["action"].ToString();
        if (_action == "export")
        {
            sqlSelect = "SELECT  * FROM Registers v where " + strQuery + orderBy;
            dtMember = db.Exec(sqlSelect);
            dtMember.Columns.Remove("BrowserName");
            dtMember.Columns.Remove("BrowserVersion");
            dtMember.Columns.Remove("CinemaIdCode");
            dtMember.Columns.Remove("FimlIdCode");
            dtMember.Columns.Remove("UserAgent");
           
            ExportData expt = new ExportData();
            expt.dtList = dtMember;
            expt.ExportGridView(DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".xls", "xls", "Danh sách đăng ký");
            Response.End();
        }

        sqlSelect = @"SELECT  * 
                                FROM 
                                    (SELECT  ROW_NUMBER() OVER(" + orderBy + @") as row, y.*
                                        FROM 
	                                    ( SELECT TOP " + _totalRows + " * from Registers where " + strQuery + orderBy + @"                           
	                                    ) y
                                    )x
                                WHERE x.row BETWEEN " + _startPage.ToString() + " AND " + _endPage.ToString();
        
        dtMember = db.Exec(sqlSelect);

        if (_maxPage > 1)
        {
            PagingCMS paging = new PagingCMS();
            paging.Last = "&raquo;";
            paging.First = "&raquo;";
            paging.Next = "&rsaquo;";
            paging.Prev = "&lsaquo;";
            paging.EventClick = "";
            pagingHtml = paging.multi(_totalRows, perPage, currentPage, Common.Webroot + "member-waiting.aspx?page=%page%" + queryString);
        }
        
    }
    private void AssignUserToType()
    {
        
    }
   
    private Dictionary<int, string> ListFilmName()
    {

        var films = new Dictionary<int, string>();
        films.Add(1, "Dạ Cổ Hoài Lang");
        films.Add(2, "Kunfu Yoga");
        films.Add(3, "Now You See Me 2");
        films.Add(4, "49 Ngày 2");
        films.Add(5, "Enders Game");
        films.Add(6, "The Expendables 3");
        return films;
    }
    public Dictionary<DateTime, List<FilmSchedule>> FilmSheduleHaNoi()
    {
        var listFilmNam = ListFilmName();

        List<Dictionary<DateTime, List<FilmSchedule>>> ddlfHn = new List<Dictionary<DateTime, List<FilmSchedule>>>();
        Dictionary<DateTime, List<FilmSchedule>> dicList = new Dictionary<DateTime, List<FilmSchedule>>();

        var listDate6 = new List<FilmSchedule>();
        // listDate6.Add(new FilmSchedule { Time = "12:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
        listDate6.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 10 });
        // listDate6.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 16 });
        // listDate6.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 16 });
        // listDate6.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = 16 });

        dicList.Add(new DateTime(2017, 05, 06), listDate6);


        var listDate7 = new List<FilmSchedule>();

        listDate7.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate7.Add(new FilmSchedule { Time = "13:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate7.Add(new FilmSchedule { Time = "15:45", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate7.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate7.Add(new FilmSchedule { Time = "20:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });

        dicList.Add(new DateTime(2017, 05, 07), listDate7);

        var listDate8 = new List<FilmSchedule>();
        // listDate8.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
        listDate8.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate8.Add(new FilmSchedule { Time = "16:25", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate8.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 08), listDate8);

        var listDate9 = new List<FilmSchedule>();
        //listDate9.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
        listDate9.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate9.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate9.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 09), listDate9);

        var listDate10 = new List<FilmSchedule>();
        // listDate10.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 12 });
        listDate10.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate10.Add(new FilmSchedule { Time = "17:15", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate10.Add(new FilmSchedule { Time = "19:05", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 10), listDate10);

        var listDate11 = new List<FilmSchedule>();
        // listDate11.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
        listDate11.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate11.Add(new FilmSchedule { Time = "17:05", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate11.Add(new FilmSchedule { Time = "19:35", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 11), listDate11);

        var listDate12 = new List<FilmSchedule>();
        //listDate12.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
        listDate12.Add(new FilmSchedule { Time = "12:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate12.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate12.Add(new FilmSchedule { Time = "17:10", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate12.Add(new FilmSchedule { Time = "20:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 12), listDate12);

        var listDate13 = new List<FilmSchedule>();
        listDate13.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate13.Add(new FilmSchedule { Time = "12:30", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate13.Add(new FilmSchedule { Time = "15:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate13.Add(new FilmSchedule { Time = "17:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate13.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 13), listDate13);

        var listDate14 = new List<FilmSchedule>();
        listDate14.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate14.Add(new FilmSchedule { Time = "12:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate14.Add(new FilmSchedule { Time = "15:20", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate14.Add(new FilmSchedule { Time = "17:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate14.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 14), listDate14);

        var listDate15 = new List<FilmSchedule>();
        //   listDate15.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
        listDate15.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate15.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate15.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 15), listDate15);

        var listDate16 = new List<FilmSchedule>();
        //    listDate16.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12});
        listDate16.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate16.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate16.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 16), listDate16);

        var listDate17 = new List<FilmSchedule>();
        //  listDate17.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
        listDate17.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate17.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate17.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 17), listDate17);

        var listDate18 = new List<FilmSchedule>();
        //  listDate18.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
        listDate18.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate18.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate18.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 18), listDate18);


        var listDate19 = new List<FilmSchedule>();
        // listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 16 });
        //listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 12 });
        listDate19.Add(new FilmSchedule { Time = "12:10", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate19.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate19.Add(new FilmSchedule { Time = "16:55", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate19.Add(new FilmSchedule { Time = "19:35", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 19), listDate19);

        var listDate20 = new List<FilmSchedule>();
        listDate20.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate20.Add(new FilmSchedule { Time = "12:10", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate20.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate20.Add(new FilmSchedule { Time = "17:15", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate20.Add(new FilmSchedule { Time = "19:15", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 20), listDate20);

        var listDate21 = new List<FilmSchedule>();
        listDate21.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        listDate21.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate21.Add(new FilmSchedule { Time = "15:05", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate21.Add(new FilmSchedule { Time = "17:05", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate21.Add(new FilmSchedule { Time = "19:25", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 21), listDate21);

        return dicList;

    }
    
    private Dictionary<DateTime, List<FilmSchedule>> FilmSheduleHoChiMinh()
    {
        var listFilmNam = ListFilmName();

        List<Dictionary<DateTime, List<FilmSchedule>>> ddlfHn = new List<Dictionary<DateTime, List<FilmSchedule>>>();
        Dictionary<DateTime, List<FilmSchedule>> dicList = new Dictionary<DateTime, List<FilmSchedule>>();

        var listDate13 = new List<FilmSchedule>();
        // listDate13.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 16 });
        listDate13.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        // listDate13.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 16 });
        //listDate13.Add(new FilmSchedule { Time = "12:30", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 16 });
        // listDate13.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = 16 });
        dicList.Add(new DateTime(2017, 05, 13), listDate13);

        var listDate14 = new List<FilmSchedule>();
        listDate14.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate14.Add(new FilmSchedule { Time = "12:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate14.Add(new FilmSchedule { Time = "15:25", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate14.Add(new FilmSchedule { Time = "17:45", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate14.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 14), listDate14);

        var listDate15 = new List<FilmSchedule>();

        // listDate15.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
        listDate15.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate15.Add(new FilmSchedule { Time = "16:25", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate15.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 15), listDate15);

        var listDate16 = new List<FilmSchedule>();
        // listDate16.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 12 });
        listDate16.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate16.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate16.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 16), listDate16);

        var listDate17 = new List<FilmSchedule>();
        // listDate17.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 12 });
        listDate17.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate17.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate17.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 17), listDate17);

        var listDate18 = new List<FilmSchedule>();
        //  listDate18.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
        listDate18.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate18.Add(new FilmSchedule { Time = "16:35", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate18.Add(new FilmSchedule { Time = "19:05", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 18), listDate18);


        var listDate19 = new List<FilmSchedule>();
        //    listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 16 });
        //listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
        listDate19.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate19.Add(new FilmSchedule { Time = "15:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate19.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate19.Add(new FilmSchedule { Time = "20:20", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 19), listDate19);

        var listDate20 = new List<FilmSchedule>();
        listDate20.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate20.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate20.Add(new FilmSchedule { Time = "14:50", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate20.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate20.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 20), listDate20);

        var listDate21 = new List<FilmSchedule>();
        listDate21.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate21.Add(new FilmSchedule { Time = "12:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate21.Add(new FilmSchedule { Time = "15:20", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        listDate21.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate21.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 21), listDate21);

        var listDate22 = new List<FilmSchedule>();
        // listDate22.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 12 });
        listDate22.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate22.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate22.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 22), listDate22);

        var listDate23 = new List<FilmSchedule>();
        //  listDate23.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
        listDate23.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        listDate23.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate23.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 23), listDate23);

        var listDate24 = new List<FilmSchedule>();
        //  listDate24.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 12 });
        listDate24.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate24.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate24.Add(new FilmSchedule { Time = "18:50", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 24), listDate24);

        var listDate25 = new List<FilmSchedule>();
        // listDate25.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
        listDate25.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
        listDate25.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        listDate25.Add(new FilmSchedule { Time = "19:05", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 25), listDate25);

        var listDate26 = new List<FilmSchedule>();
        // listDate26.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
        listDate26.Add(new FilmSchedule { Time = "11:50", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate26.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate26.Add(new FilmSchedule { Time = "16:35", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate26.Add(new FilmSchedule { Time = "19:15", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 26), listDate26);


        var listDate27 = new List<FilmSchedule>();
        listDate27.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate27.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
        listDate27.Add(new FilmSchedule { Time = "14:50", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        listDate27.Add(new FilmSchedule { Time = "17:10", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        listDate27.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 27), listDate27);


        var listDate28 = new List<FilmSchedule>();
        listDate28.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        listDate28.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
        listDate28.Add(new FilmSchedule { Time = "15:05", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
        listDate28.Add(new FilmSchedule { Time = "17:05", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
        listDate28.Add(new FilmSchedule { Time = "19:25", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
        dicList.Add(new DateTime(2017, 05, 28), listDate28);

        return dicList;

    }

    

    private void Delete(int id)
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
      //  int id = Request.Form["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (id > 0)
        {
            
        Database db = new Database();
            DataTable dtDetail = db.Exec("DELETE FROM Registers WHERE RegisterId=" + id + "");
            message.Status = 1;
            message.Message = "Thông tin được gửi thành công";
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }

	public static bool IsDateTime(string txtDate)
	{
		DateTime tempDate;
		return DateTime.TryParse(txtDate, out tempDate) ? true : false;
	}
    public class FilmSchedule
    {
        public string Time { get; set; }
        public int FilmId { get; set; }
        public string FilmName { get; set; }
        public int Total { get; set; }
    }
}

