﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Dictionary<string, object> objEmail = new Dictionary<string, object>();
        Sendmail mail = new Sendmail();
        objEmail.Add("ToEmail", "dangvantanh@gmail.com");
        objEmail.Add("Subject", "Xác nhận tài khoản");
        objEmail.Add("fullName", "Tanh Dang");
        objEmail.Add("domain", ConfigurationManager.AppSettings["FRONT_END_URL"]);
        string Content = mail.SendGemSamSung(objEmail, Server.MapPath("~/email/congratulation.html"));
        Common.WriteEnd(Content);
    }
}