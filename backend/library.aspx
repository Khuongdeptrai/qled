﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS.master" AutoEventWireup="true" CodeFile="library.aspx.cs" Inherits="library" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <section class="content-header">
        <h1>
        DANH SÁCH THAM GIA (<%=_totalRows %>)
        <small></small>
        </h1>
    </section>    
    <section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">             
              
          </h3>
        <div class="box-tools">
            <form method="get">
            <div class="input-group">       
                 <div class="input-group group-date" style="width:100%">
                     <select name="city" class="form-control" style="width:110px; margin-right:5px;">
                         <option value="">Tỉnh/Thành Phố</option>
                         <<%=listCitys %>
                     </select>                                          
                    <input type="text" class="form-control" style="width:110px; margin-right:5px;" readonly="true" name="enddate" id="enddate" style=" width: 50%; " placeholder="Ngày kết thúc" value="<%=_startDate %>" />
                    <input type="text" class="form-control" style="width:110px;margin-right:5px;" readonly="true"  name="startdate" id="startdate" style=" width: 50%; " placeholder="Ngày bắt đầu" value="<%=_endDate %>" />
                    <input type="text" name="search" class="form-control" style="width: 150px;margin-right:5px;" placeholder="Search" value="<%=search %>"/>
                    <button class="btn btn-primary" type="submit">Lọc</button>                                               
                    <a href="<%=Common.BaseUrl %>library-add.aspx?action=add" class="btn btn-primary" >Tạo mới</a>                    
                </div>                       
            </div>            
            </form>
         </div>
        </div><!-- /.box-header -->
        <div class="box-body">
           <% if (dtList == null || dtList.Rows.Count <= 0)
           { %>
           <div class="alert alert-info">
              <strong>Không tìm thấy thông tin nào</strong>
            </div>
           <%} %>
          <table class="table table-bordered table-hover js-order-data">
            <thead>
              <tr>                 
                <th><input type="checkbox" id="selectall" class="checkbox" name="cbSelectAll" /></th>
				<th>STT</th>
                <th>Hình ảnh</th>                   
                <th>Tên</th>                                                               
                <th>City</th>                                  
                <th>Tuần</th>                                  
                <th style="text-align:center">Ngày tạo</th>	
                <th style="text-align:center">Trạng thái</th>	                  			
                <th style="text-align:center">Chức năng</th>				
              </tr>
            </thead>
            <tbody>
              <% foreach(System.Data.DataRow item in dtList.Rows ) { %>
              <tr> 			    
                <td><input type="checkbox" class="selectedId" name="chkitem[]" value="<%= item["id"] %>" /></td>
				<td><%=stt%></td>                
                <td><img width="100" src="<%= Common.UploadUrl()+"gallery/"+item["Thumbnail"].ToString() %>?width=301&height=198"></td>
                <td><%= item["Name"].ToString() %></td>                                
                <td><%= item["City"].ToString() %></td>    
                <td>Tuần <%= item["WeekId"].ToString() %></td>                              
                <td style="text-align:center"><%= ((DateTime)item["CreatedDate"]).ToString("dd-MM-yyyy :HH:m:s") %></td>
                <td style="text-align:center"><%= (Boolean)item["Status"]?"<a class=\"label label-success\">Hiển thị</a>":"<a class=\"label label-warning\">Đang ẩn</a>" %></td>                
                 <td style="text-align:center">
                     <a href="<%= Common.BaseUrl %>library-edit.aspx?id=<%= item["id"].ToString() %>&action=edit" class="btn bg-olive">Edit</a>
                     <a href="<%= Common.BaseUrl %>library.aspx?id=<%= item["id"].ToString() %>&action=delete" class="btn bg-olive js-delete-item">Xóa</a>
                 </td>				
              </tr>
              <% stt = stt+1;} %>
            </tbody>
          </table>          
        </div><!-- /.box-body -->
        <div class="box-footer clearfix pagination">
            <%=pagingHtml %>
        </div>
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->

</asp:Content>
