﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class library : System.Web.UI.Page
{
    public int _totalRows = 0;
    public string search = string.Empty;
    public string pagingHtml = string.Empty;
    public DataTable dtList = new DataTable();
    public string _startDate = string.Empty;
    public string _endDate = string.Empty;
    private System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();  
    public string listCitys = "";        
    public string queryString = "";
    public int stt = 0;
    public string dir_photo = Common.UploadDir() + "gallery/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }

        if (Request.HttpMethod == "POST")
        {
            string op = Request.Form["op"] != null ? Request.Form["op"] : "";
            string action = Request.QueryString["action"] != null ? Request.QueryString["action"] : "";
            if (op == "ajax")
            {
                if (action == "delete")
                {
                    Delete();
                }
            }
        }

        Database db = new Database();
        string cityTemp = Request.QueryString["city"] != null ? Common.CleanValue(Server.HtmlDecode(Request.QueryString["city"].Trim())) : "";
        string strQuery = "Id >0";

        string orderBy = " order by Id desc";
        int perPage = 50;
        int currentPage = Request.QueryString["page"] == null ? 1 : Common.ParseInt(Request.QueryString["page"].ToString());
        currentPage = currentPage <= 0 ? 1 : currentPage;
        db.dbCommand.Parameters.Clear();

        if (cityTemp != "")
        {
            queryString += "&city=" + Server.HtmlEncode(cityTemp); ;
            //strQuery += " and City COLLATE Latin1_General_CI_AI like N'%" + cityTemp + "%' COLLATE Latin1_General_CI_AI";
            strQuery += " and City COLLATE Latin1_General_CI_AI like N'%'+@City+'%' COLLATE Latin1_General_CI_AI";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@City", DbType.String, cityTemp));

        }
        search = Request.QueryString["search"] == null ? string.Empty : Common.CleanScripts(Server.HtmlDecode(Request.QueryString["search"].ToString()));
        search = Common.CleanValue(search);

        if (search != "")
        {
            
            strQuery += search != string.Empty ? " and ( Name COLLATE Latin1_General_CI_AI like N'%'+@searchKey+'%' COLLATE Latin1_General_CI_AI or City COLLATE Latin1_General_CI_AI like N'%'+@searchKey+'%' COLLATE Latin1_General_CI_AI) " : string.Empty;            
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@searchKey", DbType.String, search));
        }

        queryString += search != string.Empty ? "&search=" + Server.HtmlEncode(search) : "";

        _startDate = Request.QueryString["startdate"] == null ? string.Empty : Common.CleanValue(Request.QueryString["startdate"].ToString());
        _endDate = Request.QueryString["enddate"] == null ? string.Empty : Common.CleanValue(Request.QueryString["enddate"].ToString());
        if (_startDate != string.Empty && IsDateTime(_startDate))
        {
            queryString += "&startdate=" + _startDate;
            strQuery += " and cast(CreatedDate as Date) >= @StartDate";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@StartDate", DbType.String, _startDate));

        }
        else
        {
            _startDate = "";
        }
        if (_endDate != string.Empty && IsDateTime(_endDate))
        {
            queryString += "&enddate=" + _endDate;
            strQuery += " and cast(CreatedDate as Date) <= @EndDate";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@EndDate", DbType.String, _endDate));
        }
        else
        {
            _endDate = "";
        }

        string sqlSelect = string.Empty;
        DataTable dtTotalRows = db.Exec("select count(Id) as total from Feedback where " + strQuery);
        _totalRows = dtTotalRows.Rows.Count > 0 ? Common.ParseInt(dtTotalRows.Rows[0]["total"]) : 0;
        int _maxPage = (_totalRows % perPage) == 0 ? _totalRows / perPage : (_totalRows / perPage) + 1;
        int _startPage = (currentPage - 1) * perPage + 1;
        int _endPage = _startPage + perPage - 1;
        stt = _startPage;

        sqlSelect = @"SELECT  * 
                                FROM 
                                    (SELECT  ROW_NUMBER() OVER(" + orderBy + @") as row, y.*
                                        FROM 
	                                    ( SELECT TOP " + _totalRows + " * from Feedback where " + strQuery + orderBy + @"                           
	                                    ) y
                                    )x
                                WHERE x.row BETWEEN " + _startPage.ToString() + " AND " + _endPage.ToString();

        dtList = db.Exec(sqlSelect);

        if (_maxPage > 1)
        {
            PagingCMS paging = new PagingCMS();
            paging.Last = "&raquo;";
            paging.First = "&raquo;";
            paging.Next = "&rsaquo;";
            paging.Prev = "&lsaquo;";
            paging.EventClick = "";
            pagingHtml = paging.multi(_totalRows, perPage, currentPage, Common.Webroot + "library.aspx?page=%page%" + queryString);
        }

        foreach (string city in Common.ListCity)
        {
            string selected = "";
            if (cityTemp.Equals(city, StringComparison.InvariantCultureIgnoreCase))
            {
                selected = "selected=\"selected\"";
            }
            listCitys += "<option " + selected + " value=\"" + city + "\">" + city + "</option>";
        }
    }
      
    private void Delete()
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        int id = Request.QueryString["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (id > 0)
        {
            Database db = new Database();
            DataTable dtDetail = db.Exec("DELETE FROM Feedback WHERE Id=" + id + "");
            if (dtDetail!=null && dtDetail.Rows.Count > 0)
            {
                DeleteFiles(dtDetail.Rows[0]["Thumbnail"].ToString());                
            }
            message.Status = 1;
            message.Message = "Thông tin được gửi thành công";
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }
    protected void DeleteFiles(string filename)
    {
        if (filename != "")
        {
            if (System.IO.File.Exists(dir_photo + "" + filename))
                System.IO.File.Delete(dir_photo + "" + filename);
            if (System.IO.File.Exists(dir_photo + "thumb_" + filename))
                System.IO.File.Delete(dir_photo + "thumb_" + filename);
        }
    }
    public static bool IsDateTime(string txtDate)
    {
        DateTime tempDate;
        return DateTime.TryParse(txtDate, out tempDate) ? true : false;
    }
}