﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="cms_Login" %>
<!DOCTYPE html>
<html>
  <head>
    <base href="<%= Common.BaseUrl %>" />
    <meta charset="UTF-8">
    <title>SAMSUNG | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="Assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="Assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="Assets/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="Assets/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

  </head>
  <body class="login-page">
    <div class="login-box">

      <div class="login-logo">
        <a href="javascript:void(0)"><b>SAMSUNG</b> CMS</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        
            <p class="login-box-msg">Sign in to start your session</p>
            <form method="post">
              <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Email" name="email" />
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                 <input type="password" class="form-control" placeholder="Password" name="password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                  <img src="<%=Common.Webroot %>captcha.aspx?v=<%=DateTime.Now.Ticks %>" />
                 <input type="text" class="form-control" placeholder="Captcha" name="captcha"/>                
              </div>
              <div class="row">
                <div class="col-xs-4">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div><!-- /.col -->
              </div>
            </form>
        
      </div><!-- /.login-box-body -->

    <div class="css-box-message" style=" margin-top: 20px; ">
        <% if (_message != string.Empty) { %>
        <div class="alert alert-danger alert-dismissable">
	      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <%=_message %>
        </div>
        <% } %>
    </div>
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.3 -->
    <script src="Assets/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="Assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="Assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
  </body>
</html>