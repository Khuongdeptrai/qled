﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sendEmail : System.Web.UI.Page
{
    private System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }
        if (Request.HttpMethod == "POST")
        {
            string op = Request.Form["op"] != null ? Request.Form["op"] : "";
            string action = Request.QueryString["action"] != null ? Request.QueryString["action"] : "";
            if (op == "ajax")
            {
                if (action == "sendMail")
                {
                    SendEmailToUser();
                }
                if (action == "sendMailTest")
                {
                    SendEmailToUserTest();
                }
            }
        }

    }
    private void SendEmailToUserTest()
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        int id = Request.Form["id"] != null ? Common.ParseInt(Request.Form["id"]) : 0;
        int type = Request.QueryString["type"] != null ? Common.ParseInt(Request.QueryString["type"]) : 0;
        string subject = Request.Form["subject"] != null ? Common.CleanValue(Request.Form["subject"]) : "";
        string emailTest = Request.Form["email"] != null ? Common.CleanValue(Request.Form["email"]) : "";
        if (subject == string.Empty)
        {
            message.Message = "Vui lòng nhập tiêu đề email";
            Response.Write(js.Serialize(message));
            Response.End();
        }
        if (!CheckEmail(emailTest))
        {
            message.Message = "Địa chỉ email không hợp lệ";
            Response.Write(js.Serialize(message));
            Response.End();
        }
        if (type > 0)
        {
            string stringTemplate = "";
            if (type == 1)
            {
                stringTemplate = Server.MapPath("~/email/congratulation.html");
            }
            else if (type == 2)
            {
                stringTemplate = Server.MapPath("~/email/offer_1package_buytv.html");
            }
            else if (type == 3 && id >= 1 && id <= 3)
            {
                
                stringTemplate = Server.MapPath("~/email/introduce_samsungtv_w" + id + ".html");
            }
            message.Content = "";
            if (stringTemplate != "")
            {  
                Dictionary<string, object> objEmail = new Dictionary<string, object>();
                Sendmail mail = new Sendmail();
                objEmail.Add("ToEmail", emailTest);
                objEmail.Add("Subject", subject);                
                objEmail.Add("domain", ConfigurationManager.AppSettings["FRONT_END_URL"]);                    
                message.Content = mail.SendGemSamSung(objEmail, stringTemplate).Replace("<!--250-->", "").Trim();
                message.Status = 1;
                message.Message = "Thông tin được gửi thành công";                
            }
            else
            {
                message.Message = "Không tồn tại email template này";
            }
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }

    private void SendEmailToUser()
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        int id = Request.Form["id"] != null ? Common.ParseInt(Request.Form["id"]) : 0;
        int type = Request.QueryString["type"] != null ? Common.ParseInt(Request.QueryString["type"]) : 0;
        string subject = Request.Form["subject"] != null ? Common.CleanValue(Request.Form["subject"]) : "";
        if (subject == string.Empty)
        {
            message.Message = "Vui lòng nhập tiêu đề email";
            Response.Write(js.Serialize(message));
            Response.End();
        }
        if (type>0)
        {
            Database db = new Database();
            string sql = "SELECT * FROM Register Where isnull(TypeId,0) <> 1";
            if (type == 1)
            {
                sql = "SELECT * FROM Register Where TypeId = 1";
            }            
            DataTable dtList = db.Exec(sql);
            if (dtList != null && dtList.Rows.Count > 0)
            {
                string stringTemplate = "";
                if (type == 1)
                {
                    stringTemplate = Server.MapPath("~/email/congratulation.html");
                }
                else if (type == 2)
                {
                    stringTemplate = Server.MapPath("~/email/offer_1package_buytv.html");
                }
                else if (type == 3)
                {
                    stringTemplate = Server.MapPath("~/email/introduce_samsungtv_w"+id+".html");
                }
                message.Content = "";
                if (stringTemplate != "")
                {
                    foreach (DataRow row in dtList.Rows)
                    {
                        string email = row["Email"].ToString();                        
                        Dictionary<string, object> objEmail = new Dictionary<string, object>();
                        Sendmail mail = new Sendmail();
                        objEmail.Add("ToEmail", email);
                        objEmail.Add("Subject", subject);
                        objEmail.Add("fullName", row["Name"]);
                        objEmail.Add("domain", ConfigurationManager.AppSettings["FRONT_END_URL"]);                        
                        db.Exec("UPDATE Register SET Sent=1, UpdateTime={FN NOW()}  where id=" + row["Id"]);
                        message.Content = mail.SendGemSamSung(objEmail, stringTemplate).Replace("<!--250-->", "").Trim();
                        message.Status = 1;
                        message.Message = "Thông tin được gửi thành công";
                    }
                }
                

            }
            else
            {
                message.Message = "Không tìm thấy thông tin email nào";
            }
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }
    public bool CheckEmail(object email)
    {
        if (email == null || string.IsNullOrWhiteSpace(email.ToString())) return false;
        string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        Regex check = new Regex(strRegex, RegexOptions.IgnorePatternWhitespace);
        return check.IsMatch(email.ToString().Trim());
    }
}