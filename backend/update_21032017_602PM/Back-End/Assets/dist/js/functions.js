﻿

$(document).ready(function () {
    XPRESS.init();
    /*
    $('.cbSelectAll').click(function () {
        $("input[@name='chkitem[]']:not([disabled='disabled'])").attr('checked', $(this).attr("checked"));
    });
    */
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', $(this).is(":checked"));
    });

});


var XPRESS = {
    tempUrl: '',
	init : function() {
	//	$(".js-order-data").rowSorter({
    //    onDrop: function(tbody, row, new_index, old_index) {
    //        console.log(row + ". row moved to " + new_index);
    //    }
	    //});
	    $('form').submit(function (e) {
	        var emptyinputs = $(this).find('input').filter(function () {
	            return !$.trim(this.value).length;  // get all empty fields
	        }).prop('disabled', true);
	        var emptyselect = $(this).find('select').filter(function () {
	            return !$.trim(this.value).length;  // get all empty fields
	        }).prop('disabled', true);
	    });
        $('.js-btnSendEdm').on('click', XPRESS.sendEmail);
        $('.js-btnSendEdmTest').on('click', XPRESS.sendEmailTest);

	    $('.js-delete-item').on('click', XPRESS.confirmDelete);
	    $('.js-list-week').on('change', XPRESS.changeWeek);
	    $('.js-list-cat').on('change', XPRESS.changeCat);

	    $('#startdate').datepicker({
	        format: 'yyyy-mm-dd'
	    });
	    $('#enddate').datepicker({
	        format: 'yyyy-mm-dd'
        });
        $('#bookdatetime').daterangepicker({
            timePicker: true,
            autoclose: true,
            autoUpdateInput: true, 
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
           
        });

        $('.js-btn-filter').on('click', XPRESS.filterDate);
        $('.js-selectAction').on('click', XPRESS.showModelPopup);
        $('.js-btnUpdateChoMuon').on('click', XPRESS.assingUserType);
        $('.js-btnGalleryRemove').on('click', XPRESS.galleryRemoveItem);
        $('.js-btnViewEdm').on("click", function (evt) {
            evt.preventDefault();            
            var id = $('.js-edmId').val();
            window.location.href = $(this).attr("href") + "&id=" + id;
        });
	},
	confirmDelete: function (e) {
	    if (confirm("Are you sure delete this item?")) {
	        XPRESS.deleteItem(this);
	    } else {
	        return false;
        }
        return false;
	},
	changeWeek: function (e) {
	    location.href = $(this).parents('form').attr('action') + "&week=" + $(this).val() + "&catid=" + $('.js-list-cat').val();
	},
	changeCat: function (e) {
	    location.href = $(this).parents('form').attr('action') + "&catid=" + $(this).val() + "&week=" + $('.js-list-week').val();
	},
	filterDate: function (e) {
	    e.preventDefault();
	    location.href = $(this).parents('form').attr('action') + "&startdate=" + $("#startdate").val() + "&enddate=" + $("#enddate").val() + "&catid=" + $('.js-list-cat').val();
    },    	

	sendEmail: function () {
	    var buttonSubmit = this;	    
	    var urlRequest = $(buttonSubmit).attr('href');
        var subject = $(buttonSubmit).parent().parent().find('input[name="subject"]').val();
        if (subject == "") {
            Message.alert("Vui lòng nhập tiêu đề email");
            return false;
        }        
	    $(buttonSubmit).text("Đang gửi...");
	    $(buttonSubmit).prop('disabled', true);	    
	    serverCall.callFunc(urlRequest, function (response) {
	       // Message.alert(response.Message);
	        Message.alert(response.Content);
            if (response.Status == "1" && response.Content=="Ok") {
	            $(buttonSubmit).text("Gửi thành công");
	        }
            else {               
	            $(buttonSubmit).text("Lỗi! Thử lại");
	            $(buttonSubmit).prop('disabled', false);
	        }

	    }, "POST", "json"
            , {
                "id": $('.js-edmId').val(),
                "subject": subject

            }
        );
	    return false;
    },
    sendEmailTest: function () {
        var buttonSubmit = this;
        var urlRequest = $(buttonSubmit).attr('href');
        var subject = $(buttonSubmit).parent().parent().find('input[name="subject"]').val();
        var email = $(buttonSubmit).parent().find('input[name="email"]').val();
        if (subject == "") {
            Message.alert("Vui lòng nhập tiêu đề email");
            return false;
        }
        if (!validateTool.isEmail(email)) {
            Message.alert("Địa chỉ email không hợp lệ");
            return false;
        }
        $(buttonSubmit).text("Đang gửi...");
        $(buttonSubmit).prop('disabled', true);
        serverCall.callFunc(urlRequest, function (response) {
            Message.alert(response.Content);
            if (response.Status == "1" && response.Content=="Ok") {
                $(buttonSubmit).text("Gửi thành công");
            }
            else {                
                $(buttonSubmit).text("Lỗi! Thử lại");
                $(buttonSubmit).prop('disabled', false);
            }

        }, "POST", "json"
            , {
                "id": $('.js-edmId').val(),
                "subject": subject,
                "email": email

            }
        );
        return false;
    },

	deleteItem: function (obj) {
	    var buttonSubmit = obj;
	    var urlRequest = $(buttonSubmit).attr('href');

	    $(buttonSubmit).text("Đang gửi...");
	    $(buttonSubmit).prop('disabled', true);
	    serverCall.callFunc(urlRequest, function (response) {
	        // Message.alert(response.Message);
	        if (response.Status == "1") {
	            $(buttonSubmit).parent().parent().remove();
	        }
	        else {
	            $(buttonSubmit).text("Lỗi! Thử lại");
	            $(buttonSubmit).prop('disabled', false);
	        }

	    }, "POST", "json"
            , {
                "id": 01,
            }
        );
	    return false;
    },
    showModelPopup: function (e) {
        e.preventDefault();
        XPRESS.tempUrl = $(this);
        $('#myModal').modal('show'); 
    },
    assingUserType: function (e) {
        e.preventDefault();
        if (XPRESS.tempUrl == null) {

            return false;
        }
        var startDate = $("#bookdatetime").data('daterangepicker').startDate.format('YYYY-MM-DD HH:mm:ss');
        var endDate = $("#bookdatetime").data('daterangepicker').endDate.format('YYYY-MM-DD HH:mm:ss');
        if ($('#bookdatetime').val() == '')
        {
            $('#bookdatetime').focus();
            return false
        }
        var buttonSubmit = XPRESS.tempUrl;
        var urlRequest = $(buttonSubmit).attr('data-href');        
        serverCall.callFunc(urlRequest, function (response) {
            // Message.alert(response.Message);
            if (response.Status == "1") {                
                $('#myModal').modal('hide'); 
                $(response.Content).html('Đã cho mượn');
                $('#bookdatetime').val('');
            }
            Message.alert(response.Message);

        }, "POST", "json"
            , {
                "typeId": $(buttonSubmit).val(),
                "startDate": startDate,
                "endDate": endDate
            }
        );
        return false;
    },
    galleryRemoveItem: function () {
        var item = $(this).attr('data-value');
        var old = $('.js-remove-items').val() + ',' + item;
        $('.js-remove-items').val(old);
        $(this).parent().parent().remove();
    }
}


Dropzone.options.dropzoneForm = {
    init: function () {
        this.on("complete", function (data) {
            alert('adad');
        });
    }
};



$('.js-upload-image').change(function (e) {
    e.preventDefault();
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Bạn vui lòng chỉ tải ảnh với định dạng .jpeg .jpg  .png  .gif");
        $(this).val('');
        return false;
    }
});


var serverCall = {
    requestUrl: "",
    isProcessing: false,
    callFunc: function (url, func, type, dataType, post) {
        if (!serverCall.isProcessing) {
            serverCall.isProcessing = true;
            if (type === undefined) {
                type = "GET";
            }
            if (post === undefined) {
                post = {};
            }
            if (dataType === undefined) {
                dataType = "text";
            }
            post.op = 'ajax';
            post.language = $("#siteLanguage").val();
            $.ajax({
                type: type,
                url: serverCall.requestUrl + url,
                data: post,
                cache: false,
                dataType: dataType,
                success: function (response) {
                    serverCall.isProcessing = false;
                    func(response);
                },
                error: function (response) {
                    serverCall.isProcessing = false;
                    console.log(response);
                }
            });
        } else {
            Message.alert("Please waiting...");
        }
    }
};

var Message = {
    error: function (msg, callBack) {
        alert(msg);
    },
    alert: function (msg, callBack) {
        alert(msg);
    },
    warning: function (msg, callBack) {
        alert(msg);
    }
};
var validateTool = {
    isUndefined: function (strObj) {
        return typeof strObj == 'undefined' ? true : false;
    },
    isEmail: function (sEmail) {
        sEmail = sEmail.trim();
        if (sEmail.search(this.regExForEmail) != -1)
            return true;
        return false;
    },

}
