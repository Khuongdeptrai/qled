﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS.master" AutoEventWireup="true" CodeFile="sendEmail.aspx.cs" Inherits="sendEmail" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <section class="content-header">
        <h1>
        Gửi EDM
        <small></small>
        </h1>
    </section>    
    <section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">             
              
          </h3>
        <div class="box-tools">
            <form method="get">
            <div class="input-group">                                             
            </div>            
            </form>
         </div>
        </div><!-- /.box-header -->
        <div class="box-body">

          <table class="table table-bordered table-hover js-order-data">
            <thead>
              <tr>                                 
				<th>STT</th>
                <th>Email</th>          
                <th>Tuần</th>                                                                     
                <th>Tiêu đề Email</th>                  
                <th>Chức năng</th>                                            
                <th>Test Email</th>
              </tr>
            </thead>
            <tbody>               
              <tr>                                 
				<td>1</td>
                <td>Cho mượn TV</td> 
                <td>&nbsp;</td>                
                <td><input type="text" class="form-control" name="subject" value="Chương trình dùng thật TV Samsung 30 Ngày" /></td>                  
                <td>
                    <a href="sendEmail.aspx?type=1&action=sendMail" class="btn btn-primary js-btnSendEdm">Gửi Email</a>
                    <a href="previewEmail.aspx?type=1" class="btn btn-primary js-btnViewEdm">Xem Email Template</a>                    
                </td>
                <td>
                    <input type="email" class="form-control-static" name="email"  />
                    <a href="sendEmail.aspx?type=1&action=sendMailTest" class="btn btn-primary js-btnSendEdmTest">Test</a>
                </td>                                          
              </tr>  
              <tr>                                 
				<td>2</td>
                  <td>Offer</td>       
                <td>&nbsp;</td>                
                <td><input type="text" class="form-control" name="subject" value="Chương trình dùng thật TV Samsung 30 Ngày" /></td>  
                <td>
                    <a href="sendEmail.aspx?type=2&action=sendMail" class="btn btn-primary js-btnSendEdm">Gửi Email</a>
                    <a href="previewEmail.aspx?type=2" class="btn btn-primary js-btnViewEdm">Xem Email Template</a>                    
                </td>      
                  <td>
                    <input type="email" class="form-control-static" name="email"  />
                    <a href="sendEmail.aspx?type=2&action=sendMailTest" class="btn btn-primary js-btnSendEdmTest">Test</a>
                </td>                                     
              </tr>   
              <tr>                                 
				<td>2</td>
                <td>Giới thiệu tính năng</td>
                <td>
                    <select class="form-control js-edmId">
                        <option value="1">Tuần 1</option>
                        <option value="2">Tuần 2</option>
                        <option value="3">Tuần 3</option>                      
                    </select>
                </td>    
                <td><input type="text" class="form-control" name="subject" value="Chương trình dùng thật TV Samsung 30 Ngày" /></td>                    
                <td>
                    <a href="sendEmail.aspx?type=3&action=sendMail" class="btn btn-primary js-btnSendEdm">Gửi Email</a>
                    <a href="previewEmail.aspx?type=3" class="btn btn-primary js-btnViewEdm">Xem Email Template</a>                    
                </td> 
                  <td>
                    <input type="email" class="form-control-static" name="email"  />
                    <a href="sendEmail.aspx?type=3&action=sendMailTest" class="btn btn-primary js-btnSendEdmTest">Test</a>
                </td>                                          
              </tr>                 
            </tbody>
          </table>

        </div><!-- /.box-body -->        
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->

</asp:Content>
