﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cms_CMS : System.Web.UI.MasterPage
{
    public string _titlePage;
    public UserLogin _userLogin = new UserLogin();
    protected void Page_Load(object sender, EventArgs e)
    {
       // if (!Page.IsPostBack)
        //{
            if (Session["CMSUSER"] == null){
				Response.Redirect(Common.Webroot+"login.aspx");
				Response.End();
			}
            _userLogin = Session["CMSUSER"] != null ? (UserLogin)Session["CMSUSER"] : _userLogin;
       // }


        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }
}
