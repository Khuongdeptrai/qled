﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class qsuitereview : System.Web.UI.Page
{
    public DataTable listReview = new DataTable();

    public int _totalRows = 0;
    public string search = string.Empty;
    public string pagingHtml = string.Empty;
    public DataTable dtMember = new DataTable();
    public string _startDate = string.Empty;
    public string _endDate = string.Empty;
    private System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
    List<string> Citys = new List<string> { "Hà Nội", "TP HCM", "An Giang", "Bà Rịa - Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước", "Bình Thuận", "Cà Mau", "Cao Bằng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương", "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái", "Phú Yên", "Cần Thơ", "Đà Nẵng", "Hải Phòng" };
    public string listCitys = "";
    public string listModels = "";
    public int sent = 0;
    public string queryString = "";
    public int stt = 0;
    public int confirmed = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }

        if (Request.HttpMethod == "POST")
        {

            string op = Request.Form["op"] != null ? Request.Form["op"] : "";
            string action = Request.Form["action"] != null ? Request.Form["action"] : "";
            if (action == "updatestatus")
            {
                Messages message = new Messages();
                message.Status = 0;
                message.Message = "The information is invalid";
                int id = Request.Form["id"] != null ? Common.ParseInt(Request.Form["id"]) : 0;
                int status = Request.Form["status"] != null ? Common.ParseInt(Request.Form["status"]) : 0;
                if (id > 0)
                {

                    Database db1 = new Database();
                    DataTable dtDetail = db1.Exec("UPDATE ReviewsQsuite set Status = " + status + " where ReviewId = " + id);
                    message.Status = 1;
                    message.Message = "Cập nhật thành công";
                }
                Response.Write(js.Serialize(message));
                Response.End();
            }

            if (action == "delete")
            {
                Messages message = new Messages();
                message.Status = 0;
                message.Message = "The information is invalid";
                int id = Request.Form["id"] != null ? Common.ParseInt(Request.Form["id"]) : 0;
               // int status = Request.Form["status"] != null ? Common.ParseInt(Request.Form["status"]) : 0;
                if (id > 0)
                {

                    Database db1 = new Database();
                    DataTable dtDetail = db1.Exec("delete ReviewsQsuite where ReviewId = " + id);
                    message.Status = 1;
                    message.Message = "Cập nhật thành công";
                }
                Response.Write(js.Serialize(message));
                Response.End();
            }

        }

        Database db = new Database();
        string cinemaId = Request.QueryString["cinemaid"] != null ? Common.CleanValue(Server.HtmlDecode(Request.QueryString["cinemaid"].Trim())) : "";
        string filmId = Request.QueryString["filmid"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["filmid"]).Trim()) : "";
        string dateView = Request.QueryString["dateview"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["dateview"]).Trim()) : "";
        string timeView = Request.QueryString["timeview"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["timeview"]).Trim()) : "";
        confirmed = Request.QueryString["confirmed"] != null ? Common.ParseInt(Request.QueryString["confirmed"]) : 0;
        string strQuery = "";
        if (Request.QueryString["confirmed"] == null)
        {
            strQuery = " Status in (0,1,2) ";
        }
        else
        {
            strQuery = "ISNULL(Status,0)=" + confirmed;
        }
        queryString += "&confirmed=" + confirmed;
        string orderBy = " order by ReviewId desc";
        int perPage = 50;
        int currentPage = Request.QueryString["page"] == null ? 1 : Common.ParseInt(Request.QueryString["page"].ToString());
        currentPage = currentPage <= 0 ? 1 : currentPage;
        db.dbCommand.Parameters.Clear();

        search = Request.QueryString["search"] == null ? string.Empty : Common.CleanScripts(Server.HtmlDecode(Request.QueryString["search"].ToString()));
        search = Common.CleanValue(search);

        if (search != "")
        {
            if (search.All(char.IsDigit))
            {

            }
            else
            {

            }

        }


        string sqlSelect = string.Empty;
        DataTable dtTotalRows = db.Exec("select count(ReviewId) as total from ReviewsQsuite where " + strQuery);
        _totalRows = dtTotalRows.Rows.Count > 0 ? Common.ParseInt(dtTotalRows.Rows[0]["total"]) : 0;
        int _maxPage = (_totalRows % perPage) == 0 ? _totalRows / perPage : (_totalRows / perPage) + 1;
        int _startPage = (currentPage - 1) * perPage + 1;
        int _endPage = _startPage + perPage - 1;
        stt = _startPage;

        string _action = Request.QueryString["action"] == null ? string.Empty : Request.QueryString["action"].ToString();
        if (_action == "export")
        {
            sqlSelect = "SELECT  * FROM Reviews v where " + strQuery + orderBy;
            ExportData expt = new ExportData();
            expt.dtList = dtMember;
            expt.ExportGridView(DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".xls", "xls", "Danh sách đăng ký");
            Response.End();
        }

        sqlSelect = @"SELECT  * 
                                FROM 
                                    (SELECT  ROW_NUMBER() OVER(" + orderBy + @") as row, y.*
                                        FROM 
	                                    ( SELECT TOP " + _totalRows + " * from ReviewsQsuite where " + strQuery + orderBy + @"                           
	                                    ) y
                                    )x
                                WHERE x.row BETWEEN " + _startPage.ToString() + " AND " + _endPage.ToString();

        dtMember = db.Exec(sqlSelect);

        if (_maxPage > 1)
        {
            PagingCMS paging = new PagingCMS();
            paging.Last = "&raquo;";
            paging.First = "&raquo;";
            paging.Next = "&rsaquo;";
            paging.Prev = "&lsaquo;";
            paging.EventClick = "";
            pagingHtml = paging.multi(_totalRows, perPage, currentPage, Common.Webroot + "addreview.aspx?page=%page%" + queryString);
        }
        
    }
}