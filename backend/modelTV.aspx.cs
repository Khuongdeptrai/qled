﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class modelTV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }
        if (!Page.IsPostBack)
        {
            LoadList();
        }
    }
    protected void LoadList()
    {
        Database db = new Database();
        DataTable dtList = db.Exec("SELECT * FROM Model_TV");
        rptLists.DataSource = dtList;
        rptLists.DataBind();
    }
}