﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class unblock : System.Web.UI.Page
{
    private string _ipAddress = Common.GetIPAddress();
    protected void Page_Load(object sender, EventArgs e)
    {
        string _pass = Request.QueryString["pass"] != null ? Common.CleanValue(Request.QueryString["pass"].ToString()) : string.Empty;
        if (_pass == "SamsungUnblock124!")
        {
            Database db = new Database();
            db.Exec(" update [users] set status=1");
            Session["countLogin"] = null;
            Common.WriteEnd(1);
        }
        Common.WriteEnd(0);
    }
}