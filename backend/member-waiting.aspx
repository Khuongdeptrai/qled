﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS.master" AutoEventWireup="true" CodeFile="member-waiting.aspx.cs" Inherits="cms_member_waiting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<%
    string cinemaid = Request.QueryString["cinemaid"]!=null ? Request.QueryString["cinemaid"].ToString() : "";
    string filmId = Request.QueryString["filmId"] != null ? Request.QueryString["filmId"].ToString(): "";
    string dateView = Request.QueryString["dateview"] != null ? Request.QueryString["dateview"].ToString() : "";
    string timeview = Request.QueryString["timeview"] != null ? HttpUtility.HtmlDecode(Request.QueryString["timeview"].ToString()) : "";
 %>
    <section class="content-header">
        <h1>
        DANH SÁCH ĐĂNG KÝ  (<%=_totalRows %>)
        <small></small>
        </h1>
    </section>    
    <script>
        var _dateView = '<%= dateView %>';
        var _timeview = '<%= timeview %>';
    </script>
    <section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">             
              
          </h3>
        <div class="box-tools">
            <form method="get">
            <div class="input-group">       
                 <div class="input-group group-date" style="width:100%">
                     <select name="cinemaid" class="form-control" style="width:110px; margin-right:5px;">
                         <option value="">Rạp chiếu phim </option>
                         <option <%= cinemaid == "1" ? "selected=selected" : "" %> value="1">AEON MALL HANOI</option>
                         <option <%= cinemaid == "2" ? "selected=selected" : "" %> value="2">CRESENT MALL HCM</option>
                     </select>                     
                     <select name="filmid" class="form-control" style="width:250px; margin-right:5px;">
							<option  value="">Tên phim</option>
							<option <%= filmId == "1" ? "selected=selected" : "" %> value="1">Dạ cổ hoài lang</option>
                            <option <%= filmId == "2" ? "selected=selected" : "" %> value="2">Kungfu Yoga</option>
                            <option <%= filmId == "3" ? "selected=selected" : "" %> value="3">Now you see me 2</option>
                            <option <%= filmId == "4" ? "selected=selected" : "" %> value="4">49 ngày 2</option>
                            <option <%= filmId == "5" ? "selected=selected" : "" %> value="5">Ender's game</option>
                            <option <%= filmId == "6" ? "selected=selected" : "" %> value="6">The Expendables 3</option>
                     </select>
                    <input type="hidden" value="<%=confirmed %>" name="confirmed" />
                    <!-- <input type="text" class="form-control" style="width:110px; margin-right:5px;" readonly="true" name="dateview" id="startdate" style=" width: 50%; " placeholder="Ngày xem phim" value="<%= dateView %>" /> -->
                     <select name="dateview" class="form-control" style="width:250px; margin-right:5px;">
                         <option value="">Chọn 1 ngày</option>
					      <% for (int i = 6; i <= 28; i++)
                            {
                            string k = i < 10 ? "0" : "";
                            string d = "2017-05-" + k + i;
                         %>
                         <option <%= dateView == d ? "selected=selected" : "" %> value="2017-05-<%= k+i %>"><%= k+i %>-05-2017</option>
                         <%} %>
                     </select>
                      <select name="timeview" class="form-control" style="width:150px; margin-right:5px;">
                         <option value="">Chọn 1 giờ</option>
							
                     </select>

                    <input type="text" name="search" class="form-control" style="width: 150px;margin-right:5px;" placeholder="Search" value="<%= search%>"/>
                    <button class="btn btn-primary" type="submit">Lọc</button>     
                     <a href="<%=Common.BaseUrl %>member-waiting.aspx?action=export<%=queryString %>" class="btn btn-primary" >Xuất Excel</a>                     
                     <a href="<%= Common.Webroot %>member-waiting.aspx?confirmed=1" class="btn btn-primary" >Reset Form</a>
                </div>                       
            </div>            
            </form>
         </div>
        </div><!-- /.box-header -->
        <div class="box-body">

          <table class="table table-bordered table-hover js-order-data">
            <thead>
              <tr>                 
                <th><input type="checkbox" id="selectall" class="checkbox" name="cbSelectAll" /></th>
				<th>STT</th>
                <th>Tên</th>                               
                <th>Điện thoại</th>
                <th>Email</th>
                <th>Rạp chiếu</th>
                <th>Tên phim</th>
                <th>Ngày xem phim</th>
                <th>Giờ xem phim</th>
                  <th>Số lượng vé</th>
                  <th>Mã Code</th>
                  <th>TV Hiện tại</th>
                <th>Ngày tạo</th>
				<th>Trạng thái</th>  
                <th>Xóa đăng kí</th>
              </tr>
            </thead>
            <tbody>
              <% foreach(System.Data.DataRow item in dtMember.Rows ) { %>
              <tr> 			    
                <td><input type="checkbox" class="selectedId" name="chkitem[]" value="<%= item["RegisterId"] %>" /></td>
				<td><%=stt%></td>
                <td><%= item["Name"].ToString() %></td>                
                 <td><%= item["Phone"].ToString() %></td>
                <td><%= item["Email"].ToString() %></td>                                
                <td><%= item["CinameId"].ToString() %></td>
                <td><%= item["FilmId"].ToString() %></td>
                  <%
                     string [] date = item["DateView"].ToString().Split('-');
                   %>
                  <td><%= date[2]+"-"+date[1]+"-"+date[0] %></td>
                <td><%= item["TimeView"].ToString() %></td>
                  <td><%= item["NumberPeople"] %></td>
                  <td><%= item["RegisterCode"] %></td>
                  <td><%= item["TvBrandOld"] %></td>
                <td><%= ((DateTime)item["Created"]).ToString("dd-MM-yyyy :HH:m:s") %></td>
				
               
                <td>                    
                    <% if (item["Success"].ToString() == "1")
                        { %>
                        <span class="js-item-<%=item["RegisterId"]%>"><a class="btn btn-primary btn-sm js-selectAction" href="#"  data-href="<%= Common.BaseUrl %>member-waiting.aspx?id=<%= item["RegisterId"].ToString() %>&action=assignType">Đăng kí thành công</a></span>
                    <%}
                    else
                    { %>
                     <span>Đăng ký thất bại</span>
                    <%} %>
                   
                </td>
                <td><a href="javascript:;" class="luc-delete" data-id="<%= item["RegisterId"] %>">Xóa đăng ký</a></td>
              </tr>
              <% stt = stt+1;} %>
            </tbody>
          </table>

        </div><!-- /.box-body -->
        <div class="box-footer clearfix pagination">
            <%=pagingHtml %>
        </div>
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Thiết lập thông tin cho mượn</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
                <label>Ngày cho mượn:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="bookdatetime" readonly="readonly">
                </div>
                <!-- /.input group -->
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="button" class="btn btn-primary js-btnUpdateChoMuon">Lưu lại</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>

    var hn = '{"2017-05-06":[{"Time":"19:30","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":10}],"2017-05-07":[{"Time":"10:00","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"13:00","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"15:45","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40},{"Time":"17:30","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"20:00","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-08":[{"Time":"14:30","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40},{"Time":"16:25","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"19:00","FilmId":6,"FilmName":"The Expendables 3","Total":40}],"2017-05-09":[{"Time":"14:00","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"16:30","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"19:20","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-10":[{"Time":"14:40","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"17:15","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40},{"Time":"19:05","FilmId":5,"FilmName":"Enders Game","Total":40}],"2017-05-11":[{"Time":"14:30","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"17:05","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"19:35","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-12":[{"Time":"12:00","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"14:40","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"17:10","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"20:00","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-13":[{"Time":"10:00","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"12:30","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"15:00","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"17:40","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40},{"Time":"19:40","FilmId":2,"FilmName":"Kunfu Yoga","Total":40}],"2017-05-14":[{"Time":"10:00","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"12:40","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"15:20","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"17:40","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40},{"Time":"19:40","FilmId":2,"FilmName":"Kunfu Yoga","Total":40}],"2017-05-15":[{"Time":"14:00","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"16:30","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"19:00","FilmId":2,"FilmName":"Kunfu Yoga","Total":40}],"2017-05-16":[{"Time":"14:00","FilmId":2,"FilmName":"Kunfu Yoga","Total":40},{"Time":"16:30","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"19:20","FilmId":2,"FilmName":"Kunfu Yoga","Total":40}],"2017-05-17":[{"Time":"14:30","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"17:00","FilmId":2,"FilmName":"Kunfu Yoga","Total":40},{"Time":"19:00","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-18":[{"Time":"14:30","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"16:50","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"19:30","FilmId":4,"FilmName":"49 Ngày 2","Total":40}],"2017-05-19":[{"Time":"12:10","FilmId":2,"FilmName":"Kunfu Yoga","Total":40},{"Time":"14:20","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"16:55","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"19:35","FilmId":4,"FilmName":"49 Ngày 2","Total":40}],"2017-05-20":[{"Time":"10:00","FilmId":2,"FilmName":"Kunfu Yoga","Total":30},{"Time":"12:10","FilmId":3,"FilmName":"Now You See Me 2","Total":30},{"Time":"14:40","FilmId":6,"FilmName":"The Expendables 3","Total":30},{"Time":"17:15","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":30},{"Time":"19:15","FilmId":4,"FilmName":"49 Ngày 2","Total":30}],"2017-05-21":[{"Time":"10:00","FilmId":4,"FilmName":"49 Ngày 2","Total":30},{"Time":"12:20","FilmId":6,"FilmName":"The Expendables 3","Total":30},{"Time":"15:05","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":30},{"Time":"17:05","FilmId":2,"FilmName":"Kunfu Yoga","Total":30},{"Time":"19:25","FilmId":4,"FilmName":"49 Ngày 2","Total":30}]}';
    var hnShc = JSON.parse(hn);
    var hcm = '{"2017-05-13":[{"Time":"19:30","FilmId":2,"FilmName":"Kunfu Yoga","Total":40}],"2017-05-14":[{"Time":"10:00","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"12:40","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"15:25","FilmId":2,"FilmName":"Kunfu Yoga","Total":40},{"Time":"17:45","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"19:40","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-15":[{"Time":"14:30","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40},{"Time":"16:25","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"19:00","FilmId":6,"FilmName":"The Expendables 3","Total":40}],"2017-05-16":[{"Time":"14:20","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"16:50","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"19:40","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-17":[{"Time":"14:40","FilmId":2,"FilmName":"Kunfu Yoga","Total":40},{"Time":"17:00","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40},{"Time":"19:00","FilmId":5,"FilmName":"Enders Game","Total":40}],"2017-05-18":[{"Time":"14:00","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"16:35","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"19:05","FilmId":2,"FilmName":"Kunfu Yoga","Total":40}],"2017-05-19":[{"Time":"12:20","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"15:00","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"17:30","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"20:20","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-20":[{"Time":"10:00","FilmId":5,"FilmName":"Enders Game","Total":30},{"Time":"12:20","FilmId":3,"FilmName":"Now You See Me 2","Total":30},{"Time":"14:50","FilmId":6,"FilmName":"The Expendables 3","Total":30},{"Time":"17:30","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":30},{"Time":"19:30","FilmId":4,"FilmName":"49 Ngày 2","Total":30}],"2017-05-21":[{"Time":"10:00","FilmId":3,"FilmName":"Now You See Me 2","Total":30},{"Time":"12:40","FilmId":6,"FilmName":"The Expendables 3","Total":30},{"Time":"15:20","FilmId":4,"FilmName":"49 Ngày 2","Total":30},{"Time":"17:30","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":30},{"Time":"19:30","FilmId":2,"FilmName":"Kunfu Yoga","Total":30}],"2017-05-22":[{"Time":"14:20","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"16:50","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"19:20","FilmId":2,"FilmName":"Kunfu Yoga","Total":40}],"2017-05-23":[{"Time":"14:00","FilmId":4,"FilmName":"49 Ngày 2","Total":40},{"Time":"16:30","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"19:20","FilmId":2,"FilmName":"Kunfu Yoga","Total":40}],"2017-05-24":[{"Time":"14:20","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"16:50","FilmId":2,"FilmName":"Kunfu Yoga","Total":40},{"Time":"18:50","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-25":[{"Time":"14:30","FilmId":5,"FilmName":"Enders Game","Total":40},{"Time":"16:50","FilmId":4,"FilmName":"49 Ngày 2","Total":40},{"Time":"19:05","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":40}],"2017-05-26":[{"Time":"11:50","FilmId":2,"FilmName":"Kunfu Yoga","Total":40},{"Time":"14:00","FilmId":6,"FilmName":"The Expendables 3","Total":40},{"Time":"16:35","FilmId":3,"FilmName":"Now You See Me 2","Total":40},{"Time":"19:15","FilmId":4,"FilmName":"49 Ngày 2","Total":40}],"2017-05-27":[{"Time":"10:00","FilmId":2,"FilmName":"Kunfu Yoga","Total":30},{"Time":"12:20","FilmId":3,"FilmName":"Now You See Me 2","Total":30},{"Time":"14:50","FilmId":4,"FilmName":"49 Ngày 2","Total":30},{"Time":"17:10","FilmId":4,"FilmName":"49 Ngày 2","Total":30},{"Time":"19:30","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":30}],"2017-05-28":[{"Time":"10:00","FilmId":4,"FilmName":"49 Ngày 2","Total":30},{"Time":"12:20","FilmId":6,"FilmName":"The Expendables 3","Total":30},{"Time":"15:05","FilmId":1,"FilmName":"Dạ Cổ Hoài Lang","Total":30},{"Time":"17:05","FilmId":2,"FilmName":"Kunfu Yoga","Total":30},{"Time":"19:25","FilmId":4,"FilmName":"49 Ngày 2","Total":30}]}';
    var hcmSch = JSON.parse(hcm);
    var films = { 1: "Dạ Cổ Hoài Lang", 2: "Kunfu Yoga", 3: "Now You See Me 2", 4: "49 Ngày 2", 5: "Enders Game", 6: "The Expendables 3" };
</script>
</asp:Content>

