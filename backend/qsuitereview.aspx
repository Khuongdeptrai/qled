﻿<%@ Page Language="C#" MasterPageFile="~/CMS.master" AutoEventWireup="true" CodeFile="qsuitereview.aspx.cs" Inherits="qsuitereview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<%
    string cinemaid = Request.QueryString["cinemaid"]!=null ? Request.QueryString["cinemaid"].ToString() : "";
    string filmId = Request.QueryString["filmId"] != null ? Request.QueryString["filmId"].ToString(): "";
    string dateView = Request.QueryString["dateview"] != null ? Request.QueryString["dateview"].ToString() : "";
    string timeview = Request.QueryString["timeview"] != null ? HttpUtility.HtmlDecode(Request.QueryString["timeview"].ToString()) : "";
 %>
    <section class="content-header">
        <h1>
        DANH SÁCH CẢM NHẬN QSUITE  (<%=_totalRows %>)
        <small></small>
        </h1>
    </section>    
    <section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">             
              
          </h3>
        <div class="box-tools">
            <form method="get">
            <div class="input-group">       
                 <div class="input-group group-date" style="width:100%"></div>                       
            </div>            
            </form>
         </div>
        </div><!-- /.box-header -->
        <div class="box-body">

          <table class="table table-bordered table-hover js-order-data">
            <thead>
              <tr>                 
                <th><input type="checkbox" id="selectall" class="checkbox" name="cbSelectAll" /></th>
				<th>STT</th>
                <th>Tên</th>                               
                <th>Email</th>
                <th>Nội dung</th>
                <th>Ngày tạo</th>
                <th>Trạng thái</th>
                <td>Cập nhật</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <% foreach(System.Data.DataRow item in dtMember.Rows ) { %>
              <tr> 			    
                <td><input type="checkbox" class="selectedId" name="chkitem[]" value="<%= item["ReviewId"] %>" /></td>
				<td><%=stt%></td>
                <td><%= item["Name"].ToString() %></td>                
                 <td><%= item["Email"].ToString() %></td>
                <td><%= item["ReviewContent"].ToString() %></td>
                <td><%= ((DateTime)item["Created"]).ToString("dd-MM-yyyy :HH:m:s") %></td>
                <td>                    
                    <% if (item["Status"].ToString() == "1")
                        { %>
                       <span id="span-<%= item["ReviewId"] %>">Approved</span> 
                    <%}
                       else if (item["Status"].ToString() == "2")
                    { %>
                     <span id="span-<%= item["ReviewId"] %>"> Block</span>
                    <%} else { %>
                       <span id="span-<%= item["ReviewId"] %>"> Pendding</span>
                   <% } %>
                </td>
                 <td>
                      <% if (item["Status"].ToString() == "1") { %>
                        <a href="javascript:;" class="changestatus" data-url="qsuite" data-id="<%= item["ReviewId"] %>" data-status="0">Pendding</a>
                    <%} else{ %>
                     <a href="javascript:;" class="changestatus"  data-url="qsuite" data-id="<%= item["ReviewId"] %>" data-status="1">Approved</a>
                    <% }  %>
                 </td>
                  <td>
                      <a href="javascript:;" class="delete-qsuite-review"   data-id="<%= item["ReviewId"] %>" >Xóa</a>
                  </td>
              </tr>
              <% stt = stt+1;} %>
            </tbody>
          </table>

        </div><!-- /.box-body -->
        <div class="box-footer clearfix pagination">
            <%=pagingHtml %>
        </div>
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->

<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Thiết lập thông tin cho mượn</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
                <label>Ngày cho mượn:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="bookdatetime" readonly="readonly">
                </div>
                <!-- /.input group -->
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="button" class="btn btn-primary js-btnUpdateChoMuon">Lưu lại</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</asp:Content>

