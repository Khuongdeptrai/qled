$(function() {

	/*tinymce.init({
    selector: "textarea",theme: "modern",width: 680,height: 300,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
	  ],
	   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
	   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
	   image_advtab: true ,

	   external_filemanager_path:"/jalkton/plugins/tinymce/filemanager/",
	   filemanager_title:"Filemanager" ,
	   external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
 	});*/



tinymce.init({
    selector: ".tinymce",
    theme: "modern",
    width: '100%',
    height: 300,
    invalid_elements: "script",
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking spellchecker",
         "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
   ],
    relative_urls: true,
    browser_spellcheck : true ,
    codemirror: {
	    indentOnInit: true, // Whether or not to indent code on init.
	    path: 'CodeMirror'
	  },
   image_advtab: true,
   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
   toolbar2: "| responsivefilemanager | image | media | link unlink anchor | print preview code  | forecolor backcolor | code"
 });
});