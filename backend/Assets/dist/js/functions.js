﻿

$(document).ready(function () {
    XPRESS.init();
    /*
    $('.cbSelectAll').click(function () {
        $("input[@name='chkitem[]']:not([disabled='disabled'])").attr('checked', $(this).attr("checked"));
    });
    */
    $('#selectall').click(function () {
        $('.selectedId').prop('checked', $(this).is(":checked"));
    });

});


var XPRESS = {
    tempUrl: '',
	init : function() {
	//	$(".js-order-data").rowSorter({
    //    onDrop: function(tbody, row, new_index, old_index) {
    //        console.log(row + ". row moved to " + new_index);
    //    }
	    //});
	    $('form').submit(function (e) {
	        var emptyinputs = $(this).find('input').filter(function () {
	            return !$.trim(this.value).length;  // get all empty fields
	        }).prop('disabled', true);
	        var emptyselect = $(this).find('select').filter(function () {
	            return !$.trim(this.value).length;  // get all empty fields
	        }).prop('disabled', true);
	    });
        $('.js-btnSendEdm').on('click', XPRESS.sendEmail);
        $('.js-btnSendEdmTest').on('click', XPRESS.sendEmailTest);

	    $('.js-delete-item').on('click', XPRESS.confirmDelete);
	    $('.js-list-week').on('change', XPRESS.changeWeek);
	    $('.js-list-cat').on('change', XPRESS.changeCat);
	    $('.changestatus').on('click', XPRESS.changeStatus);
	    $('.luc-delete').on('click', XPRESS.deleteRegister);
	    $('.delete-review-qcinema').on('click', function () {
	        if (confirm("Bạn có chắc xóa bình luận này?")) {
	            var id = $(this).attr("data-id");
	            
	            var url = "";
	            url = serverCall.requestUrl + 'addreview.aspx';

	            $.ajax({
	                type: 'post',
	                url: url,
	                data: { action: 'delete', id: id },
	                cache: false,
	                //dataType: dataType,
	                success: function (response) {
	                    response = JSON.parse(response);
	                    console.log(response);
	                    if (response.Status == 1) {

	                        alert('Xóa bình luận thành công');
	                        window.location.reload();
	                    } else {
	                        alert('Đã xảy ra lỗi');
	                        window.location.reload()
	                    }

	                },
	                error: function (response) {
	                    console.log('error', response)
	                }
	            });
	        }
	    });

	    $('.delete-qsuite-review').on('click', function () {
	        if (confirm("Bạn có chắc xóa bình luận này?")) {
	            var id = $(this).attr("data-id");

	            var url = "";
	            url = serverCall.requestUrl + 'qsuitereview.aspx';

	            $.ajax({
	                type: 'post',
	                url: url,
	                data: { action: 'delete', id: id },
	                cache: false,
	                //dataType: dataType,
	                success: function (response) {
	                    response = JSON.parse(response);
	                    console.log(response);
	                    if (response.Status == 1) {

	                        alert('Xóa bình luận thành công');
	                        window.location.reload();
	                    } else {
	                        alert('Đã xảy ra lỗi');
	                        window.location.reload()
	                    }

	                },
	                error: function (response) {
	                    console.log('error', response)
	                }
	            });
	        }
	    });

	    $('.change-register-status').on('change', function () {
	        var id = $(this).attr("data-id");
	        var status = $(this).val();
	        var url = serverCall.requestUrl + $(this).attr("data-url");

	        $.ajax({
	            type: 'post',
	            url: url,
	            data: { action: 'updatestatus', id: id,status:status },
	            cache: false,
	            //dataType: dataType,
	            success: function (response) {
	                response = JSON.parse(response);
	                console.log(response);
	                if (response.Status == 1) {
	                    alert('Cập nhật thành công');
	                    window.location.reload();
	                } else {
	                    alert('Đã xảy ra lỗi');
	                    window.location.reload()
	                }
	            },
	            error: function (response) {
	                console.log('error', response)
	            }
	        });

	    })
	    $('#startdate').datepicker({
	        format: 'yyyy-mm-dd'
	    });
	    $('#enddate').datepicker({
	        format: 'yyyy-mm-dd'
        });
        $('#bookdatetime').daterangepicker({
            timePicker: true,
            autoclose: true,
            autoUpdateInput: true, 
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
           
        });

        $('.js-btn-filter').on('click', XPRESS.filterDate);
        $('.js-selectAction').on('click', XPRESS.showModelPopup);
        $('.js-btnUpdateChoMuon').on('click', XPRESS.assingUserType);
        $('.js-btnGalleryRemove').on('click', XPRESS.galleryRemoveItem);
        $('.js-btnViewEdm').on("click", function (evt) {
            evt.preventDefault();            
            var id = $('.js-edmId').val();
            window.location.href = $(this).attr("href") + "&id=" + id;
        });
	},
	confirmDelete: function (e) {
	    if (confirm("Are you sure delete this item?")) {
	        XPRESS.deleteItem(this);
	    } else {
	        return false;
        }
        return false;
	},
	changeWeek: function (e) {
	    location.href = $(this).parents('form').attr('action') + "&week=" + $(this).val() + "&catid=" + $('.js-list-cat').val();
	},
	changeCat: function (e) {
	    location.href = $(this).parents('form').attr('action') + "&catid=" + $(this).val() + "&week=" + $('.js-list-week').val();
	},
	filterDate: function (e) {
	    e.preventDefault();
	    location.href = $(this).parents('form').attr('action') + "&startdate=" + $("#startdate").val() + "&enddate=" + $("#enddate").val() + "&catid=" + $('.js-list-cat').val();
	},
	deleteRegister :function(){
	    if (confirm("Bạn có chắc xóa đăng ký này?")) {
	        var id = $(this).attr("data-id");
	        var qsuite = $(this).attr("data-qsuite");
	        var url = "";
	        if (qsuite && qsuite == "1") { //qsuite
	            url = serverCall.requestUrl + 'qsuiteregister.aspx';
	        } else if (qsuite && qsuite == "2") {//qhome
	            url = serverCall.requestUrl + 'qhomeregister.aspx';
	        } else {//register film
	            url = serverCall.requestUrl + 'member-waiting.aspx';
	        }
	        
	        $.ajax({
	            type: 'post',
	            url: url,
	            data: { action: 'deleteRegister', id: id },
	            cache: false,
	            //dataType: dataType,
	            success: function (response) {
	                response = JSON.parse(response);
	                console.log(response);
	                if (response.Status == 1) {

	                    alert('Xóa đăng kí thành công');
	                    window.location.reload();
	                } else {
	                    alert('Đã xảy ra lỗi');
	                    window.location.reload()
	                }

	            },
	            error: function (response) {
	                console.log('error', response)
	            }
	        });
	    }

	},
	changeStatus :function(){
	    var _this = $(this);
	    var id = _this.attr('data-id');
	    var status = _this.attr('data-status'),urlAjax;
	    if ($(this).attr('data-url') == 'review') {
	        urlAjax = serverCall.requestUrl + 'addreview.aspx'
	    } else {
	        urlAjax = serverCall.requestUrl + 'qsuitereview.aspx'
	    }
	    console.log(urlAjax);
	    $.ajax({
	        type: 'post',
	        url: urlAjax,
	        data: {action:'updatestatus',id:id,status:status},
	        cache: false,
	        //dataType: dataType,
	        success: function (response) {
	            response = JSON.parse(response);
	            Message.alert(response.Message);
	            if (status == 1) {
	                
	                _this.attr('data-status', 0);
	                $('#span-' + id).text();
	              window.location.reload()
	            } else {
	                
	                _this.attr('data-status', 1);
	                _this.text('Approve');
	               window.location.reload()
	            }
	          
	        },
	        error: function (response) {
	            console.log('error',response)
	        }
	    });
	  //  alert();
	},
	sendEmail: function () {
	    var buttonSubmit = this;	    
	    var urlRequest = $(buttonSubmit).attr('href');
        var subject = $(buttonSubmit).parent().parent().find('input[name="subject"]').val();
        if (subject == "") {
            Message.alert("Vui lòng nhập tiêu đề email");
            return false;
        }        
	    $(buttonSubmit).text("Đang gửi...");
	    $(buttonSubmit).prop('disabled', true);	    
	    serverCall.callFunc(urlRequest, function (response) {
	       // Message.alert(response.Message);
	        Message.alert(response.Content);
            if (response.Status == "1" && response.Content=="Ok") {
	            $(buttonSubmit).text("Gửi thành công");
	        }
            else {               
	            $(buttonSubmit).text("Lỗi! Thử lại");
	            $(buttonSubmit).prop('disabled', false);
	        }

	    }, "POST", "json"
            , {
                "id": $('.js-edmId').val(),
                "subject": subject

            }
        );
	    return false;
    },
    sendEmailTest: function () {
        var buttonSubmit = this;
        var urlRequest = $(buttonSubmit).attr('href');
        var subject = $(buttonSubmit).parent().parent().find('input[name="subject"]').val();
        var email = $(buttonSubmit).parent().find('input[name="email"]').val();
        if (subject == "") {
            Message.alert("Vui lòng nhập tiêu đề email");
            return false;
        }
        if (!validateTool.isEmail(email)) {
            Message.alert("Địa chỉ email không hợp lệ");
            return false;
        }
        $(buttonSubmit).text("Đang gửi...");
        $(buttonSubmit).prop('disabled', true);
        serverCall.callFunc(urlRequest, function (response) {
            Message.alert(response.Content);
            if (response.Status == "1" && response.Content=="Ok") {
                $(buttonSubmit).text("Gửi thành công");
            }
            else {                
                $(buttonSubmit).text("Lỗi! Thử lại");
                $(buttonSubmit).prop('disabled', false);
            }

        }, "POST", "json"
            , {
                "id": $('.js-edmId').val(),
                "subject": subject,
                "email": email

            }
        );
        return false;
    },

	deleteItem: function (obj) {
	    var buttonSubmit = obj;
	    var urlRequest = $(buttonSubmit).attr('href');

	    $(buttonSubmit).text("Đang gửi...");
	    $(buttonSubmit).prop('disabled', true);
	    serverCall.callFunc(urlRequest, function (response) {
	        // Message.alert(response.Message);
	        if (response.Status == "1") {
	            $(buttonSubmit).parent().parent().remove();
	        }
	        else {
	            $(buttonSubmit).text("Lỗi! Thử lại");
	            $(buttonSubmit).prop('disabled', false);
	        }

	    }, "POST", "json"
            , {
                "id": 01,
            }
        );
	    return false;
    },
    showModelPopup: function (e) {
        e.preventDefault();
        XPRESS.tempUrl = $(this);
        $('#myModal').modal('show'); 
    },
    assingUserType: function (e) {
        e.preventDefault();
        if (XPRESS.tempUrl == null) {

            return false;
        }
        var startDate = $("#bookdatetime").data('daterangepicker').startDate.format('YYYY-MM-DD HH:mm:ss');
        var endDate = $("#bookdatetime").data('daterangepicker').endDate.format('YYYY-MM-DD HH:mm:ss');
        if ($('#bookdatetime').val() == '')
        {
            $('#bookdatetime').focus();
            return false
        }
        var buttonSubmit = XPRESS.tempUrl;
        var urlRequest = $(buttonSubmit).attr('data-href');        
        serverCall.callFunc(urlRequest, function (response) {
            // Message.alert(response.Message);
            if (response.Status == "1") {                
                $('#myModal').modal('hide'); 
                $(response.Content).html('Đã cho mượn');
                $('#bookdatetime').val('');
            }
            Message.alert(response.Message);

        }, "POST", "json"
            , {
                "typeId": $(buttonSubmit).val(),
                "startDate": startDate,
                "endDate": endDate
            }
        );
        return false;
    },
    galleryRemoveItem: function () {
        var item = $(this).attr('data-value');
        var old = $('.js-remove-items').val() + ',' + item;
        $('.js-remove-items').val(old);
        $(this).parent().parent().remove();
    }
}


Dropzone.options.dropzoneForm = {
    init: function () {
        this.on("complete", function (data) {
            alert('adad');
        });
    }
};



$('.js-upload-image').change(function (e) {
    e.preventDefault();
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Bạn vui lòng chỉ tải ảnh với định dạng .jpeg .jpg  .png  .gif");
        $(this).val('');
        return false;
    }
});


var serverCall = {
    requestUrl: "",
    isProcessing: false,
    callFunc: function (url, func, type, dataType, post) {
        if (!serverCall.isProcessing) {
            serverCall.isProcessing = true;
            if (type === undefined) {
                type = "GET";
            }
            if (post === undefined) {
                post = {};
            }
            if (dataType === undefined) {
                dataType = "text";
            }
            post.op = 'ajax';
            post.language = $("#siteLanguage").val();
            $.ajax({
                type: type,
                url: serverCall.requestUrl + url,
                data: post,
                cache: false,
                dataType: dataType,
                success: function (response) {
                    serverCall.isProcessing = false;
                    func(response);
                },
                error: function (response) {
                    serverCall.isProcessing = false;
                    console.log(response);
                }
            });
        } else {
            Message.alert("Please waiting...");
        }
    }
};

var Message = {
    error: function (msg, callBack) {
        alert(msg);
    },
    alert: function (msg, callBack) {
        alert(msg);
    },
    warning: function (msg, callBack) {
        alert(msg);
    }
};
var validateTool = {
    isUndefined: function (strObj) {
        return typeof strObj == 'undefined' ? true : false;
    },
    isEmail: function (sEmail) {
        sEmail = sEmail.trim();
        if (sEmail.search(this.regExForEmail) != -1)
            return true;
        return false;
    },

}


var listFilmGlobal = null;
$('select[name=dateview]').change(function () {
    var value = $(this).val();
    if(value==""){

    } else {
        var opt = "<option value=''> chọn 1 giờ </option>";
        var cid =$('select[name=cinemaid]').val() ;
        if (cid != "") {
            if (cid == "1")
                listFilmGlobal = hnShc;
            else
                listFilmGlobal = hcmSch;
        }

        $.each(listFilmGlobal,function(index,object) {
            if (index == value) {
                
                $.each(object, function (i, obj) {
                    if ($('select[name=filmid]').val() != "") {
                        if (obj.FilmId == $('select[name=filmid]').val())
                            opt += "<option value='" + obj.Time.replace(":", "-") + "'>" + obj.Time + "</option>"
                    } else {
                        opt += "<option value='" + obj.Time.replace(":", "-") + "'>" + obj.Time + "</option>"
                    }
                    
                });
               
            }
        });
        $('select[name=timeview]').html("");
        console.log(opt);
        $('select[name=timeview]').html(opt);
    }
});

$('select[name=cinemaid]').change(function () {
    var cinameId = $(this).val();
    var listFilm = null;
    if (cinameId == "1") {
        listFilm = hnShc;
        listFilmGlobal = hnShc;
    } else if (cinameId == "2") {
        listFilm = hcmSch;
        listFilmGlobal = hcmSch;
    }

    var optionDate = "<option value=''>chọn 1 ngày</value>";
    $.each(listFilm, function (index, object) {
        var indexS = index.split('-');
        optionDate += "<option value='" + index + "'>" + indexS[2] + "-" + indexS[1] + "-" + indexS[0] + "</value>"

    });
    $("select[name=dateview]").html(optionDate);

});

$('select[name=filmid]').change(function () {
    if ($('select[name=cinemaid]').val() == "") {
        $('select[name=filmid]').val('');
        alert("Vui lòng chọn 1 rap phim ");
        return false;
    }
    var filmId = $(this).val();
    var cinameId = $('select[name=cinemaid]').val();
    var listFilm = null;
    
    if (cinameId == "1") {
        listFilm = hnShc;
        listFilmGlobal = hnShc;
    } else {
        listFilm = hcmSch;
        listFilmGlobal = hcmSch;
    }

    var optionDate = "<option value=''>chọn 1 ngày</value>";
    $.each(listFilm, function (index,object) {
        var indexS = index.split('-');

        var flag = 0;
        $.each(object,function ( i,obj ) {
            if (obj.FilmId == filmId) {
                flag = 1;
            }
        });
        if (flag==1) {
            optionDate += "<option value='" + index + "'>" + indexS[2] + "-" + indexS[1] + "-" + indexS[0] + "</value>"
        }
        
    });
    $("select[name=dateview]").html(optionDate);

    
    console.log(listFilm);
});
if ( typeof hcmSch != 'undefined' && hcmSch )
{
    filterDate();
}

function filterDate() {
    var cinameId = $('select[name=cinemaid]').val();
    var filmidId = $('select[name=filmid]').val();
    var listFilm = null;
    if (cinameId != "" && filmidId != "") {
        if (cinameId == "1") {
            listFilm = hnShc;
            listFilmGlobal = hnShc
        } else {
            listFilm = hcmSch;
            listFilmGlobal = hcmSch;
        }
        var optionDate = "<option value=''>chọn 1 ngày</value>";
        $.each(listFilm, function (index, object) {
            var indexS = index.split('-');

            var flag = 0;
            $.each(object, function (i, obj) {
                if (obj.FilmId == filmidId) {
                    flag = 1;
                }
            });
            if (flag == 1) {
                optionDate += "<option value='" + index + "'>" + indexS[2] + "-" + indexS[1] + "-" + indexS[0] + "</value>"
            }

        });
        $("select[name=dateview]").html(optionDate);
        $("select[name=dateview]").val(_dateView);
        console.log(listFilm);

        if (_dateView != null) {
            var value = _timeview;//.replace('-', ":");
            var opt = "<option value=''> chọn 1 giờ </option>";
            $.each(listFilmGlobal, function (index, object) {
                if (index == $("select[name=dateview]").val()) {

                    $.each(object, function (i, obj) {
                        if (obj.FilmId == $('select[name=filmid]').val()) {
                            opt += "<option value='" + obj.Time.replace(":", "-") + "'>" + obj.Time + "</option>";
                        }
                            
                    });

                }
            });
            $('select[name=timeview]').html("");
            console.log(opt);
            $('select[name=timeview]').html(opt);
            setTimeout(function () {
                $('select[name=timeview]').val(value);
               // alert(value);
            },300);
        }
    }


    if (cinameId != "" && filmidId == "") {
        var listFilm = ""
        if (cinameId == "1") {
            listFilm = hnShc;
        } else {
            listFilm = hcmSch;
        }
        var opt = "<option value=''>chọn 1 ngày</option>";
        $.each(listFilm, function (index,object) {
            if (index == _dateView) {
                $.each(object, function (k,obj) {
                    opt += "<option value='" + obj.Time.replace(":", "-") + "'>" + obj.Time + "</option>";
                });
            }
        })
        $('select[name=timeview]').html(opt);
         setTimeout(function () {
             $('select[name=timeview]').val(_timeview);
               // alert(value);
            },300);
    }

}