﻿$(document).ready(function () {
    GOOGLEMAP.init();
});

var GOOGLEMAP = {
    _initLat: $('.js-lat').val() != '' ? $('.js-lat').val() : "10.7894162",
    _initLng: $('.js-lng').val() != '' ? $('.js-lng').val() : "106.698646",
    init: function () {
        GOOGLEMAP.initialize();
        $('.js-address-gmap').on('keypress', GOOGLEMAP.searchAddress);
    },
    initialize: function () {
        if (typeof google != 'undefined') {

            var varLocation = new google.maps.LatLng(GOOGLEMAP._initLat, GOOGLEMAP._initLng);
            var varMapOptions = {
                center: varLocation,
                zoom: 15
            };

            var varMarker = new google.maps.Marker({
                position: varLocation,
                map: varMap,
                clickable: true,
                draggable:true,
                title: "Romano"
            });

            var varMap = new google.maps.Map(document.getElementById("map-canvas"), varMapOptions);
            varMarker.setMap(varMap);

            google.maps.event.addListener(varMarker, 'drag', function () {
                $('.js-lat').val(varMarker.getPosition().lat());
                $('.js-lng').val(varMarker.getPosition().lng());
            });

            google.maps.event.addListener(varMarker, 'dragend', function () {
                $('.js-lat').val(varMarker.getPosition().lat());
                $('.js-lng').val(varMarker.getPosition().lng());
            });
        }
    },
    searchAddress: function (e) {

        if (e.which == 13 || e.keyCode == 13) {
            
            if (typeof google != 'undefined') {
                var geocoder = new google.maps.Geocoder();                
                geocoder.geocode({ address: $('.js-address-gmap').val() }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        GOOGLEMAP._initLat = results[0].geometry.location.lat();
                        GOOGLEMAP._initLng = results[0].geometry.location.lng();
                        GOOGLEMAP.initialize();

                        $('.js-lat').val(GOOGLEMAP._initLat);
                        $('.js-lng').val(GOOGLEMAP._initLng);
                    }
                });
            }
            return false;
        }
    }
}