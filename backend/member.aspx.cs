﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cms_member : System.Web.UI.Page
{
    public int _totalRows = 0;
    public string search = string.Empty;
    public string pagingHtml = string.Empty;
    public DataTable dtMember = new DataTable();
    public string _startDate = string.Empty;
    public string _endDate = string.Empty;
    private System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
    List<string> Citys = new List<string> { "Hà Nội", "TP HCM", "An Giang", "Bà Rịa - Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước", "Bình Thuận", "Cà Mau", "Cao Bằng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương", "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái", "Phú Yên", "Cần Thơ", "Đà Nẵng", "Hải Phòng" };    
    public string listCitys = "";
    public string listModels = "";
    public int sent = 0;
	public string queryString  = "";
	public int stt = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
		if (Session["CMSUSER"] == null){
				Response.Redirect(Common.Webroot+"login.aspx");
				Response.End();
		}

        if (Request.HttpMethod=="POST")
        {
            string op = Request.Form["op"] != null ? Request.Form["op"] : "";
            string action = Request.QueryString["action"] != null ? Request.QueryString["action"] : "";
            if (op == "ajax")
            {
                if (action == "sendMail")
                {
                    SendEmailToUser();
                }
                else if (action == "remindMail")
                {
                    SendEmailRemindToUser();
                }
                else if (action == "delete")
                {
                    Delete();
                }
            }
        }

        Database db = new Database();
        string cityTemp = Request.QueryString["city"] != null ? Common.CleanValue(Server.HtmlDecode(Request.QueryString["city"].Trim())) : "";
        string modelTemp = Request.QueryString["model"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["model"]).Trim()) : "";
        sent = Request.QueryString["sent"]!=null ? Common.ParseInt(Request.QueryString["sent"]) : 0;
		int typeid = Request.QueryString["typeid"]!=null ? Common.ParseInt(Request.QueryString["typeid"]) : 0;
		string strQuery = "sent = "+sent+ " and TypeId=" + typeid;        
		if (typeid > 0)
		{
			strQuery = "sent = "+sent+ " and TypeId=" + typeid;
		}
        queryString = "&sent=" + sent+ "&TypeId=" + typeid;

        
        string orderBy = " order by id desc";
        int perPage = 50;
        int currentPage = Request.QueryString["page"] == null ? 1 : Common.ParseInt(Request.QueryString["page"].ToString());
        currentPage = currentPage <= 0 ? 1 : currentPage;
        db.dbCommand.Parameters.Clear();

        if (cityTemp != "")
        {
            queryString += "&city=" + Server.HtmlEncode(cityTemp); ;
            //strQuery += " and City COLLATE Latin1_General_CI_AI like N'%" + cityTemp + "%' COLLATE Latin1_General_CI_AI";
            strQuery += " and City COLLATE Latin1_General_CI_AI like N'%'+@City+'%' COLLATE Latin1_General_CI_AI";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@City", DbType.String, cityTemp));

        }
        if (modelTemp != "")
        {
            queryString += "&ModelTrial=" + Server.HtmlEncode(modelTemp);
            strQuery += " and ModelTrial like '%'+@ModelTrial+'%'";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@ModelTrial", DbType.String, modelTemp));
        }

        search = Request.QueryString["search"] == null ? string.Empty : Common.CleanScripts(Server.HtmlDecode(Request.QueryString["search"].ToString()));
        search = Common.CleanValue(search);
        
        if (search != "")
        {
            if (search.All(char.IsDigit))
            {
                strQuery += search != string.Empty ? " and ( Phone like N'%'+@searchKey+'%' or Idno like N'%'+@searchKey+'%' ) " : string.Empty;
            }
            else
            {
                strQuery += search != string.Empty ? " and ( Name COLLATE Latin1_General_CI_AI like N'%'+@searchKey+'%' COLLATE Latin1_General_CI_AI or Phone=@searchKey or Email like N'%'+@searchKey+'%' or City COLLATE Latin1_General_CI_AI like N'%'+@searchKey+'%' COLLATE Latin1_General_CI_AI) " : string.Empty;
            }
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@searchKey", DbType.String, search));
        }

        queryString += search != string.Empty ? "&search=" + Server.HtmlEncode(search) : "";

        _startDate = Request.QueryString["startdate"] == null ? string.Empty : Common.CleanValue(Request.QueryString["startdate"].ToString());
        _endDate = Request.QueryString["enddate"] == null ? string.Empty : Common.CleanValue(Request.QueryString["enddate"].ToString());
        if (_startDate != string.Empty && IsDateTime(_startDate))
        {
            queryString += "&startdate=" + _startDate;
            strQuery += " and cast(CreatedDate as Date) >= @StartDate";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@StartDate", DbType.String, _startDate));

        }
		else{
			_startDate  = "";
		}
        if (_endDate != string.Empty && IsDateTime(_endDate) )
        {
            queryString += "&enddate=" + _endDate;
            strQuery += " and cast(CreatedDate as Date) <= @EndDate";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@EndDate", DbType.String, _endDate));
        }
		else{
			_endDate  = "";
		}

        string sqlSelect = string.Empty;        
        DataTable dtTotalRows = db.Exec("select count(id) as total from Register where " + strQuery);        
        _totalRows = dtTotalRows.Rows.Count > 0 ? Common.ParseInt(dtTotalRows.Rows[0]["total"]) : 0 ;
        int _maxPage = (_totalRows % perPage) == 0 ? _totalRows / perPage : (_totalRows / perPage) + 1;
        int _startPage = (currentPage - 1) * perPage + 1;
        int _endPage = _startPage + perPage - 1;
		stt = _startPage;

        string _action = Request.QueryString["action"] == null ? string.Empty : Request.QueryString["action"].ToString();
        if (_action == "export")
        {
            sqlSelect = "SELECT  * FROM Register v where " + strQuery + orderBy;
            dtMember = db.Exec(sqlSelect);

            ExportData expt = new ExportData();
            expt.dtList = dtMember;
            expt.ExportGridView(DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".xls", "xls", "Danh sách đăng ký");
            Response.End();
        }

        sqlSelect = @"SELECT  * 
                                FROM 
                                    (SELECT  ROW_NUMBER() OVER(" + orderBy + @") as row, y.*
                                        FROM 
	                                    ( SELECT TOP " + _totalRows + " * from Register where " + strQuery + orderBy + @"                           
	                                    ) y
                                    )x
                                WHERE x.row BETWEEN " + _startPage.ToString() + " AND " + _endPage.ToString();
        
        dtMember = db.Exec(sqlSelect);

        if (_maxPage > 1)
        {
            PagingCMS paging = new PagingCMS();
            paging.Last = "&raquo;";
            paging.First = "&raquo;";
            paging.Next = "&rsaquo;";
            paging.Prev = "&lsaquo;";
            paging.EventClick = "";
            pagingHtml = paging.multi(_totalRows, perPage, currentPage, Common.Webroot + "member.aspx?page=%page%" + queryString);
        }
        
        foreach (string city in Citys)
        {
            string selected = "";
            if (cityTemp.Equals(city, StringComparison.InvariantCultureIgnoreCase))
            {
                selected = "selected=\"selected\"";
            }
            listCitys += "<option " + selected + " value=\"" + city + "\">" + city + "</option>";
        }
        DataTable dtList = db.Exec("select * from Model_TV");
        foreach (DataRow row in dtList.Rows)
        {
            string selected = "";
            if (modelTemp.Equals(row["Name"].ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                selected = "selected=\"selected\"";
            }
            listModels += "<option " + selected + " value=\"" + row["Name"].ToString() + "\">" + row["Name"].ToString() + "</option>";
        }


    }
    private void SendEmailToUser()
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        int id = Request.QueryString["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (id > 0)
        {
            Database db = new Database();
            DataTable dtDetail = db.Exec("SELECT * FROM Register WHERE id=" + id);
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                int TypeId = Common.ParseInt(dtDetail.Rows[0]["TypeId"]);
                string stringTemplate = GetEmailTemPlate(TypeId);
                if (TypeId > 0 && stringTemplate!="")
                {   
                    string myKey = dtDetail.Rows[0]["Id"] + "" + Guid.NewGuid().ToString().Replace("-", "");
                    Dictionary<string, object> objEmail = new Dictionary<string, object>();
                    Sendmail mail = new Sendmail();
                    objEmail.Add("ToEmail", dtDetail.Rows[0]["Email"]);
                    objEmail.Add("Subject", "[Thế Hệ TV Samsung] – Thư xác nhận");
                    objEmail.Add("fullName", dtDetail.Rows[0]["Name"]);
                    objEmail.Add("DOMAIN", ConfigurationManager.AppSettings["FRONT_END_URL"]);
                    objEmail.Add("hash", myKey);
                    //  message.Content = mail.SendGemSamSung(objEmail, strTemplate);
                    db.Exec("UPDATE Register SET Sent=1, UpdateTime={FN NOW()}, ConfirmHash='" + myKey + "'  where id=" + id);

                    message.Status = 1;
                    message.Message = "Thông tin được gửi thành công";
                }
                else
                {
                    message.Message = "Không xác định loại email";
                }

            }
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }
    private void SendEmailRemindToUser()
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        int id = Request.QueryString["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (id > 0)
        {
            Database db = new Database();
            DataTable dtDetail = db.Exec("SELECT * FROM Register WHERE id=" + id+" and Sent=1");
            if (dtDetail != null && dtDetail.Rows.Count > 0)
            {
                int TypeId = Common.ParseInt(dtDetail.Rows[0]["TypeId"]);
                string stringTemplate = GetEmailTemPlate(TypeId);
                if (TypeId > 0 && stringTemplate != "")
                {
                    string myKey = dtDetail.Rows[0]["Id"] + "" + Guid.NewGuid().ToString().Replace("-", "");

                    Dictionary<string, object> objEmail = new Dictionary<string, object>();
                    Sendmail mail = new Sendmail();
                    objEmail.Add("ToEmail", dtDetail.Rows[0]["Email"]);
                    objEmail.Add("Subject", "[Thế Hệ TV Samsung] – Thư xác nhận");
                    objEmail.Add("fullName", dtDetail.Rows[0]["Name"]);
                    objEmail.Add("DOMAIN", ConfigurationManager.AppSettings["FRONT_END_URL"]);
                    //message.Content = mail.SendGemSamSung(objEmail, stringTemplate);

                    message.Status = 1;
                    message.Message = "Thông tin được gửi thành công";
                }

            }
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }
    private string GetEmailTemPlate(int typeId)
    {
        string strTemplate = "";
        switch (typeId)
        {
            case 1:
                strTemplate = Server.MapPath("~/email/congratulation.html");
                break;
            case 2:
                strTemplate = Server.MapPath("~/email/offer_1package_buytv.html");
                break;
            case 3:
                strTemplate = Server.MapPath("~/email/introduce_samsungtv.html");
                break;
        }
        return strTemplate;
    }
    private void Delete()
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        int id = Request.QueryString["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (id > 0)
        {
            Database db = new Database();
            DataTable dtDetail = db.Exec("DELETE FROM Register WHERE id=" + id + "");
            message.Status = 1;
            message.Message = "Thông tin được gửi thành công";
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }

	public static bool IsDateTime(string txtDate)
	{
		DateTime tempDate;
		return DateTime.TryParse(txtDate, out tempDate) ? true : false;
	}
}

