﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS.master" AutoEventWireup="true" CodeFile="library-edit.aspx.cs" ValidateRequest="false" Inherits="library_edit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<form id="formData"  role="form" method="post" enctype="multipart/form-data">               
    <section class="content-header">
      <h1>
        Cảm nhận khách hàng
        <small>Sửa: <%=dtRow["Name"] %></small>
      </h1>    
     <div class="pull-right box-tools margin">
         <button type="submit" class="btn btn-primary">Lưu lại</button>
         <a href="library.aspx" class="btn btn-default">Bỏ qua</a>
     </div>   
    </section>
    <section class="content">
      <div class="row">          
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            
            <!-- /.box-header -->
            <!-- form start -->            

              <div class="box-body">
                 <% if (message.Message != "")
                     {%>   
                 <div class="<%=message.Status == 1 ? "alert alert-success" : "alert alert-danger" %>">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <%=message.Message %>
                 </div>  
                <% } %>  
                <div class="form-group">                
                    <label for="statusleInputFile">Tuần</label>        
                     <select name="WeekId" class="form-control" style="width:110px; margin-right:5px;" required>
                         <option value="">Chọn tuần</option>
                         <% for(int i=1;i<=5;i++ ){ %>
                            <option value="<%=i %>" <%=dtRow!=null && dtRow["WeekId"].Equals(i) ? "selected=\"selected\"" : "" %>>Tuần <%=i %></option>
                         <%} %>
                     </select>   
                </div>    
                <div class="form-group">                
                    <label for="statusleInputFile">Tỉnh/Thành Phố</label>        
                     <select name="city" class="form-control" style="width:110px; margin-right:5px;" required>
                         <option value="">Tỉnh/Thành Phố</option>
                         <% foreach (string city in Common.ListCity){ %>
                            <option value="<%=city %>" <%=dtRow["City"].ToString().Equals(city, StringComparison.InvariantCultureIgnoreCase) ? "selected=\"selected\"" : "" %>><%=city %></option>
                         <%} %>
                     </select>   
                </div>          
                <div class="form-group">
                  <label for="exampleInputEmail1">Họ và Tên</label>
                  <input type="text" class="form-control" id="txtFullname" placeholder="Họ và Tên" name="Name" required error="Vui lòng nhập họ và tên" value="<%=dtRow["Name"] %>"  />
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Content</label>
                  <textarea class="form-control textarea" rows="10" placeholder="Enter ..." name="Content"><%=dtRow["Content"] %></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Hình Đại diện (Size 301x198)</label>
                  <input type="file" accept="image/jpg,image/png,image/jpeg,image/gif"  id="thumbnailInputFile" name="Thumbnail" />
                     <input type="hidden" name="thumbnailOld" value="<%=dtRow["Thumbnail"] %>" />
                  <% if (dtRow["Thumbnail"] != null && dtRow["Thumbnail"].ToString() != "")
                  { %>
                        <img class="img-responsive pad" src="<%= Common.UploadUrl()+"gallery/"+dtRow["Thumbnail"] %>?width=301&height=198" alt="Photo">
                       
                  <%} %>

                    <div class="checkbox">
                      <label>
                        <input type="checkbox" value="1" name="thumbnailRemove" /> Xóa hình hình đại diện
                      </label>
                    </div>
                </div>
                <div class="form-group">                
                    <div class="checkbox">   
                        <label>
                            <input class="flat-red" id="statusleInputFile" name="Status" type="checkbox" <%=(Boolean)dtRow["Status"]?"checked=\"checked\"":"" %> value="1">Hiển thị lên website               
                        </label>
                    </div>
                </div>
                  <div class="form-group">                
                    <label for="orderIdInputFile">Thứ tự</label>        
                    <br />
                    <input id="orderIdInputFile" name="OrderId" type="number" value="<%=dtRow["OrderId"] %>"> 
                        
                </div>
                <div class="form-group">                
                  <div class="row">                    
                    <div class="col-lg-12">                        
                        <input type="file" accept="image/jpg,image/png,image/jpeg,image/gif" class="btn btn-primary" id="images" name="images" multiple style="float:right"/>
                        <h1 class="page-header">Gallery</h1>                                                                  
                    </div>
                    <%  
                        foreach (string img in dtRow["Gallery"].ToString().Split(','))
                        {
                            if (img != "")
                            { %>
                    <div class="col-lg-3 col-md-4 col-xs-4 thumb" style="text-align:center" >
                        <div class="thumbnail">
                            <img class="img-responsive" src="<%=Common.UploadUrl() + "gallery/" + img %>?width=150&height=150&crop=center" alt="">
                            <input type="hidden" name="image" value="<%=img %>" />
                            <button data-value="<%=img %>" type="button" class="btn btn-danger js-btnGalleryRemove">Xóa</button>
                        </div>                        
                    </div>
                    <%}
                    } %>                      
                </div>
                </div>
                </div>
              <!-- /.box-body -->

              <div class="box-footer form-group">
                <button type="submit" class="btn btn-primary">Lưu lại</button>
                  <a href="library.aspx" class="btn btn-default">Bỏ qua</a>
              </div>
                <%=formhash %>  
                <input type="hidden" name="removeItems" class="js-remove-items" value="" />                          
          </div>
          <!-- /.box -->
        

        </div>
        
      </div>
      <!-- /.row -->  
</section>  
</form>
</asp:Content>


