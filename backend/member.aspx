﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS.master" AutoEventWireup="true" CodeFile="member.aspx.cs" Inherits="cms_member" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <section class="content-header">
        <h1>
        DANH SÁCH <% = sent==1?"ĐÃ GỬI EMAIL":"ĐĂNG KÝ MỚI" %> (<%=_totalRows %>)
        <small></small>
        </h1>
    </section>    
    <section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">             
              
          </h3>
        <div class="box-tools">
            <form method="get">
            <div class="input-group">       
                 <div class="input-group group-date" style="width:100%">
                     <select name="city" class="form-control" style="width:110px; margin-right:5px;">
                         <option value="">Tỉnh/Thành Phố</option>
                         
                     </select>                     
                     <select name="age" class="form-control" style="width:110px; margin-right:5px;">
							<option value="">Mẫu TV</option>
							<%=listModels %>  
                     </select>
                    <input type="text" class="form-control" style="width:110px; margin-right:5px;" readonly="true" name="enddate" id="enddate" style=" width: 50%; " placeholder="Ngày kết thúc" value="<%=_startDate %>" />
                    <input type="text" class="form-control" style="width:110px;margin-right:5px;" readonly="true"  name="startdate" id="startdate" style=" width: 50%; " placeholder="Ngày bắt đầu" value="<%=_endDate %>" />
                    <input type="text" name="search" class="form-control" style="width: 150px;margin-right:5px;" placeholder="Search" value=""/>
                    <button class="btn btn-primary" type="submit">Lọc</button>     
                     <a href="<%=Common.BaseUrl %>member.aspx?action=export<%=queryString %>" class="btn btn-primary" >Xuất Excel</a>                    
                     <%if (sent == 1)
                         {%>              
                     <button data-link="<%=Common.BaseUrl %>member.aspx" class="btn btn-primary js-remindEmail-button" type="submit">Remind Mail</button>               
                     <% } else { %>
                         <button data-link="<%=Common.BaseUrl %>member.aspx" class="btn btn-primary js-sendEmail-button" type="submit">Send Mail</button>                                  
                     <%} %>
                </div>                       
            </div>            
            </form>
         </div>
        </div><!-- /.box-header -->
        <div class="box-body">

          <table class="table table-bordered table-hover js-order-data">
            <thead>
              <tr>                 
                <th><input type="checkbox" id="selectall" class="checkbox" name="cbSelectAll" /></th>
				<th>STT</th>
                <th>Tên</th>                               
                <th>Điện thoại</th>
                <th>Email</th>
                <th>City</th>
                <th>Idno/Passport</th>
                <th>Ngày tạo</th>
				<th>Nguồn từ</th>               
                <th>Mẫu TV</th>
                <%if (sent==1) {%> 
                <th>Remind</th>
                <% } else{ %>
                   <th>SendMail</th>
                <%} %>
              </tr>
            </thead>
            <tbody>
              <% foreach(System.Data.DataRow item in dtMember.Rows ) { %>
              <tr> 			    
                <td><input type="checkbox" class="selectedId" name="chkitem[]" value="<%= item["id"] %>" /></td>
				<td><%=stt%></td>
                <td><%= item["Name"].ToString() %></td>                
                 <td><%= item["Phone"].ToString() %></td>
                <td><%= item["Email"].ToString() %></td>                                
                <td><%= item["City"].ToString() %></td>
                <td><%= item["Idno"].ToString() %></td>
                <td><%= ((DateTime)item["CreatedDate"]).ToString("dd-MM-yyyy :HH:m:s") %></td>
				<td><a href="<%= item["Refer"].ToString() %>" title="<%= item["Refer"].ToString() %>">Xem</a></td>                
               <td><%= item["ModelTrial"].ToString() %></td>
                <%if (sent == 1)
                    {%> 
                    <td><a href="<%= Common.BaseUrl %>member.aspx?id=<%= item["id"].ToString() %>&action=remindMail&typeid=<%= item["TypeId"].ToString() %>" class="js-remindEmail-item">Remind Email</a></td>
                <%} else { %>
                    <td><a href="<%= Common.BaseUrl %>member.aspx?id=<%= item["id"].ToString() %>&action=sendMail&typeid=<%= item["TypeId"].ToString() %>" class="js-sendEmail-item"><%= (Boolean)item["sent"]?"Sent Email":"Send Email" %></a></td>
                <%} %>                
              </tr>
              <% stt = stt+1;} %>
            </tbody>
          </table>

        </div><!-- /.box-body -->
        <div class="box-footer clearfix pagination">
            <%=pagingHtml %>
        </div>
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->

</asp:Content>

