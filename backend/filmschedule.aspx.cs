﻿		using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class filmschedule : System.Web.UI.Page
{

    public DataTable dtMember = new DataTable();
    public Dictionary<DateTime,List<FilmSchedule>> ddlf = new Dictionary<DateTime,List<FilmSchedule>>();
    public List<FilmSchedule> lf = new List<FilmSchedule>();
    public List<FilmSchedule> hcm = new List<FilmSchedule>();
    public Dictionary<int, string> FilmName =  new Dictionary<int,string>();
	private int TotalL = 40;
    private int Weekend = 30;
    private int SpecialTotal = 16;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }
        FilmName = ListFilmName();
        Database db = new Database();
        string dateView = Request.QueryString["date"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["date"]).Trim()) : "";

        if( dateView != "" ){
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@dateView", DbType.String, dateView));
            string [] dateSplit = dateView.Split('-');
            DateTime dt = new DateTime(Int32.Parse(dateSplit[0]), Int32.Parse(dateSplit[1]),Int32.Parse(dateSplit[2]));

            Dictionary<DateTime, List<FilmSchedule>> ddlf1 = FilmSheduleHaNoi();
            lf = ddlf1.Where(x => x.Key == dt).SingleOrDefault().Value;
           

            DataTable dtMemberHaNoi = db.Exec("Select * From Registers where Success = 1 and DateView = @dateView and CinemaIdCode = 1"); //ha noi
            DataTable dtMemberHochiminh = db.Exec("Select * From Registers where Success = 1 and DateView = @dateView and CinemaIdCode = 2"); //Sai gon

            int total = dtMember.Rows.Count;
            foreach (System.Data.DataRow item in dtMemberHaNoi.Rows)//hanoi
            {
                string t = item["TimeView"].ToString();
                string cinemaId = item["CinemaIdCode"].ToString();
                var obj = lf.FirstOrDefault(x => x.Time == item["TimeView"].ToString() );
                if (obj != null && Int32.Parse(cinemaId) == 1)
                {
                    obj.TotalRegister += Int32.Parse(item["NumberPeople"].ToString());
                }
            }

            Dictionary<DateTime, List<FilmSchedule>> temp = FilmSheduleHoChiMinh();
             hcm = temp.Where(x => x.Key == dt).SingleOrDefault().Value;
             foreach (System.Data.DataRow item in dtMemberHochiminh.Rows)//hanoi
            {
                string t = item["TimeView"].ToString();
                string cinemaId = item["CinemaIdCode"].ToString();
                var obj1 = hcm.FirstOrDefault(x => x.Time == item["TimeView"].ToString());
                if (obj1 != null && Int32.Parse(cinemaId) == 2) 
                {
                    obj1.TotalRegister += Int32.Parse(item["NumberPeople"].ToString());
                }
            }

        }

        
        
    }
    private Dictionary<int, string> ListFilmName()
    {

        var films = new Dictionary<int, string>();
        films.Add(1, "Dạ Cổ Hoài Lang");
        films.Add(2, "Kunfu Yoga");
        films.Add(3, "Now You See Me 2");
        films.Add(4, "49 Ngày 2");
        films.Add(5, "Enders Game");
        films.Add(6, "The Expendables 3");
        return films;
    }

    private Dictionary<DateTime, List<FilmSchedule>> FilmSheduleHaNoi()
    {
        var listFilmNam = ListFilmName();

        List<Dictionary<DateTime, List<FilmSchedule>>> ddlfHn = new List<Dictionary<DateTime, List<FilmSchedule>>>();
        Dictionary<DateTime, List<FilmSchedule>> dicList = new Dictionary<DateTime, List<FilmSchedule>>();

        var listDate6 = new List<FilmSchedule>();
		// listDate6.Add(new FilmSchedule { Time = "12:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
		listDate6.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 10 });
		// listDate6.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 16 });
		// listDate6.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 16 });
		// listDate6.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = 16 });

		dicList.Add(new DateTime(2017, 05, 06), listDate6);


		var listDate7 = new List<FilmSchedule>();

		listDate7.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
		listDate7.Add(new FilmSchedule { Time = "13:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate7.Add(new FilmSchedule { Time = "15:45", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
		listDate7.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
		listDate7.Add(new FilmSchedule { Time = "20:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });

		dicList.Add(new DateTime(2017, 05, 07), listDate7);

		var listDate8 = new List<FilmSchedule>();
		// listDate8.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
		listDate8.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
		listDate8.Add(new FilmSchedule { Time = "16:25", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
		listDate8.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 08), listDate8);

		var listDate9 = new List<FilmSchedule>();
		//listDate9.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
		listDate9.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
		listDate9.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate9.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 09), listDate9);

		var listDate10 = new List<FilmSchedule>();
		// listDate10.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 12 });
		listDate10.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate10.Add(new FilmSchedule { Time = "17:15", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
		listDate10.Add(new FilmSchedule { Time = "19:05", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 10), listDate10);

		var listDate11 = new List<FilmSchedule>();
		// listDate11.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
		listDate11.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate11.Add(new FilmSchedule { Time = "17:05", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
		listDate11.Add(new FilmSchedule { Time = "19:35", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 11), listDate11);

		var listDate12 = new List<FilmSchedule>();
		//listDate12.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
		listDate12.Add(new FilmSchedule { Time = "12:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
		listDate12.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
		listDate12.Add(new FilmSchedule { Time = "17:10", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate12.Add(new FilmSchedule { Time = "20:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 12), listDate12);

		var listDate13 = new List<FilmSchedule>();
		listDate13.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
		listDate13.Add(new FilmSchedule { Time = "12:30", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
		listDate13.Add(new FilmSchedule { Time = "15:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate13.Add(new FilmSchedule { Time = "17:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
		listDate13.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 13), listDate13);

		var listDate14 = new List<FilmSchedule>();
		listDate14.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
		listDate14.Add(new FilmSchedule { Time = "12:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate14.Add(new FilmSchedule { Time = "15:20", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
		listDate14.Add(new FilmSchedule { Time = "17:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
		listDate14.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 14), listDate14);

		var listDate15 = new List<FilmSchedule>();
		//   listDate15.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
		listDate15.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
		listDate15.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
		listDate15.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 15), listDate15);

		var listDate16 = new List<FilmSchedule>();
		//    listDate16.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12});
		listDate16.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
		listDate16.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate16.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 16), listDate16);

		var listDate17 = new List<FilmSchedule>();
		//  listDate17.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
		listDate17.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
		listDate17.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
		listDate17.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 17), listDate17);

		var listDate18 = new List<FilmSchedule>();
		//  listDate18.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
		listDate18.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
		listDate18.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate18.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 18), listDate18);


		var listDate19 = new List<FilmSchedule>();
		// listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 16 });
		//listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 12 });
		listDate19.Add(new FilmSchedule { Time = "12:10", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
		listDate19.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
		listDate19.Add(new FilmSchedule { Time = "16:55", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
		listDate19.Add(new FilmSchedule { Time = "19:35", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
		dicList.Add(new DateTime(2017, 05, 19), listDate19);

		var listDate20 = new List<FilmSchedule>();
        listDate20.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
        listDate20.Add(new FilmSchedule { Time = "12:10", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Weekend });
        listDate20.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
        listDate20.Add(new FilmSchedule { Time = "17:15", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
        listDate20.Add(new FilmSchedule { Time = "19:15", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
		dicList.Add(new DateTime(2017, 05, 20), listDate20);

		var listDate21 = new List<FilmSchedule>();
        listDate21.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
        listDate21.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
        listDate21.Add(new FilmSchedule { Time = "15:05", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
        listDate21.Add(new FilmSchedule { Time = "17:05", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
        listDate21.Add(new FilmSchedule { Time = "19:25", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
        dicList.Add(new DateTime(2017, 05, 21), listDate21);

        return dicList;

    }

    private Dictionary<DateTime, List<FilmSchedule>> FilmSheduleHoChiMinh()
    {
        var listFilmNam = ListFilmName();

        List<Dictionary<DateTime, List<FilmSchedule>>> ddlfHn = new List<Dictionary<DateTime, List<FilmSchedule>>>();
        Dictionary<DateTime, List<FilmSchedule>> dicList = new Dictionary<DateTime, List<FilmSchedule>>();

        var listDate13 = new List<FilmSchedule>();
            // listDate13.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 16 });
            listDate13.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
            // listDate13.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 16 });
            //listDate13.Add(new FilmSchedule { Time = "12:30", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 16 });
            // listDate13.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = 16 });
            dicList.Add(new DateTime(2017, 05, 13), listDate13);

            var listDate14 = new List<FilmSchedule>();
            listDate14.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
            listDate14.Add(new FilmSchedule { Time = "12:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
            listDate14.Add(new FilmSchedule { Time = "15:25", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
            listDate14.Add(new FilmSchedule { Time = "17:45", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
            listDate14.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 14), listDate14);

            var listDate15 = new List<FilmSchedule>();

            // listDate15.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
            listDate15.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
            listDate15.Add(new FilmSchedule { Time = "16:25", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
            listDate15.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 15), listDate15);

            var listDate16 = new List<FilmSchedule>();
            // listDate16.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 12 });
            listDate16.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
            listDate16.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
            listDate16.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 16), listDate16);

            var listDate17 = new List<FilmSchedule>();
            // listDate17.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 12 });
            listDate17.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
            listDate17.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
            listDate17.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 17), listDate17);

            var listDate18 = new List<FilmSchedule>();
            //  listDate18.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
            listDate18.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
            listDate18.Add(new FilmSchedule { Time = "16:35", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
            listDate18.Add(new FilmSchedule { Time = "19:05", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 18), listDate18);


            var listDate19 = new List<FilmSchedule>();
            //    listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 16 });
            //listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
            listDate19.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
            listDate19.Add(new FilmSchedule { Time = "15:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
            listDate19.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
            listDate19.Add(new FilmSchedule { Time = "20:20", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 19), listDate19);

            var listDate20 = new List<FilmSchedule>();
            listDate20.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "14:50", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 20), listDate20);

            var listDate21 = new List<FilmSchedule>();
            listDate21.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "12:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "15:20", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 21), listDate21);

            var listDate22 = new List<FilmSchedule>();
            // listDate22.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 12 });
            listDate22.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
            listDate22.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
            listDate22.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 22), listDate22);

            var listDate23 = new List<FilmSchedule>();
            //  listDate23.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
            listDate23.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
            listDate23.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
            listDate23.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 23), listDate23);

            var listDate24 = new List<FilmSchedule>();
            //  listDate24.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 12 });
            listDate24.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
            listDate24.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
            listDate24.Add(new FilmSchedule { Time = "18:50", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 24), listDate24);

            var listDate25 = new List<FilmSchedule>();
            // listDate25.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
            listDate25.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = TotalL });
            listDate25.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
            listDate25.Add(new FilmSchedule { Time = "19:05", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 25), listDate25);

            var listDate26 = new List<FilmSchedule>();
            // listDate26.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
            listDate26.Add(new FilmSchedule { Time = "11:50", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = TotalL });
            listDate26.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = TotalL });
            listDate26.Add(new FilmSchedule { Time = "16:35", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = TotalL });
            listDate26.Add(new FilmSchedule { Time = "19:15", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = TotalL });
            dicList.Add(new DateTime(2017, 05, 26), listDate26);


            var listDate27 = new List<FilmSchedule>();
            listDate27.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
            listDate27.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Weekend });
            listDate27.Add(new FilmSchedule { Time = "14:50", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            listDate27.Add(new FilmSchedule { Time = "17:10", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            listDate27.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 27), listDate27);


            var listDate28 = new List<FilmSchedule>();
            listDate28.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            listDate28.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
            listDate28.Add(new FilmSchedule { Time = "15:05", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            listDate28.Add(new FilmSchedule { Time = "17:05", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
            listDate28.Add(new FilmSchedule { Time = "19:25", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 28), listDate28);

        return dicList;

    }
  
}

public class FilmSchedule
{
    public string Time { get; set; }
    public int FilmId { get; set; }
    public string FilmName { get; set; }
    public int Total { get; set; }
    public int TotalRegister { get; set; }
}