﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS.master" AutoEventWireup="true" CodeFile="modelTV.aspx.cs" Inherits="modelTV" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <section class="content-header">
        <h1>
        DANH SÁCH Model TV
        <small></small>
        </h1>
    </section>    
    <section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">             
              
          </h3>
        <div class="box-tools">
            <form method="get">
            <div class="input-group">                                             
            </div>            
            </form>
         </div>
        </div><!-- /.box-header -->
        <div class="box-body">

          <table class="table table-bordered table-hover js-order-data">
            <thead>
              <tr>                                 
				<th>STT</th>
                <th>Mẫu</th>                               
                <th>Số lượng</th>
                <th>Đã phát</th>
                <th>Còn Lại</th>                               
              </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rptLists" runat="server">
                    <ItemTemplate>
              <tr>                                 
				<td><%# Eval("Id") %></td>
                <td><%# Eval("Name") %></td>
                <td><%# Eval("Amount") %></td>
                <td><%# Eval("Delivery") %></td>                             
                <td><%# (int)Eval("Amount")-(int)Eval("Delivery") %></td>                             
              </tr>
                </ItemTemplate>
                </asp:Repeater>
            </tbody>
          </table>

        </div><!-- /.box-body -->        
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->

</asp:Content>

