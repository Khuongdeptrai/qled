﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class library_add : System.Web.UI.Page
{
    Database db = new Database();
    public int intId = 0;
    public DataRow dtRow = null;    
    public string dir_photo = Common.UploadDir()+"gallery/";
    public Messages message = new Messages();
    public string formhash = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }       
        FormHash objFormHash = new FormHash();
        message.Message = "";
        if (Request.HttpMethod=="POST")
        {
            objFormHash.formKey = Request.Form["Formhash"] != null ? Common.CleanValue(Request.Form["Formhash"]) : "";
            if (!objFormHash.validate())
            {
                message.FormHash = objFormHash.getFormKey();                
                message.Message = "Thông tin không hợp lệ. Vui lòng thử lại";
            }
            else
            {
                Insert();
            }
        }
        formhash = objFormHash.printFormHash();        
    }
    protected void Insert()
    {
        string Name = Request.Form["Name"] != null ? Common.CleanValue(Request.Form["Name"]) : "";
        string Content = Request.Form["Content"] != null ? Common.CleanValue(Request.Form["Content"]) : "";
        int Status = Request.Form["Status"] != null ? Common.ParseInt(Request.Form["Status"]) : 0;        
        string thumbnailOld = "";
        string cityName = Request.Form["city"] != null ? Common.CleanValue(Request.Form["city"]) : "";
        int OrderId = Request.Form["OrderId"] != null ? Common.ParseInt(Request.Form["OrderId"]) : 0;
        int WeekId = Request.Form["WeekId"] != null ? Common.ParseInt(Request.Form["WeekId"]) : 0;
        if (Name == "")
        {
            message.Message = "Vui lòng nhập Họ và Tên";
        }
        else
        {
            string listGallery = "";
            if (Request.Files.Count > 0)
            {
                message = UploadMultiple();
                if (message.Status == 1)
                {
                    listGallery = message.Content;
                    HttpPostedFile fileThumb = Request.Files["Thumbnail"];
                    if (fileThumb != null && fileThumb.ContentLength > 0)
                    {

                        Random rand = new Random();                        
                        if (Common.CheckExtension(fileThumb, Common.allowedExtPic))
                        {
                            string ext = Path.GetExtension(fileThumb.FileName).ToLower();
                            if (thumbnailOld != "")
                            {
                                DeleteFiles(thumbnailOld);
                            }
                            if (!File.Exists(dir_photo + "" + fileThumb.FileName))
                            {
                                thumbnailOld = Common.Slug(Path.GetFileNameWithoutExtension(fileThumb.FileName)).ToLower() + ext;
                            }
                            else
                            {
                                thumbnailOld = Common.Slug(Path.GetFileNameWithoutExtension(fileThumb.FileName)).ToLower() + "_" + rand.Next(0, 100) + "" + ext;
                            }
                            fileThumb.SaveAs(dir_photo + "" + thumbnailOld);
                        }
                        else
                        {
                            message.Message = "Vui lòng chỉ upload những định dạng hình ảnh "+ String.Join(",", Common.allowedExtPic); ;
                        }
                    }
                }
            }
            if (string.IsNullOrWhiteSpace(message.Message))
            {
                Dictionary<string, object> dataInsert = new Dictionary<string, object>();
                dataInsert.Add("Name", Name);
                dataInsert.Add("Content", Content);
                dataInsert.Add("Status", Status);
                dataInsert.Add("Thumbnail", thumbnailOld);
                dataInsert.Add("Slug", Common.Slug(Name));
                dataInsert.Add("Gallery", listGallery);
                dataInsert.Add("City", cityName);
                dataInsert.Add("OrderId", OrderId);
                dataInsert.Add("WeekId", WeekId);
                intId = db.InsertTable("Feedback", dataInsert);
                if (intId > 0)
                {
                    message.Status = 1;
                    message.Message = "Thông tin được lưu thành công ";
                    Response.Redirect(Common.Webroot + "library-edit.aspx?id=" + intId + "&action=edit");
                    Response.End();
                }
                else
                {
                    message.Message = db.strError;
                }
            }
        }

    }
    protected Messages UploadMultiple()
    {
        Messages msg = new Messages();
        msg.Content = "";
        msg.Status = 1;
        string comas = "";
        int i = 0;
        bool isOk = true;
        foreach (string str in Request.Files.AllKeys)
        {
            if (str == "images")
            {
                HttpPostedFile file = Request.Files[i];
                if (file != null && file.ContentLength > 0)
                {
                    if (Common.CheckExtension(file, Common.allowedExtPic))
                    {
                        string ex = Path.GetExtension(file.FileName).ToLower();
                        string fname = DateTime.Now.Ticks + "_" + i;
                        file.SaveAs(dir_photo + "" + fname + ex);
                        msg.Content += comas + "" + fname + "" + ex;
                        comas = ",";
                    }
                    else
                    {
                        msg.Status = 0;
                        msg.Message = "Vui lòng chỉ upload những định dạng hình ảnh " + String.Join(",", Common.allowedExtPic);
                        isOk = false;
                        break;
                    }
                }
            }
            i = i + 1;
        }
        if (!isOk)
        {
            foreach (string img in msg.Content.Split(','))
            {
                if (img != null)
                {
                    DeleteFiles(img);
                }
            }
        }
        else
        {
            msg.Content = msg.Content.TrimEnd(new char[] { ',' });
        }
        return msg;
    }


    protected void DeleteFiles(string filename)
    {
        if (System.IO.File.Exists(dir_photo + "" + filename))
            System.IO.File.Delete(dir_photo + "" + filename);
        if (System.IO.File.Exists(dir_photo + "thumb_" + filename))
            System.IO.File.Delete(dir_photo + "thumb_" + filename);
    }
}