using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Net;


/// <summary>
/// Summary description for manager
/// </summary>
public class Sendmail
{
    public string FromEmail = "" + ConfigurationManager.AppSettings["SENDER_NAME"] + " <" + ConfigurationManager.AppSettings["SMTP_USER"] + ">";
    public string ToEmail = "";
    public string Subject = "";
    public string CCEmail = "";
    public string Error = "";
    public Sendmail()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string Send(Dictionary<string, object> Data, string Template)
    {        
        string result = string.Empty;
        Template = GetMailBody(Data, Template);
        try
        {
            FromEmail = Data.ContainsKey("FromEmail") ? Data["FromEmail"].ToString() : FromEmail;
            ToEmail = Data.ContainsKey("ToEmail") ? Data["ToEmail"].ToString() : ToEmail;
            Subject = Data.ContainsKey("Subject") ? Data["Subject"].ToString() : Subject;            
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["IS_SMTP"]))
            {
                System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
                mailMessage.From = new MailAddress(FromEmail);
                if (ToEmail.Contains(";") || ToEmail.Contains(","))
                {
                    string[] listEmail = ToEmail.Split(new char[] { ';', ',' });
                    foreach (string str in listEmail)
                    {
                        mailMessage.To.Add(new MailAddress(str));
                    }
                }
                else
                {
                    mailMessage.To.Add(new MailAddress(ToEmail));
                }
                mailMessage.Subject = Subject;
                mailMessage.Body = Template;
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = System.Text.UnicodeEncoding.UTF8;
                if (CCEmail != null && CCEmail != "")
                    mailMessage.CC.Add(new MailAddress(CCEmail));

                System.Net.Mail.SmtpClient client = new SmtpClient();
                client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTP_USER"], ConfigurationManager.AppSettings["SMTP_PASSWORD"]);          
                client.Host = ConfigurationManager.AppSettings["SMTP_SERVER"];//"smtp.gmail.com";
                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_PORT"]);
                client.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SMTP_SSL"]);                
                client.Send(mailMessage);
                result = "okie SMTP";
            }
            else
            {                
                System.Web.Mail.MailMessage mailMessage = new System.Web.Mail.MailMessage();
                mailMessage.To = ToEmail;
                mailMessage.From = FromEmail;
                if (CCEmail != null && CCEmail != "")
                    mailMessage.Cc = CCEmail;
                mailMessage.Subject = Subject;
                mailMessage.Body = Template;
                mailMessage.Priority = System.Web.Mail.MailPriority.High;
                mailMessage.BodyFormat = System.Web.Mail.MailFormat.Html;
                mailMessage.BodyEncoding = System.Text.UnicodeEncoding.UTF8;
                System.Web.Mail.SmtpMail.Send(mailMessage);
                result = "okie";
            }
        }
        catch (Exception ex)
        {
            Error = ex.Message;
            result = Error;
        }
        return result;    
    }

    public string SendGemSamSung(Dictionary<string, object> Data, string Template)
    {
        string result ="", FromName = string.Empty;
        Template = GetMailBody(Data, Template);		

        //FromEmail = Data.ContainsKey("FromEmail") ? Data["FromEmail"].ToString() : FromEmail;
        ToEmail = Data.ContainsKey("ToEmail") ? Data["ToEmail"].ToString() : ToEmail;
        Subject = Data.ContainsKey("Subject") ? Data["Subject"].ToString() : Subject;  
		FromEmail = ConfigurationManager.AppSettings["SMTP_USER"];
		FromName = ConfigurationManager.AppSettings["SENDER_NAME"];


        Uri uriStr = new Uri("http://gems.samsung.com/ems/SemsEtaSend.do");
        string strEmail = String.Format("mail_code={0}&from_mail={1}&to_mail={2}&from_name={3}&to_name={4}&subject={5}&message={6}","01", FromEmail, ToEmail, FromName, ToEmail,Subject, HttpContext.Current.Server.UrlEncode(Template) );        
        HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(uriStr + "?" + strEmail);
        objRequest.ServicePoint.Expect100Continue = false;
        objRequest.Method = WebRequestMethods.Http.Get;
        objRequest.Timeout = 80000; // 60 seconds in milliseconds
        objRequest.ContentType = "application/x-www-form-urlencoded";
        objRequest.Credentials = CredentialCache.DefaultCredentials;
        try
        {
            HttpWebResponse response = (HttpWebResponse)objRequest.GetResponse();
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream, System.Text.Encoding.UTF8);
            result = readStream.ReadToEnd();            
            response.Close();
            readStream.Close();
        }
        catch (Exception ex) {
            result = ex.Message;
        }
        return result;
    }
    public string GetMailBody(Dictionary<string, object> data, string Template)
    {
        TextReader tr = new StreamReader(Template);
        String strMailBody = tr.ReadToEnd();
        tr.Close();
        foreach (KeyValuePair<string, object> kVal in data)
        {
            strMailBody = strMailBody.Replace("{" + kVal.Key + "}", ParseString(kVal.Value));
        }
        return strMailBody;
    }
    private string ParseString(object obj){
        try{
            return obj.ToString();
        }
        catch(Exception Exception){
            
        }
        return "";
    }   
}