﻿using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System;

public class Paging
{
    public string Next = "Next";
    public string Prev = "Previous";
    public string First = "First";
    public string Last = "Last";
    public int maxpage = 5;
    public string EventClick = "";
    public string Sperator = "";
    public string searchtext = "";
    public string BaseUrl = "";
    public Paging()
    {

    }
    public string multi(int num, int perpage, int curpage, string mpurl)
    {
        curpage = curpage <= 0 ? 1 : curpage;
        int page = maxpage;
        string multipage = "";
        int realpages = 1;
        int from = 0, to = 1;
        if (num > perpage)
        {
            int offset = 2;
            realpages = num / perpage + ((num % perpage > 0) ? 1 : 0);
            int pages = realpages;//maxpage<realpages ? maxpage : realpages;            
            if (page > pages)
            {
                from = 1;
                to = pages;
            }
            else
            {
                from = curpage - offset;
                to = from + page - 1;
                if (from < 1)
                {
                    to = curpage + 1 - from;
                    from = 1;
                    if (to - from < page)
                    {
                        to = page;
                    }
                }
                else if (to > pages)
                {
                    from = pages - page + 1;
                    to = pages;
                }
            }
            multipage = (curpage - offset > 1 && pages > page && pages > maxpage && this.First != "none" ? "<a " + replace_page(EventClick, 1) + " target=\"_self\" href=\"" + replace_page(mpurl, 1) + "\" class=\"first\">" + First + "</a>" : "") +
                ((curpage > 1 && pages > maxpage) ? "<a " + replace_page(EventClick, curpage - 1) + " target=\"_self\"  href=\"" + replace_page(mpurl, curpage - 1) + "\" class=\"prev\">" + Prev + "</a>" : "");
            for (int i = from; i <= to; i++)
            {
                multipage += i == curpage ? "<span>" + i + "</span>" + (i < to ? Sperator : "") : "<a " + replace_page(EventClick, i) + " target=\"_self\" href=\"" + replace_page(mpurl, i) + "\">" + i + "</a>" + (i == to ? "" : Sperator);
            }
            multipage += (curpage < pages && pages > maxpage ? "<a " + replace_page(EventClick, curpage + 1) + " target=\"_self\" href=\"" + replace_page(mpurl, curpage + 1) + "\" class=\"next\">" + Next + "</a>" : "") +
                ((to < pages && pages > maxpage && this.Last != "none") ? "<a " + replace_page(EventClick, pages) + " target=\"_self\" href=\"" + replace_page(mpurl, pages) + "\" class=\"last\">" + Last + "</a>" : "");

            //multipage = multipage.Length > 0 ? ("<em>&nbsp;" + realpages + "&nbsp;</em>" + multipage) : "";
        }
        return multipage;
    }
    public string replace_page(string url = "", int page = 1)
    {
        url = url.Replace("%page%", page.ToString());
        url = url.Replace("%25page%25", page.ToString());
        return url;

    }
    /*
    <summary>
    Gets the markup for an advanced paging control.
    </summary>
    <param name="pagesToOutput">The quantity of numbered page links to output.</param>
    <param name="key">An arbitary key value to include with the page link to more uniquely identify the querystring item for paging.</param>
    <param name="currentPage">The current page we’re on.</param>
    <param name="pageCount">The total number of pages.</param>
    <returns></returns>
   */
    public string getPagingAdvanced(int pagesToOutput, string key, int currentPage, int totalRecord, int perpage)
    {

        if (totalRecord <= perpage) return "";

        //The number of page numbers to output should be odd so there can be an even number either side of the current page.
        if (pagesToOutput % 2 != 0)
        {
            pagesToOutput++;
        }
        int pageCount = totalRecord / perpage + ((totalRecord % perpage > 0) ? 1 : 0); //Half the number of pages to output, this is the quantity either side.
        int pagesToOutputHalfed = pagesToOutput / 2; //return pagesToOutputHalfed.ToString();
        //Url for each page.
        string pageUrl = getPageUrl(key, currentPage, totalRecord) + BaseUrl;
        //The first page number to output.
        int startPageNumbersFrom = currentPage - pagesToOutputHalfed;
        //The last page number to output.
        int stopPageNumbersAt = currentPage + pagesToOutputHalfed; ;

        StringBuilder output = new StringBuilder();

        //Paging container.
        output.Append("<ul class=\"paginator\">");

        //Do we need first and previous links?
        if (currentPage > 1)
        {
            output.Append("<li class=\"first\"><a href=\"" + string.Format(pageUrl, 1) + "\">" + First + "</a></li>");
            output.Append("<li class=\"previous\"><a href=\"" + string.Format(pageUrl, currentPage - 1) + "\">" + Prev + "</a></li>");
        }

        //Make sure we haven’t gone less than the first page with our first page to count from.
        if (startPageNumbersFrom < 1)
        {
            startPageNumbersFrom = 1;
            //As page numbers are starting at one, output an even number of pages.
            stopPageNumbersAt = pagesToOutput;
        }

        //Make sure the stop page number isn’t after the last available page.
        if (stopPageNumbersAt > pageCount)
        {
            stopPageNumbersAt = pageCount;
        }

        //Make sure we’re showing a full page range.
        if ((stopPageNumbersAt - startPageNumbersFrom) < pagesToOutput)
        {
            startPageNumbersFrom = stopPageNumbersAt - pagesToOutput;
            if (startPageNumbersFrom < 1)
            {
                startPageNumbersFrom = 1;
            }
        }

        //Output indicator that there is a previous page range.
        if (startPageNumbersFrom > 1)
        {
            //Output indicator that there is a previous page range.
            output.Append("<li class=\"previousPageRange\"><a href=\"" + string.Format(pageUrl, startPageNumbersFrom - 1) + "\">&hellip;</a></li>");
        }

        //Loop through our page range.
        for (int i = startPageNumbersFrom; i <= stopPageNumbersAt; i++)
        {
            if (currentPage == i)
                output.Append("<li class=\"selected on\">" + i.ToString() + "</li>");
            else
                output.Append("<li><a href=\"" + string.Format(pageUrl, i) + "\">" + i.ToString() + "</a></li>");

        }

        //Output indicator that there is a next page range.
        if (stopPageNumbersAt < pageCount)
        {
            output.Append("<li class=\"nextPageRange\"><a href=\"" + string.Format(pageUrl, stopPageNumbersAt + 1) + "\">&hellip;</a></li>");
        }

        //Output next and last links
        if (currentPage != pageCount)
        {
            output.Append("<li class=\"next\"><a href=\"" + string.Format(pageUrl, currentPage + 1) + "\">" + Next + "</a></li>");
            output.Append("<li class=\"last\"><a href=\"" + string.Format(pageUrl, pageCount) + "\">" + Last + "</a></li>");
        }

        return output.ToString();
    }

    /*
     Returns URL for paging.
     </summary>
     <param name="key"></param>
     <param name="currentPage"></param>
     <returns></returns>
    */
    protected string getPageUrl(string key, int currentPage, int total)
    {
        string pageUrl = "?";
        string comas = "";
        foreach (string strKey in HttpContext.Current.Request.QueryString)
        {
            if (strKey != "key" && strKey != "total" && strKey != "page" && strKey != "fromdate" && strKey != "todate")
            {
                pageUrl += comas + "" + strKey + "=" + HttpContext.Current.Request.QueryString[strKey];
                comas = "&";
            }
        }
        pageUrl += "&" + key + "page={0}&total=" + total;
        if (searchtext != "")
        {
            searchtext = Regex.Replace(searchtext, @"[\s-]+", "+").Trim();
            pageUrl += "&key=" + searchtext;
        }
        return pageUrl;

    }
}