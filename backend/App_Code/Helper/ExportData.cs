﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;

/// <summary>
/// Summary description for ExportData
/// </summary>
public class ExportData
{    
    public DataTable dtList = new DataTable();
    private HSSFWorkbook hssfworkbook;
	public ExportData()
	{        
		//
		// TODO: Add constructor logic here
		//
	}
    protected void ExportExcel(string fileName, string Subject)
    {        
        HttpContext context = HttpContext.Current;        
        context.Response.ContentType = "application/vnd.ms-excel";
        context.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
        context.Response.Clear();
        //InitializeWorkbook
        hssfworkbook = new HSSFWorkbook();
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "Report";
        hssfworkbook.DocumentSummaryInformation = dsi;
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = Subject;
        hssfworkbook.SummaryInformation = si;
        //End
        DataSet dsSQL = new DataSet();       
        dsSQL.Tables.Add(dtList);

        GenerateData(dsSQL);

        //Write the stream data of workbook
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);

        context.Response.Clear();
        context.Response.BinaryWrite(file.GetBuffer());
        context.Response.End();
        context.Response.Close();
        context.Response.Flush();
    }
    private void GenerateData(DataSet dslist)
    {
        for (int pkv = 0; pkv < dslist.Tables.Count; pkv++)
        {
            DataTable excellist = dslist.Tables[pkv];
            IRow nonHeaderRow = null;
            IRow headerRow = null;
            ISheet sheet1 = hssfworkbook.CreateSheet(excellist.TableName);

            //set header font
            NPOI.SS.UserModel.IFont font = hssfworkbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.BOLD;
            font.Color = (short)FontColor.RED;

            ICellStyle style1 = hssfworkbook.CreateCellStyle();
            style1.SetFont(font);

            headerRow = sheet1.CreateRow(0);

            //header text appears in first row of workbook

            for (int pk = 0; pk < excellist.Columns.Count; pk++)
            {
                ICell cell = headerRow.CreateCell(pk);
                cell.SetCellValue(excellist.Columns[pk].ColumnName);
                cell.CellStyle = style1;
            }

            for (int j = 0; j < excellist.Rows.Count; j++)
            {
                nonHeaderRow = sheet1.CreateRow(j + 1);                
                for (int k = 0; k < excellist.Columns.Count; k++)
                {
                    nonHeaderRow.CreateCell(k).SetCellValue(HttpContext.Current.Server.HtmlDecode( excellist.Rows[j][k].ToString()));
                }
            }

            for (int pk = 0; pk < excellist.Columns.Count; pk++)
            {
                sheet1.AutoSizeColumn(pk);
            }
        }
    }

    public void ExportGridView(string fileName, string exportType, string subject)
    {
        switch (exportType.ToLower())
        {
            case "xls":
                ExportExcel(fileName, subject);
                break;
        }
    }    
}