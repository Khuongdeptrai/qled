﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for Helper
/// </summary>
public class Helper
{
    private string _securityKey = "MAKV2SPBNI99212";
	public Helper()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string Encrypt(string clearText)
    {
        string EncryptionKey = this._securityKey;
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    public string Decrypt(string cipherText)
    {
        string EncryptionKey = this._securityKey;
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    public string GetAccessToken(int maxSize)
    {
        char[] chars = new char[62];
        chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
        byte[] data = new byte[1];
        using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
        {
            crypto.GetNonZeroBytes(data);
            data = new byte[maxSize];
            crypto.GetNonZeroBytes(data);
        }
        StringBuilder result = new StringBuilder(maxSize);
        foreach (byte b in data)
        {
            result.Append(chars[b % (chars.Length)]);
        }
        return result.ToString();
    }

    public string GetYoutubeID(string youTubeUrl)
    {
        if (string.IsNullOrWhiteSpace(youTubeUrl)) return "";
        string strRegex = "^(?:https?\\:\\/\\/)?(?:www\\.)?(?:youtu\\.be\\/|youtube\\.com\\/(?:embed\\/|v\\/|watch\\?v\\=))([\\w-]{5,20})(?:[\\&\\?\\#].*?)*?(?:[\\&\\?\\#]t=([\\dhm]+s))?$";
        if (youTubeUrl.Contains("youtu.be"))
        {
            strRegex = @"youtu(?:\.be)/([a-zA-Z0-9-_]+)";
        }
        System.Text.RegularExpressions.Regex check = new System.Text.RegularExpressions.Regex(strRegex, System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
        Match regexMatch = check.Match(youTubeUrl);
        if (regexMatch.Success)
        {
            return regexMatch.Groups[1].ToString();

        }
        else if (youTubeUrl.Contains("http://") || youTubeUrl.Contains("https://"))
        {
            Uri myUri = new Uri(youTubeUrl);
            var query = HttpUtility.ParseQueryString(myUri.Query);
            if (query.Get("v") != null)
            {
                youTubeUrl = query.Get("v").Trim();
            }
        }
        return string.Empty;
    }

    public bool IsImage(HttpPostedFile postedFile)
    {
        string _extension = Path.GetExtension(postedFile.FileName).ToString().Trim().ToLower();
        if (_extension != ".jpg" && _extension != ".png" && _extension != ".gif" && _extension != ".jpeg")
        {
            return false;
        }
        return true;
    }

    public string StringToSlug(object strTxt, int maxLength = 120, string sperator = "-")
    {
        if (strTxt == null || string.IsNullOrEmpty(strTxt.ToString())) return "";
        string str = strTxt.ToString().ToLower();
        for (int i = 1; i < VietNamChar.Length; i++)
        {
            for (int j = 0; j < VietNamChar[i].Length; j++)
                str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
        }
        str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
        str = Regex.Replace(str, @"[\s-]+", " ").Trim();
        if (maxLength > 0)
            str = str.Substring(0, str.Length <= maxLength ? str.Length : maxLength).Trim();

        str = Regex.Replace(str, @"\s", sperator);
        return str;
    }

    private readonly string[] VietNamChar = new string[]
    {
	        "aAeEoOuUiIdDyY",
	        "ăáàạảãâấầậẩẫăắằặẳẵ",
	        "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",            
	        "éèẹẻẽêếềệểễ",
	        "ÉÈẸẺẼÊẾỀỆỂỄ",
	        "óòọỏõôốồộổỗơớờợởỡ",
	        "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
	        "úùụủũưứừựửữ",
	        "ÚÙỤỦŨƯỨỪỰỬỮ",
	        "íìịỉĩ",
	        "ÍÌỊỈĨ",
	        "đ",
	        "Đ",
	        "ýỳỵỷỹ",
	        "ÝỲỴỶỸ"
    };

    private static bool UrlExists(string url)
    {
        try
        {
            new System.Net.WebClient().DownloadData(url);
            return true;
        }
        catch (System.Net.WebException e)
        {
            if (((System.Net.HttpWebResponse)e.Response).StatusCode == System.Net.HttpStatusCode.NotFound)
                return false;
            else
                throw;
        }
    }
    public static void DownloadImage(string url, string path)
    {
        try
        {
            byte[] data;
            using (WebClient client = new WebClient())
            {
                data = client.DownloadData(url);
            }
            File.WriteAllBytes(path, data);
        }
        catch (Exception ex)
        {

        }
    }
    public string DownloadYoutubeThumb(string youtubeId , string _uploadDir = "data/uploads/temp/" ) {
        string fileName = string.Empty;
        if (youtubeId != string.Empty)
        {
            fileName = youtubeId+"-"+Guid.NewGuid().ToString() + ".jpg";
            if (UrlExists("http://i1.ytimg.com/vi/" + youtubeId + "/maxresdefault.jpg"))
            {
                DownloadImage("http://i1.ytimg.com/vi/" + youtubeId + "/maxresdefault.jpg", _uploadDir + fileName);
            }
            else
            {
                DownloadImage("http://i1.ytimg.com/vi/" + youtubeId + "/hqdefault.jpg", _uploadDir + fileName);
            }

        }
        return fileName;
    }

    public string UploadFile(HttpPostedFile _fileImage , string pathDir = "")
    {
        string _fileName = string.Empty;
        if (_fileImage != null && _fileImage.ContentLength > 0 )
        {
            string _realName = StringToSlug(Path.GetFileNameWithoutExtension(_fileImage.FileName));
            _realName.Replace("%00", "");
            _realName.Replace("%zz", "");

            _fileName = DateTime.Now.ToString("MM-dd-HH-mm-ss-ffff") + "-" + _realName + Path.GetExtension(_fileImage.FileName);
            _fileImage.SaveAs( Common.UploadDir(pathDir) + _fileName);
        }
        return _fileName;
    }

}