﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FormKey
/// </summary>
public class FormHash
{
    public string formKey;
    private string old_formKey;

    public FormHash()
	{
		if(HttpContext.Current.Session["formhash"]!=null)
        {
            old_formKey = HttpContext.Current.Session["formhash"].ToString();
        }
	}
    public string printFormHash()
    {
        formKey = getFormKey();
        //Output the form key
        return "<input type='hidden' class='formhash' name='formhash' id='formhash' value='" + formKey + "' />";
    }
    public string getFormKey(){
        
        string ip = Common.GetIPAddress();        
        string myKey = Guid.NewGuid().ToString().Replace("-", "");
        //Store the form key in the session
        HttpContext.Current.Session["formhash"] = myKey;
        return myKey;
         
    }
    public bool validate()
    {
        if (formKey == old_formKey)
        {
            return true;
        }
        else
        {     
            return false;
        }
    }
}