﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;

/// <summary>
/// Summary description for Captcha
/// </summary>
public class Captcha
{	
    public string Value { get; set; }
    public Image CaptchaImg { get; set; }
    public bool CaseSensitive { get; set; }
    public int Length = 4;
    public int width = 106;
    public int height = 46;
    public int FontSize = 13;
    public int pos = 5;
    private Random rndm;

    public bool IsValid(string input)
    {
        return CaseSensitive ? this.Value.Equals(input) : this.Value.Equals(input, System.StringComparison.OrdinalIgnoreCase);
    }

    public Captcha()
    {
        
    }
    public void Show()
    {
        this.rndm = new Random();
        char[] rmcarr = this.GetRandomCArr(Length);
        this.Value = new string(rmcarr);
        this.CreateImage(rmcarr);
        HttpContext.Current.Response.ContentType = "image/GIF";
        CaptchaImg.Save(HttpContext.Current.Response.OutputStream, ImageFormat.Gif);
        CaptchaImg.Dispose();

    }
    private void CreateImage(char[] rmcarr)
    {       
        Bitmap bmp = new Bitmap(width, height);
        Graphics gImage = Graphics.FromImage(bmp);
        gImage.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
        gImage.FillRectangle(Brushes.White, 0, 0, bmp.Width, bmp.Height);

        /*PrivateFontCollection fonts;
        FontFamily family = LoadFontFamily(HttpContext.Current.Server.MapPath("~/font/gothamrnd-light_c.ttf"), out fonts);
        */

        /*FontSize = (bmp.Width / Length);//get the font size with respect to length of the string;
        if (FontSize > 24)
            FontSize = 18;*/

        //Draw lines        
        //for (int i = 0; i < 200; i = i + 5)
        //{
        //    gImage.DrawLine(Pens.LightGray, new Point(0, i + 5), new Point(i + 5, 0));
        //    gImage.DrawLine(Pens.LightGray, new Point(0, 60 - (i + 5)), new Point(i + 5, 60));
        //}
        //Draw random text        
        for (int i = 0; i < rmcarr.Length; i++)
        {           
            PointF pf  = new PointF(10+pos + i * FontSize, pos + 8 );
            gImage.DrawString(rmcarr[i].ToString(), new Font(FontFamily.GenericSansSerif, FontSize, FontStyle.Regular), GetRandomBrushes(), pf);
        }
        this.CaptchaImg = bmp;
    }

    //Get random char array
    private char[] GetRandomCArr(int length)
    {   
        var mch = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var mcstr = Enumerable.Range(1, length).Select(a => mch[rndm.Next() % mch.Length]).ToArray();
        return mcstr;
    }

    //Get a random brush
    private Brush GetRandomBrushes()
    {
        var mbrsharr = new Brush[] { Brushes.Black};
        return mbrsharr[rndm.Next() % mbrsharr.Length];
    }
    private FontFamily LoadFontFamily(string fileName, out PrivateFontCollection _myFonts)
    {
        //IN MEMORY _myFonts point to the myFonts created in the load event 11 lines up.
        _myFonts = new PrivateFontCollection();//here is where we assing memory space to myFonts
        _myFonts.AddFontFile(fileName);//we add the full path of the ttf file
        return _myFonts.Families[0];//returns the family object as usual.
    }
}