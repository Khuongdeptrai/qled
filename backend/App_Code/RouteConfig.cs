﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Membership.OpenAuth;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace local_challenge
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.EnableFriendlyUrls();

            //Account
            routes.MapPageRoute("_LOGIN", "login.html", "~/Account/Login.aspx");
            routes.MapPageRoute("_LOGIN_FACEBOOK", "login-facebook.html", "~/Account/LoginFacebook.aspx");
            routes.MapPageRoute("_REGISTER", "register.html", "~/Account/Register.aspx");
            routes.MapPageRoute("_FORGETPASS", "forget-pass.html", "~/Account/ForgetPassword.aspx");
            routes.MapPageRoute("_LOGOUT_", "logout.html", "~/Account/Logout.aspx");
            routes.MapPageRoute("_CAPTCHA", "captcha.html", "~/Account/Captcha.aspx");
            routes.MapPageRoute("_ERROR", "404.html", "~/404.aspx");

            //PAGES
            routes.MapPageRoute("_FORGETPASS_", "quen-mat-khau", "~/forget-password.aspx");
            routes.MapPageRoute("_VERIFY_", "xac-nhan-tai-khoan", "~/verifyAccount.aspx");

            routes.MapPageRoute("_HOME_", "trang-chu", "~/Home.aspx");
            routes.MapPageRoute("_TERM_", "the-le-giai-thuong", "~/term.aspx");
            routes.MapPageRoute("_NEWS_", "cong-nghe-con-nguoi", "~/news.aspx");
            routes.MapPageRoute("_NEWS_DETAIL_", "cong-nghe-con-nguoi/{itemId}/{slug}", "~/news.detail.aspx");

            //routes.MapPageRoute("_TECHNOLOGY_", "cong-nghe-con-nguoi.html", "~/news.aspx");
            //routes.MapPageRoute("_TECHNOLOGY_DETAIL_", "cong-nghe-con-nguoi/{itemId}/{slug}.html", "~/news.detail.aspx");

            routes.MapPageRoute("_GALLERY_", "bo-suu-tap", "~/gallery.aspx");
            routes.MapPageRoute("_GALLERY_DETAIL_", "bo-suu-tap/{itemId}/{slug}", "~/gallery.detail.aspx");

            routes.MapPageRoute("_FEATURE_", "chia-se-noi-bat", "~/feature.idea.aspx");
            routes.MapPageRoute("_FEATURE_DETAIL_", "chia-se-noi-bat/{itemId}/{slug}", "~/feature.idea.detail.aspx");

            routes.MapPageRoute("_SUBMIT_IDEA_", "viet-y-tuong", "~/submit.idea.aspx");
            routes.MapPageRoute("_SUBMIT_THANKS_", "gui-y-tuong-thanh-cong", "~/submit.thanks.aspx");
            routes.MapPageRoute("_ACCOUNT_", "tai-khoan", "~/signin.aspx");

        }
    }
}