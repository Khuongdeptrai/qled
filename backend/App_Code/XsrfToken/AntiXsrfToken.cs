﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for XsrfToken
/// </summary>
public class AntiXsrfToken
{
    private List<XsrfToken> _listXsrfToken = new List<XsrfToken>();
    private string _xsrfTokenKey = "AntiXsrfToken";
    public AntiXsrfToken()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string AddXsrfToken(string _key)
    {
        XsrfToken _xsrfToken = new XsrfToken();
        _xsrfToken.key = _key;
        _xsrfToken.value = Guid.NewGuid().ToString().Replace("-", "");
        if (HttpContext.Current.Session[_xsrfTokenKey] != null)
        {
            _listXsrfToken = (List<XsrfToken>)HttpContext.Current.Session[_xsrfTokenKey];
            _listXsrfToken.Add(_xsrfToken);
        }
        else
        {
            _listXsrfToken.Add(_xsrfToken);            
        }
        HttpContext.Current.Session[_xsrfTokenKey] = _listXsrfToken;  
        return _xsrfToken.value;
    }

    public List<XsrfToken> ListXsrfToken()
    {
        return _listXsrfToken;
    }
    public void RemoveXsrfToken(string _key, string _token)
    {
        if (HttpContext.Current.Session[_xsrfTokenKey] != null)
        {
            _listXsrfToken = (List<XsrfToken>)HttpContext.Current.Session[_xsrfTokenKey];
            _listXsrfToken.RemoveAll(f => f.key == _key && f.value == _token);
            HttpContext.Current.Session[_xsrfTokenKey] = _listXsrfToken;  
        }
    }

    public bool ValidationXsrfToken(string _key , string _token)
    {
        if (HttpContext.Current.Session[_xsrfTokenKey] != null)
        {
            _listXsrfToken = (List<XsrfToken>)HttpContext.Current.Session[_xsrfTokenKey];
            return _listXsrfToken.Any(f => f.key == _key && f.value == _token);
        }
        return false;
    }
}

public class XsrfToken
{
    public string key { get; set; }
    public string value { get; set; }
}