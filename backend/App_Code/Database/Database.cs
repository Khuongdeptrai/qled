﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.Common;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for Database
/// </summary>
public class Database
{
    public string strDataProviderType;
    public string strConnectionString;
    public DbProviderFactory dbProviderFactory;
    public DbConnection dbConnection;
    public DbDataAdapter dbAdaptor;
    public DbCommand dbCommand;
    public int intPerpage = 10;

    public string strError = "";
	public Database()
	{
        try
        {
            strDataProviderType = ConfigurationManager.AppSettings["PROVIDER_TYPE"].ToString();
            strConnectionString = ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["CURRENT_DBSTRING"].ToString()].ToString();
            dbProviderFactory = DbProviderFactories.GetFactory(strDataProviderType);
            dbConnection = dbProviderFactory.CreateConnection();
            dbConnection.ConnectionString = strConnectionString;
            dbAdaptor = dbProviderFactory.CreateDataAdapter();
            dbCommand = dbProviderFactory.CreateCommand();
        }
        catch (Exception ex)
        {
            HttpContext.Current.Response.Write(ex.Message);
            HttpContext.Current.Response.End();
        }
	}
    public DbParameter CreateDBParameter(string strParamName, DbType type, object value)
    {

        DbParameter dbParameter = dbCommand.CreateParameter();
        dbParameter.DbType = type;
        dbParameter.ParameterName = strParamName;
        if (value == null)
        {
            dbParameter.Value = DBNull.Value;
        }
        else
        {
            if (value.GetType() == typeof(DateTime))
            {
                DateTime dt = Convert.ToDateTime(value);
                if (dt == DateTime.MinValue)
                {
                    dbParameter.Value = DBNull.Value;
                }
                else
                {
                    dbParameter.Value = value;
                }
            }
            else
            {
                dbParameter.Value = value;
            }

        }
        return dbParameter;
    }
    public int Count(string sql)
    {
        int result = 0;
        System.Data.Common.DbDataReader dbDataReader;
        try
        {
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = sql;
            dbCommand.Connection = dbConnection;
            if (dbConnection.State == ConnectionState.Open)
                dbConnection.Close();
            dbConnection.Open();

            dbDataReader = dbCommand.ExecuteReader();
            dbDataReader.Read();
            if (dbDataReader.HasRows && !dbDataReader.IsDBNull(0))
            {
                result = dbDataReader.GetInt32(0);
            }
            dbDataReader.Close();
        }
        finally
        {
            dbCommand.Dispose();
            dbConnection.Close();
        }
        return result;
    }
    public string FirstColumn(string sql)
    {
        string result = "";
        System.Data.Common.DbDataReader dbDataReader;
        try
        {
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = sql;
            dbCommand.Connection = dbConnection;
            if (dbConnection.State == ConnectionState.Open)
                dbConnection.Close();
            dbConnection.Open();

            dbDataReader = dbCommand.ExecuteReader();
            dbDataReader.Read();
            if (dbDataReader.HasRows && !dbDataReader.IsDBNull(0))
            {
                result = dbDataReader[0].ToString();
            }
            dbDataReader.Close();
        }
        finally
        {
            dbCommand.Dispose();
            dbConnection.Close();
        }
        return result;
    }
    public DataTable Exec(string sql)
    {

        DataTable result = new DataTable();
        try
        {
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = sql;
            dbCommand.Connection = dbConnection;
            if (dbConnection.State==ConnectionState.Open)
                dbConnection.Close();
            dbConnection.Open();
            result.Load(dbCommand.ExecuteReader());

        }
        catch (Exception ex)
        {
            //HttpContext.Current.Response.Redirect(Common.Webroot);
            HttpContext.Current.Response.Write(ex.Message + "<br />" + sql);
            HttpContext.Current.Response.End();
        }
        finally
        {
            dbConnection.Close();
        }
        return result;
    }
    public bool Delete(string tablename, string where)
    {
        bool isdelete = false;
        try
        {
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = "DELETE FROM " + tablename + " WHERE " + where;
            dbCommand.Connection = dbConnection;
            dbConnection.Open();
            isdelete = Convert.ToBoolean(dbCommand.ExecuteNonQuery());

        }
        catch (Exception ex)
        {

        }
        finally
        {
            dbConnection.Close();
        }
        return isdelete;
    }

    public bool DeleteID(string tablename, string listid)
    {
        bool isdelete = false;
        try
        {
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = "DELETE FROM " + tablename + " WHERE ID IN (" + listid + ")";            
            dbCommand.Connection = dbConnection;
            dbConnection.Open();
            isdelete =  Convert.ToBoolean(dbCommand.ExecuteNonQuery());

        }
        catch (Exception ex)
        {
           
        }
        finally
        {
            dbConnection.Close();
        }
        return isdelete;
    }
    public int InsertTable(string tablename, Dictionary<string, object> data, bool identify = true)
    {
        string sql = "", insertkeysql = "", insertvaluesql = "", comma = "";
        int newid = 0;
        try
        {
            dbCommand.Parameters.Clear();
            foreach (KeyValuePair<string, object> kVal in data)
            {
                insertkeysql += comma + "[" + kVal.Key + "]";
                insertvaluesql += comma + "@" + kVal.Key + "";
                comma = " , ";
                if (kVal is DateTime)
                    dbCommand.Parameters.Add(CreateDBParameter("@" + kVal.Key, DbType.DateTime, kVal.Value));
                else 
                    dbCommand.Parameters.Add(CreateDBParameter("@" + kVal.Key, DbType.String, kVal.Value));
            }
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = "INSERT INTO " + tablename + " (" + insertkeysql + ") VALUES (" + insertvaluesql + ");" + (identify ? "SELECT SCOPE_IDENTITY();" : "");
            dbConnection.Open();
            dbCommand.Connection = dbConnection;
            if (identify)
            {
                newid = Convert.ToInt32(dbCommand.ExecuteScalar().ToString());
            }
            else
            {
                newid = dbCommand.ExecuteNonQuery();
            }

        }
        catch (Exception ex)
        {
            strError = ex.ToString();
            //HttpContext.Current.Response.Redirect(Common.Webroot);
            //HttpContext.Current.Response.Write(ex.ToString());
            //HttpContext.Current.Response.End();
        }
        finally
        {
            dbConnection.Close();
        }
        return newid;
    }
    public int UpdateTable(string sql)
    {
        int isUpdate = 0;        
        try
        {
            dbCommand.CommandType = CommandType.Text;
            dbCommand.Parameters.Clear();
            dbCommand.CommandText = sql;
            dbConnection.Open();
            dbCommand.Connection = dbConnection;
            isUpdate = Convert.ToInt32(dbCommand.ExecuteNonQuery());
        }        
        finally
        {
            dbConnection.Close();
        }
        return isUpdate;
    }
    public int UpdateTable(string tablename, Dictionary<string, object> data, Dictionary<string, object> wheresqlarr)
    {
        string setsql = "", comma = "",wheresql="";
        int newid = 0;
        try
        {
            dbCommand.Parameters.Clear();
            foreach (KeyValuePair<string, object> kVal in data)
            {
                setsql += comma + "" + kVal.Key + " = @" + kVal.Key;
                comma = ", ";
                dbCommand.Parameters.Add(CreateDBParameter("@" + kVal.Key, DbType.String, kVal.Value));
            } 
            /*Where */  
            comma = "";
            foreach (KeyValuePair<string, object> kvalue in wheresqlarr)
            {
                 wheresql += comma+""+kvalue.Key+" = @"+kvalue.Key;
                 comma =" AND ";
                 dbCommand.Parameters.Add(CreateDBParameter("@" + kvalue.Key, DbType.String, kvalue.Value));
            }
            dbCommand.CommandType = CommandType.Text;
            dbCommand.CommandText = "UPDATE " + tablename + " SET "+setsql+" WHERE "+wheresql;
            dbConnection.Open();
            dbCommand.Connection = dbConnection;            
            newid = Convert.ToInt32(dbCommand.ExecuteNonQuery());

        }
        catch (Exception ex)
        {
            strError = ex.ToString();
            //HttpContext.Current.Response.Write(ex.ToString());
            //HttpContext.Current.Response.End();
        }
        finally
        {
            dbConnection.Close();
        }
        return newid;
    }
    public int UpdateTable(string tablename, Dictionary<string, object> data, string wheresql)
    {
        string setsql = "", comma = "";
        int newid = 0;
        try
        {
            dbCommand.CommandType = CommandType.Text;
            dbCommand.Parameters.Clear();
            foreach (KeyValuePair<string, object> kVal in data)
            {
                setsql += comma + "[" + kVal.Key + "] = @" + kVal.Key;
                comma = ", ";
                dbCommand.Parameters.Add(CreateDBParameter("@" + kVal.Key, DbType.String, kVal.Value));
            }            
            dbCommand.CommandText = "UPDATE " + tablename + " SET " + setsql + " WHERE " + wheresql;
            dbConnection.Open();
            dbCommand.Connection = dbConnection;
            newid = Convert.ToInt32(dbCommand.ExecuteNonQuery());

        }
        catch (Exception ex)
        {
            strError = ex.ToString();
            //HttpContext.Current.Response.Redirect(Common.Webroot);            
        }
        finally
        {
            dbConnection.Close();
        }
        return newid;
    }
    public DataTable StoredProcedure(string strSP)
    {
        DataTable result = new DataTable();
        try
        {
            dbCommand.CommandText = strSP;
            dbCommand.CommandType = CommandType.StoredProcedure;
            dbCommand.Connection = dbConnection;
            if (dbConnection.State == ConnectionState.Open)
                dbConnection.Close();
            dbConnection.Open();
            result.Load(dbCommand.ExecuteReader());

        }
        catch (Exception ex)
        {
            HttpContext.Current.Response.Redirect(Common.Webroot);
            //HttpContext.Current.Response.Write(ex.Message);
            //HttpContext.Current.Response.End();
        }
        finally
        {
            dbConnection.Close();
            dbCommand.Dispose();
        }
        return result;
    }   

}