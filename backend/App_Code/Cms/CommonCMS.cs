﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CommonCMS
/// </summary>
public class CommonCMS
{
	public CommonCMS()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string RenderStatus(string _status = "0") {
        int status = Convert.ToInt32(_status);
        string resultHtml = "";
        switch (status)
        {
            case 1:
                resultHtml = "<span class=\"label label-success\">Approved</span>";
                break;
            case 0:
                resultHtml = "<span class=\"label label-warning\">Pending</span>";
                break;
            case -1:
                resultHtml = "<span class=\"label label-danger\">Denied</span>";
                break;
        }
        return resultHtml;
    }
}