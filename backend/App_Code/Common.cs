﻿using ImageResizer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
    public static string FacebookAppId = ConfigurationManager.AppSettings["FacebookAppId"];
    public static string FacebookAppSecret = ConfigurationManager.AppSettings["FacebookAppSecret"];    
    public static String[] allowedExtPic = { ".gif", ".png", ".jpeg", ".jpg" };
    public static string[] ListCity = new string[] { "Hà Nội", "TP HCM", "An Giang", "Bà Rịa - Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước", "Bình Thuận", "Cà Mau", "Cao Bằng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương", "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái", "Phú Yên", "Cần Thơ", "Đà Nẵng", "Hải Phòng" };
    public Common()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static string SiteUrl
    {
        get
        {
            HttpContext context = HttpContext.Current;
            string port = !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["WEBSITE_PORT"]) ? ":" + ConfigurationManager.AppSettings["WEBSITE_PORT"] : "";
            return (context.Request.Url.Scheme + "://" + context.Request.Url.Authority + "" + port + context.Request.ApplicationPath.TrimEnd('/'));
        }
    }
    public static string Webroot
    {
        get
        {
            HttpContext context = HttpContext.Current;
            return context.Request.ApplicationPath.TrimEnd('/') + '/';
        }
    }
    public static string Domain
    {
        get
        {
            HttpContext context = HttpContext.Current;
            string port = !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["WEBSITE_PORT"]) ? ":" + ConfigurationManager.AppSettings["WEBSITE_PORT"] : "";
            return context.Request.Url.Scheme + "://" + context.Request.Url.Authority + "" + port;
        }
    }
    public static string UploadUrl(string _type = "")
    {
        return ConfigurationManager.AppSettings["uploadUrl"] + (_type != string.Empty ? _type + "/" : "");
    }

    public static string UploadDir(string _type = "")
    {
        return ConfigurationManager.AppSettings["uploadDir"] + (_type != string.Empty ? _type + "/" : "");
    }
    public static string BaseUrl
    {
        get
        {
            HttpContext context = HttpContext.Current;
            if (context.Request.Browser.Type.ToUpper().Contains("IE") && Convert.ToDouble(context.Request.Browser.Version) < 10)
                return context.Request.Url.GetLeftPart(UriPartial.Authority) + Common.Webroot;
            return Common.Webroot;

        }
    }

    public static int ParseInt(object _value) {
        try
        {
            return Convert.ToInt32(_value);
        }
        catch
        {
            return 0;
        }
        
    }
    public static void WriteEnd(object _data)
    {
        HttpContext.Current.Response.Write(_data);
        HttpContext.Current.Response.End();
    }

    public static string CleanScripts(object text)
    {
        Regex ex = new Regex(@"<script[^>]*>[\s\S]*?</script>");
        string html = ex.Replace(text.ToString(), "");
        html = Regex.Replace(html, @"<script[^>]*>", "<!--");
        html = Regex.Replace(html, @"<\/script>", "-->");
        return ex.Replace(text.ToString() , "");
    }

    public static string CleanValue(object text)
    {
        if (text == null || string.IsNullOrEmpty(text.ToString())) return "";
        string strText = Regex.Replace(text.ToString(), @"<[(=^>]*>", string.Empty);
        //strText = HttpContext.Current.Server.HtmlEncode(strText);
        strText = strText.Replace("'", string.Empty);
        strText = strText.Replace("=", string.Empty);
		strText = Regex.Replace(strText, "<.*?>", String.Empty);
        strText = Regex.Replace(strText, @"</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n|\s)*?>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
        strText = Regex.Replace(strText, @"(;|\s)(exec|execute|select|insert|update|delete|create|alter|drop|rename|truncate|backup|restore)\s", string.Empty, RegexOptions.IgnoreCase);
        strText = Regex.Replace(strText, @"[*/]+", string.Empty);
        return strText;
    }

    public static string GetIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }
        return context.Request.ServerVariables["REMOTE_ADDR"];
    }
    public static bool freshcookie(int itemid, string cookieName, int timeout, bool ischeck = false)
    {
        try
        {
            bool isupdate = false;
            DateTime expireDate = DateTime.Now;
            //HttpContext.Current.Request.Cookies.Clear();
            HttpCookie itemCountCookie = HttpContext.Current.Request.Cookies["cookiepre" + cookieName];

            string CookieOld = "0";
            if (itemCountCookie != null)
                CookieOld = HttpContext.Current.Request.Cookies["cookiepre" + cookieName].Value;

            String[] itemidarr = CookieOld.Split(new char[] { '_' });
            foreach (string Val in itemidarr)
            {
                if (Val.ToString() == itemid.ToString())
                {
                    isupdate = true;
                    break;
                }
            }
            if (!isupdate && !ischeck)
            {
                //itemidarr[] = itemid.ToString();   
                HttpCookieCollection Cookies = HttpContext.Current.Response.Cookies;
                HttpCookie aCookie = new HttpCookie("cookiepre" + cookieName);
                aCookie.Expires = expireDate.AddSeconds(timeout);
                aCookie.Value = CookieOld + "_" + itemid;
                Cookies.Add(aCookie);
            }
            return isupdate;
        }
        catch (Exception ex)
        {
            return true;
        }
    }


    public static void GenerateImages(HttpPostedFile _fileImage, string _fileName)
    {
        Dictionary<string, string> versions = new Dictionary<string, string>();
        versions.Add("gallery", "width=500&height=500&mode=crop&format=jpg"); //Crop to square thumbnail
        versions.Add("large", "maxwidth=1000&maxheight=800&format=jpg"); //Fit inside 400x400 area, jpeg
        versions.Add("share", "width=600&height=315&mode=crop&format=jpg"); //Fit inside 1900x1200 area  

        string _dirUpload = HttpContext.Current.Server.MapPath("~/data/uploads/");
        foreach (string suffix in versions.Keys)
        {
            string uploadFolder = _dirUpload +  suffix + "/";
            if (!Directory.Exists(uploadFolder)) Directory.CreateDirectory(uploadFolder);

            string fileName = Path.Combine(uploadFolder, _fileName);
            ImageBuilder.Current.Build(_fileImage, fileName, new ResizeSettings(versions[suffix]), false, true);
        }

    }

    public static void GenerateImagesYoutube(string _fileName)
    {
        Dictionary<string, string> versions = new Dictionary<string, string>();
        versions.Add("gallery", "width=500&height=500&mode=crop&format=jpg"); //Crop to square thumbnail
        versions.Add("large", "maxwidth=1000&maxheight=800&format=jpg"); //Fit inside 400x400 area, jpeg
        versions.Add("share", "width=600&height=315&mode=crop&format=jpg"); //Fit inside 1900x1200 area  

        string _dirUpload = HttpContext.Current.Server.MapPath("~/data/uploads/");
        foreach (string suffix in versions.Keys)
        {
            string uploadFolder = _dirUpload + suffix + "/";
            if (!Directory.Exists(uploadFolder)) Directory.CreateDirectory(uploadFolder);

            string fileName = Path.Combine(uploadFolder, _fileName);
            ImageBuilder.Current.Build(_dirUpload + "temp/" + _fileName , fileName, new ResizeSettings(versions[suffix]), false, true);
        }

    }

    public static string ParseDateToString(object dateinput, string format = "dd/MM/yyyy h:mm", string sMsg = "")
    {
        try
        {
            DateTime date = (DateTime)dateinput;
            TimeSpan diff = DateTime.Now - date;
            if (diff.Days != 0 && diff.Days < 7)
            {
                sMsg += diff.Days + " ngày trước.";
            }
            else if (diff.Days > 7)
            {
                sMsg = date.ToString(format);
            }
            else if (diff.Hours != 0)
            {
                sMsg += diff.Hours + " giờ trước.";
            }
            else if (diff.Minutes != 0)
            {
                sMsg += diff.Minutes + " phút trước.";
            }
            else if (diff.Seconds != 0)
            {
                sMsg += diff.Seconds + " giây trước.";
            }
            else if (diff.Seconds == 0)
            {
                sMsg = "Mới đây.";
            }
            else
            {
                sMsg = "";
            }
        }
        catch (Exception ex)
        {
            sMsg = "";
        }

        return sMsg;
    }

    public static bool IsAjaxRequest()
    {
        HttpRequest request = HttpContext.Current.Request;
        if (request["isAjax"] != null)
        {
            return true;
        }
        var page = HttpContext.Current.Handler as Page;
        if (request.HttpMethod.Equals("post", StringComparison.InvariantCultureIgnoreCase) && !page.IsPostBack)
        {
            return true;
        }
        if (request != null)
        {
            return (request["X-Requested-With"] == "XMLHttpRequest") || ((request.Headers != null) && (request.Headers["X-Requested-With"] == "XMLHttpRequest"));
        }
        return false;
    }

    public static void CheckSessionAdmin() {
        if (HttpContext.Current.Session["CMSUSER"] == null)
        {
            HttpContext.Current.Response.Redirect(Common.Webroot + "login.aspx");
        }
    }

    public static string Slug(object strTxt, int maxLength = 120, string sperator = "-")
    {
        if (strTxt == null || string.IsNullOrEmpty(strTxt.ToString())) return "";
        string str = strTxt.ToString().ToLower();
        for (int i = 1; i < VietNamChar.Length; i++)
        {
            for (int j = 0; j < VietNamChar[i].Length; j++)
                str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
        }
        str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
        str = Regex.Replace(str, @"[\s-]+", " ").Trim();
        if (maxLength > 0)
            str = str.Substring(0, str.Length <= maxLength ? str.Length : maxLength).Trim();

        str = Regex.Replace(str, @"\s", sperator);
        return str;
    }

    private static readonly string[] VietNamChar = new string[]
    {
            "aAeEoOuUiIdDyY",
            "ăáàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ"
    };
    public static bool CheckExtension(HttpPostedFile f, String[] allowedExtensions)
    {

        bool fileOK = false;
        if (f != null)
        {
            String fileExtension = System.IO.Path.GetExtension(f.FileName).ToLower();
            for (int i = 0; i < allowedExtensions.Length; i++)
            {
                if (fileExtension == allowedExtensions[i].ToLower())
                {
                    fileOK = true;
                    break;
                }
            }
        }
        return fileOK;
    }

    public static string CheckExtension(string filename, String[] allowedExtensions)
    {

        string invalid = "";
        String fileExtension = System.IO.Path.GetExtension(filename).ToLower();
        for (int i = 0; i < allowedExtensions.Length; i++)
        {
            if (fileExtension == allowedExtensions[i].ToLower())
            {
                invalid = fileExtension;
                break;
            }
        }
        return invalid;
    }



}