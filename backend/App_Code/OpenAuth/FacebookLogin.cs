﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;

/// <summary>
/// Summary description for FacebookLogin
/// </summary>

    public class FacebookLogin
    {
        protected static string _appId = null;
        protected static string AppId
        {
            get
            {
                if (_appId == null)
                    _appId = ConfigurationManager.AppSettings["FacebookAppId"] ?? null;
                return _appId;
            }
        }

        protected static string _appSecret = null;
        protected static string AppSecret
        {
            get
            {
                if (_appSecret == null)
                    _appSecret = ConfigurationManager.AppSettings["FacebookAppSecret"] ?? null;
                return _appSecret;
            }
        }

        public static UserLogin CheckLogin()
        {   
            UserLogin user = new UserLogin();     
            string cookieKey = "MyAccessToken"+HttpContext.Current.Session.LCID.ToString();
            if (HttpContext.Current.Request.Cookies[cookieKey] != null && HttpContext.Current.Request.Cookies[cookieKey].Value!="")
            {                
                //user = GetUserData(HttpContext.Current.Request.Cookies[cookieKey].Value);                
            }
            else
            {                  
                string fbsr = HttpContext.Current.Request.Cookies["fbsr_" + AppId] == null ? string.Empty : HttpContext.Current.Request.Cookies["fbsr_" + AppId].Value;

                int separator = fbsr.IndexOf(".");
                if (separator == -1)
                {
                    return null;
                }

                string encodedSig = fbsr.Substring(0, separator);
                string payload = fbsr.Substring(separator + 1);

                string sig = Base64Decode(encodedSig);

                var serializer = new JavaScriptSerializer();
                Dictionary<string, string> data = serializer.Deserialize<Dictionary<string, string>>(Base64Decode(payload));

                if (data["algorithm"].ToUpper() != "HMAC-SHA256")
                {
                    return null;
                }

                HMACSHA256 crypt = new HMACSHA256(Encoding.ASCII.GetBytes(AppSecret));
                crypt.ComputeHash(Encoding.UTF8.GetBytes(payload));
                string expectedSig = Encoding.UTF8.GetString(crypt.Hash);

                if (sig != expectedSig)
                {
                    return null;
                }
                string accessTokenResponse = FileGetContents("https://graph.facebook.com/oauth/access_token?client_id=" + AppId + "&redirect_uri=&client_secret=" + AppSecret + "&code=" + data["code"]);                
                //user.Username = HttpContext.Current.Session["AccessToken"].ToString();
                if (accessTokenResponse != "")
                {
                    NameValueCollection options = HttpUtility.ParseQueryString(accessTokenResponse);
                    if (options["access_token"] != null && !string.IsNullOrWhiteSpace(options["access_token"]))
                    {
                        HttpCookie myCookie = new HttpCookie(cookieKey);
                        myCookie.Value = options["access_token"].ToString();
                        myCookie.Expires = DateTime.Now.AddMinutes(10);
                        HttpContext.Current.Response.Cookies.Add(myCookie);
                        //user = GetUserData(options["access_token"].ToString());
                    }
                }                  
            }
            return user;
        }
       
        protected static string FileGetContents(string url)
        {
            string result = "";
            try
            {                
                WebResponse response;
                WebRequest request = HttpWebRequest.Create(url);
                response = request.GetResponse();
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return result;
        }

        protected static string Base64Decode(string input)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            string encoded = input.Replace("=", string.Empty).Replace('-', '+').Replace('_', '/');
            var decoded = Convert.FromBase64String(encoded.PadRight(encoded.Length + (4 - encoded.Length % 4) % 4, '='));
            var result = encoding.GetString(decoded);
            return result;
        }

    }

    public class UserLogin
    {
        public string UID { get; set; }
        public string Name { get; set; }     
        public string Email { get; set; }
        public string FacebookID { get; set; }
        public DateTime Created { get; set; }      

    }    