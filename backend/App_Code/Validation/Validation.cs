﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Validation
/// </summary>
public class Validation : InputValidator
{
    private List<_ErrorMessage> _listError = new List<_ErrorMessage>() ;
	public Validation()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void Add(string _key , string _message ) {
        if (_message != string.Empty)
        {
            _ErrorMessage _errorMessage = new _ErrorMessage();
            _errorMessage._key = _key;
            _errorMessage._message = _message;
            _listError.Add(_errorMessage);
        }
    }

    public bool IsValid()
    {
        return _listError.Count == 0 ? true : false ;
    }

    public string ValidationMessage(string _key)
    {
        _ErrorMessage _errorMessage = new _ErrorMessage();
        string _error = string.Empty;
        if( _listError.Any(a => a._key == _key) ) {            
            _errorMessage = _listError.Where(e=>e._key == _key ).FirstOrDefault();
            _error = _errorMessage._message;
        }
        return _error;
    }

    public string RenderHtmlError()
    {
        string _error = string.Empty;
        foreach (_ErrorMessage item in _listError)
        {
            _error += "<p>" + item._message + "</p>";
        }
        return _error;
    }
}

public class _ErrorMessage {
    public string _key {get;set;}
    public string _message {get;set;}
}