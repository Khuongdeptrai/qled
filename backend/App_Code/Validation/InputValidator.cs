﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for InputValidator
/// </summary>
public class InputValidator
{
	public InputValidator()
	{
        //
		// TODO: Add constructor logic here
		//
	}

    public string RequireField(string _key, string _message = "")
    {
        string _value = HttpContext.Current.Request.Form[_key] != null ? Common.CleanValue(HttpContext.Current.Request.Form[_key].ToString()) : string.Empty;
        string _resultMessage = string.Empty;
        if (string.IsNullOrEmpty(_value))
        {
            _resultMessage = _message == string.Empty ? "Vui lòng nhập đầy đủ thông tin" : _message;
        }
        return _resultMessage;
    }
    public string CheckCaptCha(string _key, string _message = "")
    {
        string _value = HttpContext.Current.Request.Form[_key] != null ? Common.CleanValue(HttpContext.Current.Request.Form[_key].ToString()) : string.Empty;
        string _resultMessage = string.Empty;
        if (string.IsNullOrEmpty(_value))
        {
            _resultMessage = "Vui lòng nhập mã bảo mật";
        }
        if (string.IsNullOrEmpty(_resultMessage))
        {
            string _sessionCaptcha = HttpContext.Current.Session["sessionCaptcha"].ToString().ToLower();
            if (_sessionCaptcha != _value.ToLower())
            {
                _resultMessage = _message == string.Empty ? "Vui lòng nhập lại mã bảo mật" : _message;
            }
        }
        return _resultMessage;
    }

    public string CheckEmail(string _key , string _message = "")
    {
        string _email = HttpContext.Current.Request.Form[_key] != null ? Common.CleanValue(HttpContext.Current.Request.Form[_key].ToString()) : string.Empty;       
        string _resultMessage = string.Empty;
        if (_email == string.Empty || string.IsNullOrWhiteSpace(_email.ToString()))
        {
            _resultMessage = "Vui lòng nhập email của bạn";
        }
        if(string.IsNullOrEmpty(_resultMessage)) {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                    @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                    @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex check = new Regex(strRegex, RegexOptions.IgnorePatternWhitespace);
            _resultMessage = check.IsMatch(_email.ToString()) ? string.Empty : "Vui lòng nhập đúng định dạng Email";
        }
        return _resultMessage;
    }

    public string CheckPhone(string _key, string _message = "")
    {
        string _phone = HttpContext.Current.Request.Form[_key] != null ? Common.CleanValue(HttpContext.Current.Request.Form[_key].ToString()) : string.Empty;       
        string _resultMessage = string.Empty;
        if (_phone == string.Empty || string.IsNullOrWhiteSpace(_phone.ToString()))
        {
            _resultMessage = "Vui lòng nhập số điện thoại của bạn";
        }
        if (_phone.Length > 11)
        {
            _resultMessage = "Vui lòng nhập đúng định dạng số điện thoại";
        }
        if (string.IsNullOrEmpty(_resultMessage))
        {
            string strRegex = "^[0-9]+$";
            Regex check = new Regex(strRegex, RegexOptions.IgnorePatternWhitespace);
            _resultMessage = check.IsMatch(_phone.ToString()) ? string.Empty : "Vui lòng nhập đúng định dạng số điện thoại";
        }

        return _resultMessage;
    }

    public string CheckYoutube(string _key, string _message = "")
    {
        Helper _helper = new Helper();
        string _youtube = HttpContext.Current.Request.Form[_key] != null ? HttpContext.Current.Request.Form[_key].ToString() : string.Empty;
        string _resultMessage = string.Empty;
        if (_youtube == string.Empty || string.IsNullOrWhiteSpace(_youtube.ToString()))
        {
            _resultMessage = "Vui lòng nhập đường dẫn Youtube của bạn";
        } else if (_helper.GetYoutubeID(_youtube) == string.Empty)
        {
            _resultMessage = "Vui lòng nhập đúng đường dẫn Youtube";
        }
        return _resultMessage;
    }

    public string CheckImages(string _key, string _message = "")
    {
        Helper _helper = new Helper();
        HttpPostedFile _fileImage = HttpContext.Current.Request.Files[_key];
        string _resultMessage = string.Empty;
        if (_fileImage == null)
        {
            _resultMessage = "Vui lòng chọn hình ảnh của bạn";
        }
        else if (!_helper.IsImage(_fileImage))
        {
            _resultMessage = "Bạn vui lòng chỉ tải định dạng hình ảnh.";
        }
        else if (_fileImage.ContentLength > 5000000)
        {
            _resultMessage = "Vui lòng tải hình ảnh dưới 5MB.";
        }
        return _resultMessage;
    }

    public string CheckXsrfToken(string _key, string _xsrfTokenKey , string _message = "")
    {
        AntiXsrfToken _antiXsrfToken = new AntiXsrfToken();
        string _xsrfToken = HttpContext.Current.Request.Form[_key] != null ? HttpContext.Current.Request.Form[_key].ToString() : string.Empty;
        string _resultMessage = string.Empty;
        if (_xsrfToken == string.Empty || string.IsNullOrWhiteSpace(_xsrfToken.ToString()))
        {
            _resultMessage = "Vui lòng tải lại trang để sử dụng tính năng này.";
        }
        else if (!_antiXsrfToken.ValidationXsrfToken(_xsrfTokenKey, _xsrfToken))
        {
            _resultMessage = "Vui lòng tải lại trang để sử dụng tính năng này";
        }
        return _resultMessage;
    }
}