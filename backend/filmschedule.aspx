﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CMS.master" AutoEventWireup="true" CodeFile="filmschedule.aspx.cs" Inherits="filmschedule" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
  <%
      string dateQuery = Request.QueryString["date"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["date"]).Trim()) : "";
      string dateConcat = "";
      if (dateQuery!="")
      {
          string[] dateSplit = dateQuery.Split('-');
          dateConcat = dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];   
      }
      
   %>
  <div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">             
              
          </h3>
        <div class="box-tools">
            <form method="get">
            <div class="input-group">       
                 <div class="input-group group-date" style="width:100%">
                     
                    <input type="text" class="form-control" style="width:200px; margin-right:5px;" readonly="true" name="date" id="enddate"  placeholder="Ngày chiếu phim" value="<%= dateQuery %>" />
                    <button class="btn btn-primary" type="submit">Lọc</button>   
                </div>                       
            </div>            
            </form>
         </div>
        </div><!-- /.box-header -->
        <div class="box-body">
         

            <% if (lf != null && lf.Any() && lf.Count > 0)
            { %>
            <div><b>Thông tin rạp chiếu Hà nội</b></div>
              <table class="table table-bordered table-hover js-order-data">
                <thead>
                  <tr>         
                    <th>Ngày chiếu</th>                               
                    <th>Suất chiếu</th>
                    <th>Tên Phim</th>
                    <th>Tổng số ghế</th>
                    <th>Tổng số ghế đã đăng ký thành công</th>
                  </tr>
                    <% foreach(FilmSchedule fs in lf){ %>
                    <tr>
                        <td><%= dateConcat %></td>
                        <td><%= fs.Time %></td>
                        <td><%= FilmName.Where(x=>x.Key == fs.FilmId).SingleOrDefault().Value %></td>
                        <td style="<%= fs.Total == 12 ? "color:red" : "" %>"><%= fs.Total %></td>
                        <td><%= fs.TotalRegister%></td>
                        <td></td>
                    </tr>
                    <%} %>
                </thead>
                <tbody>
                </tbody>
              </table>
          <% } %>


            <% if (hcm != null && hcm.Any() && hcm.Count > 0 && hcm != null)
            { %>
            <div><b>Thông tin rạp chiếu Hồ Chí Minh</b></div>
              <table class="table table-bordered table-hover js-order-data">
                <thead>
                  <tr>         
                    <th>Ngày chiếu</th>                               
                    <th>Suất chiếu</th>
                    <th>Tên Phim</th>
                    <th>Tổng số ghế</th>
                    <th>Tổng số ghế đã đăng ký thành công</th>
                  </tr>
                    <% foreach (FilmSchedule fs in hcm){ %>
                    <tr>
                        <td><%= dateConcat %></td>
                        <td><%= fs.Time %></td>
                        <td><%= FilmName.Where(x=>x.Key == fs.FilmId).SingleOrDefault().Value %></td>
                        <td style="<%= fs.Total == 12 ? "color:red" : "" %>"><%= fs.Total %></td>
                        <td><%= fs.TotalRegister%></td>
                        <td></td>
                    </tr>
                    <%} %>
                </thead>
                <tbody>
                </tbody>
              </table>
         <% } else { %>
            Không tìm thấy dữ liệu
            <% } %>
        </div><!-- /.box-body -->
        
      </div><!-- /.box -->
    </div><!--/.col (left) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
</asp:Content>