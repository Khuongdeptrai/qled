﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class previewEmail : System.Web.UI.Page
{
    public string strTemplateContent = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }
        int type = Request.QueryString["type"] != null ? Common.ParseInt(Request.QueryString["type"]) : 0;
        int id = Request.QueryString["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        Dictionary<string, object> objEmail = new Dictionary<string, object>();
        objEmail.Add("domain", ConfigurationManager.AppSettings["FRONT_END_URL"].TrimEnd('/'));
        string stringTemplate = "";
        if (type == 1)
        {
            stringTemplate = Server.MapPath("~/email/congratulation.html");
            strTemplateContent = GetMailBody(objEmail, stringTemplate);
        }
        else if (type == 2)
        {
            stringTemplate = Server.MapPath("~/email/offer_1package_buytv.html");
            strTemplateContent = GetMailBody(objEmail, stringTemplate);
        }
        else if (type == 3 && id>=1 && id <=3)
        {
            stringTemplate = Server.MapPath("~/email/introduce_samsungtv_w" + id + ".html");
            strTemplateContent = GetMailBody(objEmail, stringTemplate);
        }
        else
        {
            strTemplateContent = "Không tồn tại email template này";
        }        

    } 
    protected string GetMailBody(Dictionary<string, object> data, string Template)
    {
        TextReader tr = new StreamReader(Template);
        String strMailBody = tr.ReadToEnd();
        tr.Close();
        foreach (KeyValuePair<string, object> kVal in data)
        {
            strMailBody = strMailBody.Replace("{" + kVal.Key + "}", kVal.Value.ToString());
        }
        return strMailBody;
    }
}