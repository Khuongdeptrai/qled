﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class qhomeregister : System.Web.UI.Page
{
    public int _totalRows = 0;
    public string search = string.Empty;
    public string pagingHtml = string.Empty;
    public DataTable dtMember = new DataTable();
    public string _startDate = string.Empty;
    public string _endDate = string.Empty;
    private System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
    List<string> Citys = new List<string> { "Hà Nội", "TP HCM", "An Giang", "Bà Rịa - Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước", "Bình Thuận", "Cà Mau", "Cao Bằng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương", "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái", "Phú Yên", "Cần Thơ", "Đà Nẵng", "Hải Phòng" };
    
    public string listModels = "";
    public int sent = 0;
    public string queryString = "";
    public int stt = 0;
    public int confirmed = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }

        if (Request.HttpMethod == "POST")
        {
            string op = Request.Form["op"] != null ? Request.Form["op"] : "";
            string action = Request.Form["action"] != null ? Request.Form["action"] : "";

            if (action == "deleteRegister") 
            {
                Delete(Int32.Parse(Request.Form["id"].ToString()));
            }
            if (action == "updatestatus")
            {
                UpdateStatus(Int32.Parse(Request.Form["id"].ToString()), Int32.Parse(Request.Form["status"].ToString()));
            }
        }

        Database db = new Database();
        string dateView = Request.QueryString["dateview"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["dateview"]).Trim()) : "";
        string strQuery = " FacebookId != '' ";
        queryString += "&confirmed=" + confirmed;
        string orderBy = " order by QhomeRegisterId desc";
        int perPage = 50;
        int currentPage = Request.QueryString["page"] == null ? 1 : Common.ParseInt(Request.QueryString["page"].ToString());

        string hotel = Request.QueryString["hotel"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["hotel"]).Trim()) : "";

        string province = Request.QueryString["province"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["province"]).Trim()) : "";
        string status = Request.QueryString["status"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["status"]).Trim()) : "";
        currentPage = currentPage <= 0 ? 1 : currentPage;
        db.dbCommand.Parameters.Clear();

        if (dateView != "")
        {

            queryString += "&dateView=" + Server.HtmlEncode(dateView);
            //dateView = dateView.Replace("-", ":");
            strQuery += " and DateView =  @DateView";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@DateView", DbType.String, dateView));
        }
        if (province != "")
        {
            queryString += "&province=" + province;
            int provinceSql = 0;
            int.TryParse(province, out provinceSql);
            strQuery += " and City =  @City";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@City", DbType.Int32, provinceSql));
        }
        if (status != "")
        {
            int statusSql = -1;
            int.TryParse(status, out statusSql);
            queryString += "&status=" + Server.HtmlEncode(status);
            //dateView = dateView.Replace("-", ":");
            strQuery += " and Status =  @status";

            db.dbCommand.Parameters.Add(db.CreateDBParameter("@status", DbType.Int32, statusSql));
        }

        search = Request.QueryString["search"] == null ? string.Empty : Common.CleanScripts(Server.HtmlDecode(Request.QueryString["search"].ToString()));
        search = Common.CleanValue(search);

        if (search != "")
        {
            if (search.All(char.IsDigit))
            {
                strQuery += search != string.Empty ? " and ( Phone like N'%'+@searchKey+'%' or Email like N'%'+@searchKey+'%' ) " : string.Empty;
            }
            else
            {
                strQuery += search != string.Empty ? " and ( Name COLLATE Latin1_General_CI_AI like N'%'+@searchKey+'%' COLLATE Latin1_General_CI_AI or Phone=@searchKey or Email like N'%'+@searchKey+'%' or Brand like N'%'+@searchKey+'%' ) " : string.Empty;
            }
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@searchKey", DbType.String, search));
        }

        queryString += search != string.Empty ? "&search=" + Server.HtmlEncode(search) : "";

        string sqlSelect = string.Empty;
        DataTable dtTotalRows = db.Exec("select count(QhomeRegisterId) as total from QhomeRegister where " + strQuery);
        _totalRows = dtTotalRows.Rows.Count > 0 ? Common.ParseInt(dtTotalRows.Rows[0]["total"]) : 0;
        int _maxPage = (_totalRows % perPage) == 0 ? _totalRows / perPage : (_totalRows / perPage) + 1;
        int _startPage = (currentPage - 1) * perPage + 1;
        int _endPage = _startPage + perPage - 1;
        stt = _startPage;

        string _action = Request.QueryString["action"] == null ? string.Empty : Request.QueryString["action"].ToString();
        if (_action == "export")
        {
            var frontUrl = System.Configuration.ConfigurationManager.AppSettings["FRONT_END_URL"] + "Uploads/";
            sqlSelect = "SELECT  *, '" + frontUrl + "'+Image as UrlImage, 'http://facebook.com/'+FacebookId  as Facebook, case Status  WHEN 0 THEN N'Mới đăng ký' when 1 THEN N'Đồng ý trải nghiệm' WHEN 2 THEN N'Không đồng ý trải nghiệm'  END AS Status FROM QhomeRegister where " + strQuery + orderBy;
            dtMember = db.Exec(sqlSelect);
            dtMember.Columns.Remove("Status");
            dtMember.Columns.Remove("FacebookId");
            dtMember.Columns.Remove("HotelName");
            dtMember.Columns.Add("CityName");
            Dictionary<int,string> provinces = ListProvince();
            foreach (DataRow dr in dtMember.Rows) // search whole table
            {
                dr["CityName"] = provinces.Where(x => x.Key == Int32.Parse(dr["City"].ToString())).SingleOrDefault().Value;
                dr["Reason"] = dr["Reason"].ToString().Trim(',');
            }
            dtMember.Columns.Remove("City");
            ExportData expt = new ExportData();
            expt.dtList = dtMember;
            expt.ExportGridView(DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".xls", "xls", "Danh sách đăng ký");
            Response.End();
        }

        sqlSelect = @"SELECT  * 
                                FROM 
                                    (SELECT  ROW_NUMBER() OVER(" + orderBy + @") as row, y.*
                                        FROM 
	                                    ( SELECT TOP " + _totalRows + " * from QhomeRegister where " + strQuery + orderBy + @"                           
	                                    ) y
                                    )x
                                WHERE x.row BETWEEN " + _startPage.ToString() + " AND " + _endPage.ToString();

        dtMember = db.Exec(sqlSelect);

        if (_maxPage > 1)
        {
            PagingCMS paging = new PagingCMS();
            paging.Last = "&raquo;";
            paging.First = "&raquo;";
            paging.Next = "&rsaquo;";
            paging.Prev = "&lsaquo;";
            paging.EventClick = "";
            pagingHtml = paging.multi(_totalRows, perPage, currentPage, Common.Webroot + "qhomeregister.aspx?page=%page%" + queryString);
        }
    }
    private void Delete(int id)
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        //  int id = Request.Form["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (id > 0)
        {
            Database db = new Database();
            DataTable dtDetail = db.Exec("DELETE FROM QhomeRegister WHERE QhomeRegisterId=" + id + "");
            message.Status = 1;
            message.Message = "Thông tin được gửi thành công";
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }
    public Dictionary<int, string> ListProvince()
    {
        Dictionary<int, string> provinces = new Dictionary<int, string>();
        provinces.Add(1, "Hà Nội");
        provinces.Add(2, "Hồ Chí Minh");
        provinces.Add(3, "Đà Nẵng");
        provinces.Add(4, "Cần Thơ");
        provinces.Add(5, "An Giang");
        provinces.Add(6, "Bà Rịa - Vũng Tàu");
        provinces.Add(7, "Bắc Giang");
        provinces.Add(8, "Bắc Kạn");
        provinces.Add(9, "Bạc Liêu");
        provinces.Add(10, "Bắc Ninh");
        provinces.Add(11, "Bến Tre");
        provinces.Add(12, "Bình Định");
        provinces.Add(13, "Bình Dương");
        provinces.Add(14, "Bình Phước");
        provinces.Add(15, "Bình Thuận");
        provinces.Add(16, "Cà Mau");
        provinces.Add(17, "Cao Bằng");
        provinces.Add(18, "Đắk Lắk");
        provinces.Add(19, "Đắk Nông");
        provinces.Add(21, "Điện Biên");
        provinces.Add(22, "Đồng Nai");
        provinces.Add(23, "Đồng Tháp");
        provinces.Add(24, "Gia Lai");
        provinces.Add(25, "Hà Giang");
        provinces.Add(26, "Hà Nam");
        provinces.Add(27, "Hà Tĩnh");
        provinces.Add(28, "Hải Dương");
        provinces.Add(29, "Hậu Giang");
        provinces.Add(30, "Hòa Bình");
        provinces.Add(31, "Hưng Yên");
        provinces.Add(32, "Khánh Hòa");
        provinces.Add(33, "Kiên Giang");
        provinces.Add(34, "Kon Tum");
        provinces.Add(35, "Lai Châu");
        provinces.Add(36, "Lâm Đồng");
        provinces.Add(37, "Lạng Sơn");
        provinces.Add(38, "Lào Cai");
        provinces.Add(39, "Long An");
        provinces.Add(40, "Nam Định");
        provinces.Add(41, "Nghệ An");
        provinces.Add(42, "Ninh Bình");
        provinces.Add(43, "Ninh Thuận");
        provinces.Add(44, "Phú Thọ");
        provinces.Add(45, "Quảng Bình");
        provinces.Add(46, "Quảng Nam");
        provinces.Add(47, "Quảng Ngãi");
        provinces.Add(48, "Quảng Ninh");
        provinces.Add(49, "Quảng Trị");
        provinces.Add(51, "Sóc Trăng");
        provinces.Add(52, "Sơn La");
        provinces.Add(53, "Tây Ninh");
        provinces.Add(54, "Thái Bình");
        provinces.Add(55, "Thái Nguyên");
        provinces.Add(56, "Thanh Hóa");
        provinces.Add(57, "Thừa Thiên Huế");
        provinces.Add(58, "Tiền Giang");
        provinces.Add(59, "Trà Vinh");
        provinces.Add(60, "Tuyên Quang");
        provinces.Add(61, "Vĩnh Long");
        provinces.Add(62, "Vĩnh Phúc");
        provinces.Add(63, "Yên Bái");
        provinces.Add(64, "Phú Yên");
        provinces.Add(65, "Hải Phòng");
        return provinces;
    }
    private void UpdateStatus(int id, int status)
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        //  int id = Request.Form["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (id > 0)
        {
            Database db = new Database();
            DataTable dtDetail = db.Exec("Update QhomeRegister set Status = " + status + " WHERE QhomeRegisterId=" + id + "");
            message.Status = 1;
            message.Message = "Thông tin được gửi thành công";
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }
}