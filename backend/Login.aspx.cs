﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cms_Login : System.Web.UI.Page
{
    public string _message = string.Empty;
    private InputValidator _inputValidator = new InputValidator();
    private UserLogin _userlogin = new UserLogin();
    public bool IsBlock = false;

    public string _email = string.Empty;

    private string _ipAddress = Common.GetIPAddress();
    protected void Page_Load(object sender, EventArgs e)
    {
        //samsung159357#
        Database db = new Database();

        if (Request.HttpMethod == "POST")
        {
            Validation _validation = new Validation();
            _validation.Add("email", _inputValidator.CheckEmail("email"));
            _validation.Add("password", _inputValidator.RequireField("password", "Vui lòng nhập mật khẩu của bạn"));
            _validation.Add("captcha", _inputValidator.RequireField("captcha", "Vui lòng nhập mã bảo mật"));
            if (_validation.IsValid())
            {                
                _email = Common.CleanValue(Request.Form["email"]);
                string _password = Common.CleanValue(Request.Form["password"]);
                string _captcha = Common.CleanValue(Request.Form["captcha"]);

                if (Session["sessionCaptcha"] != null && _captcha != Session["sessionCaptcha"].ToString())
                {                   
                    _validation.Add("captcha", "Vui lòng nhập lại mã bảo mật");
                    _message = _validation.RenderHtmlError();
                }
                else
                {

                    
                    Helper _helper = new Helper();                    
                    DataTable _dataUser = db.Exec("select top 1 * from users where (email='" + _email + "' or username='" + _email + "' ) and password='" + _helper.Encrypt(_password) + "' ");
                    

                    if (_dataUser.Rows.Count > 0)
                    {
                        if (Common.ParseInt(_dataUser.Rows[0]["status"].ToString()) == 1)
                        {
                            _userlogin = new UserLogin();
                            _userlogin.UID = _dataUser.Rows[0]["id"].ToString();
                            _userlogin.Name = _dataUser.Rows[0]["fullname"].ToString();
                            _userlogin.Email = _dataUser.Rows[0]["email"].ToString();
                            Session["CMSUSER"] = _userlogin;

                            _message = "Xin chúc mừng bạn đã đăng nhập thành công";
                            Response.Redirect(Common.Webroot);
                        }
                        else
                        {
                            _message = "Tài khoản của bạn đã bị khóa.";
                        }
                    }
                    else
                    {
                        Session["sessionCaptcha"] = null;
                        _message = "Vui lòng kiểm tra lại mật khẩu hoặc email";
                        if (_email != string.Empty)
                        {
                            DataTable _dataCheckUser = db.Exec("select top 1 * from users where email='" + _email + "' ");
                            if (_dataCheckUser.Rows.Count > 0)
                            {
                                db.Exec("update users set count=count+1 where email='" + _email + "'  ");
                                if (Common.ParseInt(_dataCheckUser.Rows[0]["count"].ToString()) >= 4)
                                {
                                    _message = "Tài khoản của bạn đã bị khóa.";
                                    db.Exec("update users set status=0 where email='" + _email + "'  ");
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                _message = _validation.RenderHtmlError();
            }            
        }

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }
}