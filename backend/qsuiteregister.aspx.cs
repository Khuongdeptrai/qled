﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class qsuiteregister : System.Web.UI.Page
{
    public int _totalRows = 0;
    public string search = string.Empty;
    public string pagingHtml = string.Empty;
    public DataTable dtMember = new DataTable();
    public string _startDate = string.Empty;
    public string _endDate = string.Empty;
    private System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
    List<string> Citys = new List<string> { "Hà Nội", "TP HCM", "An Giang", "Bà Rịa - Vũng Tàu", "Bắc Giang", "Bắc Kạn", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước", "Bình Thuận", "Cà Mau", "Cao Bằng", "Đắk Lắk", "Đắk Nông", "Điện Biên", "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương", "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng", "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình", "Thái Nguyên", "Thanh Hóa", "Thừa Thiên Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc", "Yên Bái", "Phú Yên", "Cần Thơ", "Đà Nẵng", "Hải Phòng" };
   
    public string listModels = "";
    public int sent = 0;
    public string queryString = "";
    public int stt = 0;
    public int confirmed = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CMSUSER"] == null)
        {
            Response.Redirect(Common.Webroot + "login.aspx");
            Response.End();
        }

        if (Request.HttpMethod == "POST")
        {
            string op = Request.Form["op"] != null ? Request.Form["op"] : "";
            string action = Request.Form["action"] != null ? Request.Form["action"] : "";

            if (action == "deleteRegister")
            {
                Delete(Int32.Parse(Request.Form["id"].ToString()));
            }
            if (action == "updatestatus")
            {
                UpdateStatus(Int32.Parse(Request.Form["id"].ToString()), Int32.Parse(Request.Form["status"].ToString()));
            }
        }

        Database db = new Database();
        string dateView = Request.QueryString["dateview"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["dateview"]).Trim()) : "";
        string strQuery = " FacebookId != '' ";
        queryString += "&confirmed=" + confirmed;
        string orderBy = " order by QsuiteRegisterId desc";
        int perPage = 50;
        int currentPage = Request.QueryString["page"] == null ? 1 : Common.ParseInt(Request.QueryString["page"].ToString());

        string hotel = Request.QueryString["hotel"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["hotel"]).Trim()) : "";

        string status = Request.QueryString["status"] != null ? Common.CleanScripts(Server.HtmlDecode(Request.QueryString["status"]).Trim()) : "";
        currentPage = currentPage <= 0 ? 1 : currentPage;
        db.dbCommand.Parameters.Clear();

        if (dateView != "")
        {

            queryString += "&dateView=" + Server.HtmlEncode(dateView);
            //dateView = dateView.Replace("-", ":");
            strQuery += " and DateView =  @DateView";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@DateView", DbType.String, dateView));
        }
        if (hotel != "")
        {

            queryString += "&hotel=" + Server.HtmlEncode(hotel);
            //dateView = dateView.Replace("-", ":");
            strQuery += " and HotelName =  @hotelName";
            var hotelName = hotel == "1" ? "Khách sạn JW Marriott" : "Khách sạn Le Méridien";
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@hotelName", DbType.String, hotelName));
        }
        if (status != "")
        {
            int statusSql = -1;
            int.TryParse(status, out statusSql);
            queryString += "&status=" + Server.HtmlEncode(status);
            //dateView = dateView.Replace("-", ":");
            strQuery += " and Status =  @status";

            db.dbCommand.Parameters.Add(db.CreateDBParameter("@status", DbType.Int32, statusSql));
        }
        
        search = Request.QueryString["search"] == null ? string.Empty : Common.CleanScripts(Server.HtmlDecode(Request.QueryString["search"].ToString()));
        search = Common.CleanValue(search);

        if (search != "")
        {
            if (search.All(char.IsDigit))
            {
                strQuery += search != string.Empty ? " and ( Phone like N'%'+@searchKey+'%' or Email like N'%'+@searchKey+'%' ) " : string.Empty;
            }
            else
            {
                strQuery += search != string.Empty ? " and ( Name COLLATE Latin1_General_CI_AI like N'%'+@searchKey+'%' COLLATE Latin1_General_CI_AI or Phone=@searchKey or Email like N'%'+@searchKey+'%' or Brand like N'%'+@searchKey+'%' ) " : string.Empty;
            }
            db.dbCommand.Parameters.Add(db.CreateDBParameter("@searchKey", DbType.String, search));
        }

        queryString += search != string.Empty ? "&search=" + Server.HtmlEncode(search) : "";

        string sqlSelect = string.Empty;
        DataTable dtTotalRows = db.Exec("select count(QsuiteRegisterId) as total from QsuiteRegister where " + strQuery);
        _totalRows = dtTotalRows.Rows.Count > 0 ? Common.ParseInt(dtTotalRows.Rows[0]["total"]) : 0;
        int _maxPage = (_totalRows % perPage) == 0 ? _totalRows / perPage : (_totalRows / perPage) + 1;
        int _startPage = (currentPage - 1) * perPage + 1;
        int _endPage = _startPage + perPage - 1;
        stt = _startPage;

        string _action = Request.QueryString["action"] == null ? string.Empty : Request.QueryString["action"].ToString();
        if (_action == "export")
        {
            sqlSelect = "SELECT  *,'http://facebook.com/'+FacebookId  as Facebook, case Status  WHEN 0 THEN N'Mới đăng ký' when 1 THEN N'Đồng ý trải nghiệm' WHEN 2 THEN N'Không đồng ý trải nghiệm'  END AS Status FROM QsuiteRegister where " + strQuery + orderBy;
            dtMember = db.Exec(sqlSelect);
            dtMember.Columns.Remove("Status");
            dtMember.Columns.Remove("FacebookId");
            ExportData expt = new ExportData();
            expt.dtList = dtMember;
            expt.ExportGridView(DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss") + ".xls", "xls", "Danh sách đăng ký");
            Response.End();
        }

        sqlSelect = @"SELECT  * 
                                FROM 
                                    (SELECT  ROW_NUMBER() OVER(" + orderBy + @") as row, y.*
                                        FROM 
	                                    ( SELECT TOP " + _totalRows + " * from QsuiteRegister where " + strQuery + orderBy + @"                           
	                                    ) y
                                    )x
                                WHERE x.row BETWEEN " + _startPage.ToString() + " AND " + _endPage.ToString();

        dtMember = db.Exec(sqlSelect);

        if (_maxPage > 1)
        {
            PagingCMS paging = new PagingCMS();
            paging.Last = "&raquo;";
            paging.First = "&raquo;";
            paging.Next = "&rsaquo;";
            paging.Prev = "&lsaquo;";
            paging.EventClick = "";
            pagingHtml = paging.multi(_totalRows, perPage, currentPage, Common.Webroot + "qsuiteregister.aspx?page=%page%" + queryString);
        }
    }
    private void Delete(int id)
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        //  int id = Request.Form["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (id > 0)
        {
            Database db = new Database();
            DataTable dtDetail = db.Exec("DELETE FROM QsuiteRegister WHERE QsuiteRegisterId=" + id + "");
            message.Status = 1;
            message.Message = "Thông tin được gửi thành công";
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }

    private void UpdateStatus(int id,int status)
    {
        Messages message = new Messages();
        message.Status = 0;
        message.Message = "The information is invalid";
        //  int id = Request.Form["id"] != null ? Common.ParseInt(Request.QueryString["id"]) : 0;
        if (id > 0)
        {
            Database db = new Database();
            DataTable dtDetail = db.Exec("Update QsuiteRegister set Status = "+status+" WHERE QsuiteRegisterId=" + id + "");
            message.Status = 1;
            message.Message = "Thông tin được gửi thành công";
        }
        Response.Write(js.Serialize(message));
        Response.End();
    }
}