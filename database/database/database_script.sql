
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fullname] [nvarchar](50) NULL,
	[username] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[status] [int] NOT NULL,
	[created] [datetime] NOT NULL,
	[count] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[users] ON
INSERT [dbo].[users] ([id], [fullname], [username], [email], [password], [status], [created], [count]) VALUES (1, N'Administrator', N'admin', N'idea.samsung@gmail.com', N'Lm9fJM79RFPYyID4zrhZk5Ir9zzoeRBVQBPOBNjaQOM=', 1, CAST(0x0000A4D801483349 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[users] OFF
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reviews](
	[ReviewId] [int] IDENTITY(1,1) NOT NULL,
	[ReviewContent] [ntext] NULL,
	[Status] [int] NULL,
	[Created] [datetime] NULL,
	[Email] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReviewId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Reviews] ON
INSERT [dbo].[Reviews] ([ReviewId], [ReviewContent], [Status], [Created], [Email], [Name]) VALUES (5, N'Âm thanh trong rạp vô cùng sống động, cho mình cảm giác mọi thứ trên phim như đang xảy ra thật xung quanh mình vậy.', 1, CAST(0x0000A7620144C180 AS DateTime), N'tung@gmail.com', N'Trần Thanh Tùng')
INSERT [dbo].[Reviews] ([ReviewId], [ReviewContent], [Status], [Created], [Email], [Name]) VALUES (6, N'QLED TV có độ tương phản tốt nhất trong số các TV mà mình xem, và khi xem lâu mình cũng không bị mỏi hay chói mắt. Đây thực sự là chiếc TV dành cho những người “nghiện” phim và các chương trình giải trí như mình', 1, CAST(0x0000A7620144D8F0 AS DateTime), N'dang@yahoo.com', N'Nguyễn Hải Đăng')
INSERT [dbo].[Reviews] ([ReviewId], [ReviewContent], [Status], [Created], [Email], [Name]) VALUES (7, N'Mình rất thích màu sắc chân thực của QLED TV. Với những bộ phim có nhiều cảnh quay thiên nhiên thì xem trên QLED rất tuyệt vời vì màu sắc của TV rất gần với màu của cuộc sống.', 1, CAST(0x0000A7620144F63C AS DateTime), N'chau@gmail.com', N'Đặng Minh Châu')
INSERT [dbo].[Reviews] ([ReviewId], [ReviewContent], [Status], [Created], [Email], [Name]) VALUES (8, N'Mình rất ấn tượng với khả năng hiển thị màu sắc và hình ảnh rõ nét ở mọi góc nhìn của QLED TV. Dù được xếp ngồi phía góc trái của phòng, mình vẫn xem được mọi chi tiết rất rõ và đẹp.', 1, CAST(0x0000A76201451130 AS DateTime), N'trang@yahoo.com', N'Phạm Thu Trang')
INSERT [dbo].[Reviews] ([ReviewId], [ReviewContent], [Status], [Created], [Email], [Name]) VALUES (9, N'fsdfsdfds', 0, CAST(0x0000A762016DA5DC AS DateTime), N'dsfdsf@gmai.com', N'fdsfdsf')
SET IDENTITY_INSERT [dbo].[Reviews] OFF
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Registers](
	[RegisterId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[Phone] [varchar](30) NULL,
	[NumberPeople] [int] NULL,
	[DateView] [varchar](20) NULL,
	[TimeView] [varchar](20) NULL,
	[FilmId] [nvarchar](255) NULL,
	[CinameId] [nvarchar](50) NULL,
	[UserAgent] [nvarchar](255) NULL,
	[Ip] [varchar](50) NULL,
	[BrowserName] [nvarchar](100) NULL,
	[BrowserVersion] [varchar](50) NULL,
	[Created] [datetime] NULL,
	[Success] [int] NULL,
	[CinemaIdCode] [int] NULL,
	[FimlIdCode] [int] NULL,
	[RegisterCode] [nvarchar](50) NULL,
	[TvBrandOld] [nvarchar](50) NULL,
	[Refer] [nvarchar](1000) NULL,
	[SendEmail] [int] NULL,
	[SendSms] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RegisterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Registers] ON
INSERT [dbo].[Registers] ([RegisterId], [Name], [Email], [Phone], [NumberPeople], [DateView], [TimeView], [FilmId], [CinameId], [UserAgent], [Ip], [BrowserName], [BrowserVersion], [Created], [Success], [CinemaIdCode], [FimlIdCode], [RegisterCode], [TvBrandOld], [Refer], [SendEmail], [SendSms]) VALUES (45, N'Khổng Hữu Hiệp', N'h2k.2223@gmail.com', N'0939357579', 2, N'2017-05-13', N'17:00', N'Dạ Cổ Hoài Lang', N'Cresent Mall Hồ Chí Minh', N'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', N'113.161.100.120', N'Chrome', N'57.0', CAST(0x0000A76300E92014 AS DateTime), 1, 2, 1, N'RLD8JS', N'Samsung', N'', 0, 1)
INSERT [dbo].[Registers] ([RegisterId], [Name], [Email], [Phone], [NumberPeople], [DateView], [TimeView], [FilmId], [CinameId], [UserAgent], [Ip], [BrowserName], [BrowserVersion], [Created], [Success], [CinemaIdCode], [FimlIdCode], [RegisterCode], [TvBrandOld], [Refer], [SendEmail], [SendSms]) VALUES (46, N'Khổng Hữu Hiệp', N'huuhiepkhong@gmail.com', N'01652747468', 2, N'2017-05-14', N'17:00', N'Dạ Cổ Hoài Lang', N'Cresent Mall Hồ Chí Minh', N'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', N'113.161.100.120', N'Chrome', N'57.0', CAST(0x0000A76300EA2D9C AS DateTime), 1, 2, 1, N'0LM990', N'LG', N'', 0, 1)
INSERT [dbo].[Registers] ([RegisterId], [Name], [Email], [Phone], [NumberPeople], [DateView], [TimeView], [FilmId], [CinameId], [UserAgent], [Ip], [BrowserName], [BrowserVersion], [Created], [Success], [CinemaIdCode], [FimlIdCode], [RegisterCode], [TvBrandOld], [Refer], [SendEmail], [SendSms]) VALUES (47, N'Đặng Lê Ngọc Minh', N'lengocminh.dang@gmail.com', N'0944194233', 2, N'2017-05-13', N'17:00', N'Dạ Cổ Hoài Lang', N'Cresent Mall Hồ Chí Minh', N'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', N'113.161.64.44', N'Chrome', N'57.0', CAST(0x0000A76300EB4358 AS DateTime), 1, 2, 1, N'5F74QK', N'Samsung', N'', 0, 1)
INSERT [dbo].[Registers] ([RegisterId], [Name], [Email], [Phone], [NumberPeople], [DateView], [TimeView], [FilmId], [CinameId], [UserAgent], [Ip], [BrowserName], [BrowserVersion], [Created], [Success], [CinemaIdCode], [FimlIdCode], [RegisterCode], [TvBrandOld], [Refer], [SendEmail], [SendSms]) VALUES (54, N'Duy Nguyen', N'duynguyen@digitop.vn', N'0983495464', 1, N'2017-05-20', N'19:30', N'Kungfu Yoga', N'Cresent Mall Hồ Chí Minh', N'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', N'113.22.170.177', N'Chrome', N'58.0', CAST(0x0000A76400D0F6EC AS DateTime), 1, 2, 2, N'P1W0L4', N'Samsung', N'', 1, 1)
INSERT [dbo].[Registers] ([RegisterId], [Name], [Email], [Phone], [NumberPeople], [DateView], [TimeView], [FilmId], [CinameId], [UserAgent], [Ip], [BrowserName], [BrowserVersion], [Created], [Success], [CinemaIdCode], [FimlIdCode], [RegisterCode], [TvBrandOld], [Refer], [SendEmail], [SendSms]) VALUES (55, N'Lam Tran', N'buulamtran@gmail.com', N'0909567223', 1, N'2017-05-25', N'14:30', N'Ender’s Game', N'Cresent Mall Hồ Chí Minh', N'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', N'42.116.158.51', N'Chrome', N'58.0', CAST(0x0000A76400DACC1C AS DateTime), 1, 2, 5, N'E9PWJ3', N'Sony', N'', 1, 1)
INSERT [dbo].[Registers] ([RegisterId], [Name], [Email], [Phone], [NumberPeople], [DateView], [TimeView], [FilmId], [CinameId], [UserAgent], [Ip], [BrowserName], [BrowserVersion], [Created], [Success], [CinemaIdCode], [FimlIdCode], [RegisterCode], [TvBrandOld], [Refer], [SendEmail], [SendSms]) VALUES (56, N'Luc', N'phamluc1010a@gmail.com', N'0948988191', 1, N'2017-05-07', N'15:45', N'Dạ Cổ Hoài Lang', N'Aeon Mall Hà Nội', N'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36', N'14.169.25.178', N'Chrome', N'57.0', CAST(0x0000A76400DFD2D4 AS DateTime), 1, 1, 1, N'8B0FN0', N'Sony', N'', 1, 1)
SET IDENTITY_INSERT [dbo].[Registers] OFF
