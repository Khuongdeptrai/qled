﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Samsung
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            ViewEngines.Engines.Add(new RazorViewEngine());
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_BeginRequest(Object source, EventArgs e)
        {
            if (!Context.Request.IsSecureConnection && !Request.Url.Host.Contains("localhost") && !Request.Url.Host.Contains("deptraiphaithe.vn"))
            {
              //  Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            }

        }
        /*protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            HttpException httpException = exception as HttpException;

            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Home");

            if (httpException == null)
            {
                routeData.Values.Add("action", "Index");
            }
            else 
            {
                switch (httpException.GetHttpCode())
                {
                    case 404:
                        //Response.Write("<script>window.location.href = window.location.origin + '/Home/Error'</script>");
                        HttpContext.Current.Response.StatusCode = 404;
                        HttpContext.Current.Response.RedirectToRoute("404");
                        break;
                  /*  case 500:
                    case 403:
                      // HttpContext.Current.Response.StatusCode 

                       // HttpContext.Current.Response.RedirectToRoute("500");
                      //  Response.StatusCode = 403;
                     //   string json = "{\"error\":\"403\"}";
                      //  Response.Clear();
                      //  Response.ContentType = "application/json; charset=utf-8";
                     //   Response.Write(json);
                      //  Response.End();
                        break;
                    default:

                        break;
                }
            }
         Server.ClearError();

        }*/

    }
}