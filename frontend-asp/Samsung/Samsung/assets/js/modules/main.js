/* GAME POSTER
Author: KHUONGDINH
================================================== */



$(function(){
  if(typeof path_resource == "undefined" || !path_resource) path_resource = "";
  GScript.loadList([
    // external libraries
    path_resource + "js/libraries/plugin.js",
    path_resource + "js/libraries/tweenmax/TweenMax.min.js",
    path_resource + "js/libraries/tweenmax/plugins/ScrollToPlugin.min.js",

    // goon
    path_resource + "js/plugins/before-after.js",
    path_resource + "js/modules/goon.js",
    
  ], function(result){
    QLED.init();
  })


})


var wScreen = $(window).outerWidth();

var QLED = {

  init : function(){
    
    if( $('#dgtHome').length > 0 ) {
      QLED.intro();
    } else if( $('#dgtLibrary').length > 0 ) {
      QLED.library();
    } else if ( $('#dgtBooking').length > 0  ) {
      QLED.movie();
    } else if( $('#dgtGalerry').length > 0 ) {
      QLED.galery();
    } else if ( $('#dgtQcinema').length > 0 ) {
      $('.dgt_subheader').css({ 'display': 'block' });
    } 
    
    if(wScreen > 1024) {
      QLED.stick();
    }
    QLED.popup();
    QLED.mobile();
    QLED.term();
  },

  stick : function() {

    var $fwindow = $(window);
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    var h = $('.dgt_header').outerHeight() ; 

    var navPosition=$('.dgt_header').offset().top;
    var scrollTop=$(window).scrollTop();



    $fwindow.on('scroll resize', function() {
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      wScreen = $(window).outerWidth();
      console.log(navPosition);
      console.log(scrollTop);
      if(navPosition < scrollTop){
        $('.dgt_header').css({ 'position': 'fixed' });
      } else{
        $('.dgt_header').css({ 'position': 'relative' });
      }

    });
  },

  intro : function() {  
    //$('.dgt_kv').twentytwenty();
    
    $(window).on('load resize', function(event) {
      $(".dgt_cinema").width( $(window).width()/2 );
      $(".dgt_else").width( $(window).width()/2 );
    });
    
    setTimeout(function() {
      $("#dgtHome").css('visibility', 'visible');
      TweenMax.from('#dgtHome',1, { opacity: 0 })
    }, 200);

    
  },

  library: function() {

    $('.dgt_subheader').css({ 'display': 'block' });

    $('.js_dgt__library').slick({
      arrow: false,
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            arrow: false,
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: true,
            centerPadding: '100px',
            variableWidth: true
          }
        }
      ]
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__library').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__library').slick('slickNext');
    })

    

  },

  movie : function() {

    $('.dgt_subheader').css({ 'display': 'block' });

    $('.js_dgt__library').slick({
      arrow: false,
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            arrow: false,
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: true,
            centerPadding: '100px',
            variableWidth: true
          }
        }
      ]
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__library').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__library').slick('slickNext');
    })



  },
  
  popup: function() {
    $('#dgt_alert .ovl').on('click', function() {
        $('#dgt_alert').hide();
        $('#dgt_alert2').hide();
    });

    $('.dgt_popup .btn-close-2').on('click', function(e){
      e.preventDefault();
      $('#dgt_alert').hide();
    })
  },

  galery: function() {

    $('.js_dgt__photo').slick({
      dots: false,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      variableWidth: true
    });

    

    $('.js_dgt__video').slick({
      dots: false,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      variableWidth: true
    });

    $('.dgt_js_tabs li').on('click', function() {

      var addres = $(this).attr('rel');
      var ytb = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/VpBUfx9uVh0?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>'; 

      $('.dgt_js_tabs li').removeClass('active');
      $(this).addClass('active');

      $('.dgt_glr__item').hide();
      $('#'+addres).show();

      if( addres=='dgt_photo') {
        $('.dgt_ytb').html('');
      } else {
        $('.dgt_ytb').html('').append(ytb);
      }

    });


    $('.js_dgt__video .slick-slide').on('click', function() {

      var id = $(this).attr('data');
      console.log(id);
      var ytb = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+id+'?autoplay=0&rel=0" frameborder="0" allowfullscreen></iframe>'; 
      $('.dgt_ytb').html('').append(ytb);

    });

    $('.js_dgt__photo .slick-slide').on('click', function() {

      var src = $(this).find('.thumb').attr('src');
      $('#dgt_photo .dgt_glr__copy > img').attr('src',src)
    });

  
  },

  mobile: function() {
    
    $('.btn-menu').on('click', function(event) {
      event.preventDefault();
      if ($(this).hasClass('menu-open')) {
        $(this).removeClass('menu-open').addClass('menu-closed');
        $('#dgt_nav').hide();
      } else {
        $(this).removeClass('menu-closed').addClass('menu-open');
        $('#dgt_nav').show();
        
      };
    });




  },

  term: function() {
    
    $('.js_dgt_term').on('click', function(evt) {
      evt.preventDefault();
      $('.dgt_popup_term').show();
    });

    $('.js_dgt_closeterm').on('click', function(evt) {
      evt.preventDefault();
      $('.dgt_popup_term').hide();
    });

  }

  




};
 

var randomNum = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};



function showAlert() {
  $('#dgt_alert').show()
}










