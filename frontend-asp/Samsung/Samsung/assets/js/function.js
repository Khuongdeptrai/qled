﻿var path_resource = $("meta[name=webroot]").attr("content") + "assets/"
var absolutPath = $("meta[name=webroot]").attr("content");
var folder = "";
var SAMSUNGFUNCTION = (function () {
    var getOriginDomain = function () {
        if (!window.location.origin) {
            return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        } else {
            return window.location.origin;
        }
    }
    var _dateCurrent = new Date();
    var _timeCurrent =  typeof moment == 'function' ? " "+moment().hour() + ":" + moment().minute() + ":" + moment().second() : "";
    var timeZone = "Asia/Ho_Chi_Minh";
    var formatTime = "YYYY-MM-DD";
    var formatTime2 = "YYYY-MM-DD HH:mm:ss";
    var dateCurrent = typeof moment == 'function' ? moment(moment(_dateCurrent), formatTime)  : "";
    
    var FilmModel = Backbone.Model.extend({
        
    });
    var _filmModel = new FilmModel();
    var filteFomrModel =  Backbone.Model.extend({
        defaults: {
            cinema: 0,
            date: "",
            time: ""
        },
        initialize: function () {
            this.on("change:date", function (model) {
                _filmView.renderSlide({key:'date', value : model.toJSON().date});
            });


            this.on("change:time", function (model) {
                if (model.toJSON().time == "") {
                    return false;
                }
                _filmView.renderSlide({ key: 'time', value: model.toJSON().time });
            });
        }
    });
    var isProcessing = 0;
    var _filterModel = new filteFomrModel();
    var FilmView = Backbone.View.extend({
        el: '#dgtBooking',
        render : function(){
            
        },
        reRenderSlide: function (htmlContent) {
            $('.js_dgt__library').slick('unslick');
            $('.js_dgt__library').empty();
            $('.js_dgt__library').append(htmlContent);
            $('.js_dgt__library').slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                variableWidth: true
            });
        },
        renderSlide: function (model) {
            
        },
        renderFormFilter: function (model) {
            //console.log(model);
            var _model = model;
            var selectDate = "<option value='' >Lựa chọn ngày xem phim</option>";
            if (_.size(_model) > 0) {
                _.each(_model, function (obj, key) {
                    //console.log(key);
                    var checkShowDate = 0;
                    if (typeof obj[0] != "undefined") {
                        selectDate += "<option data-id=" + moment(key).format("YYYY-MM-DD") + " value=" + moment(key).format("YYYY-MM-DD") + ">" + moment(key).format("DD-MM-YYYY") + "</option>";                
                   }
                });
                this.$el.find('.filter-date').html('');
                this.$el.find('.filter-date').html(selectDate);
            }
        },
        events: {
            //'click a': 'clickSlide',
            'change select.filter-date': 'changeDate',
            'change .filter-cinema': 'changeCinema',
            'change .filter-time': 'changeTime',
            'click  .filter-date' : 'clickDate'
        },
        clickSlide: function (e) {
            
            var href = $(e.currentTarget).attr('href');
            var form = _filterModel.toJSON();
            var msg = "";
            if (form.cinema == 0) {
                msg += "<p>Vui lòng chọn rạp phim</p>"
            }
            if (form.date == "") {
                msg += "<p>Vui lòng chọn ngày xem phim</p>"
            }
            if (form.time == "") {
                msg += "<p>Vui lòng chọn giờ xem phim</p>"
            }
            if(msg != ""){
                alert(msg);
                msg = "";
                return false;
            }
            href += "/" + form.cinema + "/" + form.date + "/" + form.time.replace(':','-');
       //     console.log(getOriginDomain() + href);
            window.location.href = getOriginDomain()  + href;
            return false;
        },
        changeTime : function(e){
            var time = $(e.target).val();
            _filterModel.set({ 'time': time });
            this.countTicket();
        },
        countTicket: function () {
            if (isProcessing == 1) {
                alert("Xin vui lòng đợi đang xử lý yêu cầu của bạn");
                return false;
            }
            var cinema = $(".filter-cinema").val();
            var date = $(".filter-date option:selected").attr("data-id");
            var time = $(".filter-time").val();
            if (cinema != "" && date != "" && time != "") {
                isProcessing = 1;
                ajaxFunction('POST', ucountticket, { cinema: cinema, date: date, time: time, filmid: FilmId }, function (response) {
                    if (response.status == "1") {
                        $(".dgt_count").show();
                        $(".dgt_count").html(response.msg);
                        isProcessing = 0;
                    } else {
                        $(".dgt_count").show();
                        alert(response.msg);
                        isProcessing = 0;
                    }
                }, function () { isProcessing = 0; $(".dgt_count").hide()})
            }
        },
        clickDate : function(){
            if ($('.filter-cinema').val() == "") {
                alert("Vui lòng chọn rạp chiếu phim");
                return false;
            }
        },
        changeDate: function (e) {
            var cinemaSelected = $('.filter-cinema').val();
            if (cinemaSelected == "") {
                return false;
            }
            var value = $(".filter-date option:selected").attr('data-id');
            var options = '<option value="">Lựa chọn giờ xem phim</option>';
            if (value == undefined) {
                this.$el.find('.filter-time').html(options);
                return false;
            }
            

            _filterModel.set({ 'date': value });
            var _model = null;
            if (cinemaSelected == "1") {
                _model = scheduleHanoi;
            } else {
                _model = scheduleHcm;
            }
            if (_.size(_model[value]) > 0) {
                var listFilm = _model[value];
               
                _.each(listFilm, function (obj, key) {
                    var checkShowTime = 0;
                    if (typeof obj != 'undefined') {
                        if (obj.FilmId == FilmId) {
                            options += '<option value="' + obj.Time + '">' + obj.Time + '</option>';
                        }
                    }
                })
                this.$el.find('.filter-time').html(options);
                if ($('.filter-time option').size() == 2) {
                    $('.filter-time option').eq(1).prop('selected', true);
                }
            } else {
                //thong bao het xuat chieu
            }

            this.countTicket();
        },
        changeCinema: function (e) {
            var cinema = $(e.target).val();
            _filterModel.set({ 'cinema': cinema });

            if (cinema == "") {
                $('.filter-date').html('<option value="">Lựa chọn ngày xem phim</option>');
                return false;
            }
            
           
            if (cinema == "1") {//ha noi
                _filmView.renderFormFilter(scheduleHanoi);
            } else if (cinema == "2") {
                _filmView.renderFormFilter(scheduleHcm);
            }
            $('.filter-time').html('<option value="">Lựa chọn giờ xem phim</option>');

            this.countTicket();
            
        }
    })
    var _filmView = new FilmView();
    var filterDate = function (dateTimeCurrent) {
        var dateSchedule = null;
        $.each(scheduleHanoi, function (index, object) {

            dateSchedule = moment(moment(index, formatTime), formatTime);
            var dateScheduleFilmCompare = dateSchedule.tz(timeZone);
            var dateTimeCurrentCompare = dateCurrent.tz(timeZone);
            
            if (dateScheduleFilmCompare.format(formatTime) < dateTimeCurrentCompare.format(formatTime)) {
               
                delete scheduleHanoi[index];
            }
            
            if (dateScheduleFilmCompare.format(formatTime) == dateTimeCurrentCompare.format(formatTime)) {
              
                var _size = _.size(scheduleHanoi[index]);
                for (var i = 0; i < _size ; i++) {
                    obj = object[i];
                    var schedule = moment(index + " " + obj.Time + ":00", formatTime2).tz(timeZone);
                    var currentTimeSchedule = moment(_dateCurrent + _timeCurrent, formatTime2).tz(timeZone);
                    var rs = schedule.diff(currentTimeSchedule, 'minutes');
                   
                    if (currentTimeSchedule >= schedule || rs < 60) { 
                        delete scheduleHanoi[index][i];
                    } else {
                        //console.log(schedule.format() +"|"+  currentTimeSchedule.format());
                    }
                }
            }
            var flag = 0;
            _.each(object, function (obj, key) {
                if (obj != null)
                {
                    if (obj.FilmId == FilmId)
                        flag = 1;
                }
            });
            if (flag == 0)
                delete scheduleHanoi[index];
           
        })

        $.each(scheduleHcm, function (index, object) {

            dateSchedule = moment(moment(index, formatTime), formatTime);
            var dateScheduleFilmCompare = dateSchedule.tz(timeZone);
            var dateTimeCurrentCompare = dateCurrent.tz(timeZone);

            if (dateScheduleFilmCompare.format(formatTime) < dateTimeCurrentCompare.format(formatTime)) {
                delete scheduleHcm[index];
            }

            if (dateScheduleFilmCompare.format(formatTime) == dateTimeCurrentCompare.format(formatTime)) {

                var _size = _.size(scheduleHcm[index]);
                for (var i = 0; i < _size ; i++) {
                    obj = object[i];
                    var schedule = moment(index + " " + obj.Time + ":00", formatTime2).tz(timeZone);
                    var currentTimeSchedule = moment(_dateCurrent + _timeCurrent, formatTime2).tz(timeZone);

                    if (currentTimeSchedule >= schedule) {
                        delete scheduleHcm[index][i];
                    } else {
                        //console.log(schedule.format() +"|"+  currentTimeSchedule.format());
                    }
                }
            }
            _.each(object, function (obj, key) {
                var flag = 0;
                _.each(object, function (obj, key) {
                    if (obj.FilmId == FilmId)
                        flag = 1;
                });
                if (flag == 0)
                    delete scheduleHcm[index];
            });

        })
      //  console.log(scheduleHcm);
        //_filmModel.set({ films: scheduleHanoi });
    }
    var FormRegisterModel = Backbone.Model.extend({
        defaults: {
            name: '',
            brand: '',
            email: '',
            phone: '',
            number_of_people: '',
            fimlid: $('input[name=filmid]').val(),
            date: $('input[name=date]').val(),
            time: $('input[name=time]').val(),
            cinema: $('input[name=cinema]').val(),
            
        },
        url : '/Home/Register',
        initialize: function () {
            // console.log('image model init');
        },
        saveFormRegister:function(model){
            this.save(model, {
                wait: true,
                success: function (response) {
                    //console.log(response);
                }
            });
        }
    });

    
    var FormRegisterView = Backbone.View.extend({
        el: '#dgtBooking',
        events: {
            'keyup input[name=name],blur input[name=name]': 'enterName',
            'change select[name=brand],blur select[name=brand]': 'changeTivi',
            'keyup input[name=email],blur input[name=email]': 'enterEmail',
            'keyup input[name=phone],blur input[name=phone]': 'enterPhone',
            'change select[name=number_of_people],blur select[name=number_of_people]': 'changeNumber',
            'click input.dgt_btn,blur input.dgt_btn': 'submitForm'
        },
        enterName: function (e) {
            _FormRegisterModel.set({ name: $(e.currentTarget).val() });
        },
        changeTivi: function (e) {
            _FormRegisterModel.set({ brand: $(e.currentTarget).val() });
            var tiviModel = $(e.currentTarget).val();
            if (tiviModel == "Other") {
                $('.name-tv-other').show();
            } else {
                $('.name-tv-other').hide();
                $('input[name=tivi-other]').val('');
            }
        },
        enterEmail: function (e) {
            _FormRegisterModel.set({ email: $(e.currentTarget).val() });
        },
        enterPhone: function (e) {
            _FormRegisterModel.set({ phone: $(e.currentTarget).val() });
        },
        changeNumber: function (e) {
            _FormRegisterModel.set({ number_of_people: $(e.currentTarget).val() });
        },
        submitForm: function (e) {
            $(e.target).hide();

            if (!$('input[name=checkbox_term]').is(":checked")) {
                alert("Vui lòng đọc và đồng ý với điều khoản của chương trình");
                $('.dgt_btn').show();
                return false;
            }

            _FormRegisterModel.set({'name':$('input[name=name]').val()});
            
            _FormRegisterModel.set({ 'email': $('input[name=email]').val().trim() });
            _FormRegisterModel.set({ 'phone': $('input[name=phone]').val() });
            _FormRegisterModel.set({ 'number_of_people': $('select[name=number_of_people]').val() });
            _FormRegisterModel.set({ fimlid: filmIdString });
            _FormRegisterModel.set({ time: $('.filter-time').val() });
            _FormRegisterModel.set({ date: $('.filter-date').val() });
            _FormRegisterModel.set({ cinema: $('.filter-cinema').val() });

           var tiviModel = ""
           if ($('input[name=tivi-other]').val() != "") {
               tiviModel = $('input[name=tivi-other]').val();
           } else {
               tiviModel = $('select[name=brand]').val();
           }
           
           _FormRegisterModel.set({ 'brand': tiviModel });
            var message = "";

            if ($('.filter-cinema').val() == "") {
                message += "<p>Vui lòng chọn rạp chiếu phim </p>";
            }

            if ($('.filter-date').val() == "") {
                message += "<p>Vui lòng chọn ngày chiếu phim </p>";
            }
            if ($('.filter-time').val() == "") {
                message += "<p>Vui lòng chọn giờ chiếu phim </p>";
            }
            if (_FormRegisterModel.get("name").trim() == "") {
                message += "<p>Vui lòng nhập họ tên </p>";
            }
            if (_FormRegisterModel.get("brand").trim() == "") {
                
                if ($('select[name=brand]').val()=='')
                    message += "<p>Vui lòng chọn nhãn hiệu TV bạn đang dùng </p>";
                else
                    message += "<p>Vui lòng nhập nhãn hiệu TV bạn đang dùng </p>";
                
            } else {
                if (tiviModel == "Other") {
                    message += "<p>Vui lòng nhập nhãn hiệu TV bạn đang dùng </p>";
                }
            }
            if (_FormRegisterModel.get("number_of_people").trim() == "") {
                message += "<p>Vui lòng chọn số người tham gia </p>";
            }
            if (_FormRegisterModel.get("email").trim() == "") {
                message += "<p>Vui lòng nhập email </p>";
            } else {
                if (!validateTool.isEmail(_FormRegisterModel.get("email").trim()))
                    message += "<p>Vui lòng nhập email đúng định dạng</p>";
            }

            if (_FormRegisterModel.get("phone").trim() == "") {
                message += "<p>Vui lòng nhập số điện thoại</p>";
            } else {
                if (!validateTool.isPhone(_FormRegisterModel.get("phone")))
                    message += "<p>Vui lòng nhập số điện thoại đúng định dạng </p>";
            }

            if (message != "") {
                alert(message);
                message = "";
                $(e.target).show();
                return false;
            }
            // _FormRegisterModel.saveFormRegister(_FormRegisterModel.toJSON());
            ajaxFunction('post', ajaxUrl, _FormRegisterModel.toJSON(),
                function(reponse){
                    if (reponse.status == 1) {

                        if (typeof (Storage) !== "undefined") {
                            localStorage.setItem("filmregister" , FilmId);
                        }
                        $('#dgt_alert').hide();
                        $('#dgt_alert2').show();
                        $('.dgt_form__booking').find('input[type=text]').val("");
                        $('.dgt_form__booking').find('select').val("");
                    } else {
                        $('#dgt_alert .dgt_copy').html('');
                        $('#dgt_alert .dgt_copy').html(reponse.msg);
                        $('#dgt_alert').show();
                        $('.dgt_btn').show();
                    }
                }, function (reponse) {
                    $('.dgt_btn').show();
                    if (typeof response.status != 'undefined' && response.status == "1")
                    {
                        if (typeof response.message != 'undefined') {
                            alert(response.message);
                            return false;
                        }
                    } else {
                        
                    }
                },
                function () {
                    $('.dgt_btn').show();
                    if (typeof response.message != 'undefined') {
                        alert(response.message);
                        return false;
                    }
                }
            )
        }
    })
    var _FormRegisterView = new FormRegisterView();
    var _FormRegisterModel = new FormRegisterModel();

    var shareFacebook = function () {
        $('.dgt_form__booking').click(function () {

        });
    }

    this.ajaxFunction = function (type, url, data, successCallback, errorCallBack) {
        data['__RequestVerificationToken'] = $('input[name=__RequestVerificationToken]').val();
        $.ajax({
            type: type,
            url: folder + url,
            // contentType: "application/json; charset=utf-8",
            data: data,
            dataType: "json",
            success: typeof successCallback === 'function' ? successCallback : function () { },
            error: typeof errorCallBack === 'function' ? errorCallBack : function () { console.log('ajax error') },
            beforeSend: function () {
                //if (this.url ===  '/Home/Register')
                    //$('.loading').show();
            },
            statusCode: {
                404: function () {
                    alert("Không tìm thấy dữ liệu bạn yêu cầu");
                },
                500: function () {
                    alert("Đã xảy ra lỗi trong quá trình xử lý");
                }
            },
        }).done(function () {
            //if (this.url ===  '/Process/Register')
                //$('.loading').hide();
        });
    }
    var shareFacebook = function () {
        $('#dgt_alert2 .dgt_btn').on('click', function () {
            var cinemaId = $('.filter-cinema').val() == "" ? 1 : $('.filter-cinema').val();
            FB.ui({
                method: 'feed',
                name: 'Trải nghiệm tivi Qled',
                caption: 'trainghiemqled.com',
                link: getOriginDomain() + absolutPath + "Home/Sharefacebook/" + FilmId + "/" + cinemaId,
                picture: getOriginDomain() + absolutPath + "assets/images/thumb5.jpg",
                description: 'Mô tả trải nghiệm tivi Qled'
            }, function (rs) {
               // console.log(rs);

            });
        });
    }

    var writeReview = function () {
        $('.dgt_writearea .dgt_btn').click(function () {
            var name = $('input[name=name]').val().trim();
            var email = $('input[name=email]').val().trim();
            var content = $('textarea[name=content-review]').val().trim();

            var message = "";
            if (name == "") {
                message += "<p>Vui lòng nhập họ tên</p>";
            }
            if(email == ""){
                message += "<p>Vui lòng nhập email</p>";
            } else {
                if (!validateTool.isEmail(email)) {
                    message += "<p>Vui lòng nhập email hợp lệ</p>";
                }
            }
            if (content == "") {
                message += "<p>Vui lòng nhập nội dung cảm nhận</p>";
            }

            if (message != "") {
                alert(message);
               // console.log(message);
                message = "";
                return false;
            }


            ajaxFunction('post', urlWriteReview, { name: name, email: email, content: content },
                function (response) {
                  //  console.log(response);
                    if (response.status == 1) {
                        alert(response.msg);
                        $('input[name=name]').val("");
                        $('input[name=email]').val("");
                        $('textarea[name=content-review]').val("");
                    }else {
                        alert(response.msg);
                    }
                },
                function (response) {

                })

           
        });
    }

    var init = function () {
        switch(action){
            case "Films":
               // setTimeout(function () {  }, 1000);
                window.onload = function () {
                    $(window).trigger("scroll");
                }
            //    console.log('trigger scroll');
                filterDate();
                if (typeof (Storage) !== "undefined") {
                    if (localStorage.getItem("filmregister") != null) {
                        setTimeout(function () {
                            $(window).scrollTop($(".dgt_booking__wrap").offset().top - $(".dgt_header").outerHeight());
                        },300);
                        
                    }
                }
                
                break;
            case "Sharefacebook":
            case "ShareFacebook":
                
                window.location.href = getOriginDomain() + absolutPath + "phim/" + pageRegisterR;
                break;
            case "Review":
                writeReview();
                break;
        }
        
        shareFacebook();
        window.alert = function (msg) {
            $('#dgt_alert .dgt_copy').html('');
            $('#dgt_alert .dgt_copy').html(msg);
            $('#dgt_alert').show();
        }
    }
    init();
})()
    var validateTool = {
        regExForEmail: /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}$/,

        isUndefined: function (strObj) {
            return typeof strObj == 'undefined' ? true : false;
        },
        isPhone: function (phoneNo) {
            phoneNo = phoneNo.trim();
            if (isNaN(phoneNo) || phoneNo.length >= 15 || phoneNo.length <= 5)
                return false;
            return true;
        },
        isEmail: function (sEmail) {
            sEmail = sEmail.trim();
            if (sEmail.search(this.regExForEmail) != -1)
                return true;
            return false;
        },
        isIdno: function (idNo) {
            idNo = idNo.trim();
            if (isNaN(idNo) || idNo.length > 12 || idNo.length < 9)
                return false;
            return true;
        },
    }