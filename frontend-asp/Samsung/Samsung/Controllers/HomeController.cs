﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Samsung.Models;
using System.Net;
using System.Text;
using System.IO;
namespace Samsung.Controllers
{
     [FilterPostActionFilter]
    public class HomeController : Controller
    {
         private int Total = 40;
         private int SpecialTotal = 16;
         private int Weekend = 30;
        //
        // GET: /Home/
        private string[] ListFilm = new string[] { "da-co-hoai-lang", "kungfu-yoga", "now-you-see-me-2", "49-ngay-2", "enders-game", "the-expendables-3" };
        public ActionResult Index()
        {
            ViewBag.ClassCss = "dgtHome";
           return View();
        }

        public ActionResult Qcinema() {
            ViewBag.ClassCss = "dgtQcinema";
            return View();
        }

        public ActionResult Library() {
            ViewBag.ClassCss = "dgtLibrary";
            var listFilms = ListFilm;
            ViewBag.ListFilm = listFilms;
            return View();
        }
        private Dictionary<int, string> ListFilmName() {

            var films = new Dictionary<int, string>();
            films.Add(1, "Dạ Cổ Hoài Lang");
            films.Add(2, "Kunfu Yoga");
            films.Add(3, "Now You See Me 2");
            films.Add(4, "49 Ngày 2");
            films.Add(5, "Enders Game");
            films.Add(6, "The Expendables 3");
            return films;
        }
        #region Dictionary FilmSchedule Ha noi
        private Dictionary<DateTime, List<FilmSchedule>> FilmSheduleHaNoi()
        {
            var listFilmNam = ListFilmName();

            List<Dictionary<DateTime, List<FilmSchedule>>> ddlfHn = new List<Dictionary<DateTime, List<FilmSchedule>>>();
            Dictionary<DateTime, List<FilmSchedule>> dicList = new Dictionary<DateTime, List<FilmSchedule>>();

            var listDate6 = new List<FilmSchedule>();
           // listDate6.Add(new FilmSchedule { Time = "12:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
            listDate6.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 10 });
           // listDate6.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 16 });
           // listDate6.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 16 });
           // listDate6.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = 16 });

            dicList.Add(new DateTime(2017, 05, 06), listDate6);


            var listDate7 = new List<FilmSchedule>();

            listDate7.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate7.Add(new FilmSchedule { Time = "13:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate7.Add(new FilmSchedule { Time = "15:45", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            listDate7.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate7.Add(new FilmSchedule { Time = "20:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });

            dicList.Add(new DateTime(2017, 05, 07), listDate7);

            var listDate8 = new List<FilmSchedule>();
           // listDate8.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
            listDate8.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            listDate8.Add(new FilmSchedule { Time = "16:25", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate8.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 08), listDate8);

            var listDate9 = new List<FilmSchedule>();
            //listDate9.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
            listDate9.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate9.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate9.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 09), listDate9);

            var listDate10 = new List<FilmSchedule>();
           // listDate10.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 12 });
            listDate10.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate10.Add(new FilmSchedule { Time = "17:15", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            listDate10.Add(new FilmSchedule { Time = "19:05", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 10), listDate10);

            var listDate11 = new List<FilmSchedule>();
           // listDate11.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
            listDate11.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate11.Add(new FilmSchedule { Time = "17:05", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate11.Add(new FilmSchedule { Time = "19:35", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 11), listDate11);

            var listDate12 = new List<FilmSchedule>();
            //listDate12.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
            listDate12.Add(new FilmSchedule { Time = "12:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate12.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate12.Add(new FilmSchedule { Time = "17:10", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate12.Add(new FilmSchedule { Time = "20:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 12), listDate12);

            var listDate13 = new List<FilmSchedule>();
            listDate13.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate13.Add(new FilmSchedule { Time = "12:30", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate13.Add(new FilmSchedule { Time = "15:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate13.Add(new FilmSchedule { Time = "17:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            listDate13.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 13), listDate13);

            var listDate14 = new List<FilmSchedule>();
            listDate14.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate14.Add(new FilmSchedule { Time = "12:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate14.Add(new FilmSchedule { Time = "15:20", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate14.Add(new FilmSchedule { Time = "17:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            listDate14.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 14), listDate14);

            var listDate15 = new List<FilmSchedule>();
         //   listDate15.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
            listDate15.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate15.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate15.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 15), listDate15);

            var listDate16 = new List<FilmSchedule>();
        //    listDate16.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12});
            listDate16.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            listDate16.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate16.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 16), listDate16);

            var listDate17 = new List<FilmSchedule>();
          //  listDate17.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
            listDate17.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate17.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            listDate17.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 17), listDate17);

            var listDate18 = new List<FilmSchedule>();
          //  listDate18.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
            listDate18.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate18.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate18.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 18), listDate18);


            var listDate19 = new List<FilmSchedule>();
           // listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 16 });
            //listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 12 });
            listDate19.Add(new FilmSchedule { Time = "12:10", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            listDate19.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate19.Add(new FilmSchedule { Time = "16:55", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate19.Add(new FilmSchedule { Time = "19:35", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 19), listDate19);

            var listDate20 = new List<FilmSchedule>();
            listDate20.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "12:10", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "17:15", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "19:15", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 20), listDate20);

            var listDate21 = new List<FilmSchedule>();
            listDate21.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "15:05", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "17:05", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "19:25", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 21), listDate21);

            return dicList;

        }
        #endregion

        #region Dictionary FilmScheduleHoChiMinh
        private Dictionary<DateTime, List<FilmSchedule>> FilmSheduleHoChiMinh()
        {
            var listFilmNam = ListFilmName();

            List<Dictionary<DateTime, List<FilmSchedule>>> ddlfHn = new List<Dictionary<DateTime, List<FilmSchedule>>>();
            Dictionary<DateTime, List<FilmSchedule>> dicList = new Dictionary<DateTime, List<FilmSchedule>>();

            var listDate13 = new List<FilmSchedule>();
            // listDate13.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 16 });
            listDate13.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            // listDate13.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 16 });
            //listDate13.Add(new FilmSchedule { Time = "12:30", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 16 });
            // listDate13.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = 16 });
            dicList.Add(new DateTime(2017, 05, 13), listDate13);

            var listDate14 = new List<FilmSchedule>();
            listDate14.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate14.Add(new FilmSchedule { Time = "12:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate14.Add(new FilmSchedule { Time = "15:25", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            listDate14.Add(new FilmSchedule { Time = "17:45", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate14.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 14), listDate14);

            var listDate15 = new List<FilmSchedule>();

            // listDate15.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
            listDate15.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            listDate15.Add(new FilmSchedule { Time = "16:25", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate15.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 15), listDate15);

            var listDate16 = new List<FilmSchedule>();
            // listDate16.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 12 });
            listDate16.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate16.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate16.Add(new FilmSchedule { Time = "19:40", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 16), listDate16);

            var listDate17 = new List<FilmSchedule>();
            // listDate17.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = 12 });
            listDate17.Add(new FilmSchedule { Time = "14:40", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            listDate17.Add(new FilmSchedule { Time = "17:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            listDate17.Add(new FilmSchedule { Time = "19:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 17), listDate17);

            var listDate18 = new List<FilmSchedule>();
            //  listDate18.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = 12 });
            listDate18.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate18.Add(new FilmSchedule { Time = "16:35", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate18.Add(new FilmSchedule { Time = "19:05", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 18), listDate18);


            var listDate19 = new List<FilmSchedule>();
            //    listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 16 });
            //listDate19.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
            listDate19.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate19.Add(new FilmSchedule { Time = "15:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate19.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate19.Add(new FilmSchedule { Time = "20:20", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 19), listDate19);

            var listDate20 = new List<FilmSchedule>();
            listDate20.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "14:50", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            listDate20.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 20), listDate20);

            var listDate21 = new List<FilmSchedule>();
            listDate21.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "12:40", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "15:20", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "17:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            listDate21.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 21), listDate21);

            var listDate22 = new List<FilmSchedule>();
            // listDate22.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 12 });
            listDate22.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate22.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate22.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 22), listDate22);

            var listDate23 = new List<FilmSchedule>();
            //  listDate23.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
            listDate23.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Total });
            listDate23.Add(new FilmSchedule { Time = "16:30", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate23.Add(new FilmSchedule { Time = "19:20", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 23), listDate23);

            var listDate24 = new List<FilmSchedule>();
            //  listDate24.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = 12 });
            listDate24.Add(new FilmSchedule { Time = "14:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate24.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            listDate24.Add(new FilmSchedule { Time = "18:50", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 24), listDate24);

            var listDate25 = new List<FilmSchedule>();
            // listDate25.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = 12 });
            listDate25.Add(new FilmSchedule { Time = "14:30", FilmId = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 5).SingleOrDefault().Value, Total = Total });
            listDate25.Add(new FilmSchedule { Time = "16:50", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Total });
            listDate25.Add(new FilmSchedule { Time = "19:05", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 25), listDate25);

            var listDate26 = new List<FilmSchedule>();
            // listDate26.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = 12 });
            listDate26.Add(new FilmSchedule { Time = "11:50", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Total });
            listDate26.Add(new FilmSchedule { Time = "14:00", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Total });
            listDate26.Add(new FilmSchedule { Time = "16:35", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Total });
            listDate26.Add(new FilmSchedule { Time = "19:15", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Total });
            dicList.Add(new DateTime(2017, 05, 26), listDate26);


            var listDate27 = new List<FilmSchedule>();
            listDate27.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
            listDate27.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 3).SingleOrDefault().Value, Total = Weekend });
            listDate27.Add(new FilmSchedule { Time = "14:50", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            listDate27.Add(new FilmSchedule { Time = "17:10", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            listDate27.Add(new FilmSchedule { Time = "19:30", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 27), listDate27);


            var listDate28 = new List<FilmSchedule>();
            listDate28.Add(new FilmSchedule { Time = "10:00", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            listDate28.Add(new FilmSchedule { Time = "12:20", FilmId = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 6).SingleOrDefault().Value, Total = Weekend });
            listDate28.Add(new FilmSchedule { Time = "15:05", FilmId = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 1).SingleOrDefault().Value, Total = Weekend });
            listDate28.Add(new FilmSchedule { Time = "17:05", FilmId = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 2).SingleOrDefault().Value, Total = Weekend });
            listDate28.Add(new FilmSchedule { Time = "19:25", FilmId = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Key, FilmName = listFilmNam.Where(x => x.Key == 4).SingleOrDefault().Value, Total = Weekend });
            dicList.Add(new DateTime(2017, 05, 28), listDate28);


            return dicList;

        }
        #endregion
        
        public ActionResult Films(string id,int cinema = 0,string date = "",string time="")
        {
            try { 
                var listFilms = ListFilm;
                string fimlmName = "da-co-hoai-lang";
                if (listFilms.Contains(id))
                {
                    int form = 1;
                    ViewBag.ClassCss = "dgtBooking";
                    var view = "dacohoailang";
                    int filmId = 1;
                    switch (id)
                    {
                        case "kungfu-yoga":
                            view = "kungfuyoga";
                            filmId = 2;
                            fimlmName = "kungfu-yoga";
                            break;
                        case "now-you-see-me-2":
                            view = "nowyouseeme";
                            fimlmName = "now-you-see-me";
                            filmId = 3;
                            break;
                        case "49-ngay-2":
                            view = "bonchinngay";
                            fimlmName = "49-ngay-2";
                            filmId = 4;
                            break;
                        case "enders-game":
                            view = "endersgame";
                            fimlmName = "ender-game";
                            filmId = 5;
                            break;
                        case "the-expendables-3":
                            view = "theexpendables";
                            fimlmName = "the-expendables-3";
                            filmId = 6;
                            break;
                    }

                    if ( cinema == 1 || cinema == 2)
                    {
                        form = 1;
                    }else{
                        form = 0;
                    }
                    date = date == null ? "" : date;
                    string[] dateArr = FilmSheduleHaNoi().Keys.Select(x => x.ToString("yyyy-MM-dd")).ToArray();
                    if (!dateArr.Contains(date))
                    {
                        form = 0;
                    }
                    if (time == null || time == "")
                    {
                        form = 0;
                    }
                    form = 1;

                    ViewBag.ListFilm = ListFilm;
                    Dictionary<DateTime, List<FilmSchedule>> ddlf = FilmSheduleHaNoi();
                    Dictionary<string, List<FilmSchedule>> dictDateString = new Dictionary<string, List<FilmSchedule>>();
                    foreach (var obj in ddlf)
                    {
                        dictDateString.Add(obj.Key.ToString("yyyy-MM-dd"), obj.Value);
                    }

                    Dictionary<DateTime, List<FilmSchedule>> ddlfHcm = FilmSheduleHoChiMinh();
                    Dictionary<string, List<FilmSchedule>> dictDateStringHcm = new Dictionary<string, List<FilmSchedule>>();
                    foreach (var obj in ddlfHcm)
                    {
                        dictDateStringHcm.Add(obj.Key.ToString("yyyy-MM-dd"), obj.Value);
                    }

                    ViewBag.DictFilmShceduleHanoi = Newtonsoft.Json.JsonConvert.SerializeObject(dictDateString);
                    ViewBag.DictFilmShceduleHoChiMinh = Newtonsoft.Json.JsonConvert.SerializeObject(dictDateStringHcm);

                    ViewBag.ListFilm = listFilms;
                    ViewBag.Form = form;
                    ViewBag.FilmIdString = id;
                    ViewBag.Cinema = cinema;
                    ViewBag.Date = date;
                    ViewBag.Time = time;
                    ViewBag.FilmId = filmId;
                    ViewBag.FilmName = fimlmName;
                    return View(view);
                }
                else 
                {
                    return new HttpNotFoundResult();
                }
            }
            catch(Exception ex){
                return new HttpNotFoundResult();
            }
        }
        public ActionResult LibraryPhoto() {
            ViewBag.ClassCss = "dgtGalerry";
            return View();
        }


        public ActionResult SendSms()
        {
            if (Request.Form["username"] == null || Request.Form["password"] == null)
            {
                return Json(new { code = "403" }, JsonRequestBehavior.AllowGet);
            }

            if (Request.Form["username"].ToString() != "digitop" && Request.Form["password"].ToString() != "digitop^%$#1") {
                return Json(new { code = "403" }, JsonRequestBehavior.AllowGet);
            }

            if (Request.Form["phone_number"] != null && Request.Form["code"] != null &&  Request.Form["time"] != null)
            {
                try
                {
                    
                   
                    if (Request.Form["username"].ToString() == "digitop" && Request.Form["password"].ToString() == "digitop^%$#1")
                    {
                        string phoneNumber = Request.Form["phone_number"].ToString();
                        string code = Request.Form["code"].ToString();
                        string time = Request.Form["time"].ToString();
                        string cinema = Request.Form["cinema"].ToString();

                        string cinemaNameSms = "Cresent Mall HCM";
                        if (cinema.Contains("Aeon"))
                            cinemaNameSms = "Aeon Mall HN";

                        string msg = "Dang ky thanh cong trai nghiem QCinema cung QLED TV. Ma dat ve:  " + code + ". Suat: " + time + " - " + cinemaNameSms + ". Vui long co mat 30 phut truoc gio chieu.";
                       // string msg = "Dang ky thanh cong DIEN ANH DINH CAO cung QLED TV. Ma dat ve: " + code + ". Thoi gian: " + time + ". Vui long den truoc 30 phut truoc gio chieu.";

                        if (phoneNumber.StartsWith("+84"))
                        {
                            phoneNumber = phoneNumber.Replace("+84", string.Empty);
                        }

                        if (phoneNumber.StartsWith("84"))
                        {
                            phoneNumber = phoneNumber.Replace("84", string.Empty);
                        }
                        if (!phoneNumber.StartsWith("0"))
                        {
                            phoneNumber = "0" + phoneNumber;
                        }

                        string pass = HttpContext.Server.UrlEncode("Digitop@124&(#");
                        string sms = String.Format("dest=" + phoneNumber + "&name=SAMSUNG&msgBody=" + msg + "&contentType=text&serviceID=10604&cpID=10604&username=digitop&password=" + pass);
                        Uri uriStr = new Uri("http://210.245.81.132/jetnav4cp/jetnavsms.asmx/SendMT");

                        HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uriStr);
                        myHttpWebRequest.Method = WebRequestMethods.Http.Post;

                        byte[] data = Encoding.ASCII.GetBytes(sms);

                        myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
                        myHttpWebRequest.ContentLength = data.Length;

                        Stream requestStream = myHttpWebRequest.GetRequestStream();
                        requestStream.Write(data, 0, data.Length);
                        requestStream.Close();

                        HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                        Stream responseStream = myHttpWebResponse.GetResponseStream();

                        StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);

                        string pageContent = myStreamReader.ReadToEnd();
                        System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
                        xml.LoadXml(pageContent);
                        string status = xml.DocumentElement.InnerText;

                        myStreamReader.Close();
                        responseStream.Close();

                        myHttpWebResponse.Close();
                        Response.Write(status);
                        Response.End();
                    }
                    else {
                        return Json(new { code = "500" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { code = "500" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { code = "403" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FilterPostActionFilter]
        public ActionResult Register() {
            string message = "";

            var fimlId = "";
            var cinema = "";
            var time = "";
            var date = "";
            var number_of_people = "";
            int number_view = 0;
            var phone = "";
            var email = "";
            var name = "";
            var brand = "";
            
           
                if (Request.Form["fimlid"] == null || string.IsNullOrEmpty(Request.Form["fimlid"].ToString()))
                    message += "<p>Vui lòng chọn lại phim</p>";
                else
                    fimlId = Request.Form["fimlid"].ToString();

                if (Request.Form["cinema"] == null || string.IsNullOrEmpty(Request.Form["cinema"].ToString()))
                    message += "<p>Vui lòng chọn rạp chiếu</p>";
                else
                    cinema = Request.Form["cinema"].ToString();

                if (Request.Form["time"] == null || string.IsNullOrEmpty(Request.Form["time"].ToString()))
                    message += "<p>Vui lòng chọn giờ chiếu phim</p>";
                else
                    time = Request.Form["time"].ToString().Replace("-", ":");

                if (Request.Form["date"] == null || string.IsNullOrEmpty(Request.Form["date"].ToString()))
                    message += "<p>Vui lòng chọn ngày chiếu phim</p>";
                else
                    date = Request.Form["date"].ToString();

                if (Request.Form["number_of_people"] == null || string.IsNullOrEmpty(Request.Form["number_of_people"].ToString()))
                    message += "<p>Vui lòng chọn số người xem phim hợp lệ</p>";
                else
                {
                    number_of_people = Request.Form["number_of_people"].ToString();
                    int.TryParse(number_of_people, out number_view);
                    if (number_view == 0)
                    {
                        message += "<p>Vui lòng chọn số người xem phim hợp lệ</p>";
                    }
                    else
                    {
                        if (number_view > 2)
                        {
                            message += "<p>Vui lòng chọn số người xem phim hợp lệ</p>";
                        }
                    }
                }
                if (Request.Form["phone"] == null || string.IsNullOrEmpty(Request.Form["phone"].ToString()))
                    message += "<p>Vui lòng nhập số điện thoại</p>";
                else
                    phone = Request.Form["phone"].ToString();

                if (Request.Form["email"] == null || string.IsNullOrEmpty(Request.Form["email"].ToString()))
                    message += "<p>Vui lòng nhập email</p>";
                else
                {
                    if (Helper.Common.IsValidEmail(Request.Form["email"].ToString()))
                    {
                        email = Request.Form["email"].ToString();
                    }
                    else
                    {
                        message += "<p>Vui lòng nhập email hợp lệ </p>";
                    }
                }

                if (Request.Form["name"] == null || string.IsNullOrEmpty(Request.Form["name"].ToString()))
                    message += "<p>Vui lòng nhập họ tên</p>";
                else
                    name = Request.Form["name"].ToString();

                if (Request.Form["brand"] == null || string.IsNullOrEmpty(Request.Form["brand"].ToString()))
                    message += "<p>Vui chọn nhãn hiệu TV</p>";
                else
                    brand = Request.Form["brand"].ToString();

                if (message != "")
                {
                    return Json(new { status = 0, msg = message });
                }

                if (!ListFilm.Contains(fimlId))
                {
                    return Json(new { status = 0, msg = "<p>Không tìm thấy phim bạn yêu cầu</p>" });
                }

                string queryCount = "Select count(RegisterId) from Registers where (email = @email or phone = @phone) and Success = 1";
                using (var db = new samsungEntitiesEntities())
                {
                    int countRegister = db.Database.SqlQuery<int>(queryCount, new SqlParameter("email", email), new SqlParameter("phone", phone)).SingleOrDefault();
                    if (countRegister > 0)
                        return Json(new { status = -1, msg = "<p>Số điện thoại và email này đã được sử dụng để đăng ký trước đó</p>" });
                }

                string[] dates = date.Split('-');
                DateTime dt = new DateTime(Int32.Parse(dates[0]), Int32.Parse(dates[1]), Int32.Parse(dates[2]));
                Dictionary<DateTime, List<FilmSchedule>> dicts = new Dictionary<DateTime, List<FilmSchedule>>();
                if (cinema == "1")
                    dicts = FilmSheduleHaNoi();
                else
                    dicts = FilmSheduleHoChiMinh();

                var rs = dicts.Where(x => x.Key == dt).SingleOrDefault();
                if (rs.Value == null)
                {
                    return Json(new { status = 0, msg = "<p>Không tìm thấy phim bạn yêu cầu</p>" });
                }
                int index = Array.IndexOf(ListFilm, fimlId) + 1;
                var listfimls = rs.Value;

                var sch = listfimls.Where(x => x.Time == time && x.FilmId == index).SingleOrDefault();
                if (sch == null)
                {
                    return Json(new { status = 0, msg = "<p>Không tìm thấy phim bạn yêu cầu</p>" });
                }
                string FilmName = "";
                int filmNumber = 1;

                switch (fimlId)
                {
                    case "kungfu-yoga":
                        FilmName = "Kungfu Yoga";
                        filmNumber = 1;
                        break;
                    case "now-you-see-me-2":
                        FilmName = "Now You See Me 2";
                        filmNumber = 2;
                        break;
                    case "49-ngay-2":
                        FilmName = "49 Ngày 2";
                        filmNumber = 3;
                        break;
                    case "enders-game":
                        FilmName = "Ender’s Game";
                        filmNumber = 4;
                        break;
                    case "the-expendables-3":
                        FilmName = "Expendables 3 ";
                        filmNumber = 5;
                        break;
                    case "da-co-hoai-lang":
                        FilmName = "Dạ Cổ Hoài Lang";
                        filmNumber = 6;
                        break;
                }
                string code = Samsung.Helper.Common.RandomString(6);
                Samsung.Models.Register register = new Samsung.Models.Register();
                register.Name = Helper.Common.ParseQueryString(name);
                register.Email = Helper.Common.ParseQueryString(email);
                register.Phone = Helper.Common.ParseQueryString(phone);
                register.NumberPeople = number_view;
                register.DateView = Helper.Common.ParseQueryString(date);
                register.FilmId = Helper.Common.ParseQueryString(FilmName);
                register.CinameId = cinema == "1" ? "Aeon Mall Hà Nội" : "Cresent Mall Hồ Chí Minh";
                register.UserAgent = Request.UserAgent;
                register.BrowserName = Request.Browser.Browser;
                register.BrowserVersion = Request.Browser.Version;
                register.Ip = Request.UserHostAddress;
                register.TimeView = Helper.Common.ParseQueryString(time);
                register.Created = Samsung.Helper.Common.GetTimeInVietNam();

                register.FimlIdCode = index;
                register.RegisterCode = code;
                register.CinemaIdCode = Int16.Parse(cinema);
                register.TvBrandOld = brand;
                register.Refer = Session["REFER_SITE"] != null ? Session["REFER_SITE"].ToString() : "";

                DateTime dtCurent = Helper.Common.GetTimeInVietNam();

                string[] dateSplit = date.Split('-');
                string []timeSplit = time.Split(':');
                DateTime dtRegister = new DateTime(int.Parse(dateSplit[0]), int.Parse(dateSplit[1]), int.Parse(dateSplit[2]), int.Parse(timeSplit[0]), int.Parse(timeSplit[1]),0);
                TimeSpan span = dtRegister.Subtract ( dtCurent );
                if ( span.Days <= 0 && span.Hours <= 0 &&  span.Minutes<60)
                {
                    return Json(new { status = 0, msg = "<p>Xin lỗi !Đã hết thời gian đăng ký cho suất chiếu này</p>" });
                }

                using (var db = new Samsung.Models.samsungEntitiesEntities())
                {
                    string query = "select  COALESCE(sum(NumberPeople),0) from Registers where  DateView = @date and TimeView = @time and CinemaIdCode = @cinemaidcode and FimlIdCode = @filmcode and Success = 1";
                    int count = db.Database.SqlQuery<int>(query, new SqlParameter("date", date), new SqlParameter("time", time), new SqlParameter("cinemaidcode", Int16.Parse(cinema)), new SqlParameter("filmcode", index)).FirstOrDefault();
                    if (count + number_view <= sch.Total)
                    {//
                        register.Success = 1;
                        db.Registers.Add(register);
                        db.SaveChanges();

                        string contenemail = System.IO.File.ReadAllText(Server.MapPath("~/Views/edm/email.html"));
                        contenemail = contenemail.Replace("{domain}", Request.Url.GetLeftPart(UriPartial.Authority) + Samsung.Helper.UrlHelperPage.Webroot);
                        contenemail = contenemail.Replace("{image}", filmNumber + ".jpg");
                        contenemail = contenemail.Replace("{film_name}", register.FilmId);
                        contenemail = contenemail.Replace("{name}", register.Name);
                        contenemail = contenemail.Replace("{cinema}", register.CinameId);
                        contenemail = contenemail.Replace("{id}", code);
                        contenemail = contenemail.Replace("{date}", register.DateView);
                        contenemail = contenemail.Replace("{time}", register.TimeView);
                        contenemail = contenemail.Replace("{number}", register.NumberPeople.ToString());

                        //(string toEmail ,string fromEmail,string from_name,string to_name,string subject,string message)
                        var contentEndCode = HttpContext.Server.UrlEncode(contenemail);
                        var rsEmail = Samsung.Helper.Common.GemSendEmail(register.Email, Samsung.Properties.Settings.Default.EmailFrom, Samsung.Properties.Settings.Default.EmailName, register.Name, Samsung.Properties.Settings.Default.EmailSubject, contentEndCode);
                        var rsSms = Helper.Common.SendSms(phone, code, register.TimeView + " " + register.DateView, "digitop", "digitop^%$#1", register.CinemaIdCode == 1 ? "Aeon Mall Ha Noi" : "Cresent Mall Ho Chi Minh");
                        Register registerUpdate = db.Registers.Where(x => x.RegisterId == register.RegisterId).SingleOrDefault();
                        if(registerUpdate != null){
                            registerUpdate.SendEmail = 1;
                            registerUpdate.SendSms = rsSms == true ? 1 : 0;
                            db.SaveChanges();
                        }
                        return Json(new { status = 1, msg = "<p>Đăng ký vé xem phim thành công</p>" });
                    }
                    else
                    {
                        register.Success = 0;
                        db.Registers.Add(register);
                        db.SaveChanges();
                        return Json(new { status = 0, msg = "<p>Xin lỗi !Đã hết suất đăng ký</p>" });
                    }
                }
            
        }
        public ActionResult Review(){
            List<Review> lr = new List<Review>();
            using (var db = new samsungEntitiesEntities()) {
                string sqlQuery = "Select * from Reviews where Status = 1 order by ReviewId DESC";
                lr = db.Database.SqlQuery<Review>(sqlQuery).ToList();
            }
            ViewBag.ClassCss = "dgtReview";
            ViewBag.ListReview = lr;
            return View();
        }

        public ActionResult ShareFacebook(int? id, int? cinema) {
            ViewBag.Id = 0;
            ViewBag.Id = 0;
            if (id.HasValue) {
                ViewBag.Id = id;
            }
            if(cinema.HasValue){
                ViewBag.Cinema = cinema;
            }
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [FilterPostActionFilter]
        public ActionResult WriteReview() {

            try
            {
                var message = "";
                var name = "";
                var email = "";
                var content = "";
                if (Request.Form["name"] == null || string.IsNullOrEmpty(Request.Form["name"].ToString()))
                {
                    message += "<p>Vui lòng nhập họ tên</p>";
                }
                else
                {
                    name = Request.Form["name"].ToString().Trim();
                }

                if (Request.Form["email"] == null || string.IsNullOrEmpty(Request.Form["email"].ToString()))
                    message += "<p>Vui lòng nhập email</p>";
                else
                {
                    if (Helper.Common.IsValidEmail(Request.Form["email"].ToString()))
                    {
                        email = Request.Form["email"].ToString().Trim();
                    }
                    else
                    {
                        message += "<p>Vui lòng nhập email hợp lệ </p>";
                    }
                }

                if (Request.Form["content"] == null || string.IsNullOrEmpty(Request.Form["content"].ToString()))
                {
                    message += "<p>Vui lòng nhập cảm nhận</p>";
                }
                else
                {
                    content = Request.Form["content"].ToString().Trim();
                }

                if (content.Length > 500 || email.Length > 100 || name.Length > 100)
                {
                    message += "Bạn đã vượt quá số kí tự cho phép";
                }
                if (message != "")
                {
                    return Json(new { status = 0, msg = message });
                }

                using (var db = new Samsung.Models.samsungEntitiesEntities())
                {
                    Review reviewW = new Review();
                    reviewW.Name = Helper.Common.ParseQueryString(name);
                    reviewW.Email = Helper.Common.ParseQueryString(email);
                    reviewW.ReviewContent = Helper.Common.ParseQueryString(content);
                    reviewW.Status = 0;
                    reviewW.Created = Helper.Common.GetTimeInVietNam();
                    db.Reviews.Add(reviewW);
                    
                    if (db.SaveChanges() > 0)
                    {
                        return Json(new { status = 1, msg = "<p> Xin cảm ơn lưu cảm nhận thành công </p>" });
                    }
                    else
                    {
                        return Json(new { status = 0, msg = "<p> Đã xảy ra lỗi trong quá trình lưu dữ liệu </p>" });
                    }
                }
            }
            catch(SqlException ex) {
                return Json(new { status = 0, msg = "<p> ex Đã xảy ra lỗi </p>" });
            }
        }

        public ActionResult Product() {
           // return Redirect("http://www.samsung.com/vn/tvs/premium-uhd/?qled-tv");
            return Redirect("http://www.samsung.com/vn/tvs/qled-q8c/QA65Q8CAMKXXV/");
        }


        public ActionResult Qhome()
        {
            return Redirect("https://vn.campaign.samsung.com/trainghiemqled/qhome");
        }
        public ActionResult Qsuite()
        {
            return Redirect("https://vn.campaign.samsung.com/trainghiemqled/qsuite");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckTicket() { 
            if(Request.IsAjaxRequest()){

                if (Request.Form["date"] != null && Request.Form["cinema"] != null && Request.Form["time"] != null && Request.Form["filmid"] != null)
                {
                    try
                    {
                        string date = Request.Form["date"].ToString();
                        string cinema = Request.Form["cinema"].ToString();
                        string time = Request.Form["time"].ToString();
                        string[] dates = date.Split('-');
                        DateTime dt = new DateTime(Int32.Parse(dates[0]), Int32.Parse(dates[1]), Int32.Parse(dates[2]));
                        List<FilmSchedule> lf = new List<FilmSchedule>();

                        if (cinema == "1")
                        {
                            Dictionary<DateTime, List<FilmSchedule>> ddlf = FilmSheduleHaNoi();
                            lf = ddlf.Where(x => x.Key == dt).SingleOrDefault().Value;
                        }
                        else
                        {
                            Dictionary<DateTime, List<FilmSchedule>> ddlfh = FilmSheduleHoChiMinh();
                            lf = ddlfh.Where(x => x.Key == dt).SingleOrDefault().Value;
                        }
                        if (lf.Count == 0)
                        {
                            return Json(new { status = 0, msg = "Không tìm thấy phim bạn yêu cầu" });
                        }
                        int filmId = Int32.Parse(Request.Form["filmid"].ToString());
                        FilmSchedule fs = lf.Where(x => x.Time == time && x.FilmId == filmId).SingleOrDefault();
                        if (fs == null)
                        {
                            return Json(new { status = 0, msg = "Không tìm thấy phim bạn yêu cầu" });
                        }

                        string queryCount = "Select COALESCE(sum(NumberPeople),0) from Registers where Success = 1 and DateView = @DateView and TimeView = @TimeView and FimlIdCode = @FimlIdCode";
                        using(var db = new samsungEntitiesEntities()){
                            int count = db.Database.SqlQuery<int>(queryCount, new SqlParameter("DateView", date), new SqlParameter("TimeView", time), new SqlParameter("FimlIdCode", filmId)).SingleOrDefault();

                            int rs = fs.Total - count;
                            if (rs <= 0)
                            {
                                return Json(new { status = 1, msg = "Số lượng vé còn <strong>0</strong> vé" });
                            }
                            else
                            {
                                return Json(new { status = 1, msg = "Số lượng vé còn <strong>" + rs + "</strong> vé" });
                            }
                        }
                        
                    }
                    catch (Exception ex) {
                        return Json(new { status = 0, msg = "Không tìm thấy phim bạn yêu cầu" });
                    }
                }
                else {
                    return Json(new { status = 0,msg = "Không tìm thấy phim bạn yêu cầu"});
                }

            }
            return View();
        }

       
        public ActionResult ListFilmFilter() {
            List<Register> lr = new List<Register>();
            ViewBag.Date = "";
                    ViewBag.Time = "";
                    ViewBag.Cinema= "";
                    ViewBag.FilmId = 0;
            int count = 0;
            if (Request.HttpMethod.ToLower() == "post")
            {
                var date = Request.Form["date"] != null ? Helper.Common.ParseQueryString(Request.Form["date"].ToString().Trim()) : "";
                var time = Request.Form["time"] != null ? Helper.Common.ParseQueryString(Request.Form["time"].ToString().Trim()) : "";
                var cinema = Request.Form["cinema"] != null ? Helper.Common.ParseQueryString(Request.Form["cinema"].ToString().Trim()) : "";
                var filmId = Request.Form["filmid"] != null ? Helper.Common.ParseQueryString(Request.Form["filmid"].ToString().Trim()) : "";
                if(date != "" || time != "" || cinema != "" || filmId != ""){
                    
                    using(var db = new samsungEntitiesEntities()){
                        lr = db.Registers.Where(x=>x.Success==1).OrderByDescending(x=>x.RegisterId).ToList();
                        if (cinema != "")
                            lr = lr.Where(x => x.CinemaIdCode.Value == Int32.Parse(cinema)).ToList() ;

                        if (date != "")
                            lr = lr.Where(x => x.DateView == date).ToList();

                        if(time != "")
                            lr = lr.Where(x => x.TimeView == time).ToList();
                        if (filmId != "")
                            lr = lr.Where(x => x.FimlIdCode.Value == Int32.Parse(filmId)).ToList();

                        string query = "select  COALESCE(sum(NumberPeople),0) from Registers where Success = 1 ";
                        var whereCondition = "";
                        if (date != "")
                            whereCondition += " and DateView = @date ";
                        if(cinema != "")
                            whereCondition += " and CinemaIdCode = @cinemaidcode ";
                        if(time!="")
                            whereCondition += " and TimeView = @time ";
                        if(filmId!="")
                            whereCondition += " and FimlIdCode = @filmcode ";

                        if (whereCondition!="")
                        {
                            count = db.Database.SqlQuery<int>(query + whereCondition, new SqlParameter("date", date), new SqlParameter("time", time), new SqlParameter("cinemaidcode", cinema != "" ? Int16.Parse(cinema) : 0), new SqlParameter("filmcode", filmId != "" ? Int32.Parse(filmId) : 0)).FirstOrDefault();
                        }
                        
                    }
                    ViewBag.TotalUser = count;
                    ViewBag.Date = date;
                    ViewBag.Time = time;
                    ViewBag.Cinema= cinema;
                    ViewBag.FilmId = filmId != "" ? Int32.Parse(filmId) : 0;
                }
            }
            ViewBag.ListFilm = lr;
            return View();
        }

        /*public ActionResult Test() {
            DateTime dtCurent = Helper.Common.GetTimeInVietNam();

            string[] dateSplit = {"2017","04","30"};
            string[] timeSplit = {"01","20"};
            DateTime dtRegister = new DateTime(int.Parse(dateSplit[0]), int.Parse(dateSplit[1]), int.Parse(dateSplit[2]), int.Parse(timeSplit[0]), int.Parse(timeSplit[1]), 0);
            TimeSpan span = dtRegister.Subtract(dtCurent);
            if (span.Days <= 0 && span.Hours <= 0 && span.Minutes < 60)
            {
                return Json(new { status = 0, msg = "<p>Xin lỗi !Đã hết thời gian đăng ký cho suất chiếu này</p>" });
            }
            return
                View();
        }*/

    }
   
    public class FilmSchedule {
        public string Time { get; set; }
        public int FilmId { get; set; }
        public string FilmName { get; set; }
        public int Total { get; set; }
    }

    public class ReviewCustom {
        public string Name { get; set; }
        public string ReviewContent { get; set; }
        public DateTime Created { get; set; }
    }

}
public class FilterPostActionFilter : ActionFilterAttribute
{
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        try
        {
            var method = filterContext.RequestContext.HttpContext.Request.HttpMethod.ToLower();
            
            if (method.Equals("post"))
            {
                var forms = filterContext.RequestContext.HttpContext.Request;
                if (forms.Form["__RequestVerificationToken"] == null)
                {
                    throw new Exception();
                }
                string[] keys = forms.RequestContext.HttpContext.Request.Form.AllKeys;
                foreach (var k in keys)
                {
                    if (k.Equals("__RequestVerificationToken"))
                        continue;
                    var value = Samsung.Helper.Common.ParseQueryString(forms[k]);
                    if (!value.Equals(forms[k]))
                    {
                        throw new Exception();
                    }
                }
            }
            else
            {
                 base.OnActionExecuting(filterContext);
                //  var routeValue = filterContext.RequestContext.RouteData.Values;
                 string parameters = filterContext.RequestContext.HttpContext.Request.RawUrl;
                 if (parameters != "")
                 {
                     string source = "";
                     if (filterContext.RequestContext.HttpContext.Request.UrlReferrer != null && filterContext.RequestContext.HttpContext.Request.UrlReferrer.ToString() != "")
                     {
                         source = "" + filterContext.RequestContext.HttpContext.Request.UrlReferrer;
                     }
                     if (parameters.IndexOf('?') > 0)
                     {
                         parameters += "&source=" + HttpUtility.UrlEncode(source);
                     }
                     else
                     {
                         if(source != "")
                             parameters += "?source=" + HttpUtility.UrlEncode(source);
                     }
                     filterContext.RequestContext.HttpContext.Response.Redirect("https://vn.campaign.samsung.com/trainghiemqled" + parameters);
                     filterContext.RequestContext.HttpContext.Response.End();
                 }
                 else
                 {
                    filterContext.RequestContext.HttpContext.Response.Redirect("https://vn.campaign.samsung.com/trainghiemqled");
                    filterContext.RequestContext.HttpContext.Response.End();

                 }





                if (filterContext.RequestContext.HttpContext.Request.QueryString["source"] != null && Samsung.Helper.Common.ParseQueryString(filterContext.RequestContext.HttpContext.Request.QueryString["source"].ToString()) != "")
                 {
                     filterContext.RequestContext.HttpContext.Session["REFER_SITE"] = filterContext.RequestContext.HttpContext.Request.QueryString["source"];
                 }
                 else if (filterContext.RequestContext.HttpContext.Session["REFER_SITE"] == null || filterContext.RequestContext.HttpContext.Session["REFER_SITE"].ToString() == "")
                 {
                     filterContext.RequestContext.HttpContext.Session["REFER_SITE"] = filterContext.RequestContext.HttpContext.Request.UrlReferrer != null ? Samsung.Helper.Common.ParseQueryString(filterContext.RequestContext.HttpContext.Request.UrlReferrer.ToString()) : "";
                 }


                //  var routeValue = filterContext.RequestContext.RouteData.Values;
                /*string parameters = filterContext.RequestContext.HttpContext.Request.RawUrl;
                if (filterContext.RequestContext.HttpContext.Request.QueryString["source"] != null && Samsung.Helper.Common.ParseQueryString(filterContext.RequestContext.HttpContext.Request.QueryString["source"].ToString()) != "")
                {
                    filterContext.RequestContext.HttpContext.Session["REFER_SITE"] = filterContext.RequestContext.HttpContext.Request.QueryString["source"];
                }
                else if (filterContext.RequestContext.HttpContext.Session["REFER_SITE"] == null || filterContext.RequestContext.HttpContext.Session["REFER_SITE"].ToString() == "")
                {
                    filterContext.RequestContext.HttpContext.Session["REFER_SITE"] = filterContext.RequestContext.HttpContext.Request.UrlReferrer != null ? Samsung.Helper.Common.ParseQueryString(filterContext.RequestContext.HttpContext.Request.UrlReferrer.ToString()) : "";
                }*/
            }
        }
        catch (Exception ex)
        {
            filterContext.Result = new HttpStatusCodeResult(404);
           /* filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.ContentType = "application/json; charset=utf-8";
            filterContext.HttpContext.Response.Write("{status:0,message:'<p>Không tìm thấy đường dẫn bạn yêu cầu</p>'}");
            filterContext.HttpContext.Response.End();*/
           
        }
    }

}