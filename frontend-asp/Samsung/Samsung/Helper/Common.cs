﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using System.Text.RegularExpressions;
using System.IO;
using System.Text;
namespace Samsung.Helper
{
    public static class Common
    {
      

        #region Convert , format string to date time
        /// <summary>
        /// Convert format string to datetime
        /// </summary>
        /// <param name="value">string</param>
        /// <param name="format">string</param>
        /// <returns>DateTime?</returns>
        public static DateTime? ConvertDate( string value, string format) {
            DateTime date = new DateTime();
            try
            {
               date = DateTime.ParseExact(value, format, null);
            }
            catch {
                return null;
            }

            return date;
        }
        #endregion

        #region Get Time In VietNam
        /// <summary>
        /// Retrun Datetime current in VietNam
        /// </summary>
        /// <returns>Datetime</returns>
        public static DateTime GetTimeInVietNam()
        {
            string ZoneId = "SE Asia Standard Time"; //(GMT+07:00) Bangkok, Hanoi, Jakarta
            DateTime localtime = DateTime.Now;
            TimeZoneInfo timeZoneInfo = System.TimeZoneInfo.FindSystemTimeZoneById(ZoneId);
            DateTime dataTimeByZoneId = System.TimeZoneInfo.ConvertTime(localtime, System.TimeZoneInfo.Local, timeZoneInfo);
            string test = dataTimeByZoneId.ToString("yyyy-MM-dd HH:mm:ss");

            return DateTime.ParseExact(test, "yyyy-MM-dd HH:mm:ss", null);
        }
        #endregion

        #region ToTitleCase
        /// <summary>
        /// ToTitleCase
        /// </summary>
        /// <param name="str"></param>
        /// <returns>string</returns>
        public static string ToTitleCase(string str)
        {
            if (String.IsNullOrEmpty(str))
                return "";
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }
        #endregion

        #region ConvertFormInputToInt
        /// <summary>
        /// Convert Form input to int 
        /// </summary>
        /// <param name="formiputs"></param>
        /// <returns>int?</returns>
        public static int? ConvertFormInputToInt(string formiputs) {
            try
            {
                return Int32.Parse( formiputs );
            }
            catch {
                return null;
            }
        }
        #endregion

        #region Status
        /// <summary>
        /// Return List SelectListItem For Select Option
        /// </summary>
        /// <returns>List SelectListItem</returns>
        public static List<SelectListItem> GetStatus()
        {
            List<SelectListItem> ls = new List<SelectListItem>();
            ls.Add(new SelectListItem {Text = "Approved", Value = "1"  });
            ls.Add(new SelectListItem { Text = "Pendding", Value = "2" });
            ls.Add(new SelectListItem { Text = "Deleted", Value = "3" });
            return ls;
        }
        #endregion 

   
        public static bool IsValidEmail(string email)
        {
            return  Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }
        

        #region ConvertQueryString
        public static string ConvertQueryString(Object obj)
        {
            return obj != null && obj.ToString() != "" ? obj.ToString() : "";
        }
        #endregion

        #region ListProvince
        public static Dictionary<int, string> ListProvince() {
            Dictionary<int, string> provinces = new Dictionary<int, string>();
            provinces.Add(1, "Hà Nội");
            provinces.Add(2, "Hồ Chí Minh");
            provinces.Add(3, "Đà Nẵng");
            provinces.Add(4, "Cần Thơ");
            provinces.Add(5, "An Giang");
            provinces.Add(6, "Bà Rịa - Vũng Tàu");
            provinces.Add(7, "Bắc Giang");
            provinces.Add(8, "Bắc Kạn");
            provinces.Add(9, "Bạc Liêu");
            provinces.Add(10, "Bắc Ninh");
            provinces.Add(11, "Bến Tre");
            provinces.Add(12, "Bình Định");
            provinces.Add(13, "Bình Dương");
            provinces.Add(14, "Bình Phước");
            provinces.Add(15, "Bình Thuận");
            provinces.Add(16, "Cà Mau");
            provinces.Add(17, "Cao Bằng");
            provinces.Add(18, "Đắk Lắk");
            provinces.Add(19, "Đắk Nông");
            provinces.Add(21, "Điện Biên");
            provinces.Add(22, "Đồng Nai");
            provinces.Add(23, "Đồng Tháp");
            provinces.Add(24, "Gia Lai");
            provinces.Add(25, "Hà Giang");
            provinces.Add(26, "Hà Nam");
            provinces.Add(27, "Hà Tĩnh");
            provinces.Add(28, "Hải Dương");
            provinces.Add(29, "Hậu Giang");
            provinces.Add(30, "Hòa Bình");
            provinces.Add(31, "Hưng Yên");
            provinces.Add(32, "Khánh Hòa");
            provinces.Add(33, "Kiên Giang");
            provinces.Add(34, "Kon Tum");
            provinces.Add(35, "Lai Châu");
            provinces.Add(36, "Lâm Đồng");
            provinces.Add(37, "Lạng Sơn");
            provinces.Add(38, "Lào Cai");
            provinces.Add(39, "Long An");
            provinces.Add(40, "Nam Định");
            provinces.Add(41, "Nghệ An");
            provinces.Add(42, "Ninh Bình");
            provinces.Add(43, "Ninh Thuận");
            provinces.Add(44, "Phú Thọ");
            provinces.Add(45, "Quảng Bình");
            provinces.Add(46, "Quảng Nam");
            provinces.Add(47, "Quảng Ngãi");
            provinces.Add(48, "Quảng Ninh");
            provinces.Add(49, "Quảng Trị");
            provinces.Add(51, "Sóc Trăng");
            provinces.Add(52, "Sơn La");
            provinces.Add(53, "Tây Ninh");
            provinces.Add(54, "Thái Bình");
            provinces.Add(55, "Thái Nguyên");
            provinces.Add(56, "Thanh Hóa");
            provinces.Add(57, "Thừa Thiên Huế");
            provinces.Add(58, "Tiền Giang");
            provinces.Add(59, "Trà Vinh");
            provinces.Add(60, "Tuyên Quang");
            provinces.Add(61, "Vĩnh Long");
            provinces.Add(62, "Vĩnh Phúc");
            provinces.Add(63, "Yên Bái");
            provinces.Add(64, "Phú Yên");
            provinces.Add(65, "Hải Phòng");
            return provinces;
        }
        #endregion

        public static bool GemSendEmail(string toEmail ,string fromEmail,string from_name,string to_name,string subject,string message)
        {
            Uri uriStr = new Uri("http://gems.samsung.com/ems/SemsEtaSend.do");

            string strEmail = String.Format("mail_code={0}&from_mail={1}&to_mail={2}&from_name={3}&to_name={4}&subject={5}&message={6}",
                                            "01", fromEmail, toEmail, from_name,to_name, subject, message);

            HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(uriStr + "?" + strEmail);
            objRequest.ServicePoint.Expect100Continue = false;
            objRequest.Method = WebRequestMethods.Http.Get;
            objRequest.Timeout = 80000; // 60 seconds in milliseconds
            objRequest.ContentType = "application/x-www-form-urlencoded";
            objRequest.Credentials = CredentialCache.DefaultCredentials;

            // Set credentials to use for this request.
            objRequest.Credentials = CredentialCache.DefaultCredentials;

            try
            {
                HttpWebResponse response = (HttpWebResponse)objRequest.GetResponse();
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, System.Text.Encoding.UTF8);
                string rs = readStream.ReadToEnd();
                HttpContext.Current.Session["erroremail"] = readStream.ReadToEnd();
                response.Close();
                readStream.Close();

                if(rs.Contains("ok") || rs.Equals("")){
                    return true;
                }
                

                return false;
            }
            catch (WebException ex)
            {
                HttpContext.Current.Session["erroremail"] = ex.Message;
                return false;
            }

        }


        public static string RandomString(int length) {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        #region Return list page config key,value
        /// <summary>
        /// Return Dictionary page key => string, name page => string
        /// </summary>
        /// <returns>Dictionary<string,string></returns>
        public static Dictionary<string, string> PageConfig()
        {
            Dictionary<string, string> dss = new Dictionary<string, string>();
            dss.Add("index", "Trang chủ");
            dss.Add("tearm", "Trang thể lệ");
            dss.Add("gallery", "Trang hình ảnh");
            dss.Add("add_image", "Trang Upload hình");
            dss.Add("video", "Trang Video");
            dss.Add("add_video", "Trang Upload Video");
            dss.Add("award", "Trang giải thưởng");
            dss.Add("user_award", "Trang danh sách người trúng thưởng");
            dss.Add("default", "Mặc định");
            dss.Add("uploadimage360", "Upload hình 360");
            dss.Add("videodetail", "Chi tiết Video");
            return dss;
        }

        #endregion
        public static string ConvertToUnsign1(string str)
        {
            string[] signs = new string[] { 
                    "aAeEoOuUiIdDyY",
                    "áàạảãâấầậẩẫăắằặẳẵ",
                    "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
                    "éèẹẻẽêếềệểễ",
                    "ÉÈẸẺẼÊẾỀỆỂỄ",
                    "óòọỏõôốồộổỗơớờợởỡ",
                    "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
                    "úùụủũưứừựửữ",
                    "ÚÙỤỦŨƯỨỪỰỬỮ",
                    "íìịỉĩ",
                    "ÍÌỊỈĨ",
                    "đ",
                    "Đ",
                    "ýỳỵỷỹ",
                    "ÝỲỴỶỸ"
               };
            for (int i = 1; i < signs.Length; i++)
            {
                for (int j = 0; j < signs[i].Length; j++)
                {
                    str = str.Replace(signs[i][j], signs[0][i - 1]);
                }
            }
            return str.Replace(" ","") ;
        }

        public static string ParseQueryString(object text)
        {
            if (text == null || string.IsNullOrEmpty(text.ToString())) return "";
            string strText = Regex.Replace(text.ToString(), @"<[(=^>]*>", string.Empty);
            //strText = HttpContext.Current.Server.HtmlDecode(strText);
            strText = strText.Replace("'", string.Empty);
            strText = strText.Replace("=", string.Empty);
            strText = Regex.Replace(strText, "<.*?>", String.Empty);
            strText = Regex.Replace(strText, @"</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n|\s)*?>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            strText = Regex.Replace(strText, @"(;|\s)(exec|execute|select|insert|update|delete|create|alter|drop|rename|truncate|backup|restore)\s", string.Empty, RegexOptions.Singleline | RegexOptions.IgnoreCase);
            //strText = Regex.Replace(strText, @"[*/]+", string.Empty);        
            return strText;
        }

        public static bool SendSms(string phoneNumber,string code, string time,string username,string password, string cinema)
        {
            
            Uri uriStr = new Uri(Samsung.Properties.Settings.Default.SmsUrl);

            HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(uriStr);
            myHttpWebRequest.Method = WebRequestMethods.Http.Post;
            string postData = "phone_number=" + phoneNumber + "&code=" + code + "&__RequestVerificationToken=QWERTYUIOIUYTR&time=" + time + "&username=" + username + "&password=" + password + "&cinema=" + cinema;
            
            byte[] data = Encoding.ASCII.GetBytes(postData);

            myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
            myHttpWebRequest.ContentLength = data.Length;
           
            Stream requestStream = myHttpWebRequest.GetRequestStream();
            requestStream.Write(data, 0, data.Length);
            requestStream.Close();

            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            Stream responseStream = myHttpWebResponse.GetResponseStream();

            StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);

            string pageContent = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            responseStream.Close();
            if (pageContent == "200")
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static string ParseDateToString(DateTime dateinput, string format = "dd/MM/yyyy h:mm", string sMsg = "")
    {
        try
        {
            DateTime date = dateinput;
            TimeSpan diff = DateTime.Now - date;
            if (diff.Days != 0 && diff.Days < 7)
            {
                sMsg += diff.Days + " ngày trước.";
            }
            else if (diff.Days > 7)
            {
                sMsg = date.ToString(format);
            }
            else if (diff.Hours != 0)
            {
                sMsg += diff.Hours + " giờ trước.";
            }
            else if (diff.Minutes != 0)
            {
                sMsg += diff.Minutes + " phút trước.";
            }
            else if (diff.Seconds != 0)
            {
                sMsg += diff.Seconds + " giây trước.";
            }
            else if (diff.Seconds == 0)
            {
                sMsg = "Mới đây.";
            }
            else
            {
                sMsg = "";
            }
        }
        catch (Exception ex)
        {
            sMsg = "";
        }

        return sMsg;
    }


    }

  
    public  class Status 
    {
        public Status(int id, string value) {
            this.Id = id;
            this.Value = value;
        }

        public int Id { get; set; }
        public string Value { get; set; }
    }
}