/* BEFORE AFTER EFFECT
Author: GOON NGUYEN
================================================== */

$(function(){
  $(".before-after").each(function(i){
    apply($(this));
  });

  function apply(target){
    target.find("img").attr("draggable", "false");
    target.find(".before-after-scrubber").on("mousedown touchstart", onStart);

    $(window).on('resize', resize);
    resize();

    function onStart(e){
      e.preventDefault();
      var x = (e.pageX || e.originalEvent.touches[0].pageX);
      var y = (e.pageY || e.originalEvent.touches[0].pageY)
      var localPos = {};
      localPos.x = x - target.offset().left;
      localPos.y = y - target.offset().top;

      $(window).on('mouseup touchend', onStop);
      $(window).on('mousemove touchmove', onMove);
    }

    function onMove(e){
      e.preventDefault();

      var x = (e.pageX || e.originalEvent.touches[0].pageX);
      var y = (e.pageY || e.originalEvent.touches[0].pageY)
      var localPos = {};
      localPos.x = x - target.offset().left;
      localPos.y = y - target.offset().top;

      var perX = localPos.x / target.outerWidth();
      if(perX < 0) perX = 0;
      if(perX > 1) perX = 1;

      $(".before-after-slider").css("left", (perX * 100) + "%");
      $(".before-after-left").css("width", (perX * 100) + "%");
      //console.log(localPos)
    }

    function onStop(e){
      e.preventDefault();
      /*var x = (e.pageX || e.originalEvent.touches[0].pageX);
      var y = (e.pageY || e.originalEvent.touches[0].pageY)
      var localPos = {};
      localPos.x = x - target.offset().left;
      localPos.y = y - target.offset().top;*/

      $(window).off('mouseup touchend', onStop);
      $(window).off('mousemove touchmove', onMove);
    }

    function resize(e){
      target.find("img").each(function(i){
        $(this).width( target.outerWidth() );
        $(this).css('height', 'auto');

        if($(this).outerHeight() < target.outerHeight()){
          $(this).height( target.outerHeight() );
          $(this).css('width', 'auto');
        }

        $(this).css("margin-left", (target.outerWidth() - $(this).outerWidth())/2);
        $(this).css("margin-top", (target.outerHeight() - $(this).outerHeight())/2);
      });
    }
  }
});

