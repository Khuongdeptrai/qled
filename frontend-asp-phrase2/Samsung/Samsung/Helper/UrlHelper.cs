﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Samsung.Helper
{
    public class UrlHelperPage
    {
        public static string SiteUrl
        {
            get
            {
                HttpContext context = HttpContext.Current;
                return context.Request.Url.OriginalString + context.Request.ApplicationPath.TrimEnd('/');
            }
        }

        public static string Webroot
        {
            get
            {
                HttpContext context = HttpContext.Current;
                return context.Request.ApplicationPath.TrimEnd('/') + '/';
            }
        }

        public static string BaseUrl
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if (context.Request.Browser.Type.ToUpper().Contains("IE") && Convert.ToDouble(context.Request.Browser.Version) < 10)
                    return context.Request.Url.GetLeftPart(UriPartial.Authority) + UrlHelperPage.Webroot;
                return UrlHelperPage.Webroot;

            }
        }
    }
}