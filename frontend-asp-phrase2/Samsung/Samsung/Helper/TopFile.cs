﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;

namespace Samsung.Helper
{
    public static class TopFile
    {
        #region Convert base 64 to image
        /// <summary>
        /// Convert base 64 to image file
        /// </summary>
        /// <param name="base64"></param>
        /// <param name="folder"></param>
        /// <returns>Object InfoFile</returns>
        public static InfoFile UploadBase64(string base64, string folder)
        {
            string converBase64 = Regex.Replace(base64, "^data:image/(jpg|jpeg|png|gif|PNG|JPG|JPEG|GIF);base64,", "");
            InfoFile iF = new InfoFile();
            try
            {
                
                Byte[] bytes = Convert.FromBase64String(converBase64);
                DateTime today = DateTime.Today;
                string todayFolder = folder + today.ToString("yyyy/MM/dd");
                //CreateFolder(todayFolder);

                string ImageName = System.IO.Path.GetRandomFileName();

                // File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/" + folder + "/" + ImageName + ".jpg"), bytes);
               Image image;
               using (MemoryStream ms = new MemoryStream(bytes))
               {
                    image = new Bitmap(Image.FromStream(ms)); //Image.FromStream(ms);
               }

                image.Save(HttpContext.Current.Server.MapPath("~/" + folder + "/" + ImageName + ".jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);
                iF.Name = folder + "/" + ImageName + ".jpg";
                iF.NameOnly = ImageName + ".jpg";
                iF.FolderDate = todayFolder;
                iF.Ext = "jpg";
                iF.Folder = folder;
            }
            catch (Exception ex)
            {

                string message = " | TopFile Helper | UploadBase64 +" + ex.InnerException + " | " + ex.ToString();

                if (HttpContext.Current.Session["TIM_LOI"] == null)
                {
                    HttpContext.Current.Session["TIM_LOI"] = message;
                }
                else
                {
                    string oldError = HttpContext.Current.Session["TIM_LOI"].ToString();
                    HttpContext.Current.Session["TIM_LOI"] = message + oldError;
                }
                iF = null;
            }
            return iF;
        }
        #endregion

        #region Create Folder
        /// <summary>
        /// Create folder 
        /// </summary>
        /// <param name="folder">string folder name</param>
        /// <returns>Void</returns>
        private static void CreateFolder(string folder)
        {
            string[] folders = folder.Split('/');
            string tempFolder = "~/";
            foreach (string f in folders)
            {
                tempFolder += f+"/" ;
                
                if ( !Directory.Exists( HttpContext.Current.Server.MapPath(tempFolder) ))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(tempFolder));
                }
            }

        }
        #endregion

        #region UploadImageFileBase
        /// <summary>
        /// Upload Image HttpPostedFileBase
        /// </summary>
        /// <param name="_ImageFile">HttpPostedFileBase</param>
        /// <param name="_folder">string</param>
        /// <returns><InfoFile/returns>
        public static InfoFile UploadImageFileBase( HttpPostedFileBase _ImageFile, string _folder ) {
            InfoFile iF = new InfoFile();
            try {
                
                int MaxContentLength = 1024 * 1024 * 10; //10 MB
                string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".jpeg" };
                 byte[] image = new byte[_ImageFile.ContentLength];

                // int fileSizeInBytes = _ImageFile.ContentLength;
               //  MemoryStream target = new MemoryStream();
               //  _ImageFile.InputStream.CopyTo(target);
              //   byte[] data = target.ToArray();

               //  var image1 = new Bitmap(_ImageFile.InputStream);

                if (_ImageFile == null || _ImageFile.ContentLength <= 0)
                {
                    iF.Error = 1;
                    iF.Message = "Vui lòng upload file";
                    return iF;
                }
                else if (!AllowedFileExtensions.Contains(_ImageFile.FileName.ToLower().Substring(_ImageFile.FileName.ToLower().LastIndexOf('.'))))
                {
                    iF.Error = 1;
                    iF.Message = "Vui lòng lựa chọn file hình";
                    return iF;
                }
                else if (_ImageFile.ContentLength > MaxContentLength)
                {
                    iF.Error = 1;
                    iF.Message = "Vui lòng lựa chọn file hình kích thước nhỏ hơn 10 MB";
                    return iF;
                }

                DateTime today = DateTime.Today;
                //string todayFolder = _folder + today.ToString("yyyy/MM/dd");
                //"Uploads/Avatars/"

                // CreateFolder(todayFolder);
                iF.Ext = Path.GetExtension(_ImageFile.FileName);
                iF.Size = _ImageFile.ContentLength;
                iF.Name = System.IO.Path.GetRandomFileName() + iF.Ext;
                iF.Folder = _folder + "/";


                WebImage img = new WebImage(_ImageFile.InputStream);
                if (img.Width > 1000)
                {
                    img.Resize(800, 800);
                }
                img.Save(HttpContext.Current.Server.MapPath("~/" + _folder + "/" + iF.Name),"jpg",false);   

               // var path = Path.Combine();
              //  _ImageFile.SaveAs(path);
                
            }
            catch
            {
                iF = null;
            }
            return iF;
        }
        #endregion
    }

    
    public class InfoFile{
        public string Name { get; set; }
        public string NameOnly { get; set; }
        public string Folder { get;set;}
        public string FolderDate { get; set; }
        public string Ext {get;set; }
        public int Size { get;set; }
        public int Error { get; set; }
        public string Message { get; set; }
    }
}