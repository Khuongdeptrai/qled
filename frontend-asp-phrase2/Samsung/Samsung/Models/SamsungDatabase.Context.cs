﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Samsung.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class samsungEntitiesEntities : DbContext
    {
        public samsungEntitiesEntities()
            : base("name=samsungEntitiesEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<QhomeRegister> QhomeRegisters { get; set; }
        public DbSet<QsuiteRegister> QsuiteRegisters { get; set; }
        public DbSet<Register> Registers { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<ReviewsQsuite> ReviewsQsuites { get; set; }
        public DbSet<user> users { get; set; }
        public DbSet<UsersRegister> UsersRegisters { get; set; }
    }
}
