
CREATE TABLE [dbo].[UsersRegister] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Email] nvarchar(255) NULL ,
[FacebookId] nvarchar(255) NULL ,
[Name] nvarchar(255) NOT NULL ,
[Gender] nvarchar(50) NULL ,
[Created] datetime NULL ,
[Invite] int NULL ,
[Birthday] datetime NULL ,
[Phone] varchar(20) NULL ,
[Address] nvarchar(500) NULL ,
[Province] int NULL ,
[UserNameSearch] nvarchar(255) NULL ,
[FacebookToken] nvarchar(700) NULL ,
[FacebookName] nvarchar(255) NULL ,
[Refer] nvarchar(MAX) NULL ,
[Utm] nvarchar(MAX) NULL 
)


GO

-- ----------------------------
-- Indexes structure for table UsersRegister
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table UsersRegister
-- ----------------------------
ALTER TABLE [dbo].[UsersRegister] ADD PRIMARY KEY ([Id])
GO
