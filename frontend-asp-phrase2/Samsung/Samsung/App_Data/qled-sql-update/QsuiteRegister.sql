
CREATE TABLE [dbo].[QsuiteRegister] (
[QsuiteRegisterId] int NOT NULL IDENTITY(1,1) ,
[Name] nvarchar(255) NULL ,
[Brand] nvarchar(155) NULL ,
[Email] nvarchar(255) NULL ,
[Phone] nvarchar(50) NULL ,
[Acreage] nvarchar(50) NULL ,
[HotelName] nvarchar(255) NULL ,
[Status] int NULL ,
[Created] datetime NULL ,
[FacebookId] nvarchar(255) NULL ,
[Birthday] int NULL ,
[Address] nvarchar(MAX) NULL ,
[Job] nvarchar(MAX) NULL ,
[FacebookName] nvarchar(255) NULL ,
[Refer] nvarchar(MAX) NULL ,
[Utm] nvarchar(MAX) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[QsuiteRegister]', RESEED, 89)
GO

-- ----------------------------
-- Indexes structure for table QsuiteRegister
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table QsuiteRegister
-- ----------------------------
ALTER TABLE [dbo].[QsuiteRegister] ADD PRIMARY KEY ([QsuiteRegisterId])
GO
