
DROP TABLE [dbo].[ReviewsQsuite]
GO
CREATE TABLE [dbo].[ReviewsQsuite] (
[ReviewId] int NOT NULL IDENTITY(1,1) ,
[ReviewContent] ntext NULL ,
[Status] int NULL ,
[Created] datetime NULL ,
[Email] nvarchar(255) NULL ,
[Name] nvarchar(255) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ReviewsQsuite]', RESEED, 4)
GO

-- ----------------------------
-- Indexes structure for table ReviewsQsuite
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ReviewsQsuite
-- ----------------------------
ALTER TABLE [dbo].[ReviewsQsuite] ADD PRIMARY KEY ([ReviewId])
GO
