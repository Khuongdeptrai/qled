
CREATE TABLE [dbo].[QhomeRegister] (
[QhomeRegisterId] int NOT NULL IDENTITY(1,1) ,
[Name] nvarchar(255) NULL ,
[Brand] nvarchar(155) NULL ,
[Email] nvarchar(255) NULL ,
[Phone] nvarchar(50) NULL ,
[Acreage] nvarchar(50) NULL ,
[HotelName] nvarchar(255) NULL ,
[Status] int NULL ,
[Created] datetime NULL ,
[FacebookId] nvarchar(255) NULL ,
[Image] nvarchar(255) NULL ,
[Birthday] int NULL ,
[Address] nvarchar(MAX) NULL ,
[Job] nvarchar(255) NULL ,
[FacebookName] nvarchar(255) NULL ,
[City] int NULL ,
[Refer] nvarchar(MAX) NULL ,
[Utm] nvarchar(MAX) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[QhomeRegister]', RESEED, 1046)
GO

-- ----------------------------
-- Indexes structure for table QhomeRegister
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table QhomeRegister
-- ----------------------------
ALTER TABLE [dbo].[QhomeRegister] ADD PRIMARY KEY ([QhomeRegisterId])
GO



CREATE TABLE [dbo].[QsuiteRegister] (
[QsuiteRegisterId] int NOT NULL IDENTITY(1,1) ,
[Name] nvarchar(255) NULL ,
[Brand] nvarchar(155) NULL ,
[Email] nvarchar(255) NULL ,
[Phone] nvarchar(50) NULL ,
[Acreage] nvarchar(50) NULL ,
[HotelName] nvarchar(255) NULL ,
[Status] int NULL ,
[Created] datetime NULL ,
[FacebookId] nvarchar(255) NULL ,
[Birthday] int NULL ,
[Address] nvarchar(MAX) NULL ,
[Job] nvarchar(MAX) NULL ,
[FacebookName] nvarchar(255) NULL ,
[Refer] nvarchar(MAX) NULL ,
[Utm] nvarchar(MAX) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[QsuiteRegister]', RESEED, 89)
GO

-- ----------------------------
-- Indexes structure for table QsuiteRegister
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table QsuiteRegister
-- ----------------------------
ALTER TABLE [dbo].[QsuiteRegister] ADD PRIMARY KEY ([QsuiteRegisterId])
GO




CREATE TABLE [dbo].[UsersRegister] (
[Id] int NOT NULL IDENTITY(1,1) ,
[Email] nvarchar(255) NULL ,
[FacebookId] nvarchar(255) NULL ,
[Name] nvarchar(255) NOT NULL ,
[Gender] nvarchar(50) NULL ,
[Created] datetime NULL ,
[Invite] int NULL ,
[Birthday] datetime NULL ,
[Phone] varchar(20) NULL ,
[Address] nvarchar(500) NULL ,
[Province] int NULL ,
[UserNameSearch] nvarchar(255) NULL ,
[FacebookToken] nvarchar(700) NULL ,
[FacebookName] nvarchar(255) NULL ,
[Refer] nvarchar(MAX) NULL ,
[Utm] nvarchar(MAX) NULL 
)


GO

-- ----------------------------
-- Indexes structure for table UsersRegister
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table UsersRegister
-- ----------------------------
ALTER TABLE [dbo].[UsersRegister] ADD PRIMARY KEY ([Id])
GO



CREATE TABLE [dbo].[ReviewsQsuite] (
[ReviewId] int NOT NULL IDENTITY(1,1) ,
[ReviewContent] ntext NULL ,
[Status] int NULL ,
[Created] datetime NULL ,
[Email] nvarchar(255) NULL ,
[Name] nvarchar(255) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[ReviewsQsuite]', RESEED, 4)
GO

-- ----------------------------
-- Indexes structure for table ReviewsQsuite
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ReviewsQsuite
-- ----------------------------
ALTER TABLE [dbo].[ReviewsQsuite] ADD PRIMARY KEY ([ReviewId])
GO
