
CREATE TABLE [dbo].[QhomeRegister] (
[QhomeRegisterId] int NOT NULL IDENTITY(1,1) ,
[Name] nvarchar(255) NULL ,
[Brand] nvarchar(155) NULL ,
[Email] nvarchar(255) NULL ,
[Phone] nvarchar(50) NULL ,
[Acreage] nvarchar(50) NULL ,
[HotelName] nvarchar(255) NULL ,
[Status] int NULL ,
[Created] datetime NULL ,
[FacebookId] nvarchar(255) NULL ,
[Image] nvarchar(255) NULL ,
[Birthday] int NULL ,
[Address] nvarchar(MAX) NULL ,
[Job] nvarchar(255) NULL ,
[FacebookName] nvarchar(255) NULL ,
[City] int NULL ,
[Refer] nvarchar(MAX) NULL ,
[Utm] nvarchar(MAX) NULL 
)


GO
DBCC CHECKIDENT(N'[dbo].[QhomeRegister]', RESEED, 1046)
GO

-- ----------------------------
-- Indexes structure for table QhomeRegister
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table QhomeRegister
-- ----------------------------
ALTER TABLE [dbo].[QhomeRegister] ADD PRIMARY KEY ([QhomeRegisterId])
GO
