/* ADDITIONAL CODE
Author: GOON NGUYEN
================================================== */

var EFFECTS = {
  init: function(){
    EFFECTS.animateSlideArrows();

    if($("#dgtHome").length > 0) EFFECTS.animateHomepage();

    if($("#dgtQcinema").length > 0) EFFECTS.animateLandingCinema();

    if($("#dgtLibrary").length > 0) EFFECTS.animateMovieThumb();

    if($("#dgtBooking").length > 0) EFFECTS.animateMovieDetail();

    if($("#dgtReview").length > 0) EFFECTS.animateReview();
  },

  animateSlideArrows: function(){
    TweenMax.to(".before-after-left-arrow", .4, {x: -10, ease: Sine.easeInOut, yoyo: true, repeat: -1});
    TweenMax.to(".before-after-right-arrow", .4, {x: 10, ease: Sine.easeInOut, yoyo: true, repeat: -1});
  },

  animateHomepage: function(){
    TweenMax.set(".twentytwenty-container .twentytwenty-before", {opacity: 0, transformOrigin: "25% 50%"});
    TweenMax.set(".twentytwenty-container .twentytwenty-after", {opacity: 0, transformOrigin: "75% 50%"});

    TweenMax.to(".twentytwenty-container .twentytwenty-before", 0.8, {opacity: 1, scale:1, ease: Quart.easeOut})
    TweenMax.to(".twentytwenty-container .twentytwenty-after", 0.8, {opacity: 1, scale:1, ease: Quart.easeOut})

    TweenMax.set(".dgt_copy h2", {opacity: 0, y: 80});
    TweenMax.set(".dgt_copy p", {opacity: 0, y: 80});

    TweenMax.to(".dgt_copy h2", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.5});
    TweenMax.to(".dgt_copy p", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.6});

    // $("#dgtHome .dgt_else .dgt_btn").click(function(e){
    //   e.preventDefault();
    //   alert("Hoạt động trải nghiệm này sẽ được cập nhật vào ngày 13/05/2017.");
    // });
  },

  animateLandingCinema: function(){
    TweenMax.set("#dgtQcinema .dgt_copy .dgt_tv img", {scale: 0.6, opacity: 0});
    TweenMax.set("#dgtQcinema .dgt_copy h3", {y: 50, opacity: 0});
    TweenMax.set("#dgtQcinema .dgt_copy p", {y: 50, opacity: 0});
    TweenMax.set("#dgtQcinema .dgt_copy .dgt_btn", {y: 50, opacity: 0});

    TweenMax.to("#dgtQcinema .dgt_copy .dgt_tv img", 0.6, {scale: 1, opacity: 1, ease: Quart.easeOut});
    TweenMax.to("#dgtQcinema .dgt_copy h3", 0.5, {y: 0, opacity: 1, ease: Quart.easeOut, delay: 0.1});
    TweenMax.to("#dgtQcinema .dgt_copy p", 0.5, {y: 0, opacity: 1, ease: Quart.easeOut, delay: 0.15});
    TweenMax.to("#dgtQcinema .dgt_copy .dgt_btn", 0.5, {y: 0, opacity: 1, ease: Quart.easeOut, delay: 0.2});
  },

  animateMovieThumb: function(){
    $(".dgt_library > div .thumb").hover(function(e){
      TweenMax.to($(this), 0.4, {scale: 0.95, ease: Quart.easeOut});
    }, function(e){
      TweenMax.to($(this), 0.4, {scale: 1, ease: Quart.easeOut});
    })
    
  },

  animateMovieDetail: function(){
    var tvScale = ($(window).width() <= 480) ? 0.8 : 1;
    TweenMax.set(".dgt_tv__trailer", {scale: 0.5, opacity: 0});
    TweenMax.to(".dgt_tv__trailer", 0.8, {scale: tvScale, opacity: 1, ease: Quart.easeOut});

    $(".btn-booknow").click(function(e){
      e.preventDefault();

      var scope = this;
      scope.a = $(window).scrollTop();
      var toBookingPostion = $(".dgt_booking__wrap").offset().top - $(".dgt_header").outerHeight();
      TweenMax.to(scope, 0.8, {a: toBookingPostion, ease: Quart.easeInOut, onUpdate: function(){
        $(window).scrollTop(scope.a);
      }})
    });

    if($(window).width() <= 480){
      $(".dgt_tv__booking").height( $(window).outerHeight() );
    } else {
      $(".dgt_tv__booking").height( $(window).outerHeight() - $(".dgt_header").outerHeight() );
    }
  },

  animateReview: function(){
    $(".dgt_btn_write").click(function(e){
      e.preventDefault();
      $(this).hide();
      $(".dgt_writearea").show();
    })
  }

}

$(function(){
  setTimeout(EFFECTS.init, 200);
});
