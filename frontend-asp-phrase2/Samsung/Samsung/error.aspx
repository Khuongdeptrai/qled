﻿<!doctype html>
<html class="no-js" lang="">
<head>
    <base href="/trainghiemqled/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- SEO -->
    <title>Lỗi</title>
       <meta name="description" content="Samsung QLED TV mang đến màu sắc hoàn hảo và trải nghiệm xem đặc biệt sâu hơn, thật hơn với độ sắc nét hoàn hảo của từng khung hình bất kể ngày đêm. Khám phá ngay!">
            <meta name="keywords" content="QLED, quantum dot, Q picture, 4K, TV 4K tốt nhất, Q color, màu sắc, Q contrast, Q HDR, Q HDR2000, QHDR1500, chi tiết, Q viewing angle">
    <meta property="fb:app_id" content="1175364199258085" />

    <link rel="image_src" href="https://vn.campaign.samsung.com/trainghiemqled/assets/images/favicon.gif" />
    <link rel="icon" type="image/gif" href="https://vn.campaign.samsung.com/trainghiemqled/assets/images/favicon.ico" />

    <!-- Spiders must use meta description -->

    <meta name="robots" content="noodp, noydir">

    <!-- No Google Translate toolbar -->

    <meta name="google" content="notranslate">

    <!-- Viewport and mobile -->

    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="HandheldFriendly" content="true">
    <meta name="MobileOptimized" content="320">
    <meta http-equiv="cleartype" content="on">
    <meta name="webroot" content="/trainghiemqled/" />
    <meta name="siteUrl" content="https://vn.campaign.samsung.com/trainghiemqled" />
    <link rel="stylesheet" href="https://vn.campaign.samsung.com/trainghiemqled/assets/css/main.css">
    <!--[if IE]>
        <link rel="stylesheet" type="text/css" href="/trainghiemqled/assets/css/ie.css" />
    <![endif]-->
    <script>
        var action = 'Index';
        var urlShare = '/trainghiemqled/thu-vien-phim';
    </script>
        <script src="https://vn.campaign.samsung.com/trainghiemqled/assets/js/s_code_microsite.js"></script>
		
		<style>
	   #dgt_errorPage h2,#dgt_errorPage h3{font-family:SamsungSharpSans-Bold;color:#333;margin:0;font-weight:400;text-transform:uppercase}#dgt_errorPage h2{font-size:50px;text-align:center}#dgt_errorPage h3{font-size:30px}#dgt_errorPage p{font-family:samsung-400;color:#000;font-size:17px;line-height:25px}#dgt_errorPage a{font-family:samsung-800;text-transform:uppercase;color:#fff;font-size:17px}#dgt_errorPage{width:100%;min-height:100vh;height:100%;position:relative;overflow:hidden;clear:both}#dgt_errorPage .errorPage-wrap{position:absolute;top:50%;left:50%;-webkit-transform:translate3d(-50%,-50%,0);transform:translate3d(-50%,-50%,0);max-width:550px;width:100%}#dgt_errorPage .errorPage-item{display:none;text-align:center}#dgt_errorPage .errorPage-item>*{margin-bottom:20px}#dgt_errorPage .errorPage-item.active{display:block}#dgt_errorPage h3,#dgt_errorPage p{text-align:left}#dgt_errorPage .dgt_btn-back{display:inline-block;text-align:center;padding:10px 20px;background-color:#333;border-radius:10px;-webkit-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease;margin:0}#dgt_errorPage .dgt_btn-back:hover{background-color:#003daa;-webkit-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}@media screen and (max-width:1024px){#dgt_errorPage h3,#dgt_errorPage p{text-align:center}}@media all and (max-width:812px) and (orientation:landscape){#dgt_errorPage .errorPage-item>*{margin-bottom:10px}#dgt_errorPage h3{font-size:20px}#dgt_errorPage .dgt_btn-back,#dgt_errorPage p{font-size:14px}}@media screen and (max-width:414px){#dgt_errorPage{padding:100px 10px 0}#dgt_errorPage .errorPage-wrap{width:100%;padding:0 10px}#dgt_errorPage .errorPage-item>*{margin-bottom:10px}#dgt_errorPage h3{font-size:20px}#dgt_errorPage .dgt_btn-back,#dgt_errorPage p{font-size:14px}#dgt_errorPage p br{display:none}}
	</style>
</head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65210468-38', 'auto');
  ga('send', 'pageview');

</script>
<body>

    <div class="dgt_header">
        <div class="dgt_contain">
            <a href="/trainghiemqled/" class="logo" onclick="omniture_click('vn:trainghiemqled:logo');"></a>
            <ul id="dgt_nav">
                <li>
                    <a onclick="omniture_click('vn:trainghiemqled:menu:dienanhdinhcao');" class="" href="/trainghiemqled/qcinema">Qcinema</a>
                    <div class="dgt_subheader">
                        <ul>
                            <li><a href="/trainghiemqled/thu-vien-phim" onclick="omniture_click('vn:trainghiemqled:menu:dienanhdinhcao:thuvienphim');">THƯ VIỆN PHIM</a></li>
                            <li><a href="/trainghiemqled/cam-nhan-cua-khach-hang" onclick="omniture_click('vn:trainghiemqled:menu:dienanhdinhcao:camnhancuakhachang');">CẢM NHẬN CỦA KHÁN GIẢ</a></li>
                            <li><a href="/trainghiemqled/thu-vien-hinh-anh" onclick="omniture_click('vn:trainghiemqled:menu:dienanhdinhcao:hinhanh');" >HÌNH ẢNH</a></li>
                            <li class="dgt_pc"><a href="http://www.samsung.com/vn/tvs/qled-q8c/QA75Q8CAMKXXV/" target="_blank"  onclick="omniture_click('vn:trainghiemqled:menu:dienanhdinhcao:khamphaqledtv'); engagement_click('QA75Q8CAMKXXV|;QA75Q8CAMK');">ĐẶT MUA QLED TV</a></li>
                            <li class="dgt_pc"><a href="http://www.samsung.com/vn/audio-video/soundbar/"  target="_blank" onclick="omniture_click('vn:trainghiemqled:menu:dienanhdinhcao:khamphasoundbar'); engagement_click('HW-MS6501/XV|;HW-MS6501');">ĐẶT MUA SOUNDBAR</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a class="" href="/trainghiemqled/qsuite" onclick="omniture_click('vn:trainghiemqled:menu:phongcachdinhcao');">Qsuite</a>
                    <div class="dgt_subheader js_qsuite__menu">
                        <ul>
                            <li><a href="/trainghiemqled/jw-marriot-hanoi" onclick="omniture_click('vn:trainghiemqled:menu:phongcachdinhcao:jw-marriot-hanoi');">JW Marriott</a></li>
                            <li><a href="/trainghiemqled/le-meridien-hotel" onclick="omniture_click('vn:trainghiemqled:menu:phongcachdinhcao:le-meridien-hotel');">Le Méridien</a></li>
                            <li><a href="/trainghiemqled/cam-nhan-cua-nguoi-trai-nghiem" onclick="omniture_click('vn:trainghiemqled:menu:phongcachdinhcao:cam-nhan-trai-nghiem');">CẢM NHẬN CỦA NGƯỜI TRẢI NGHIỆM</a></li>
                            <li><a href="/trainghiemqled/qsuite-danh-sach-nguoi-thang-cuoc" onclick="omniture_click('vn:trainghiemqled:menu:phongcachdinhcao:danh-sach-nguoi-thang-cuoc');">Danh sách người thắng cuộc</a></li>
                            <li class="dgt_pc"><a href="http://www.samsung.com/vn/tvs/qled-q8c/QA75Q8CAMKXXV/" target="_blank"  onclick="omniture_click('vn:trainghiemqled:menu:dienanhdinhcao:khamphaqledtv'); engagement_click('QA75Q8CAMKXXV|;QA75Q8CAMK');">ĐẶT MUA QLED TV</a></li>
                            <li class="dgt_pc"><a href="http://www.samsung.com/vn/audio-video/soundbar/"  target="_blank" onclick="omniture_click('vn:trainghiemqled:menu:dienanhdinhcao:khamphasoundbar'); engagement_click('HW-MS6501/XV|;HW-MS6501');">ĐẶT MUA SOUNDBAR</a></li>
                        </ul>
                    </div>
                </li>
                <li >
                    <a class="" href="/trainghiemqled/qhome" onclick="omniture_click('vn:trainghiemqled:menu:kientaophongkhach');" >Qhome</a>
                    <div class="dgt_subheader js_qhome__menu">
                        <ul>
                            <li><a href="/trainghiemqled/kien-tao-phong-khach" onclick="omniture_click('vn:trainghiemqled:menu:kientaophongkhach:register');">Tham gia</a></li>
                            
                            <li><a href="/trainghiemqled/thu-vien-hinh-anh-qhome" onclick="omniture_click('vn:trainghiemqled:menu:kientaophongkhach:thuvienhinhanh');">HÌNH ẢNH</a></li>
                            <li><a href="/trainghiemqled/qhome-danh-sach-nguoi-thang-cuoc" onclick="omniture_click('vn:trainghiemqled:menu:kientaophongkhach:danhsachnguoithangcuoc');">Danh sách người thắng cuộc </a></li>
                            <li><a href="http://www.samsung.com/vn/tvs/qled-q8c/QA75Q8CAMKXXV/" target="_blank" onclick="omniture_click('vn:trainghiemqled:menu:kientaophongkhach:khamphaqledtv'); engagement_click('QA75Q8CAMKXXV|;QA75Q8CAMK');">ĐẶT MUA QLED TV</a></li>
                            <li><a href="http://www.samsung.com/vn/audio-video/soundbar/" target="_blank" onclick="omniture_click('vn:trainghiemqled:menu:kientaophongkhach:khamphasoundbar'); engagement_click('HW-MS6501/XV|;HW-MS6501');">ĐẶT MUA SOUNDBAR</a></li>
                        </ul>
                    </div>
                </li>
                <li class="dgt_mb">
                    <div class="dgt_socials">
                        <a href="https://www.facebook.com/SamsungVietnam" target="_blank" class="ql_fb" onclick="omniture_click('vn:trainghiemqled:menu:facebook');"></a>
                        <a href="https://www.youtube.com/user/SamsungVietnam" target="_blank" class="ql_ytb" onclick="omniture_click('vn:trainghiemqled:menu:youtube');"></a>
                    </div>
                </li>
            </ul>
            <div class="dgt_socials">
                <a href="https://www.facebook.com/SamsungVietnam" target="_blank" class="ql_fb" onclick="omniture_click('vn:trainghiemqled:menu:facebook');"></a>
                <a href="https://www.youtube.com/user/SamsungVietnam" target="_blank" class="ql_ytb" onclick="omniture_click('vn:trainghiemqled:menu:youtube');"></a>
            </div>
        </div>
        <div class="menu-toggle-wrapper" aria-hidden="true">
            <button class="btn-menu menu-closed">
               <span class="menu-icon"></span>
            </button>
        </div>
    </div>
    <section id="dgtHome" class="">
        

<!-- Omniture -->
<script type="text/javascript">
    var OMNI_CAMPAIGN_NAME = "tv:trainghiemqled";
    var OMNI_PAGE_NAME = "home";
    s.channel = "vn:campaign";
    s.pageName = "vn:campaign:savina:" + OMNI_CAMPAIGN_NAME + ":" + OMNI_PAGE_NAME;
    s.hier1 = "vn>campaign>savina>" + OMNI_CAMPAIGN_NAME + ">" + OMNI_PAGE_NAME;
    s.prop1 = "vn";
    s.prop2 = "vn:campaign";
    s.prop3 = "vn:campaign:savina";
    s.prop4 = "vn:campaign:savina:" + OMNI_CAMPAIGN_NAME + "";
    s.prop5 = "vn:campaign:savina:" + OMNI_CAMPAIGN_NAME + ":" + OMNI_PAGE_NAME;
    function omniture_click(ClickName) {
        var s = s_gi(s_account);
        s.linkTrackVars = "eVar33,events";
        s.linkTrackEvents = "event45";
        s.eVar33 = ClickName;
        s.events = "event45";
        s.tl(this, 'o', ClickName);
        s.linkTrackVars = "none";
        s.linkTrackEvents = "none";
    }


    function engagement_click(ClickName) {
        var s = s_gi(s_account);
        s.linkTrackVars = "eVar35,eVar41,products,events";
        s.linkTrackEvents = "event35";
        var evarData = ClickName.split("|");
        s.eVar35 = "buy now";
        s.eVar41 = evarData[0];
        s.products = evarData[1];
        s.events = "event35";
        s.tl(this, 'o', ClickName);
        s.linkTrackVars = "none";
        s.linkTrackEvents = "none";
    }
    var s_code = s.t();
    if (s_code) document.write(s_code);
</script>
<script language="JavaScript" type="text/javascript">
    if (navigator.appVersion.indexOf('MSIE') >= 0) document.write(unescape('%3C') + '\!-' + '-')
</script>
<noscript>
    <a href="http://www.omniture.com" title="Web Analytics">
        <img src="http://nmetrics.samsung.com/b/ss/sssamsung4vn,sssamsung4mstglobal/1/H.24--NS/0" height="1" width="1"
             border="0" alt="dot" />
    </a>
</noscript>
<!--// Omniture -->
<div class="dgt_kv">
    <main id="dgt_errorPage">
		<div class="errorPage-wrap">
		  <div class="errorPage-item active">
			<h3>Trang mà bạn yêu cầu hiện<br />không sử dụng được</h3>
			<p>Xin lỗi, chúng tôi đang gặp sự cố khi thực hiện yêu cầu của bạn.<br />Có thể là link thường dùng của bạn đã cũ hoặc bạn gặp phải link hỏng.</p>
		  </div>
		</div>
	</main>
</div>


    </section>
    <div class="dgt_popup" id="dgt_alert2">
        <div class="dgt_copy">
            <a href="/trainghiemqled/" class="btn-close" onclick="omniture_click('vn:trainghiemqled:hoantatdatve:close-btn');"><img src="assets/images/i-close.png" /></a>
            <img src="assets/images/tv2.png">
            <h5>Bạn đã hoàn tất thủ tục đặt vé</h5>
            <p>
                Vui lòng đợi hệ thống xác nhận trong 24h giờ và cập nhận thông tin xác
                nhận qua email hoặc tin nhắn của bạn. Chân thành cảm ơn!
            </p>
            <p>
                <small>
                    * Để đảm bảo thời gian cho xuất chiếu phim và chất lượng phục vụ, xin vui lòng tới
                    QCinema 30 phút trước giờ chiếu để hoàn tất thủ tục nhận vé, nếu không
                    vé sẽ tự động huỷ
                </small>
            </p>
            <p>Chia sẻ để bạn bè cùng tham gia trải nghiệm</p>
            <a href="javascript:;" class="dgt_btn" onclick="omniture_click('vn:trainghiemqled:hoantatdatve:fb-sharing');">Chia sẻ trên Facebook</a>
        </div>
        <span class="ovl"></span>
    </div>

    <div class="dgt_popup" id="dgt_preloader">
      <p><img src="assets/images/preloader.gif"></p>
      <span class="ovl"></span>
    </div>
    <!-- inject:js -->
    <script src="https://vn.campaign.samsung.com/trainghiemqled/assets/js/libs/jquery-1.11.1.js"></script>
    <script src="https://vn.campaign.samsung.com/trainghiemqled/assets/js/underscore.js"></script>
    <script src="https://vn.campaign.samsung.com/trainghiemqled/assets/js/backbone.js"></script>

        <script src="https://vn.campaign.samsung.com/trainghiemqled/assets/js/function.js"></script>
    <script src="https://vn.campaign.samsung.com/trainghiemqled/assets/js/modules/gscript.js"></script>
    <script src="https://vn.campaign.samsung.com/trainghiemqled/assets/js/modules/main.js"></script>


    <div class="dgt_popup" id="dgt_alert">
        <div class="dgt_copy">
            <a href="#" class="btn-close btn-close-2" onclick="omniture_click('vn:kientaophongkhach:dangkyhoantat:close-btn');"><img src="assets/images/i-close.png" /></a>
            <img src="assets/images/tv2.png">
            <h5>Bạn đã hoàn tất thủ tục đặt vé</h5>
            <p>
                Vui lòng đợi hệ thống xác nhận trong 24h giờ và cập nhận thông tin xác
                nhận qua email hoặc tin nhắn của bạn. Chân thành cảm ơn!
            </p>
            <p>
                <small>
                    * Để đảm bảo thời gian cho xuất chiếu phim và chất lượng phục vụ, xin vui lòng tới
                    QCinema 30 phút trước giờ chiếu để hoàn tất thủ tục nhận vé, nếu không
                    vé sẽ tự động huỷ
                </small>
            </p>
            <a href="javascript:;" class="dgt_btn" onclick="omniture_click('vn:kientaophongkhach:hoantatdangky:fb-sharing');">Chia sẻ trên Facebook</a>
        </div>
        <span class="ovl"></span>
    </div>
   

</body>
</html>