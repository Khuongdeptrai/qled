﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Samsung.Models;
using System.Data.SqlClient;
using System.IO;
using System.Data.Entity.Validation;
namespace Samsung.Controllers
{
    [FilterPostActionFilter]
    public class PharseTwoController : Controller
    {
        //
        // GET: /PharseTwo/
        #region Hotel1
        public ActionResult Hotel1()
        {
            ViewBag.ClassCss = "dgtQsuiteDetail";
            return View();
        }
        #endregion

        #region Hotel2
        public ActionResult Hotel2()
        {
            ViewBag.ClassCss = "dgtQsuiteDetail";
            return View();
        }
        #endregion

        #region QsuiteReview
        public ActionResult QsuiteReview() {

            List<ReviewsQsuite> lr = new List<ReviewsQsuite>();
            using (var db = new samsungEntitiesEntities())
            {
                string sqlQuery = "Select * from ReviewsQsuite where Status = 1 order by ReviewId DESC";
                lr = db.Database.SqlQuery<ReviewsQsuite>(sqlQuery).ToList();
            }
            ViewBag.ListReview = lr;
            ViewBag.ShowFooter = 1;
            ViewBag.ClassCss = "dgtReview";
            return View();
        }
        #endregion

        #region QhomeJoin
        public ActionResult QhomeJoin()
        {
            ViewBag.ShowFooter = 1;
            ViewBag.ClassCss = "dgtQhomeJoin";
            return View();
        }
        #endregion

        #region QhomeGallery
        public ActionResult QhomeGallery(){

            ViewBag.ClassCss = "dgtGalerry";
            return View();
        }
        #endregion

        #region Qhome
        public ActionResult Qhome()
        {
            ViewBag.ClassCss = "dgtQhome";
            return View();
        }
        #endregion

        #region QSuite
        public ActionResult QSuite()
        {
            ViewBag.ClassCss = "dgtQsuite";
            return View();
        }
        #endregion

        #region QsuiteRegister
        public ActionResult QsuiteRegister(string id)
        {
           if (string.IsNullOrEmpty(id) || string.IsNullOrWhiteSpace(id))
            {
                return RedirectToRoute("Default");
            }

            string [] hotels =  {"khach-san-jw-marriott","khach-san-le-meridien"};
            if(!hotels.Contains(id)){
                return RedirectToRoute("default");
            }
            UsersRegister ur = (UsersRegister)Session["user"];
          /*  if(ur == null){
                return RedirectToRoute("qsuite");
            }*/
            ViewBag.UserResgister = ur;
            ViewBag.HotelName = id;
            ViewBag.ClassCss = "dgtQsuiteRegister";
           // ViewBag.ShowFooter = 1;
            return View();
        }
        #endregion

        #region QhomeRegister
        public ActionResult QhomeRegister()
        {
            UsersRegister ur = (UsersRegister)Session["user"];
            if (ur == null)
            {
                return RedirectToRoute("qhomejoin");
            }
            ViewBag.UserResgister = ur;
            ViewBag.ClassCss = "dgtQhomeRegister";
            // ViewBag.ShowFooter = 1;
            return View();
        }
        #endregion

        #region WriteReview
        [HttpPost]
        [ValidateAntiForgeryToken]
        [FilterPostActionFilter]
        public ActionResult WriteReview()
        {

            if (Session["recaptcha"] != null)
            {
                if (Session["captcha_valid"] == null || Session["captcha_valid"].ToString() != "1")
                {
                    return Json(new { status = -1, message = "Vui lòng click chọn vào captcha " });
                }

            }

            try
            {
                var message = "";
                var name = "";
                var email = "";
                var content = "";
                if (Request.Form["name"] == null || string.IsNullOrEmpty(Request.Form["name"].ToString()))
                {
                    message += "<p>Vui lòng nhập họ tên</p>";
                }
                else
                {
                    name = Request.Form["name"].ToString().Trim();
                }

                if (Request.Form["email"] == null || string.IsNullOrEmpty(Request.Form["email"].ToString()))
                    message += "<p>Vui lòng nhập email</p>";
                else
                {
                    if (Helper.Common.IsValidEmail(Request.Form["email"].ToString()))
                    {
                        email = Request.Form["email"].ToString().Trim();
                    }
                    else
                    {
                        message += "<p>Vui lòng nhập email hợp lệ </p>";
                    }
                }

                if (Request.Form["content"] == null || string.IsNullOrEmpty(Request.Form["content"].ToString()))
                {
                    message += "<p>Vui lòng nhập cảm nhận</p>";
                }
                else
                {
                    content = Request.Form["content"].ToString().Trim();
                }

                if (content.Length > 500 || email.Length > 100 || name.Length > 100)
                {
                    message += "Bạn đã vượt quá số kí tự cho phép";
                }
                if (message != "")
                {
                    return Json(new { status = 0, msg = message });
                }
                if (Request.Form["review_qsuite"] == null)
                { 
                    using (var db = new Samsung.Models.samsungEntitiesEntities())
                    {

                        Review reviewW = new Review();
                        reviewW.Name = Helper.Common.ParseQueryString(name);
                        reviewW.Email = Helper.Common.ParseQueryString(email);
                        reviewW.ReviewContent = Helper.Common.ParseQueryString(content);
                        reviewW.Status = 0;
                        reviewW.Created = Helper.Common.GetTimeInVietNam();
                        db.Reviews.Add(reviewW);
                        Session["recaptcha"] = 1;
                        Session["captcha_valid"] = null;
                        if (db.SaveChanges() > 0)
                        {
                            return Json(new { status = 1, msg = "<p> Xin cảm ơn lưu cảm nhận thành công </p>" });
                        }
                        else
                        {
                            return Json(new { status = 0, msg = "<p> Đã xảy ra lỗi trong quá trình lưu dữ liệu </p>" });
                        }
                    }
                }
                else
                {
                    using(var db = new samsungEntitiesEntities()){
                        ReviewsQsuite rq = new ReviewsQsuite();
                        rq.Name = Helper.Common.ParseQueryString(name);
                        rq.Email = Helper.Common.ParseQueryString(email);
                        rq.ReviewContent = Helper.Common.ParseQueryString(content);
                        rq.Status = 0;
                        rq.Created = Helper.Common.GetTimeInVietNam();
                        db.ReviewsQsuites.Add(rq);
                        Session["recaptcha"] = 1;
                        Session["captcha_valid"] = null;
                        if (db.SaveChanges() > 0)
                        {
                            return Json(new { status = 1, msg = "<p> Xin cảm ơn lưu cảm nhận thành công </p>" });
                        }
                        else
                        {
                            return Json(new { status = 0, msg = "<p> Đã xảy ra lỗi trong quá trình lưu dữ liệu </p>" });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, msg = "<p> Đã xảy ra lỗi </p>" });
            }
        }
        #endregion

        #region FacebookLogin
        public ActionResult FacebookLogin() {
            int status = 0;
            var updateInfo = 0;
            var message = "";
            UsersRegister u = new UsersRegister();
            try
            {
                string email = "";
                string name = Request.Form["name"].ToString();

                string fb_id = Request.Form["id"].ToString();
                if (Request.Form["email"] != null)
                {
                    email = Request.Form["email"].ToString();
                }


                if (!string.IsNullOrEmpty(fb_id))
                {
                    //string token = Request.Form["token"].ToString();
                    using (var topDatabase = new samsungEntitiesEntities())
                    {
                        u = topDatabase.UsersRegisters.Where(x => x.FacebookId == fb_id).SingleOrDefault();
                        if (u == null)
                        {
                            u = new UsersRegister();
                            u.FacebookId = Samsung.Helper.Common.ParseQueryString(fb_id);
                            u.Email = Samsung.Helper.Common.ParseQueryString(email);
                            u.Name = Samsung.Helper.Common.ParseQueryString(name);
                            u.FacebookName = Samsung.Helper.Common.ParseQueryString(name);
                            u.Created = Samsung.Helper.Common.GetTimeInVietNam();
                            u.FacebookToken = "";
                            topDatabase.UsersRegisters.Add(u);
                            topDatabase.SaveChanges();
                           
                        }
                        Session["user"] = u;
                    }
                    updateInfo = 1;
                    status = 1;
                    message = "Xin chức mừng !Đăng nhập Facebook thành công";
                }
                else
                {
                    status = 0;
                    message = "Xin lỗi !Đăng nhập Facebook không thành công";
                }
            }
            catch (DbEntityValidationException e)
            {
                status = 0;
                message = "Xin lỗi !Đăng nhập Facebook không thành công";
                /*foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;*/
            }
            return Json(new { status = status, message = message });
        }
        #endregion

        #region QsuiteRegister
        public ActionResult SubmitQsuiteRegister()
        {
            if (Session["user"] == null)
            {
                return Json(new { status = -2, msg = "<p>Vui lòng đăng nhập lại bằng Facebook để thự hiện thao tác này</p>" });
            }
            UsersRegister ur = (UsersRegister)Session["user"];

            string message = "";
            var acreage = "";
            var acreageNum = 0;
            var phone = "";
            var email = "";
            var name = "";
            var brand = "";
            var job = "";
            var address = "";

            try
            {
                if (Request.Form["acreage"] == null || string.IsNullOrEmpty(Request.Form["acreage"].ToString()))
                    message += "<p>Vui lòng nhập diện tích không gian sống</p>";
                else
                {
                    acreage = Request.Form["acreage"].ToString();
                    int.TryParse(acreage, out acreageNum);
                    if (acreageNum == 0)
                    {
                        message += "<p>Vui lòng nhập diện tích không gian sống hợp lệ chỉ nhập số</p>";
                    }
                    else
                    {
                        /*if (acreageNum > 1000)
                        {
                            message += "<p>Vui lòng nhập diện tích không gian sống hợp lệ chỉ nhập số</p>";
                        }*/
                    }
                }
                if (Request.Form["phone"] == null || string.IsNullOrEmpty(Request.Form["phone"].ToString()))
                    message += "<p>Vui lòng nhập số điện thoại</p>";
                else
                    phone = Request.Form["phone"].ToString();

                if (Request.Form["email"] == null || string.IsNullOrEmpty(Request.Form["email"].ToString()))
                    message += "<p>Vui lòng nhập email</p>";
                else
                {
                    if (Helper.Common.IsValidEmail(Request.Form["email"].ToString()))
                    {
                        email = Request.Form["email"].ToString();
                    }
                    else
                    {
                        message += "<p>Vui lòng nhập email hợp lệ </p>";
                    }
                }

                if (Request.Form["name"] == null || string.IsNullOrEmpty(Request.Form["name"].ToString()))
                    message += "<p>Vui lòng nhập họ tên</p>";
                else
                    name = Request.Form["name"].ToString();

                if (Request.Form["brand"] == null || string.IsNullOrEmpty(Request.Form["brand"].ToString()))
                    message += "<p>Vui chọn nhãn hiệu TV</p>";
                else
                    brand = Request.Form["brand"].ToString();


                string[] hotels = { "khach-san-jw-marriott", "khach-san-le-meridien" };
                if (Request.Form["hotel"] == null || !hotels.Contains(Request.Form["hotel"].ToString()))
                    message += "<p>Không tìm thấy khách sạn bạn yêu cầu</p>";

                if (Request.Form["job"] == null || string.IsNullOrEmpty(Request.Form["job"].ToString()))
                    message += "<p>Vui chọn nhập nghề nghiệp</p>";
                else
                    job = Request.Form["job"].ToString();

                int birthday = 0;
                if (Request.Form["birthday"] == null || string.IsNullOrEmpty(Request.Form["birthday"].ToString()))
                {
                    message += "<p>Vui chọn nhập năm sinh</p>";
                }
                else
                {
                    Int32.TryParse(Request.Form["birthday"].ToString(), out birthday);
                    if (birthday < 1900 || birthday > 2000)
                    {
                        message += "<p>Năm sinh không hợp lệ</p>";
                    }
                }

                if (message != "")
                {
                    return Json(new { status = 0, msg = message });
                }
                var hotel = Request.Form["hotel"].ToString();

                string queryCount = "Select count(QsuiteRegisterId) from QsuiteRegister where (email = @email or phone = @phone or FacebookId = @facebookid) ";
                using (var db = new samsungEntitiesEntities())
                {
                    int countRegister = db.Database.SqlQuery<int>(queryCount, new SqlParameter("email", email), new SqlParameter("phone", phone), new SqlParameter("facebookid", ur.FacebookId)).SingleOrDefault();
                    if (countRegister > 0)
                        return Json(new { status = -1, msg = "<p>Số điện thoại hoặc email hoặc tài khoản Facebook của bạn đã được sử dụng để đăng ký trước đó</p>" });
                }

                QsuiteRegister qr = new QsuiteRegister();
                qr.Name = Helper.Common.ParseQueryString(name);
                qr.Brand = Helper.Common.ParseQueryString(brand);
                qr.Email = Helper.Common.ParseQueryString(email);
                qr.Phone = Helper.Common.ParseQueryString(phone);
                qr.Acreage = Helper.Common.ParseQueryString(acreage);
                qr.Status = 0;
                qr.Created = Helper.Common.GetTimeInVietNam();
                qr.HotelName = hotel.Equals("khach-san-jw-marriott") ? "Khách sạn JW Marriott" : "Khách sạn Le Méridien";
                qr.FacebookId = ur.FacebookId;
                qr.Birthday = birthday;
                qr.FacebookName = ur.FacebookName;
                qr.Job = job;
                qr.Refer = Session["REFER_SITE"] != null ? Session["REFER_SITE"].ToString() : "";
                qr.Utm = Session["UTM"] != null ? Session["UTM"].ToString() : "";
                using (var db = new samsungEntitiesEntities())
                {
                    db.QsuiteRegisters.Add(qr);
                    db.SaveChanges();
                    string contenemail = System.IO.File.ReadAllText(Server.MapPath("~/Views/edm/email-qsuite.html"));
                    contenemail = contenemail.Replace("{domain}", Request.Url.GetLeftPart(UriPartial.Authority) + Samsung.Helper.UrlHelperPage.Webroot);
                    var contentEndCode = HttpContext.Server.UrlEncode(contenemail);
                    var rsEmail = Samsung.Helper.Common.GemSendEmail(qr.Email, Samsung.Properties.Settings.Default.EmailFrom, Samsung.Properties.Settings.Default.EmailName, qr.Name, Samsung.Properties.Settings.Default.EmailSubjectQSuite, contentEndCode);
                    return Json(new { status = 1, msg = "<p>Đăng ký trải nghiệm phong cách sống đỉnh cao thành công</p>" });
                }
            }
            catch (Exception ex)
            {
                Session["DebugError"] = "SaveQsuite :" + ex.InnerException;
                return Json(new { status = -1, msg = "<p>Đã xảy ra lỗi trong quá trình xử lý</p>" });
            }
        }

        #endregion

        public ActionResult SaveQHome(HttpPostedFileBase file)
        {
            try
            {
                string filename = "";
                string message = "";
                if (Session["user"] == null)
                {
                    message += "<p>Vui lòng đăng nhập lại bằng Facebook để thự hiện thao tác này</p>";
                }
                UsersRegister ur = (UsersRegister)Session["user"];
                Dictionary<string, string> dss = new Dictionary<string, string>();

                if (Request.Files["Image"].ContentLength > 0)
                {
                    Samsung.Helper.InfoFile _if = Samsung.Helper.TopFile.UploadImageFileBase(Request.Files["Image"], "Uploads");
                    
                    if (_if != null)
                    {
                        if (_if.Error == 1)
                        {
                            message += "<p>File hình ảnh không hợp lệ </p>";
                        }
                        else
                        {
                            filename = _if.Name;
                        }
                    }
                    else
                    {
                       // message += "<p>File hình ảnh không hợp lệ </p>";
                    }
                }
                else
                {
                  //  message += "<p>File hình ảnh không hợp lệ </p>";
                }

                var acreage = "";
                var cityNumber = 0;
                var phone = "";
                var email = "";
                var name = "";
                var brand = "";
                var job = "";
                var address = "";
                var reason = "";
                

                if (Request.Form["acreage"] == null || string.IsNullOrEmpty(Request.Form["acreage"].ToString()))
                    message += "<p>Vui lòng nhập diện tích không gian sống</p>";
                else
                {
                    int acreageNum = 0;
                    acreage = Request.Form["acreage"].ToString();
                    int.TryParse(acreage, out acreageNum);
                    if (acreageNum == 0)
                    {
                        message += "<p>Vui lòng nhập diện tích không gian sống hợp lệ chỉ nhập số</p>";
                    }
                    else
                    {
                        dss.Add("acreage", acreage);
                    }
                }

                if (Request.Form["city"] == null || string.IsNullOrEmpty(Request.Form["city"].ToString()))
                    message += "<p>Vui lòng lựa chọn thành phố</p>";
                else
                {
                    string city = Request.Form["city"].ToString();
                    int.TryParse(city, out cityNumber);

                    int[] cityAllow = { 1, 2 };
                    if (!Samsung.Helper.Common.ListProvince().ContainsKey(cityNumber))
                    {
                        message += "<p>Vui lòng chọn thành phố hợp lệ</p>";
                    }
                    else
                    {
                        dss.Add("city", city);
                    }

                }
                if (Request.Form["reason"] == null || string.IsNullOrEmpty(Request.Form["reason"].ToString()))
                {
                 //   message += "<p>Vui lòng nhập lí do bạn muốn kiến tạo phòng khách của mình với QLED TV </p>";
                }
                else
                {
                    if (Request.Form["reason"].ToString().Length > 600)
                    {
                        message += "<p>Vui lòng nhâp lí do bạn muốn kiến tạo phòng khách nhỏ hơn 600 kí tự </p>";
                    }
                    else
                    {
                        reason = Request.Form["reason"].ToString();
                        dss.Add("reason", reason);
                    }
                }
                if (Request.Form["phone"] == null || string.IsNullOrEmpty(Request.Form["phone"].ToString()))
                    message += "<p>Vui lòng nhập số điện thoại</p>";
                else
                {
                    phone = Request.Form["phone"].ToString();
                    dss.Add("phone", reason);
                }
                   

                if (Request.Form["email"] == null || string.IsNullOrEmpty(Request.Form["email"].ToString()))
                    message += "<p>Vui lòng nhập email</p>";
                else
                {
                    if (Helper.Common.IsValidEmail(Request.Form["email"].ToString()))
                    {
                        email = Request.Form["email"].ToString();
                        dss.Add("email", email);
                    }
                    else
                    {
                        message += "<p>Vui lòng nhập email hợp lệ </p>";
                    }
                }

                if (Request.Form["name"] == null || string.IsNullOrEmpty(Request.Form["name"].ToString()))
                {
                    message += "<p>Vui lòng nhập họ tên</p>";
                }
                else
                {
                    name = Request.Form["name"].ToString();
                    dss.Add("name", name);
                }
                   

                if (Request.Form["brand"] == null || string.IsNullOrEmpty(Request.Form["brand"].ToString()))
                    message += "<p>Vui chọn nhãn hiệu TV</p>";
                else
                {
                    brand = Request.Form["brand"].ToString();
                    dss.Add("brand", brand);
                }
                if (Request.Form["tivi-other"] != null && !string.IsNullOrEmpty(Request.Form["tivi-other"].ToString()))
                    brand = Request.Form["tivi-other"].ToString();
                
                if (Request.Form["job"] == null || string.IsNullOrEmpty(Request.Form["job"].ToString()))
                    message += "<p>Vui chọn nhập nghề nghiệp</p>";
                else
                {
                    job = Request.Form["job"].ToString();
                    dss.Add("job", brand);
                }
                    


                if (Request.Form["address"] == null || string.IsNullOrEmpty(Request.Form["address"].ToString()))
                    message += "<p>Vui chọn nhập địa chỉ</p>";
                else
                {
                    address = Request.Form["address"].ToString();
                    dss.Add("address", address);
                }
                    

                int birthday = 0;
                if (Request.Form["birthday"] == null || string.IsNullOrEmpty(Request.Form["birthday"].ToString()))
                {
                    message += "<p>Vui chọn nhập năm sinh</p>";
                }
                else
                {
                    Int32.TryParse(Request.Form["birthday"].ToString(), out birthday);
                    if (birthday < 1900 || birthday > 2000)
                    {
                        message += "<p>Năm sinh không hợp lệ</p>";
                    }
                    else
                    {
                        dss.Add("birthday", Request.Form["birthday"].ToString());
                    }
                }


                if (message != "")
                {
                    Session["FormQhome"] = dss;
                    Session["ErrorMessage"] = message;
                    return RedirectToRoute("qhomejoin");
                }


                string queryCount = "Select count(QhomeRegisterId) from QhomeRegister where (email = @email or phone = @phone or FacebookId = @facebookid) ";
                using (var db = new samsungEntitiesEntities())
                {
                    int countRegister = db.Database.SqlQuery<int>(queryCount, new SqlParameter("email", email), new SqlParameter("phone", phone), new SqlParameter("facebookid", ur.FacebookId)).SingleOrDefault();
                    if (countRegister > 0)
                    {
                        message = "<p>Số điện thoại hoặc email hoặc tài khoản Facebook của bạn đã được sử dụng để đăng ký trước đó</p>";
                    }
                        
                }



                if (message != "")
                {
                    Session["FormQhome"] = dss;
                    Session["ErrorMessage"] = message;
                    return RedirectToRoute("qhomejoin");
                }

                // upload image
              //  Helper.InfoFile _if = Helper.TopFile.UploadBase64(Request.Form["image"].ToString(), "Uploads/");

               
                
              /*  string path = HttpContext.Server.MapPath("~/" + _if.Name);
                string folder = HttpContext.Server.MapPath("~/Uploads");
                string nameOnly = _if.NameOnly;
                System.Web.Helpers.WebImage img = new System.Web.Helpers.WebImage(path);
                string imageResize = "";
                if (img.Width > 1000)
                {
                    //if (Request.Form["Orientation"] != null && Request.Form["Orientation"].ToString() == "6")
                    //  img.RotateRight();
                    img.Resize(350, 200).Save(folder + "/_thumb_" + nameOnly, null, false);

                    imageResize = "_thumb_" + nameOnly;
                }
                else
                {
                    img.Resize(350, 200).Save(folder + "/_thumb_" + nameOnly, null, false);
                }*/

                Samsung.Models.QhomeRegister qr = new Samsung.Models.QhomeRegister();
                qr.Name = Helper.Common.ParseQueryString(name);
                qr.Brand = Helper.Common.ParseQueryString(brand);
                qr.Email = Helper.Common.ParseQueryString(email);
                qr.Phone = Helper.Common.ParseQueryString(phone);
                qr.Acreage = Helper.Common.ParseQueryString(acreage);
                qr.Status = 0;
                qr.Created = Helper.Common.GetTimeInVietNam();
                qr.Image = filename;
                qr.Job = Helper.Common.ParseQueryString(job);
                qr.Birthday = birthday;
                qr.Address = Helper.Common.ParseQueryString(address);
                qr.FacebookId = ur.FacebookId;
                qr.FacebookName = ur.FacebookName;
                qr.City = cityNumber;
                qr.Reason = reason;
                qr.Refer = Session["REFER_SITE"] != null ? Session["REFER_SITE"].ToString() : "";
                qr.Utm = Session["UTM"] != null ? Session["UTM"].ToString() : "";
                using (var db = new samsungEntitiesEntities())
                {
                    db.QhomeRegisters.Add(qr);
                    db.SaveChanges();

                    string contenemail = System.IO.File.ReadAllText(Server.MapPath("~/Views/edm/email-qhome.html"));
                    contenemail = contenemail.Replace("{domain}", Request.Url.GetLeftPart(UriPartial.Authority) + Samsung.Helper.UrlHelperPage.Webroot);
                    var contentEndCode = HttpContext.Server.UrlEncode(contenemail);
                    var rsEmail = Samsung.Helper.Common.GemSendEmail(qr.Email, Samsung.Properties.Settings.Default.EmailFrom, Samsung.Properties.Settings.Default.EmailName, qr.Name, Samsung.Properties.Settings.Default.EmailSubjectQhome, contentEndCode);
                    Session["QhomeSuccess"] = 1;
                    Session["FormQhome"] = null;
                    return RedirectToRoute("qhomejoin");
                }
            }
            catch (Exception ex)
            {

                string message = " | PhaseTwoContoller | SaveQHome +" + ex.InnerException + " | " + ex.ToString();

                if (Session["TIM_LOI"] == null)
                {
                    Session["TIM_LOI"] = message;
                }
                else
                {
                    string oldError = Session["TIM_LOI"].ToString();
                    Session["TIM_LOI"] = message + " | " + oldError;
                }


              //  Session["FormQhome"] = dss;
                Session["ErrorMessage"] = message;
                return RedirectToRoute("qhomejoin");
            }

        }


        #region SaveQHome_old
        public ActionResult SaveQHome_old()
        {
            try
            {
                if (Session["user"] == null)
                {
                    return Json(new { status = -2, msg = "<p>Vui lòng đăng nhập lại bằng Facebook để thự hiện thao tác này</p>" });
                }
                UsersRegister ur = (UsersRegister)Session["user"];

                string message = "";
                var acreage = "";
                var cityNumber = 0;
                var phone = "";
                var email = "";
                var name = "";
                var brand = "";
                var job = "";
                var address = "";
                var reason = "";
                if (Request.Form["image"] == null ||  Request.Form["image"].ToString() == "" )
                    message += "<p>Vui lòng upload hình ảnh phòng khách của bạn</p>";

                if (Request.Form["acreage"] == null || string.IsNullOrEmpty(Request.Form["acreage"].ToString()))
                    message += "<p>Vui lòng nhập diện tích không gian sống</p>";
                else
                {
                    int acreageNum = 0;
                    acreage = Request.Form["acreage"].ToString();
                    int.TryParse(acreage, out acreageNum);
                    if (acreageNum == 0)
                    {
                        message += "<p>Vui lòng nhập diện tích không gian sống hợp lệ chỉ nhập số</p>";
                    }
                    else
                    {
                      /*  if (acreageNum > 1000)
                        {
                            message += "<p>Vui lòng nhập diện tích không gian sống hợp lệ chỉ nhập số</p>";
                        }*/
                    }
                }

                if (Request.Form["city"] == null || string.IsNullOrEmpty(Request.Form["city"].ToString()))
                    message += "<p>Vui lòng lựa chọn thành phố</p>";
                else
                {
                    string city = Request.Form["city"].ToString();
                    int.TryParse(city, out cityNumber);

                    int []cityAllow = {1,2};
                    if (!Samsung.Helper.Common.ListProvince().ContainsKey(cityNumber))
                    {
                        message += "<p>Vui lòng chọn thành phố hợp lệ</p>";
                    }
                   
                }
                if (Request.Form["reason"] == null || string.IsNullOrEmpty(Request.Form["reason"].ToString()))
                {
                    message += "<p>Vui lòng nhập lí do bạn muốn kiến tạo phòng khách của mình với QLED TV </p>";
                }
                else
                {
                    if (Request.Form["reason"].ToString().Length > 600)
                    {
                        message += "<p>Vui lòng nhâp lí do bạn muốn kiến tạo phòng khách nhỏ hơn 600 kí tự </p>";
                    }
                    else
                    {
                        reason = Request.Form["reason"].ToString();
                    }
                }
                if (Request.Form["phone"] == null || string.IsNullOrEmpty(Request.Form["phone"].ToString()))
                    message += "<p>Vui lòng nhập số điện thoại</p>";
                else
                    phone = Request.Form["phone"].ToString();

                if (Request.Form["email"] == null || string.IsNullOrEmpty(Request.Form["email"].ToString()))
                    message += "<p>Vui lòng nhập email</p>";
                else
                {
                    if (Helper.Common.IsValidEmail(Request.Form["email"].ToString()))
                    {
                        email = Request.Form["email"].ToString();
                    }
                    else
                    {
                        message += "<p>Vui lòng nhập email hợp lệ </p>";
                    }
                }

                if (Request.Form["name"] == null || string.IsNullOrEmpty(Request.Form["name"].ToString()))
                    message += "<p>Vui lòng nhập họ tên</p>";
                else
                    name = Request.Form["name"].ToString();

                if (Request.Form["brand"] == null || string.IsNullOrEmpty(Request.Form["brand"].ToString()))
                    message += "<p>Vui chọn nhãn hiệu TV</p>";
                else
                    brand = Request.Form["brand"].ToString();

                if (Request.Form["job"] == null || string.IsNullOrEmpty(Request.Form["job"].ToString()))
                    message += "<p>Vui chọn nhập nghề nghiệp</p>";
                else
                    job = Request.Form["job"].ToString();


                if (Request.Form["address"] == null || string.IsNullOrEmpty(Request.Form["address"].ToString()))
                    message += "<p>Vui chọn nhập địa chỉ</p>";
                else
                    address = Request.Form["address"].ToString();

                int birthday = 0;
                if (Request.Form["birthday"] == null || string.IsNullOrEmpty(Request.Form["birthday"].ToString()))
                {
                    message += "<p>Vui chọn nhập năm sinh</p>";
                }else{
                    Int32.TryParse(Request.Form["birthday"].ToString(),out birthday);
                    if(birthday < 1900 || birthday > 2000){
                        message += "<p>Năm sinh không hợp lệ</p>";
                    }
                }


                if (message != "")
                {
                    return Json(new { status = 0, msg = message });
                }


                string queryCount = "Select count(QhomeRegisterId) from QhomeRegister where (email = @email or phone = @phone or FacebookId = @facebookid) ";
                using (var db = new samsungEntitiesEntities())
                {
                    int countRegister = db.Database.SqlQuery<int>(queryCount, new SqlParameter("email", email), new SqlParameter("phone", phone), new SqlParameter("facebookid", ur.FacebookId)).SingleOrDefault();
                    if (countRegister > 0)
                        return Json(new { status = -1, msg = "<p>Số điện thoại hoặc email hoặc tài khoản Facebook của bạn đã được sử dụng để đăng ký trước đó</p>" });
                }


                // upload image
                Helper.InfoFile _if = Helper.TopFile.UploadBase64(Request.Form["image"].ToString(), "Uploads/");

                if (_if == null)
                {
                    return Json(new { status = -1, msg = "<p>Hình ảnh không hợp lệ </p>" });
                }

                string path = HttpContext.Server.MapPath("~/" + _if.Name);
                string folder = HttpContext.Server.MapPath("~/Uploads");
                string nameOnly = _if.NameOnly;
                System.Web.Helpers.WebImage img = new System.Web.Helpers.WebImage(path);
                string imageResize = "";
                if (img.Width > 1000)
                {
                    //if (Request.Form["Orientation"] != null && Request.Form["Orientation"].ToString() == "6")
                      //  img.RotateRight();
                    img.Resize(350, 200).Save(folder + "/_thumb_" + nameOnly, null, false);

                    imageResize = "_thumb_" + nameOnly;
                }
                else
                {
                    img.Resize(350, 200).Save(folder + "/_thumb_" + nameOnly, null, false);
                }
                
                Samsung.Models.QhomeRegister qr = new Samsung.Models.QhomeRegister();
                qr.Name = Helper.Common.ParseQueryString(name);
                qr.Brand = Helper.Common.ParseQueryString(brand);
                qr.Email = Helper.Common.ParseQueryString(email);
                qr.Phone = Helper.Common.ParseQueryString(phone);
                qr.Acreage = Helper.Common.ParseQueryString(acreage);
                qr.Status = 0;
                qr.Created = Helper.Common.GetTimeInVietNam();
                qr.Image = nameOnly;
                qr.Job = Helper.Common.ParseQueryString(job);
                qr.Birthday = birthday;
                qr.Address = Helper.Common.ParseQueryString(address);
                qr.FacebookId = ur.FacebookId;
                qr.FacebookName = ur.FacebookName;
                qr.City = cityNumber;
                qr.Reason = reason;
                qr.Refer = Session["REFER_SITE"] != null ? Session["REFER_SITE"].ToString() : "";
                qr.Utm = Session["UTM"] != null ? Session["UTM"].ToString() : "";
                using (var db = new samsungEntitiesEntities())
                {
                    db.QhomeRegisters.Add(qr);
                    db.SaveChanges();

                    string contenemail = System.IO.File.ReadAllText(Server.MapPath("~/Views/edm/email-qhome.html"));
                    contenemail = contenemail.Replace("{domain}", Request.Url.GetLeftPart(UriPartial.Authority) + Samsung.Helper.UrlHelperPage.Webroot);
                    var contentEndCode = HttpContext.Server.UrlEncode(contenemail);
                    var rsEmail = Samsung.Helper.Common.GemSendEmail(qr.Email, Samsung.Properties.Settings.Default.EmailFrom, Samsung.Properties.Settings.Default.EmailName, qr.Name, Samsung.Properties.Settings.Default.EmailSubjectQhome, contentEndCode);
                    return Json(new { status = 1, msg = "<p>Đăng ký kiến tạo phòng khách thành công</p>" });
                }
            }
            catch(Exception ex){
               
                string message = " | PhaseTwoContoller | SaveQHome +" + ex.InnerException + " | " + ex.ToString();

                if (Session["TIM_LOI"] == null)
                {
                    Session["TIM_LOI"] = message;
                }
                else
                {
                    string oldError = Session["TIM_LOI"].ToString();
                    Session["TIM_LOI"] = message + " | " + oldError;
                }


                return Json(new { status = -1, msg = "<p>Đã xảy ra lỗi trong quá trình xử lý</p>" });
            }
            
        }
        #endregion


        public ActionResult WinnerQhome() {

            return View();
        }

        public ActionResult WinnerQsuite() {

            return View();
        }

        public ActionResult DebugError()
        {
            if (Session["DebugError"]!=null)
            {
                Response.Write(Session["DebugError"].ToString());
                Response.End();
            }
            return View();
        }

    }
}
