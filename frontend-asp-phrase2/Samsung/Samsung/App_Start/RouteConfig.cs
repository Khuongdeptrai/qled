﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Samsung
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
           
            routes.MapRoute(
              name: "save_qhome",
              url: "SaveQhomeRegister",
              defaults: new { controller = "PharseTwo", action = "SaveQHome" }
            );
            routes.MapRoute(
              name: "save_qsuite",
              url: "SaveQsuiteRegister",
              defaults: new { controller = "PharseTwo", action = "SubmitQsuiteRegister" }
            );
            routes.MapRoute(
              name: "facebook_login",
              url: "FacebookLogin",
              defaults: new { controller = "PharseTwo", action = "FacebookLogin" }
            );
            routes.MapRoute(
              name: "writereviewq",
              url: "writereviewq",
              defaults: new { controller = "PharseTwo", action = "WriteReview" }
            );

            routes.MapRoute(
              name: "qhomeregister",
              url: "dang-ky-chuong-trinh-kien-tao-phong-khach",
              defaults: new { controller = "PharseTwo", action = "QhomeRegister" }
            );

            routes.MapRoute(
              name: "qsuiteregister",
              url: "dang-ky-trai-nghiem/{id}",
              defaults: new { controller = "PharseTwo", action = "QsuiteRegister" }
            );
            routes.MapRoute(
              name: "qsuite",
              url: "qsuite",
              defaults: new { controller = "PharseTwo", action = "QSuite" }
            );

            routes.MapRoute(
              name: "qhome",
              url: "qhome",
              defaults: new { controller = "PharseTwo", action = "Qhome" }
            );

            routes.MapRoute(
              name: "qhomegallery",
              url: "thu-vien-hinh-anh-qhome",
              defaults: new { controller = "PharseTwo", action = "QhomeGallery" }
            );

            routes.MapRoute(
               name: "qhomejoin",
               url: "kien-tao-phong-khach",
               defaults: new { controller = "PharseTwo", action = "QhomeJoin" }
           );

            routes.MapRoute(
                name: "qsuitereview",
                url: "cam-nhan-cua-nguoi-trai-nghiem",
                defaults: new { controller = "PharseTwo", action = "QsuiteReview" }
            );

            routes.MapRoute(
                name: "hotel2",
                url: "le-meridien-hotel",
                defaults: new { controller = "PharseTwo", action = "Hotel2" }
            );

            routes.MapRoute(
                name: "hotel1",
                url: "jw-marriot-hanoi",
                defaults: new { controller = "PharseTwo", action = "Hotel1" }
            );

            routes.MapRoute(
                name: "gapit-sms",
                url: "gapit-sms",
                defaults: new { controller = "Home", action = "SendSms" }
            );
            routes.MapRoute(
                name: "redirect",
                url: "p",
                defaults: new { controller = "Home", action = "Product" }
            );

            routes.MapRoute(
                name: "qcinema",
                url: "qcinema",
                defaults: new { controller = "Home", action = "Qcinema" }
            );
            routes.MapRoute(
                name: "library",
                url: "thu-vien-phim",
                defaults: new { controller = "Home", action = "Library" }
            );
            routes.MapRoute(
                name: "library-image",
                url: "thu-vien-hinh-anh",
                defaults: new { controller = "Home", action = "LibraryPhoto" }
            );

            routes.MapRoute(
                name: "404",
                url: "loi-404",
                defaults: new { controller = "Home", action = "Error404" }
            );
            routes.MapRoute(
                name: "500",
                url: "500",
                defaults: new { controller = "Home", action = "Error500", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "403",
                url: "403",
                defaults: new { controller = "Home", action = "Error403", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "filmdetail",
                url: "phim/{id}/{cinema}/{date}/{time}",
                defaults: new { controller = "Home", action = "Films", id = UrlParameter.Optional, cinema = UrlParameter.Optional, date = String.Empty, time = String.Empty }
            );

            routes.MapRoute(
                name: "review",
                url: "cam-nhan-cua-khach-hang",
                defaults: new { controller = "Home", action = "Review" }
            );
            routes.MapRoute(
                name: "writereview",
                url: "writereview",
                defaults: new { controller = "Home", action = "WriteReview" }
            );
            
            routes.MapRoute(
              name: "winner_qsuite",
              url: "qsuite-danh-sach-nguoi-thang-cuoc",
              defaults: new { controller = "PharseTwo", action = "WinnerQsuite" }
            );
            routes.MapRoute(
             name: "winner_qhome",
             url: "qhome-danh-sach-nguoi-thang-cuoc",
             defaults: new { controller = "PharseTwo", action = "WinnerQhome" }
           );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "sharefaceook1",
                url: "Home/ShareFacebook/{id}/{cinema}",
                defaults: new { controller = "Home", action = "ShareFacebook", id = UrlParameter.Optional, cinema = UrlParameter.Optional }

            );
            
        }
    }
}