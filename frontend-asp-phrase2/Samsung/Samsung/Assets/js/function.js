﻿var path_resource = $("meta[name=webroot]").attr("content") + "assets/"
var absolutPath = $("meta[name=webroot]").attr("content");
var folder = "";
var SAMSUNGFUNCTION = (function () {
    var getOriginDomain = function () {
        if (!window.location.origin) {
            return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        } else {
            return window.location.origin;
        }
    }
    var _dateCurrent = new Date();
    var _timeCurrent =  typeof moment == 'function' ? " "+moment().hour() + ":" + moment().minute() + ":" + moment().second() : "";
    var timeZone = "Asia/Ho_Chi_Minh";
    var formatTime = "YYYY-MM-DD";
    var formatTime2 = "YYYY-MM-DD HH:mm:ss";
    var dateCurrent = typeof moment == 'function' ? moment(moment(_dateCurrent), formatTime)  : "";
    
    var FilmModel = Backbone.Model.extend({
        
    });
    var _filmModel = new FilmModel();
    var filteFomrModel =  Backbone.Model.extend({
        defaults: {
            cinema: 0,
            date: "",
            time: ""
        },
        initialize: function () {
            this.on("change:date", function (model) {
                _filmView.renderSlide({key:'date', value : model.toJSON().date});
            });


            this.on("change:time", function (model) {
                if (model.toJSON().time == "") {
                    return false;
                }
                _filmView.renderSlide({ key: 'time', value: model.toJSON().time });
            });
        }
    });
    var isProcessing = 0;
    var _filterModel = new filteFomrModel();
    var FilmView = Backbone.View.extend({
        el: '#dgtBooking',
        render : function(){
            
        },
        reRenderSlide: function (htmlContent) {
            $('.js_dgt__library').slick('unslick');
            $('.js_dgt__library').empty();
            $('.js_dgt__library').append(htmlContent);
            $('.js_dgt__library').slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                variableWidth: true
            });
        },
        renderSlide: function (model) {
            
        },
        renderFormFilter: function (model) {
            //console.log(model);
            var _model = model;
            var selectDate = "<option value='' >Lựa chọn ngày xem phim</option>";
            if (_.size(_model) > 0) {
                _.each(_model, function (obj, key) {
                    //console.log(key);
                    var checkShowDate = 0;
                    if (typeof obj[0] != "undefined") {
                        selectDate += "<option data-id=" + moment(key).format("YYYY-MM-DD") + " value=" + moment(key).format("YYYY-MM-DD") + ">" + moment(key).format("DD-MM-YYYY") + "</option>";                
                   }
                });
                this.$el.find('.filter-date').html('');
                this.$el.find('.filter-date').html(selectDate);
            }
        },
        events: {
            //'click a': 'clickSlide',
            'change select.filter-date': 'changeDate',
            'change .filter-cinema': 'changeCinema',
            'change .filter-time': 'changeTime',
            'click  .filter-date' : 'clickDate'
        },
        clickSlide: function (e) {
            
            var href = $(e.currentTarget).attr('href');
            var form = _filterModel.toJSON();
            var msg = "";
            if (form.cinema == 0) {
                msg += "<p>Vui lòng chọn rạp phim</p>"
            }
            if (form.date == "") {
                msg += "<p>Vui lòng chọn ngày xem phim</p>"
            }
            if (form.time == "") {
                msg += "<p>Vui lòng chọn giờ xem phim</p>"
            }
            if(msg != ""){
                alert(msg);
                msg = "";
                return false;
            }
            href += "/" + form.cinema + "/" + form.date + "/" + form.time.replace(':','-');
       //     console.log(getOriginDomain() + href);
            window.location.href = getOriginDomain()  + href;
            return false;
        },
        changeTime : function(e){
            var time = $(e.target).val();
            _filterModel.set({ 'time': time });
            this.countTicket();
        },
        countTicket: function () {
            if (isProcessing == 1) {
                alert("Xin vui lòng đợi đang xử lý yêu cầu của bạn");
                return false;
            }
            var cinema = $(".filter-cinema").val();
            var date = $(".filter-date option:selected").attr("data-id");
            var time = $(".filter-time").val();
            if (cinema != "" && date != "" && time != "") {
                isProcessing = 1;
                ajaxFunction('POST', ucountticket, { cinema: cinema, date: date, time: time, filmid: FilmId }, function (response) {
                    if (response.status == "1") {
                        $(".dgt_count").show();
                        $(".dgt_count").html(response.msg);
                        isProcessing = 0;
                    } else {
                        $(".dgt_count").show();
                        alert(response.msg);
                        isProcessing = 0;
                    }
                }, function () { isProcessing = 0; $(".dgt_count").hide()})
            }
        },
        clickDate : function(){
            if ($('.filter-cinema').val() == "") {
                alert("Vui lòng chọn rạp chiếu phim");
                return false;
            }
        },
        changeDate: function (e) {
            var cinemaSelected = $('.filter-cinema').val();
            if (cinemaSelected == "") {
                return false;
            }
            var value = $(".filter-date option:selected").attr('data-id');
            var options = '<option value="">Lựa chọn giờ xem phim</option>';
            if (value == undefined) {
                this.$el.find('.filter-time').html(options);
                return false;
            }
            

            _filterModel.set({ 'date': value });
            var _model = null;
            if (cinemaSelected == "1") {
                _model = scheduleHanoi;
            } else {
                _model = scheduleHcm;
            }
            if (_.size(_model[value]) > 0) {
                var listFilm = _model[value];
               
                _.each(listFilm, function (obj, key) {
                    var checkShowTime = 0;
                    if (typeof obj != 'undefined') {
                        if (obj.FilmId == FilmId) {
                            options += '<option value="' + obj.Time + '">' + obj.Time + '</option>';
                        }
                    }
                })
                this.$el.find('.filter-time').html(options);
                if ($('.filter-time option').size() == 2) {
                    $('.filter-time option').eq(1).prop('selected', true);
                }
            } else {
                //thong bao het xuat chieu
            }

            this.countTicket();
        },
        changeCinema: function (e) {
            var cinema = $(e.target).val();
            _filterModel.set({ 'cinema': cinema });

            if (cinema == "") {
                $('.filter-date').html('<option value="">Lựa chọn ngày xem phim</option>');
                return false;
            }
            
           
            if (cinema == "1") {//ha noi
                _filmView.renderFormFilter(scheduleHanoi);
            } else if (cinema == "2") {
                _filmView.renderFormFilter(scheduleHcm);
            }
            $('.filter-time').html('<option value="">Lựa chọn giờ xem phim</option>');

            this.countTicket();
            
        }
    })
    var _filmView = new FilmView();
    var filterDate = function (dateTimeCurrent) {
        var dateSchedule = null;
        $.each(scheduleHanoi, function (index, object) {

            dateSchedule = moment(moment(index, formatTime), formatTime);
            var dateScheduleFilmCompare = dateSchedule.tz(timeZone);
            var dateTimeCurrentCompare = dateCurrent.tz(timeZone);
            
            if (dateScheduleFilmCompare.format(formatTime) < dateTimeCurrentCompare.format(formatTime)) {
               
                delete scheduleHanoi[index];
            }
            
            if (dateScheduleFilmCompare.format(formatTime) == dateTimeCurrentCompare.format(formatTime)) {
              
                var _size = _.size(scheduleHanoi[index]);
                for (var i = 0; i < _size ; i++) {
                    obj = object[i];
                    var schedule = moment(index + " " + obj.Time + ":00", formatTime2).tz(timeZone);
                    var currentTimeSchedule = moment(_dateCurrent + _timeCurrent, formatTime2).tz(timeZone);
                    var rs = schedule.diff(currentTimeSchedule, 'minutes');
                   
                    if (currentTimeSchedule >= schedule || rs < 60) { 
                        delete scheduleHanoi[index][i];
                    } else {
                        //console.log(schedule.format() +"|"+  currentTimeSchedule.format());
                    }
                }
            }
            var flag = 0;
            _.each(object, function (obj, key) {
                if (obj != null)
                {
                    if (obj.FilmId == FilmId)
                        flag = 1;
                }
            });
            if (flag == 0)
                delete scheduleHanoi[index];
           
        })

        $.each(scheduleHcm, function (index, object) {

            dateSchedule = moment(moment(index, formatTime), formatTime);
            var dateScheduleFilmCompare = dateSchedule.tz(timeZone);
            var dateTimeCurrentCompare = dateCurrent.tz(timeZone);

            if (dateScheduleFilmCompare.format(formatTime) < dateTimeCurrentCompare.format(formatTime)) {
                delete scheduleHcm[index];
            }

            if (dateScheduleFilmCompare.format(formatTime) == dateTimeCurrentCompare.format(formatTime)) {

                var _size = _.size(scheduleHcm[index]);
                for (var i = 0; i < _size ; i++) {
                    obj = object[i];
                    var schedule = moment(index + " " + obj.Time + ":00", formatTime2).tz(timeZone);
                    var currentTimeSchedule = moment(_dateCurrent + _timeCurrent, formatTime2).tz(timeZone);

                    if (currentTimeSchedule >= schedule) {
                        delete scheduleHcm[index][i];
                    } else {
                        //console.log(schedule.format() +"|"+  currentTimeSchedule.format());
                    }
                }
            }
            _.each(object, function (obj, key) {
                var flag = 0;
                _.each(object, function (obj, key) {
                    if (obj.FilmId == FilmId)
                        flag = 1;
                });
                if (flag == 0)
                    delete scheduleHcm[index];
            });

        })
      //  console.log(scheduleHcm);
        //_filmModel.set({ films: scheduleHanoi });
    }
    var FormRegisterModel = Backbone.Model.extend({
        defaults: {
            name: '',
            brand: '',
            email: '',
            phone: '',
            number_of_people: '',
            fimlid: $('input[name=filmid]').val(),
            date: $('input[name=date]').val(),
            time: $('input[name=time]').val(),
            cinema: $('input[name=cinema]').val(),
            
        },
        url : '/Home/Register',
        initialize: function () {
            // console.log('image model init');
        },
        saveFormRegister:function(model){
            this.save(model, {
                wait: true,
                success: function (response) {
                    //console.log(response);
                }
            });
        }
    });

    
    var FormRegisterView = Backbone.View.extend({
        el: '#dgtBooking',
        events: {
            'keyup input[name=name],blur input[name=name]': 'enterName',
            'change select[name=brand],blur select[name=brand]': 'changeTivi',
            'keyup input[name=email],blur input[name=email]': 'enterEmail',
            'keyup input[name=phone],blur input[name=phone]': 'enterPhone',
            'change select[name=number_of_people],blur select[name=number_of_people]': 'changeNumber',
            'click input.dgt_btn,blur input.dgt_btn': 'submitForm'
        },
        enterName: function (e) {
            _FormRegisterModel.set({ name: $(e.currentTarget).val() });
        },
        changeTivi: function (e) {
            _FormRegisterModel.set({ brand: $(e.currentTarget).val() });
            var tiviModel = $(e.currentTarget).val();
            if (tiviModel == "Other") {
                $('.name-tv-other').show();
            } else {
                $('.name-tv-other').hide();
                $('input[name=tivi-other]').val('');
            }
        },
        enterEmail: function (e) {
            _FormRegisterModel.set({ email: $(e.currentTarget).val() });
        },
        enterPhone: function (e) {
            _FormRegisterModel.set({ phone: $(e.currentTarget).val() });
        },
        changeNumber: function (e) {
            _FormRegisterModel.set({ number_of_people: $(e.currentTarget).val() });
        },
        submitForm: function (e) {
            $(e.target).hide();

            if (!$('input[name=checkbox_term]').is(":checked")) {
                alert("Vui lòng đọc và đồng ý với điều khoản của chương trình");
                $('.dgt_btn').show();
                return false;
            }

            _FormRegisterModel.set({'name':$('input[name=name]').val()});
            
            _FormRegisterModel.set({ 'email': $('input[name=email]').val().trim() });
            _FormRegisterModel.set({ 'phone': $('input[name=phone]').val() });
            _FormRegisterModel.set({ 'number_of_people': $('select[name=number_of_people]').val() });
            _FormRegisterModel.set({ fimlid: filmIdString });
            _FormRegisterModel.set({ time: $('.filter-time').val() });
            _FormRegisterModel.set({ date: $('.filter-date').val() });
            _FormRegisterModel.set({ cinema: $('.filter-cinema').val() });

           var tiviModel = ""
           if ($('input[name=tivi-other]').val() != "") {
               tiviModel = $('input[name=tivi-other]').val();
           } else {
               tiviModel = $('select[name=brand]').val();
           }
           
           _FormRegisterModel.set({ 'brand': tiviModel });
            var message = "";

            if ($('.filter-cinema').val() == "") {
                message += "<p>Vui lòng chọn rạp chiếu phim </p>";
            }

            if ($('.filter-date').val() == "") {
                message += "<p>Vui lòng chọn ngày chiếu phim </p>";
            }
            if ($('.filter-time').val() == "") {
                message += "<p>Vui lòng chọn giờ chiếu phim </p>";
            }
            if (_FormRegisterModel.get("name").trim() == "") {
                message += "<p>Vui lòng nhập họ tên </p>";
            }
            if (_FormRegisterModel.get("brand").trim() == "") {
                
                if ($('select[name=brand]').val()=='')
                    message += "<p>Vui lòng chọn nhãn hiệu TV bạn đang dùng </p>";
                else
                    message += "<p>Vui lòng nhập nhãn hiệu TV bạn đang dùng </p>";
                
            } else {
                if (tiviModel == "Other") {
                    message += "<p>Vui lòng nhập nhãn hiệu TV bạn đang dùng </p>";
                }
            }
            if (_FormRegisterModel.get("number_of_people").trim() == "") {
                message += "<p>Vui lòng chọn số người tham gia </p>";
            }
            if (_FormRegisterModel.get("email").trim() == "") {
                message += "<p>Vui lòng nhập email </p>";
            } else {
                if (!validateTool.isEmail(_FormRegisterModel.get("email").trim()))
                    message += "<p>Vui lòng nhập email đúng định dạng</p>";
            }

            if (_FormRegisterModel.get("phone").trim() == "") {
                message += "<p>Vui lòng nhập số điện thoại</p>";
            } else {
                if (!validateTool.isPhone(_FormRegisterModel.get("phone")))
                    message += "<p>Vui lòng nhập số điện thoại đúng định dạng </p>";
            }

            if (message != "") {
                alert(message);
                message = "";
                $(e.target).show();
                return false;
            }
            // _FormRegisterModel.saveFormRegister(_FormRegisterModel.toJSON());
            ajaxFunction('post', ajaxUrl, _FormRegisterModel.toJSON(),
                function(reponse){
                    if (reponse.status == 1) {

                        if (typeof (Storage) !== "undefined") {
                            localStorage.setItem("filmregister" , FilmId);
                        }
                        $('#dgt_alert').hide();
                        $('#dgt_alert2').show();
                        $('.dgt_form__booking').find('input[type=text]').val("");
                        $('.dgt_form__booking').find('select').val("");
                    } else {
                        $('#dgt_alert .dgt_copy').html('');
                        $('#dgt_alert .dgt_copy').html(reponse.msg);
                        $('#dgt_alert').show();
                        $('.dgt_btn').show();
                    }
                }, function (reponse) {
                    $('.dgt_btn').show();
                    if (typeof response.status != 'undefined' && response.status == "1")
                    {
                        if (typeof response.message != 'undefined') {
                            alert(response.message);
                            return false;
                        }
                    } else {
                        
                    }
                },
                function () {
                    $('.dgt_btn').show();
                    if (typeof response.message != 'undefined') {
                        alert(response.message);
                        return false;
                    }
                }
            )
        }
    })
    var _FormRegisterView = new FormRegisterView();
    var _FormRegisterModel = new FormRegisterModel();

    var shareFacebook = function () {
        $('.dgt_form__booking').click(function () {

        });
    }

    this.ajaxFunction = function (type, url, data, successCallback, errorCallBack) {
        data['__RequestVerificationToken'] = $('input[name=__RequestVerificationToken]').val();
        $.ajax({
            type: type,
            url: folder + url,
            // contentType: "application/json; charset=utf-8",
            data: data,
            dataType: "json",
            success: typeof successCallback === 'function' ? successCallback : function () { },
            error: typeof errorCallBack === 'function' ? errorCallBack : function () { console.log('ajax error') },
            beforeSend: function () {
                //if (this.url ===  '/Home/Register')
                    //$('.loading').show();
            },
            statusCode: {
                404: function () {
                    alert("Không tìm thấy dữ liệu bạn yêu cầu");
                },
                500: function () {
                    alert("Đã xảy ra lỗi trong quá trình xử lý");
                }
            },
        }).done(function () {
            //if (this.url ===  '/Process/Register')
                //$('.loading').hide();
        });
    }
    var shareFacebook = function () {
        $('#dgt_alert2 .dgt_btn').on('click', function () {
            var cinemaId = $('.filter-cinema').val() == "" ? 1 : $('.filter-cinema').val();
            FB.ui({
                method: 'feed',
                name: 'Trải nghiệm tivi Qled',
                caption: 'trainghiemqled.com',
                link: getOriginDomain() + absolutPath + "Home/Sharefacebook/" + FilmId + "/" + cinemaId,
                picture: getOriginDomain() + absolutPath + "assets/images/thumb5.jpg",
                description: 'Mô tả trải nghiệm tivi Qled'
            }, function (rs) {
               // console.log(rs);

            });
        });
    }
    var valid = 0;
    var writeReview = function () {
        
        $('.dgt_writearea .dgt_btn').click(function () {
            
            if (captcha == 1 && valid == 0) {
                ajaxFunction("POST", "Home/ValidateCaptcha", { 'g-recaptcha-response': $('textarea[name=g-recaptcha-response]').val() },
                    function (response) { 
                        if (response.status != "1") {
                            alert("Vui lòng click vào captcha");
                            return false;
                        } else {
                            saveReview();
                            valid = 1;
                        }
                    },
                    function () {
                        
                    }
                );
            } else {
                saveReview();
            }

        });
    }
    var saveReview = function () {
        var name = $('input[name=name]').val().trim();
        var email = $('input[name=email]').val().trim();
        var content = $('textarea[name=content-review]').val().trim();

        var message = "";
        if (name == "") {
            message += "<p>Vui lòng nhập họ tên</p>";
        }
        if (email == "") {
            message += "<p>Vui lòng nhập email</p>";
        } else {
            if (!validateTool.isEmail(email)) {
                message += "<p>Vui lòng nhập email hợp lệ</p>";
            }
        }
        if (content == "") {
            message += "<p>Vui lòng nhập nội dung cảm nhận</p>";
        }

        if (message != "") {
            alert(message);
            // console.log(message);
            message = "";
            return false;
        }

        var data = { name: name, email: email, content: content };
        if (typeof $('input[name=review_qsuite]') == 'object') {
            data["review_qsuite"] = 1;
        }
        ajaxFunction('post', urlWriteReview, data,
            function (response) {
                //  console.log(response);
                if (response.status == "1") {
                    $('.ovl').on('click', function () {
                        window.location.reload();
                    });
                    alert(response.msg);
                    $('input[name=name]').val("");
                    $('input[name=email]').val("");
                    $('textarea[name=content-review]').val("");
                    grecaptcha.reset();
                    valid = 0;
                   
                } else {
                    alert(response.msg);
                }
            },
            function (response) {

            })
    }
    var hotel = function () {

        $('#dgtQsuiteDetail .dgt_btn').on('click', function(event) {
            event.preventDefault();
            
            loginFacebook(function (response) {
                ajaxFunction('POST', urlLoginFacebook, response,
                        function (response) {
                            if (response.status == "1") {
                                window.location.href = getOriginDomain()  + qsuiteRegisterUrl;
                            } else {
                                alert("Không tìm thấy yêu cầu của bạn");
                            }
                        },
                        function () {

                        }
                    )
            });
        });
    }
    var registerQsuite = function () {
        var other = 0;
        $("select[name=brand]").on('change', function () {
            var brand = $(this).val();
            if (brand == "Other") {
                $(".name-tv-other").show();
                other = 1;
            } else {
                $(".name-tv-other").hide();
                other = 0;
            }
        });
        $('#dgt_alert_q_register a.dgt_btn').on('click', function () {
            var url = window.location.href.split('?');
            FB.ui({
                link: url[0],
                method:'feed'
            }, function (rs) {
                // console.log(rs);
            });
        });
        $('#dgtQsuiteRegister input.register-qsuite').on('click', function () {

            if (!$('input[name=checkbox_term]').is(":checked")) {
                alert("Vui lòng đọc và đồng ý với điều khoản của chương trình");
                return false;
            }

            var _this = $(this);
            var error = "";
            var nameInput = $('input[name=name]');
            var emailInput = $('input[name=email]');
            var phoneInput = $('input[name=phone]');
            var acreageInput = $('input[name=acreage]');
            var birthdayInput = $('input[name=birthday]');
            var jobInput = $('input[name=job]');

            if (other == 0)
                var brandInput = $('select[name=brand]');
            else
                var brandInput = $('input[name=tivi-other]');

            if (nameInput.length == 0 || nameInput.val().trim() == "") {
                error += "<p>Vui lòng nhập họ tên</p>";
            }
            if (emailInput.length == 0 || emailInput.val().trim() == "") {
                error += "<p>Vui lòng nhập email</p>";
            } else {
                if (!validateTool.isEmail(emailInput.val().trim()))
                    error += "<p>Vui lòng nhập email hợp lệ</p>";
            }

            if (phoneInput.length == 0 || phoneInput.val().trim()== "")
            {
                error += "<p>Vui lòng nhập số điện thoại</p>";
            } else {
                if (!validateTool.isPhone(phoneInput.val()))
                    error += "<p>Vui lòng nhập số điện thoại hợp lệ</p>";
            }

            /*if (acreageInput.length == 0 || acreageInput.val().trim() == "") {
                error += "<p>Vui lòng nhập diện tích không gian sống</p>";
            } else {
                if(isNaN(acreageInput.val().trim()))
                    error += "<p>Vui lòng nhập diện tích không gian sống hợp lệ chỉ nhập số</p>";
            }*/

            if (brandInput.length == 0 || brandInput.val().trim() == "") {
                if(other == 0)
                    error += "<p>Vui lòng chọn nhãn hiệu TV bạn đang dùng</p>";
                else
                    error += "<p>Vui lòng nhập nhãn hiệu TV bạn đang dùng</p>";
            }

            if (jobInput.length == 0 || jobInput.val().trim() == "") {
                error += "<p>Vui lòng nhập nghề nghiệp</p>";
            }

            if (birthdayInput.length == 0 || birthdayInput.val().trim() == "") {
                error += "<p>Vui lòng nhập năm sinh</p>";
            } else {
                if (isNaN(birthdayInput.val().trim())) {
                    error += "<p>Vui lòng nhập năm sinh hợp lệ</p>";
                } else {
                    var birth = parseInt(birthdayInput.val().trim());
                    if (birth < 1900 || birth > 2000) {
                        error += "<p>Vui lòng nhập năm sinh hợp lệ</p>";
                    }
                }
            }
            
            _this.hide();
            if (error != "") {
                _this.show();
                alert(error);
                error = "";
                return false;
            }
            var data = { name: nameInput.val(), email: emailInput.val().trim(), phone: phoneInput.val(), acreage: '10', brand: brandInput.val(),birthday :birthdayInput.val(),job : jobInput.val(), hotel: $('input[name=hotel_name]').val() };
            loginFacebook(function (response) {
                ajaxFunction('POST', urlLoginFacebook, response,
                    function (response) {
                        if (response.status == "1") {
                            ajaxFunction('post', saveQsuite, data,
                                         function (response) {
                                             console.log(response);
                                             if (response.status == 1) {
                                                 nameInput.val("");
                                                 brandInput.val("");
                                                 emailInput.val("");
                                                 //  acreageInput.val("");
                                                 phoneInput.val("");
                                                 //alert(response.msg);
                                                 $('#dgt_alert_q_register').show();
                                             } else {
                                                 _this.show();
                                                 alert(response.msg);
                                                 return false;
                                             }
                                         },
                                         function () {
                                             
                                         }
                                     );
                        } else {
                            alert("Không tìm thấy yêu cầu của bạn");
                        }
                    },
                    function () {
                        _this.show();
                    }
                )
            }, function () {
                _this.show();
            })

        });
    }
    var login = false;
    var info = {};
    var loginFacebook = function (callback,callbackfail) {
        
        if (typeof FB == 'undefined') {
            alert("Xin vui lòng kiểm tra kết nối mạng");
        }
        var status = false;
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                status = true;
            }
        });
        if (!login){
            FB.login(function (response) {
                if (response.authResponse) {
                  //  console.log(response.authResponse);
                    login = true;
                    var access_token = FB.getAuthResponse()['accessToken'];
                    FB.api('/me', null, { fields: 'email,name' }, function (response) {
                        
                        response['token'] = access_token;
                        response['__RequestVerificationToken'] = $('input[name=__RequestVerificationToken]').val();
                        info = response;
                        if(typeof callback == "function"){
                            callback(response);
                        }
                    
                    });
                } else {
                    console.log('Vui lòng đăng nhập bằng Facebook để thự hiện thao tác này');
                    //alert('Vui lòng đăng nhập bằng Facebook để thự hiện thao tác này');
                    hideLoading();
                    if (typeof callbackfail == 'function')
                        callbackfail();
                }
            }, { scope: 'email', return_scopes: true });
        } else {
            if(typeof callback == 'function')
                callback(info);
        }
    }
    var registerQhome = function () {
        var other = 0;
        var imageBase64 = "";
        $("#select-image").change(function () {
            readURL(this);
        });

        $("select[name=brand]").on('change', function () {
            var brand = $(this).val();
            if (brand == "Other") {
                $(".name-tv-other").show();
                other = 1;
            } else {
                $(".name-tv-other").hide();
                other = 0;
            }
        });
        $('#dgt_alert_q_register a.dgt_btn').on('click', function () {
            var url = window.location.href.split('?');
            FB.ui({
                link: url[0],
                method: 'feed'
            }, function (rs) {
                // console.log(rs);
            });
        });
        $('#dgtQhomeRegister input.dgt_btn').on('click', function () {

            if (!$('input[name=checkbox_term]').is(":checked")) {
                alert("Vui lòng đọc và đồng ý với điều khoản của chương trình");
                return false;
            }

            var _this = $(this);
             var error = "";
            var nameInput = $('input[name=name]');
            var emailInput = $('input[name=email]');
            var phoneInput = $('input[name=phone]');
            var acreageInput = $('input[name=acreage]');
            var reasonTextarea = $('textarea[name=reason]');
            var birthdayInput = $('input[name=birthday]');
            var jobInput = $('input[name=job]');
            var addressInput = $('input[name=address]');
            var citySelect = $('select[name=city]');
            if (other == 0)
                var brandInput = $('select[name=brand]');
            else
                var brandInput = $('input[name=tivi-other]');

            if (nameInput.length == 0 || nameInput.val().trim() == "") {
                error += "<p>Vui lòng nhập họ tên</p>";
            }
            if (emailInput.length == 0 || emailInput.val().trim() == "") {
                error += "<p>Vui lòng nhập email</p>";
            } else {
                if (!validateTool.isEmail(emailInput.val().trim()))
                    error += "<p>Vui lòng nhập email hợp lệ</p>";
            }

            if (phoneInput.length == 0 || phoneInput.val().trim() == "") {
                error += "<p>Vui lòng nhập số điện thoại</p>";
            } else {
                if (!validateTool.isPhone(phoneInput.val()))
                    error += "<p>Vui lòng nhập số điện thoại hợp lệ</p>";
            }

           if (acreageInput.length == 0 || acreageInput.val().trim() == "") {
                error += "<p>Vui lòng nhập diện tích không gian sống</p>";
            } else {
                if (isNaN(acreageInput.val().trim()))
                    error += "<p>Vui lòng nhập diện tích không gian sống hợp lệ chỉ nhập số</p>";
            }

            if (brandInput.length == 0 || brandInput.val().trim() == "") {
                if (other == 0)
                    error += "<p>Vui lòng chọn nhãn hiệu TV bạn đang dùng</p>";
                else
                    error += "<p>Vui lòng nhập nhãn hiệu TV bạn đang dùng</p>";
            }

            if (reasonTextarea.length == 0 || reasonTextarea.val().trim() == "") {
              //  error += "<p>Vui lòng nhập lý do bạn cần phong cách QLED</p>";
            }

            if (birthdayInput.length == 0 || birthdayInput.val().trim() == "") {
                error += "<p>Vui lòng nhập năm sinh</p>";
            } else {
                if (isNaN(birthdayInput.val().trim())) {
                    error += "<p>Vui lòng nhập năm sinh hợp lệ</p>";
                } else {
                    var birth = parseInt(birthdayInput.val().trim());
                    if (birth < 1900 || birth > 2000) {
                        error += "<p>Vui lòng nhập năm sinh hợp lệ</p>";
                    }
                }
            }

            if (jobInput.length == 0 || jobInput.val().trim() == "") {
                error += "<p>Vui lòng nhập nghề nghiệp</p>";
            }

            if (addressInput.length == 0 || addressInput.val().trim() == "") {
                error += "<p>Vui lòng nhập địa chỉ nhà</p>";
            }

            if (citySelect.length == 0 || citySelect.val() == "") {
                error += "<p>Vui lòng lựa chọn thành phố </p>";
            }

            if (imageBase64 == "")
               // error += "<p>Vui lòng upload hình ảnh phòng khách của bạn </p>";



            if (!isNaN(acreageInput.val().trim())) {
                var acr = parseInt(acreageInput.val().trim());
                if (acr > 100000) {
                    error += "<p>Vui lòng nhập diện tích phòng khách nhỏ hơn 100000 </p>";
                }
            }
            _this.hide();
            if (error != "") {
                _this.show();
                alert(error);
                error = "";
                return false;
            } else {
                showLoading();
            }
            
            var data = { name: nameInput.val(), email: emailInput.val().trim(), phone: phoneInput.val(), acreage: acreageInput.val(), brand: brandInput.val(), job: jobInput.val(), birthday: birthdayInput.val(), address: addressInput.val(), city: citySelect.val(), reason: reasonTextarea.val(), image: imageBase64 };
            
            loginFacebook(function (response) {
                ajaxFunction('POST', urlLoginFacebook, response,
                    function (response) {
                        
                        if (response.status == 1) {
                            
                            $('.save-qhome').trigger("submit");
                        } else {
                            alert("Không tìm thấy yêu cầu của bạn");
                        }
                        return false;
                    },
                    function () {
                        return false;
                    }
                )
            }, function () {
                _this.show();
                hideLoading();
            })

            return false;
        });

        
        var readURL = function (input) {
            
            //http://danielhindrikes.se/web/get-coordinates-from-photo-with-javascript/
            if (input.files && input.files[0]) {
                showLoading();
                var reader = new FileReader();

                var file = input.files[0];
                var imagefile = file.type;
                var match = ["image/jpeg", "image/png", "image/jpg", "image/gif"];
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                    alert("Xin vui lòng upload file hình ảnh");
                    hideLoading();
                    return false;
                }

                var size = file.size;
                if (size / (1024 * 1024) > 10) {
                    alert("Xin vui lòng upload file hình ảnh dưới 10 MB");
                    hideLoading();
                    return false;
                }
                
                reader.onload = function (e) {

                    var image = document.getElementById('image-resize');
                    var w, h;
                    image.onload = function () {
                        w = this.width;
                        h = this.height;
                        
                       
                    };
                    
                    var maxWidth = 1000;
                    var maxHeight = 1000;

                    setTimeout(function () {
                       // console.log(w / h);
                        if( w>1000 && h>1000){
                            var ratio = [maxWidth / w, maxHeight / h];
                            ratio = Math.min(ratio[0], ratio[1]);
                          //  alert(ratio);
                            if (ratio == 1) {
                                image.width = 1000;
                                image.height = 1000;
                            }else{
                                image.width = w * ratio;
                                image.height = h * ratio;
                            }
                        }
                        

                        var canvas = document.getElementById('canvas-resize');
                        canvas.width = image.width;
                        canvas.height = image.height;
                        var ctx = canvas.getContext('2d');
                        ctx.drawImage(image, 0, 0, image.width,  image.height );

                        var imageRotate = new ImageRotate(w, h);
                        var rs = imageRotate.rotate(e.target.result,canvas.toDataURL(), function (rs) {
                            imageBase64 = rs;
                            $(".luc-content").css({ opacity: 0 });
                            $(".dgt_qhome_preview").css({ 'background-image': "url(" + rs + ")", "background-size": "cover" });
                            hideLoading();
                        });

                    }, 500);

                    image.src = e.target.result;
                }

                reader.readAsDataURL(input.files[0]);

            }
        }
    }
    var qhomeLoginFacebook = function () {
        $('#dgtQhomeJoin a.dgt_btn_login_facebook').on('click', function (e) {
            $('html, body').animate({
                scrollTop: $("#dgtQhomeRegister").offset().top - 200
            }, 1000);
            return false;
            var _this = $(this); 
            if (loginL == "1") {
                $('html, body').animate({
                    scrollTop: $("#dgtQhomeRegister").offset().top - 200
                }, 1000);
            } else {
                loginFacebook(function (response) {
                    ajaxFunction('POST', urlLoginFacebook, response,
                        function (response) {
                            if (response.status == "1") {
                                //$(window).scrollTop($("#dgtQhomeRegister h2").offset().top - $(".dgt_header").outerHeight());
                                localStorage.scroll = 1;
                                window.location.reload();
                            } else {
                                alert("Không tìm thấy yêu cầu của bạn");
                            }
                        },
                        function () {

                        }
                    )
                })
            }   
        })
    

        $('#dgtQhome a.login_facebook').on('click', function (e) {
            e.preventDefault();
           // alert("ok ");
            var _this = $(this);
            loginFacebook(function (response) {
                ajaxFunction('POST', urlLoginFacebook, response,
                        function (response) {
                            if (response.status == "1") {
                                //$(window).scrollTop($("#dgtQhomeRegister h2").offset().top - $(".dgt_header").outerHeight());
                                if (typeof (Storage) !== "undefined") {
                                    localStorage.setItem('scroll', '0');
                                }
                                window.location.href = getOriginDomain() + _this.attr('href');

                            } else {
                                alert("Không tìm thấy yêu cầu của bạn");
                            }
                        },
                        function () {

                        }
                    )
            });
        });
       
}

    var init = function () {
        switch (action) {
            case "Films":
                
                window.onload = function () {
                    $(window).trigger("scroll");
                }
                
                filterDate();
                if (typeof (Storage) !== "undefined") {
                    if (localStorage.getItem("filmregister") != null) {
                        setTimeout(function () {
                            $(window).scrollTop($(".dgt_booking__wrap").offset().top - $(".dgt_header").outerHeight());
                        }, 300);

                    }
                }

                break;
            case "Sharefacebook":
            case "ShareFacebook":
                window.location.href = getOriginDomain() + absolutPath + "phim/" + pageRegisterR;
                break;
            case "Review":
            case "QsuiteReview":
                writeReview();
                break;
            case "Hotel1":
            case "Hotel2":
               // hotel();
                break;
            case "QsuiteRegister":
                registerQsuite();
                break;
            case "QhomeJoin":
               qhomeLoginFacebook();
               registerQhome();
               $('.save-qhome').on("submit", function () { });


               setTimeout(function () {
                   if (qhomeSuccess == 1) {
                       $('#dgt_alert_q_register').show();
                   }
                   if (qhomeError != '') {
                       alert(qhomeError);
                   }
               }
                   ,2000);
                break;
            case "QhomeRegister":
                registerQhome();
                break;
            case "Qhome":
               // qhomeLoginFacebook();
                break;
        }

        shareFacebook();
        window.alert = function (msg) {
            $('#dgt_alert .dgt_copy').html('');
            $('#dgt_alert .dgt_copy').html(msg);
            $('#dgt_alert').show();
        }
    }
    
    init();
})()
    var validateTool = {
        regExForEmail: /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}$/,

        isUndefined: function (strObj) {
            return typeof strObj == 'undefined' ? true : false;
        },
        isPhone: function (phoneNo) {
            phoneNo = phoneNo.trim();
            if (isNaN(phoneNo) || phoneNo.length >= 15 || phoneNo.length <= 5)
                return false;
            return true;
        },
        isEmail: function (sEmail) {
            sEmail = sEmail.trim();
            if (sEmail.search(this.regExForEmail) != -1)
                return true;
            return false;
        },
        isIdno: function (idNo) {
            idNo = idNo.trim();
            if (isNaN(idNo) || idNo.length > 12 || idNo.length < 9)
                return false;
            return true;
        },
    }