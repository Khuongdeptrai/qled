/*
 Last updated: 22:00 28/3/2017
 List of plugins:
  - GDevice 1.1
  - GMath 1.0
  - GArray 1.0
  - GLayoutCSS 1.0
  - GUpload 1.1
  - GBrowser 1.0
  - GStats 1.0
  - GLoader 1.0
  - GURL 1.0
*/

/* GURL - version 1.0
// Introduction: deep linking with hash
Author: Goon Nguyen
================================================== */

var GURL = {
    prev: '',
    _onChange: null,

    init: function() {
        if(GURL._onChange != null) GURL._onChange(GURL.paths());
        $(window).on('hashchange', function() {
            if(GURL._onChange != null) GURL._onChange(GURL.paths());
        });
    },

    set: function(value) {
        GURL.prev = location.hash;
        location.hash = value;
        //console.log(GURL.paths());
        //console.log(GURL._onChange);
        //if(GURL._onChange != null) GURL._onChange(GURL.paths());
    },

    get: function() {
        return location.hash;
    },

    paths: function() {
        var url = location.hash.substring(1);
        var arr = [];
        if(url.indexOf('/') >= 0){
            arr = url.split('/');
        } else {
            arr.push(url);
        }
        //alert(arr)
        if (arr.length == 0) arr[0] = '';
        return arr;
    },

    onChange: function(callback) {
        GURL._onChange = callback;
    }
}

/* GLoader - version 1.1
// detect: WebGL, Browser name,...
Author: Goon Nguyen
================================================== */

var GLoader = {
    arr: [],
    onComplete: null,
    onProgress: null,
    progress: 0,
    loadCount: 0,
    add: function(url){
        this.arr.push(url);
    },
    load: function(url){
        if(typeof url == "undefined"){
            // load list
            //console.log(GLoader.arr);
            var curURL = GLoader.arr[GLoader.loadCount];
            var img = new Image();
            img.onload = function(){
                //console.log(GLoader.loadCount);
                GLoader.loadCount++;
                if(GLoader.loadCount >= GLoader.arr.length){
                    if(GLoader.onComplete != null) GLoader.onComplete();
                    // clear all events
                    GLoader.onProgress = GLoader.onComplete = null;
                } else {
                    curURL = GLoader.arr[GLoader.loadCount];
                    GLoader.load(curURL);
                }
                GLoader.progress = GLoader.loadCount / GLoader.arr.length;
                if(GLoader.onProgress != null) GLoader.onProgress(GLoader.progress);
            }
            img.src = curURL;
        } else {
            GLoader.progress = 0;
            // load single
            var img = new Image();
            img.onload = function(){
                GLoader.progress = 1;
                if(GLoader.onProgress != null) GLoader.onProgress(GLoader.progress);
                if(GLoader.onComplete != null) GLoader.onComplete();
                // clear all events
                GLoader.onProgress = GLoader.onComplete = null;
            }
            img.src = url;
        }
    },
    on: function(event, callback){
        if(event == "complete"){
            GLoader.onComplete = callback;
        }
        if(event == "progress"){
            GLoader.onProgress = callback;
        }
    }
}

/* GStats - version 1.0
// get: FPS,...
Author: Goon Nguyen
================================================== */

var GStats = {
    startTime: 0,
    frameNumber: 0,
    getFPS: function() {
        this.frameNumber++;
        var d = new Date().getTime(),
            currentTime = (d - this.startTime) / 1000,
            result = Math.floor((this.frameNumber / currentTime));
        if (currentTime > 1) {
            this.startTime = new Date().getTime();
            this.frameNumber = 0;
        }
        return result;
    }
}

/* GBrowser - version 1.0
// detect: WebGL, Browser name,...
Author: Goon Nguyen
================================================== */

var GBrowser = {
    get isSupportWebGL() {
        /*try {
            var canvas = document.createElement("canvas");
            if( !!window.WebGLRenderingContext && (canvas.getContext("webgl") || canvas.getContext("experimental-webgl")) ){
                return true;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }*/
        if (!!window.WebGLRenderingContext) {
            var canvas = document.createElement("canvas"),
                 names = ["webgl", "experimental-webgl", "moz-webgl", "webkit-3d"],
               context = false;

            for(var i=0;i<4;i++) {
                try {
                    context = canvas.getContext(names[i]);
                    if (context && typeof context.getParameter == "function") {
                        // else, return just true
                        return true;
                    }
                } catch(e) {
                    console.log("!")
                }
            }

            // WebGL is supported, but disabled
            return false;
        }

        // WebGL not supported
        return false;
    }
}

/* GDevice - version 1.1
Author: Goon Nguyen
================================================== */

var GDevice = {
    tmpOri: "portrait", //landscape
    ratio: 16 / 9,
    tmpType: "mobile",
    _isIE: false,

    get isIE(){
        GDevice.resize();
        return GDevice._isIE;
    },

    get type() {
        GDevice.resize();
        return GDevice.tmpType;
    },

    get orientation() {
        GDevice.resize();
        return GDevice.tmpOri;
    },

    get width() {
        return $(window).width();
    },

    get height() {
        return $(window).height();
    },

    init: function() {
        $(window).resize(GDevice.resize);
        GDevice.resize();
    },
    resize: function(e) {
        var sw = $(window).width();
        var sh = $(window).height();

        GDevice.ratio = sw / sh;

        if (GDevice.ratio > 1) {
            GDevice.tmpOri = "landscape"

            if (sw > 1024) {
                GDevice.tmpType = "desktop"
            } else {
                if (sw > 640) {
                    GDevice.tmpType = "tablet"
                } else {
                    GDevice.tmpType = "mobile"
                }
            }

        } else if (GDevice.ratio < 1) {
            GDevice.tmpOri = "portrait"

            //console.log("sw: " + sw);
            if (sw > 770) {
                GDevice.tmpType = "desktop"
            } else {
                if (sw > 480) {
                    GDevice.tmpType = "tablet"
                } else {
                    GDevice.tmpType = "mobile"
                }
            }
        } else {
            GDevice.tmpOri = "square"
            GDevice.tmpType = "desktop"
        }

        //console.log(window.navigator.userAgent);
        GDevice._isIE = ( window.navigator.userAgent.indexOf('MSIE') >= 0 || window.navigator.userAgent.indexOf('NET4.0C') >= 0 );

        //console.log(GDevice);
    }
}
window.onload = GDevice.init;

/* GMath - version 1.0
Author: Goon Nguyen
================================================== */

var GMath = {
    random: function(number) {
        return number * Math.random();
    },
    randomInt: function(number) {
        return Math.floor(GMath.random(number));
    },
    randomPlusMinus: function(number) {
        return number * (Math.random() - Math.random());
    },
    randomIntPlusMinus: function(number) {
        return Math.round(GMath.randomPlusMinus(number));
    },
    randomFromTo: function(from, to) {
        return from + (to - from) * Math.random();
    },
    randomIntFromTo: function(from, to) {
        return Math.floor(GMath.randomFromTo(from, to));
    },

    angleRadBetween2Points: function(p1, p2) {
        return Math.atan2(p2.y - p1.y, p2.x - p1.x);
    },

    angleDegBetween2Points: function(p1, p2) {
        return GMath.radToDeg(GMath.angleRadBetween2Points(p1, p2));
    },

    degToRad: function(deg) {
        return deg * Math.PI / 180;
    },

    radToDeg: function(rad) {
        return rad * 180 / Math.PI;
    },

    angleRadBetween3Points: function(A, B, C) {
        var AB = Math.sqrt(Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2));
        var BC = Math.sqrt(Math.pow(B.x - C.x, 2) + Math.pow(B.y - C.y, 2));
        var AC = Math.sqrt(Math.pow(C.x - A.x, 2) + Math.pow(C.y - A.y, 2));
        return Math.acos((BC * BC + AB * AB - AC * AC) / (2 * BC * AB));
    },

    getPointWithAngleAndRadius: function(angle, radius) {
        var p = {
            x: 0,
            y: 0
        };
        p.x = radius * Math.cos(angle);
        p.y = radius * Math.sin(angle);
        return p;
    },

    distanceBetweenPoints: function(p1, p2) {
        var x1 = p1.x;
        var y1 = p1.y;

        var x2 = p2.x;
        var y2 = p2.y;

        var d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

        return d;
    }
}

/* GArray - version 1.0
Author: Goon Nguyen
================================================== */

var GArray = {
    remove: function(item, array) {
        var index = array.indexOf(item);
        if (index > -1) {
            array.splice(index, 1);
        }
        return array;
    }
}

/* GLayoutCSS - version 1.0
Author: Goon Nguyen
================================================== */

var GLayoutCSS = {
    init: function() {
        //console.log("[GLayoutCSS 1.0] Initialized!");
        $(window).resize(GLayoutCSS.resize);
        GLayoutCSS.resize();
    },
    resize: function(e) {
        var sw = $(window).width();
        var sh = $(window).height();
        if ($('.helper-layout-fullscreen').length > 0) {
            $('.helper-layout-fullscreen').width(sw);
            $('.helper-layout-fullscreen').height(sh);
        }
    }
}
$(function() {
    GLayoutCSS.init();
})

/* GUpload - version 1.1
Author: Goon Nguyen
================================================== */

var GUpload = {
    inputElement: null,
    customClass: "",
    customPostName: "photo",
    customUploadType: "images/*",
    reader: null,
    file: null,

    onSelect: null, // (base64)
    onCancel: null,
    onStart: null,
    checkSelectedInt: null,

    browse: function(callback) {
        this.onSelect = callback;

        GUpload.resultBase64 = "";
        GUpload.file = null;
        this.inputElement = null;

        if ($(".gupload-input").length > 0) {
            $(".gupload-input").remove();
            console.log("input is existed");
        }

        $("body").append('<input class="gupload-input helper-hide ' + GUpload.customClass + '" type="file" name="' + GUpload.customPostName + '" accept="' + GUpload.customUploadType + '">');

        this.inputElement = $(".gupload-input");

        this.inputElement.on("change", onChangeHandler);
        $(".gupload-input")[0].onChange = onChangeHandler;
        //$(".gupload-input")[0].addEventListener("change", onChangeHandler);

        this.inputElement.click();

        /*$(window).focusout(function(){
            console.log("This input field has lost its focus.");
        });*/

        $(window).focusin(function() {
            //console.log("This input field has gained its focus.");
            clearTimeout(GUpload.checkSelectedInt)
            GUpload.checkSelectedInt = setTimeout(checkSelected, 500);
        });

        function checkSelected() {
            if (!GUpload.file) {
                console.log("[GUpload] onCancel");
                $(".gupload-input").remove();
                if (GUpload.onCancel) GUpload.onCancel();

                $(window).off("focusin");

                GUpload.file = null;
            } else {
                //
            }
        }

        //this.inputElement.focusin();

        //--

        function onChangeHandler() {
            //console.log($(this).val());
            if ($(this)[0].files[0]) {
                if (GUpload.onStart) GUpload.onStart();

                GUpload.file = $(this)[0].files[0];

                if (window.FileReader && window.FileReader.prototype.readAsArrayBuffer) {
                    GUpload.reader = new FileReader();

                    GUpload.reader.addEventListener("load", GUpload.onReadDataURL);

                    if (GUpload.file) {
                        GUpload.reader.readAsDataURL(GUpload.file);
                    }
                } else {
                    $(".gupload-input").remove();
                    alert("Please upload your browser to use this feature.");
                    throw "This browser is too old to use this feature.";
                }
            } else {
                console.log("[GUpload] onCancel");
                GUpload.file = null;
                GUpload.resultBase64 = "";
                $(".gupload-input").remove();
                if (GUpload.onCancel) GUpload.onCancel();
            }
        } //--
    },

    onReadDataURL: function(e) {
        GUpload.resultBase64 = GUpload.reader.result;

        //GUpload.reader.removeEventListener("load", GUpload.onReadDataURL);

        var reader = new FileReader();
        //GUpload.reader.onload = GUpload.onReadBuffer;
        if (reader.readAsArrayBuffer) {
            reader.addEventListener("load", GUpload.onReadBuffer);
            reader.readAsArrayBuffer(GUpload.file.slice(0, 64 * 1024));
        } else {
            console.log("[GUpload] onSelect");
            if (GUpload.onSelect) GUpload.onSelect(GUpload.resultBase64, null);
            $(".gupload-input").remove();
            GUpload.file = null;
        }
    },

    onReadBuffer: function(e) {
        var orientation = GUpload.getOrientation(e.target.result);
        var params = {}
        switch (orientation) {
            case 6:
                params.orientation = -90;
                break;
            case 8:
                params.orientation = 90;
                break;
            case 3:
                params.orientation = 180;
                break;
            default:
                params.orientation = 0;
                break;
        }

        $(".gupload-input").remove();
        GUpload.file = null;

        console.log("[GUpload] onSelect");
        if (GUpload.onSelect) GUpload.onSelect(GUpload.resultBase64, params);
    },

    getOrientation: function(dataBuffer) {
        var view = new DataView(dataBuffer);
        if (view.getUint16(0, false) != 0xFFD8) return -2;
        var length = view.byteLength,
            offset = 2;
        while (offset < length) {
            var marker = view.getUint16(offset, false);
            offset += 2;
            if (marker == 0xFFE1) {
                if (view.getUint32(offset += 2, false) != 0x45786966) return -1;
                var little = view.getUint16(offset += 6, false) == 0x4949;
                offset += view.getUint32(offset + 4, little);
                var tags = view.getUint16(offset, little);
                offset += 2;
                for (var i = 0; i < tags; i++)
                    if (view.getUint16(offset + (i * 12), little) == 0x0112)
                        return view.getUint16(offset + (i * 12) + 8, little);
            } else if ((marker & 0xFF00) != 0xFF00) break;
            else offset += view.getUint16(offset, false);
        }
        return -1;
    },

    onRead: function() {},

    onFailRead: function() {

    }
}