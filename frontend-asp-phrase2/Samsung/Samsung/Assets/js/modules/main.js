/* GAME POSTER
Author: KHUONGDINH
================================================== */



$(function(){
  if(typeof path_resource == "undefined" || !path_resource) path_resource = "";
  GScript.loadList([
    // external libraries
    path_resource + "js/libraries/plugin.js",
    path_resource + "js/libraries/tweenmax/TweenMax.min.js",
    path_resource + "js/libraries/tweenmax/plugins/ScrollToPlugin.min.js",
    path_resource + "js/libraries/scrollmagic/uncompressed/ScrollMagic.js", 
    path_resource + "js/libraries/scrollmagic/uncompressed/plugins/animation.gsap.js", 
    path_resource + "js/libraries/scrollmagic/uncompressed/plugins/debug.addIndicators.js",

    path_resource + "js/libraries/three.min.js",
    path_resource + "js/libraries/D.js",
    path_resource + "js/libraries/uevent.min.js",
    path_resource + "js/libraries/doT.min.js",

    path_resource + "js/libraries/CanvasRenderer.js",
    path_resource + "js/libraries/Projector.js",
    path_resource + "js/libraries/EffectComposer.js",
    path_resource + "js/libraries/RenderPass.js",
    path_resource + "js/libraries/ShaderPass.js",
    path_resource + "js/libraries/MaskPass.js",
    path_resource + "js/libraries/CopyShader.js",
    path_resource + "js/plugins/classie.js",

    path_resource + "js/libraries/DeviceOrientationControls.js",
    path_resource + "js/libraries/helper.js",
    path_resource + "js/libraries/photo-sphere-viewer.min.js",

    // goon
    path_resource + "js/plugins/before-after.js",
    path_resource + "js/libraries/goon.js"
    
  ], function(result){
    QLED.init();
  })


})


var wScreen = $(window).outerWidth();

var QLED = {

  init : function(){
    
    if( $('#dgtHome').length > 0 ) {
      QLED.intro();
    } else if( $('#dgtLibrary').length > 0 ) {
      QLED.library();
    } else if ( $('#dgtBooking').length > 0  ) {
      QLED.movie();
    } else if( $('#dgtGalerry').length > 0 ) {
      QLED.galery();
    } else if ( $('#dgtQcinema').length > 0 ) {
      $('.js_qcinema__menu').css({ 'display': 'block' });
    } else if ( $('#dgtQhomeJoin').length > 0 ) {
      QLED.qhome();
    } else if ( $('#dgtQhomeRegister').length > 0 ) {
      $('.js_qhome__menu').css({ 'display': 'block' });
    } else if ( $('#dgtQhome').length > 0 ) {
      QLED.qhomemain();
    } else if ( $('#dgtQsuite').length > 0 ) {
      QLED.qsuite();
    } else if ( $('#dgtQsuiteRegister').length > 0 ) {
      QLED.qsuiteRegister();
    } else if ( $('#dgtQsuiteDetail').length > 0 ) {
      QLED.qsuitePanorama();
    } else if ( $('#dgtReview').length > 0 ) {
      QLED.review();
    } else if ( $('#dgtWinner').length > 0 ) {
      QLED.winners();
    }

    
    if(wScreen > 1024) {
      QLED.stick();
    }
    QLED.popup();
    QLED.mobile();
    QLED.term();
  },

  stick : function() {

    var $fwindow = $(window);
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    var h = $('.dgt_header').outerHeight() ; 

    var navPosition=$('.dgt_header').offset().top;
    var scrollTop=$(window).scrollTop();

    $fwindow.on('scroll resize', function() {
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      wScreen = $(window).outerWidth();
      if(navPosition < scrollTop){
        $('body').css({
          'padding-top': '60px'
        });
        $('.dgt_header').css({ 
          'position': 'fixed',
          'top': 0
         });
      } else{
        
        $('body').css({
          'padding-top': '0'
        });
        $('.dgt_header').css({ 'position': 'relative' });
      }
    });


  },

  intro : function() {  
    
    $(window).on('load resize', function(event) {
      $(".dgt_cinema").width( $(window).width()/2 );
      $(".dgt_else").width( $(window).width()/2 );
    });
    
    setTimeout(function() {
      $("#dgtHome").css('visibility', 'visible');
      TweenMax.from('#dgtHome',1, { opacity: 0 })
    }, 200);

    
  },

  library: function() {

    $('.js_qcinema__menu').css({ 'display': 'block' });

    $('.js_dgt__library').slick({
      arrows: false,
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            arrows: false,
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            variableWidth: true
          }
        }
      ]
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__library').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__library').slick('slickNext');
    })

    
  },

  movie : function() {

    $('.js_qcinema__menu').css({ 'display': 'block' });

    $('.js_dgt__library').slick({
      arrows: false,
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            arrows: false,
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: true,
            centerPadding: '100px',
            variableWidth: true
          }
        }
      ]
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__library').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__library').slick('slickNext');
    })

  },
  
  popup: function() {
    $('#dgt_alert .ovl').on('click', function() {
        $('#dgt_alert').hide();
        $('#dgt_alert2').hide();
    });

    $('.dgt_popup .btn-close-2').on('click', function(e){
      e.preventDefault();
      $('#dgt_alert').hide();
    })

  },

  galery: function() {

    $('.js_dgt__photo').slick({
      arrows: false,
      dots: false,
      infinite: false,
      slidesToShow: 5,
      slidesToScroll: 5,
      variableWidth: true
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__photo').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__photo').slick('slickNext');
    })

    

    $('.js_dgt__video').slick({
      arrows: false,
      dots: false,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      variableWidth: true
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__video').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__video').slick('slickNext');
    })

    $('.dgt_js_tabs li').on('click', function() {

      var addres = $(this).attr('rel');
      var id = $('.js_dgt__video .slick-active').attr('data');
      var ytb = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+id+'?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>';  

      $('.dgt_js_tabs li').removeClass('active');
      $(this).addClass('active');

      $('.dgt_glr__item').hide();
      $('#'+addres).show();

      if( addres=='dgt_photo') {
        $('.dgt_ytb').html('');
      } else {
        $('.dgt_ytb').html('').append(ytb);
      }

    });


    $('.js_dgt__video .slick-slide').on('click', function() {

      var id = $(this).attr('data');
      console.log(id);
      var ytb = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+id+'?autoplay=0&rel=0" frameborder="0" allowfullscreen></iframe>'; 
      $('.dgt_ytb').html('').append(ytb);

    });

    $('.js_dgt__photo .slick-slide').on('click', function() {

      var src = $(this).find('.thumb').attr('src');
      $('#dgt_photo .dgt_glr__copy > img').attr('src',src)
      clearInterval(photoAutoSlider);
    });

    var photoAutoSlider = setInterval(function(){ 
      autoSlider() 
    }, 3000);

    var thumbId = 0;
    var totalThumb = $('.js_dgt__photo .slick-slide').length;
    function autoSlider() {
      
      if(thumbId<totalThumb) {
        var src = $('.js_dgt__photo .slick-slide:eq('+thumbId+')').find('.thumb').attr('src');
        thumbId++; 
        if(thumbId==totalThumb) {
          thumbId = 0;
        }

        TweenMax.to('#dgt_photo .dgt_glr__copy img', 0.4, {opacity: 0, ease: Sine.easeOut, onComplete: function(){
          TweenMax.to('#dgt_photo .dgt_glr__copy img', 0.4, {opacity: 1, ease: Sine.easeOut});

          $('#dgt_photo .dgt_glr__copy > img').attr('src', src)
          
        }})
      }
    }  

  },

  mobile: function() {
    
    $('.btn-menu').on('click', function(event) {
      event.preventDefault();
      if ($(this).hasClass('menu-open')) {
        $(this).removeClass('menu-open').addClass('menu-closed');
        $('#dgt_nav').hide();
      } else {
        $(this).removeClass('menu-closed').addClass('menu-open');
        $('#dgt_nav').show();
        
      };
    });

  },
  review : function() {
    TweenMax.set(".dgt_banner h2", {opacity: 0, y: -80});
    TweenMax.to(".dgt_banner h2", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.2});
  },

  term: function() {
    
    $('.js_dgt_term').on('click', function(evt) {
      evt.preventDefault();
      $('.dgt_popup_term').show();
    });

    $('.js_dgt_closeterm').on('click', function(evt) {
      evt.preventDefault();
      $('.dgt_popup_term').hide();
    });

  },

  qhome: function() {

    $('.js_qhome__menu').css({ 'display': 'block' });

    $('.js_dgt_qhome__slider').slick({
      arrows: true,
      dots: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      centerMode: false,
      variableWidth: false,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            arrows: false,
          }
        }
      ]
    });

    $('.js_to_qhomeRegister').on('click', function(e) {
      e.preventDefault();
      var scope = this;
      scope.a = $(window).scrollTop();
      var toBookingPostion = $("#dgtQhomeRegister").offset().top - $(".dgt_header").outerHeight();
      TweenMax.to(scope, 0.8, {a: toBookingPostion, ease: Quart.easeInOut, onUpdate: function(){
        $(window).scrollTop(scope.a);
      }})
    });
    
    // var controller = new ScrollMagic.Controller();

    // $('[data-qhome]').each(function (index, elem) {

    //   var scene = new ScrollMagic.Scene({triggerElement: elem, triggerHook: '0.8'})
    //   .addTo(controller)
    //   .on("enter", function (e) {
    //     $(elem).addClass('display');
    //   });

    // });
    
    TweenMax.set(".dgt_video", {opacity: 0, y: -80});
    TweenMax.set(".dgt_qhome__video h2 ", {opacity: 0, y: 80});

    TweenMax.to(".dgt_video", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.5});
    TweenMax.to(".dgt_qhome__video h2", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.6});

  },

  qhomemain: function() { 

    var hScreen = getDocumentSize(3);
    var wScreen = getDocumentSize(0);

    $(window).on('load resize', function() {
      hScreen = getDocumentSize(3);
      wScreen = getDocumentSize(0);
    });
   
    if (wScreen < 901) {
      // $('.js_dgt__popup').slick({
      //   arrows: false,
      //   dots: true,
      //   infinite: true,
      //   slidesToShow: 1,
      //   slidesToScroll: 1,
      //   variableWidth: true,
      //   centerMode: true
      // });
    } 
    
    TweenMax.set(".dgt_copy h3 , .dgt_copy p", {opacity: 0, y: 80});
    TweenMax.set(".dgt_copy .dgt_btn ", {opacity: 0, y: 80});

    TweenMax.to(".dgt_copy h3", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.5});
    TweenMax.to(".dgt_copy p", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.6});
    TweenMax.to(".dgt_copy .dgt_btn", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.7});


    // showPopupRandon();
    // function showPopupRandon() {
    //   $('.dgt_discover li .thumb').css({ 'visibility': 'visible'});
    
    QLED.initPopup();
  },

  initPopup: function() {

    var i = 0;

    var discover = setInterval(function(){ initPopup() }, 3000);

    function initPopup() {

      TweenMax.to($('.dgt_discover li').find('.thumb'), 0 , { y: -30 , visibility: 'hidden',
        onComplete: showPopupRandon
      });

    }

    function showPopupRandon() {

      TweenMax.to($('.dgt_discover li:eq('+i+')').find('.thumb'), .3 , { y: 0 , visibility: "visible" , delay: 1});
        i++;
        if(i===6) {
          clearInterval(discover);
        }
    }

    $('#dgt_room360').on('click touchstart', function() {
      clearInterval(discover);
      TweenMax.to('.dgt_discover li .thumb', .1, { y: -30 , visibility: "hidden" });
      $('.dgt_discover').hide();
    });


    $('.dgt_discover').on('mouseenter', function() {
      clearInterval(discover);
      TweenMax.to('.dgt_discover li .thumb', .1, { y: -30 , visibility: "hidden" });
      if($('#dgtQsuiteDetail').length> 0) {
        $('.dgt_discover').hide();
      }
    });

    $('.dgt_discover li').on('mouseenter mouseleave touchstart', function (event) {

        var _index = $(this).index();
        console.log(_index);
        var _popup = "";
        switch (_index) {
            case 0:
                _popup = "popup1";
                break;
            case 1:
                _popup = "popup3";
                break;
            case 2:
                _popup = "popup2_1";
                break;
            case 3:
                _popup = "popup4";
                break;
        }
        omniture_click('vn:trainghiemqled:highlights:images:' + _popup)
      if( event.type === 'mouseenter' || event.type === 'touchstart' && getDocumentSize(0)> 1025 ) {
        var element = $(this);
        clearInterval(discover);
        TweenMax.to('.dgt_discover li .thumb', 0, { y: -30 , visibility: "hidden" });
        TweenMax.to( element.find('.thumb'), .3, { y: 0 , visibility: "visible", delay: 0.3 });
      } else {
        TweenMax.to('.dgt_discover li .thumb', .3, { y: -30 , visibility: "hidden" });
      }

    });

  },

  qsuite: function() {

    TweenMax.set(".dgt_copy h2 , .dgt_copy p", {opacity: 0, y: 80});
    TweenMax.set(".dgt_buttons ", {opacity: 0, y: 80});

    TweenMax.to(".dgt_copy h2", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.5});
    TweenMax.to(".dgt_copy p", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.6});
    TweenMax.to(".dgt_buttons", 1.5, {opacity: 1, y: 0, ease: Quart.easeOut, delay: 0.7});

  },

  winners: function() {

    var total = ($('.dgt_winners__item').length)/2;

    if(getDocumentSize(0)> 567) {
      $('.js_dgt_winner__slider').slick({
        arrows: true,
        dots: false,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        variableWidth: false,
        rows: 2,
        // responsive: [
        //   {
        //     breakpoint: 567,
        //     settings: {

        //       slidesToShow: 1,
        //       slidesToScroll: 1,
        //       variableWidth: false,
        //       rows: 2,
        //     }
        //   }
        // ]
      });
    }

    
    

    $('.dgt_winners__content_list').slick({
      arrows: true,
      dots: true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: false,
      draggable: false,
      touchMove: false,
      responsive: [
        {
          breakpoint: 567,
          settings: {

          }
        }
      ]
    });

    $('.dgt_winners__qsuitelist').slick({
      arrows: true,
      dots: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
    
          }
        }
      ]
    });

    

    WINNER.init();

  },

  qsuiteRegister: function() {

    $('.js_dgt__gallery').slick({
      arrows: false,
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 567,
          settings: {
            arrows: false,
            dots: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: '100px',
            variableWidth: true
          }
        }
      ]
    });

    $('.slick-prev').click(function(){
      $('.js_dgt__gallery').slick('slickPrev');
    })

    $('.slick-next').click(function(){
      $('.js_dgt__gallery').slick('slickNext');
    })
  },


  qsuitePanorama: function() {

    var hScreen = getDocumentSize(3);
    var wScreen = getDocumentSize(0);
    var img = 'hanoi';

    $(window).on('load resize', function() {
      hScreen = getDocumentSize(3);
      wScreen = getDocumentSize(0);
    });
   
    if (wScreen < 901) {
      $('.js_dgt__popup').slick({
        arrows: false,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        centerMode: true
      });
    } 
    

    if($('.dgt_hanoi').length > 0){
      img= "hanoi";

      var PSVHanoi = new PhotoSphereViewer({
        panorama: path_resource + '/images/'+img+'.jpg',
        container: 'dgt_room360',
        loading_img: path_resource + '/images/preloader.gif',
        time_anim: false,
        default_fov: 90,
        default_lat: 0,
        navbar: false,
        mousewheel: false,
        size: {
          height: hScreen
        },
        markers: [
          {
            id: 'image',
            x: 1980,
            y: 1040,
            image: path_resource + '/images/dot.png',
            width: 16,
            height: 16,
            tooltip: {
              content: '<img src="assets/images/popup1_1.png"/>',
              position: 'top center'
            }
          },
          {
            id: 'image1',
            x: 2550,
            y: 1040,
            image: path_resource + '/images/dot.png',
            width: 16,
            height: 16,
            tooltip: {
               content: '<img src="assets/images/popup3_1.png"/>',
              position: 'top center'
            }
          },
          {
            id: 'image2',
            x: 2650,
            y: 1300,
            image: path_resource + '/images/dot.png',
            width: 16,
            height: 16,
            tooltip: {
               content: '<img src="assets/images/popup4.png"/>',
              position: 'top center'
            }
          },
          {
            id: 'image3',
            x: 1900,
            y: 1370,
            image: path_resource + '/images/dot.png',
            width: 16,
            height: 16,
            tooltip: {
              content: '<img src="assets/images/popup2_1.png"/>',
              position: 'top center'
            }
          }
        ]
      });

      PSVHanoi.on('ready', function(enabled) {

        QLED.initPopup();
        $('#dgtQsuiteDetail .dgt_btn ').css({ 'visibility': 'visible' });
        
        PSVHanoi.animate({
          x: 2260,
          y: 1250
        }, 1000);

      });

      if (wScreen < 1024) {
      
        PSVHanoi.on('ready', function(enabled) {
          //PSVHanoi.clearMarkers();
          PSVHanoi.updateMarker({
            id: 'image1', x: 2500
          });
          PSVHanoi.updateMarker({
            id: 'image', x: 2050
          });
        });

        setTimeout(function(){
          if(GDevice.type != "desktop"){
            //PSVHanoi.startGyroscopeControl();
          }
        }, 500);
      }
    } 

    else if($('.dgt_saigon').length > 0) {
      img= "saigon";

      var PSVSaigon = new PhotoSphereViewer({
        panorama: path_resource + '/images/'+img+'.jpg',
        container: 'dgt_room360',
        loading_img: path_resource + '/images/preloader.gif',
        time_anim: false,
        default_fov: 70,
        default_lat: 0,
        navbar: false,
        mousewheel: false,
        size: {
          height: hScreen
        },
        markers: [
          {
            id: 'image',
            x: 2440,
            y: 1120,
            image: path_resource + '/images/dot.png',
            width: 16,
            height: 16,
            tooltip: {
              content: '<img src="assets/images/popup1_1.png"/>',
              position: 'top center'
            }
          },
          {
            id: 'image1',
            x: 2740,
            y: 1120,
            image: path_resource + '/images/dot.png',
            width: 16,
            height: 16,
            tooltip: {
              content: '<img src="assets/images/popup3_1.png"/>',
              position: 'top center'
            }
          },
          {
            id: 'image2',
            x: 2365,
            y: 1300,
            image: path_resource + '/images/dot.png',
            width: 16,
            height: 16,
            tooltip: {
              content: '<img src="assets/images/popup2_1.png"/>',
              position: 'top center'
            }
          },
          {
            id: 'image3',
            x: 2830,
            y: 1400,
            image: path_resource + '/images/dot.png',
            width: 16,
            height: 16,
            tooltip: {
              content: '<img src="assets/images/popup4.png"/>',
              position: 'top center'
            }
          }
        ]
      });
      PSVSaigon.on('ready', function(enabled) {
        QLED.initPopup();
        $('#dgtQsuiteDetail .dgt_btn ').css({ 'visibility': 'visible' });
        PSVSaigon.animate({
          x: 2580,
          y: 1250
        }, 1000);
      });
    }
  
  }
  
};
 

var randomNum = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};



function showAlert() {
  $('#dgt_alert').show()
}

function showLoading() {
  $('#dgt_preloader').show();
}
function hideLoading() {
  $('#dgt_preloader').hide();
}



var WINNER = {
  init: function() {

    var gridEl = document.getElementById('dgtWinner'),
        gridItemsContainer = gridEl.querySelector('.js_dgt_winner__slider'),
        contentItemsContainer = gridEl.querySelector('.dgt_winners__content'),
        gridItems = gridItemsContainer.querySelectorAll('.dgt_winners__item'),
        contentItems = contentItemsContainer.querySelectorAll('.dgt_winners__content__item'),
        closeCtrl = contentItemsContainer.querySelector('.close-button'),
        current = -1,
        isAnimating = false;

    function init() {
      initEvents();
    }

    function initEvents() {     
      var randomRange = (Math.floor(Math.random() * 6) + 0) / 1000;

      [].slice.call(gridItems).forEach(function(item, pos) {

        item.addEventListener('click', function(ev) {
          ev.preventDefault();
          if(isAnimating) {
            return false;
          }
          isAnimating = true;
          current = pos;

          classie.add(item, 'grid__item--loading');
          $('.dgt_winners__content_list .slick-dots li:eq('+current+')').trigger('click');
          setTimeout(function() {
            TweenMax.staggerTo('.dgt_winners__item', .5, { opacity:0, scale: 0 }, .1);
            setTimeout(function() { loadContent(item); }, 1000);
          }, 500);

        });
        
      });

      closeCtrl.addEventListener('click', function() {
        // hide content
        hideContent();
      });

      // keyboard esc - hide content
      document.addEventListener('keydown', function(ev) {
        if(!isAnimating && current !== -1) {
          var keyCode = ev.keyCode || ev.which;
          if( keyCode === 27 ) {
            ev.preventDefault();
            if ("activeElement" in document)
                document.activeElement.blur();
            hideContent();
          }
        }
      } );

    }

    function loadContent(item) {

      classie.add(contentItemsContainer, 'content--show');
      classie.add(contentItems[current], 'content__item--show');
      classie.add(closeCtrl, 'close-button--show');
      isAnimating = false;
    }

    function hideContent() {
      var gridItem = gridItems[current], contentItem = contentItems[current];
      TweenMax.staggerTo('.dgt_winners__item', .5, { opacity:1, scale: 1 }, .1);
      classie.remove(contentItem, 'content__item--show');
      classie.remove(contentItemsContainer, 'content--show');
      classie.remove(closeCtrl, 'close-button--show');
      classie.remove(gridItem, 'grid__item--loading');
      current = -1;
    }


    init();


  }
}








